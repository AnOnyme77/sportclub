<?php
    class Niveau
    {
        private $niveau;
        private $libelle;
        private $nameSection;
        private $logo;
        private $supprime;
        
        public function Niveau(){
            $this->supprime="F";
        }

        /* get & set */
        public function getNiveau()
        {
            return($this->niveau);
        }
        public function setNiveau($niveau)
        {
            $this->niveau=$niveau;
        }
        public function getLibelle()
        {
            return($this->libelle);
        }
        public function setLibelle($libelle)
        {
            $this->libelle=$libelle;
        }
        public function getNameSection()
        {
            return($this->nameSection);
        }
        public function setNameSection($nameSection)
        {
            $this->nameSection=$nameSection;
        }
        public function getLogo()
        {
            return($this->logo);
        }
        public function setLogo($logo)
        {
            $this->logo=$logo;
        }
        public function getSupprime()
        {
            return($this->supprime);
        }
        public function setSupprime($supprime)
        {
            $this->supprime=$supprime;
        }

        /* fin get & set */

    }
?>
