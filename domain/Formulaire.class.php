<?php

class Formulaire {
	
    private static $CHEMINFORM = "/outils";
	
    private function Formulaire() {}
    
    public static function formulaireDate($id="", $dateBase=null){
    	$vue = new Vue("");
    	if(!isset($id)) $id="";
    	if(isset($dateBase))echo($vue->genererFichier(Parser::getCheminRacine().self::$CHEMINFORM."/formulaireDate.php",
    						array("id"=>$id,
                                  "jour"=>$dateBase->format("d"),
                                  "mois"=>$dateBase->format("m"),
                                  "annee"=>$dateBase->format("Y"))));
        else echo($vue->genererFichier(Parser::getCheminRacine().self::$CHEMINFORM."/formulaireDate.php",array("id"=>$id)));
    }

    public static function formulaireDateReadonly($id="", $dateBase=null){
      $vue = new Vue("");
      if(!isset($id)) $id="";
      if(isset($dateBase))echo($vue->genererFichier(Parser::getCheminRacine().self::$CHEMINFORM."/formulaireDateReadonly.php",
                array("id"=>$id,
                                  "jour"=>$dateBase->format("d"),
                                  "mois"=>$dateBase->format("m"),
                                  "annee"=>$dateBase->format("Y"))));
        else echo($vue->genererFichier(Parser::getCheminRacine().self::$CHEMINFORM."/formulaireDateReadonly.php",array("id"=>$id)));
    }
    
    
}