<?php
    class Competence
    {
        private $niveau;
        private $nameSection;
        private $titre;
        private $detail;

        /* get & set */

        public function getNiveau()
        {
            return($this->niveau);
        }
        public function setNiveau($niveau)
        {
            $this->niveau=$niveau;
        }
        public function getNameSection()
        {
            return($this->nameSection);
        }
        public function setNameSection($nameSection)
        {
            $this->nameSection=$nameSection;
        }
        public function getTitre()
        {
            return($this->titre);
        }
        public function setTitre($titre)
        {
            $this->titre=$titre;
        }
        public function getDetail()
        {
            return($this->detail);
        }
        public function setDetail($detail)
        {
            $this->detail=$detail;
        }

        /* fin get & set */

    }
?>
