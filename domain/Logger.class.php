<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class Logger{

    private static $insertion="insert into logs('fichier','message') values(?,?)";
    private static $cheminDepuisRacine="/logs/";
    private static $nomDB="logs.db";
    private static $instance;
    private static $active=false;
    private static $nomConfig="config/logger.ini";
    private $connexion;
    private $config;
    
    private function Logger(){
        date_default_timezone_set('Europe/Paris');
        
        $parserConfig=new ParserLogger();
        $this->config=$parserConfig->parser(self::$nomConfig);
        if(($this->config!=null && $this->config->getActif()) || self::$active){
            $this->connexion=$this->getConnexion();
            $this->creationTable();
        }
    }
    
    private function getConnexion(){
        $pdo=false;
		try{
			$pdo = new PDO('sqlite:'.Parser::getCheminRacine().self::$cheminDepuisRacine.self::$nomDB);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
		}
		catch(Exception $e) {
			echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
		}
        return($pdo);
    }
    
    private function creationTable(){
        $this->connexion->query("create table if not exists logs(id  integer primary key autoincrement, fichier varchar(255),message varchar(255),moment timestamp default current_timestamp)");
    }
    
    public static function getInstance() {
		if (true === is_null(self :: $instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
    
    //Retourne une chaine de caractère représentant le chemin complet vers le fichier
    private function getNomFichier($nomFichier){
        
        if(is_object($nomFichier) and !is_string($nomFichier)){
            $nomFichier=get_class($nomFichier).".log";
        }
        
        return(Parser::getCheminRacine().Logger::$cheminDepuisRacine.$nomFichier);
    }
    
    //Lors de la fermeture, on cloture la connexion aux fichiers ouverts
    public function __destruct(){
        $this->connexion=null;
    }
    
    
    //Logifie une informations dans un fichier
    public function logify($nomFichier,$message){
        $ps=null;
        if(($this->config!=null && $this->config->getActif()) || self::$active){
            try{
                $ps = $this->connexion->prepare(self::$insertion);
                $ps->execute(array($this->getNomFichier($nomFichier),$message));
            }
            catch (Exception $ex) {
                print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            }
            $this->cloturer($ps);
        }
    }
    
    public function cloturer($ps){
        try{
			$ps->closeCursor();
		}catch(Exception $e ){
			$ps = null;
		}
    }
}
?>