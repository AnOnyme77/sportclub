<?php
    class Seance
    {
        private $noSeance;
        private $cours;
        private $dateCours;
        private $nbMembreMax;
        private $idProf;

        /* get & set */

        public function getNoSeance()
        {
            return($this->noSeance);
        }
        public function setNoSeance($noSeance)
        {
            $this->noSeance=$noSeance;
        }
        public function getCours()
        {
            return($this->cours);
        }
        public function setCours($cours)
        {
            $this->cours=$cours;
        }
        public function getDateCours()
        {
            return($this->dateCours);
        }
        public function setDateCours($dateCours)
        {
            $this->dateCours=$dateCours;
        }
        public function getNbMembreMax()
        {
            return($this->nbMembreMax);
        }
        public function setNbMembreMax($nbMembreMax)
        {
            $this->nbMembreMax=$nbMembreMax;
        }
        public function getIdProf()
        {
            return($this->idProf);
        }
        public function setIdProf($idProf)
        {
            $this->idProf=$idProf;
        }

        /* fin get & set */

    }
?>
