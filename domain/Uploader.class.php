<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class Uploader{
    public static $IMAGE = array("jpg","png","bmp","gif","bifl");
    private static $MAXSIZE=1000000;
    public static $UTILISATEUR = "/images/profilsMembres/";
	public static $NIVEAU = "/images/niveaux/";
	public static $PARAMETRES = "/images/parametres/";
    private static $instance;
    private $nomImage=array();
    
    private function Uploader(){
        $this->nomImage[Uploader::$UTILISATEUR]=0;
		$this->nomImage[Uploader::$NIVEAU]=0;
		$this->nomImage[Uploader::$PARAMETRES]=0;
        foreach($this->nomImage as $key=>$value){
            $i=0;
            do{
				$tabTrouve=array();
                foreach(Uploader::$IMAGE as $extension){
                    $tabTrouve[]=file_exists(Parser::getCheminRacine().$key.$i.".".$extension);
                }
                $trouve=in_array(true,$tabTrouve);
				if($trouve)
					$i++;
            }while($trouve);
            $this->nomImage[$key]=$i;
        }
    }
    
    public function upload($type,$place,$fichier){
        $retour=false;
        // Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur
        if (isset($fichier) AND $fichier['error'] == 0){
            // Testons si le fichier n'est pas trop gros
            if ($fichier['size'] <= Uploader::$MAXSIZE){
                // Testons si l'extension est autorisée
                $infosfichier = pathinfo($fichier['name']);
                 $extension_upload = $infosfichier['extension'];
                if (in_array($extension_upload, $type)){
                    // On peut valider le fichier et le stocker définitivement
                    move_uploaded_file($fichier['tmp_name'], Parser::getCheminRacine().$place.$this->nomImage[$place].".".$extension_upload);
                    $retour=$place.$this->nomImage[$place].".".$extension_upload;
					$this->nomImage[$place]++;
                }
                else{
                    Logger::getInstance()->logify($this,"Extension non autorisée: ".$extension_upload);
                }
            }
            else{
                Logger::getInstance()->logify($this,"Le fichier est trop gros");
            }
        }
        else{
            Logger::getInstance()->logify($this,"Les informations n'ont pas été reçues");
        }
        return($retour);
    }
    
    public static function getInstance() {
		if (true === is_null(self :: $instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}
?>