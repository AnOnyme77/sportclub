<?php
class Adresse
{
    private $rue;
    private $codePostale;
    private $ville;
    private $pays;

    public function __construct($rue=null,$cp=null,$ville=null,$pays=null)
    {
        $this->rue=$rue;
        $this->codePostale=$cp;
        $this->ville=$ville;
        $this->pays=$pays;
    }
    
    /* get & set */
    public function getRue()
    {
        return($this->rue);
    }
    public function setRue($rue)
    {
        $this->rue=$rue;
    }
    public function getCodePostale()
    {
        return($this->codePostale);
    }
    public function setCodePostale($codePostale)
    {
        $this->codePostale=$codePostale;
    }
    public function getVille()
    {
        return($this->ville);
    }
    public function setVille($ville)
    {
        $this->ville=$ville;
    }
    public function getPays()
    {
        return($this->pays);
    }
    public function setPays($pays)
    {
        $this->pays=$pays;
    }

    /* fin get & set */

}