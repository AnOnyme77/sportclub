<?php
class Examen
{
    private $datePassage;
    private $cote;
    private $commentaire;
    private $niveau;
    private $nameSection;
    private $idMembre;
    private $idProf;

	/* constructeur */
	
    public function __construct(){}

    /* get & set */

    public function getDatePassage()
    {
        return($this->datePassage);
    }
    public function setDatePassage($datePassage)
    {
    	if( !is_object($datePassage) || get_class($datePassage) != "DateTime") $datePassage = new DateTime($datePassage);
        $this->datePassage=$datePassage;
        
    }
    public function getCote()
    {
        return($this->cote);
    }
    public function setCote($cote)
    {
        $this->cote=$cote;
    }
    public function getCommentaire()
    {
        return($this->commentaire);
    }
    public function setCommentaire($commentaire)
    {
        $this->commentaire=$commentaire;
    }
    public function getNiveau()
    {
        return($this->niveau);
    }
    public function setNiveau($niveau)
    {
        $this->niveau=$niveau;
    }
    public function getNameSection()
    {
        return($this->nameSection);
    }
    public function setNameSection($nameSection)
    {
        $this->nameSection=$nameSection;
    }
    public function getIdMembre()
    {
        return($this->idMembre);
    }
    public function setIdMembre($idMembre)
    {
        $this->idMembre=$idMembre;
    }
    public function getIdProf()
    {
        return($this->idProf);
    }
    public function setIdProf($idProf)
    {
        $this->idProf=$idProf;
    }

    /* fin get & set */
}