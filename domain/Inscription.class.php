<?php
    class Inscription
    {
        private $noSeance;
        private $idMembre;
        private $present;

        /* get & set */

        public function getNoSeance()
        {
            return($this->noSeance);
        }
        public function setNoSeance($noSeance)
        {
            $this->noSeance=$noSeance;
        }
        public function getIdMembre()
        {
            return($this->idMembre);
        }
        public function setIdMembre($idMembre)
        {
            $this->idMembre=$idMembre;
        }
        public function getPresent()
        {
            return($this->present);
        }
        public function setPresent($present)
        {
            $this->present=$present;
        }

        /* fin get & set */

    }
?>
