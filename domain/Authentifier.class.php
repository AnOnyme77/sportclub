<?php
    class Authentifier
    {
        private $email;
        private $mdp;
    
        public function __construct($email, $mdp)
        {
            $this->email=$email;
            $this->mdp=$mdp;
        }
        
        /* get & set */
        public function getEmail()
        {
            return($this->email);
        }
        public function setEmail($email)
        {
            $this->email=$email;
        }
        
        public function getMdp()
        {
            return($this->mdp);
        }
        public function setMdp($mdp)
        {
            $this->mdp=$mdp;
        }
        
        /*Fin des get et des set*/
    
    }
?>
