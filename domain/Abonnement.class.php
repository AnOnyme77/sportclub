<?php
    class Abonnement
    {
        public static $FORMATDATE = "Y-m-d"; //format date
        public static $FORMATDATEFR = "d/m/Y"; //format date FR
        public static $FORMATIMESTAMP = "Y-m-d H:i"; //format datetime
        public static $FORMATIMESTAMPFR = "d/m/Y H:i"; //format datetime FR
        public static $FORMATIMESTAMPFRHEURES = "d-m-Y H:i:s"; //format datetime FR
        
        private $idMembre;
        private $libelle;
        private $dateDebut;
        private $dateAchat;
        private $nbUniteRest;
        private $prix;
        private $dateFin;
        
        /* Constructor */
        public function __construct()
        {
            /*Vide*/
        }

        /* get & set */

        public function getIdMembre()
        {
            return($this->idMembre);
        }
        public function setIdMembre($idMembre)
        {
            $this->idMembre=$idMembre;
        }
        public function getLibelle()
        {
            return($this->libelle);
        }
        public function setLibelle($libelle)
        {
            $this->libelle=$libelle;
        }
        public function getDateDebut()
        {
            return($this->dateDebut);
        }
        public function setDateDebut($dateDebut)
        {
            $this->dateDebut=$dateDebut;
        }
        public function getDateAchat()
        {
            return($this->dateAchat);
        }
        public function setDateAchat($dateAchat)
        {
            $this->dateAchat=$dateAchat;
        }
        public function getNbUniteRest()
        {
            return($this->nbUniteRest);
        }
        public function setNbUniteRest($nbUniteRest)
        {
            $this->nbUniteRest=$nbUniteRest;
        }
        public function getPrix()
        {
            return($this->prix);
        }
        public function setPrix($prix)
        {
            $this->prix=$prix;
        }
        public function getDateFin()
        {
            return($this->dateFin);
        }
        public function setDateFin($dateFin)
        {
            $this->dateFin=$dateFin;
        }

        /* Spéciaux */
        public function getDateFormatPres($date){
        return $date->format(Abonnement::$FORMATIMESTAMP);
        }

        public function getDateFormatPresFrench($date){
        return $date->format(Abonnement::$FORMATIMESTAMPFR);
        }

        public function getDateFormatPresFrenchHeures($date){
        return $date->format(Abonnement::$FORMATIMESTAMPFRHEURES);
        }

        public function getDateFormatPresDate($date){
        return $date->format(Abonnement::$FORMATDATE);
        }

        public function getDateFormatPresDateFrench($date){
        return $date->format(Abonnement::$FORMATDATEFR);
        }

        /* fin get & set */

    }
?>
