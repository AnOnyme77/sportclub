<?php
	/* Auteur : Dev4u
	 * creer le 17/02/2014
	 * */
class Bundle {

	public static $MESSAGE = "message";
	public static $OPERATION_REUSSIE = "operationReussie";
	public static $LISTE = "liste";
	public static $MEMBRE = "membre";
	public static $AUTHENTIFIER = "authentifier";
	public static $IDMEMBRE = "IdMembre";
	public static $PARAMETRE = "Parametre";
	public static $EMAIL = "email";
	public static $NIVEAU = "niveau";
	public static $COMPETENCE = "competence";
	public static $SEANCE = "seance";
	public static $ABONNEMENT = "abonnement";
	public static $TYPEABONNEMENT = "typeAbonnement";
	public static $INSCRIPTION="Inscrption";
	public static $ENTIER="Int";
	public static $EXAMEN="examen";

	public static $SEARCHOK="Recherche effectuée avec succès.";
	public static $SEARCHKO="Recherche échouée";
	public static $GETOK="Récupération effectuée avec succès.";
	public static $GETKO="Récupération échouée.";
	public static $LISTOK="Listage effectué avec succès.";
	public static $LISTKO="Listage échoué.";
	public static $MODOK="Modification effectuée avec succès.";
	public static $MODKO="Modification échouée.";
	public static $ADDOK="Ajout effectué avec succès.";
	public static $ADDKO="Ajout échoué.";
	public static $DELOK="Suppression effectuée avec succès.";
	public static $DELKO="Suppression échouée.";
	public static $DECOK="Décrémentation effectuée avec succès.";
	public static $DECKO="Décrémentation échouée.";
	public static $ABOKO="L'abonnement ne possède plus d'unités.";
	public static $CONKO="Email et/ou mot de passe incorrect.";

	public static $NOTADD="Il n'y a rien à ajouter.";
	public static $NOTMOD="Il n'y a rien à modifier.";
	public static $NOTDEL="Il n'y a rien à supprimer.";

	public static $ERROR="Une erreur est survenue";

	public static $EXIST="Elément déjà existant.";
	public static $NOTEXIST="Elément inexistant.";

	private $map = array ();

	function Bundle() {
		$this->map[self::$OPERATION_REUSSIE] = true;
		$this->map[self::$MESSAGE] = "";
	}

	public function put($clef, $valeur) {
		$this->map[$clef] = $valeur;
	}

	public function get($clef) {
		return $this->map[$clef];
	}
	public function vider(){
		$this->map=array();
	}
}