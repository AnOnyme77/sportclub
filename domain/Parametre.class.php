<?php
    require_once("Adresse.class.php");
    class Parametre
    {
        private $nomEntreprise;
        private $numTel;
        private $adresseEntreprise;
        private $superLogin;
        private $superMdp;
        private $nomResp;
        private $prenomResp;
        private $coteMax;
        private $logo;
        private $tailleLogoHaut;
        private $tailleLogoLarge;
        private $tailleLogoNivHaut;
        private $tailleLogoNivLarge;
        private $tailleLogoMembreHaut;
        private $tailleLogoMembreLarge;
        private $libNiveau;
        private $libExamen;
        private $libCompetence;
        private $libNiveaux;
        private $libExamens;
        private $libCompetences;
        
        /*Constructeur*/
        public function Parametre(){}

        /* get & set */

        public function getNomEntreprise()
        {
            return($this->nomEntreprise);
        }
        public function setNomEntreprise($nomEntreprise)
        {
            $this->nomEntreprise=$nomEntreprise;
        }
        public function getNumTel()
        {
            return($this->numTel);
        }
        public function setNumTel($numTel)
        {
            $this->numTel=$numTel;
        }
        public function getAdresseEntreprise()
        {
            return($this->adresseEntreprise);
        }
        public function setAdresseEntreprise($adresseEntreprise)
        {
            $this->adresseEntreprise=$adresseEntreprise;
        }
        public function getSuperLogin()
        {
            return($this->superLogin);
        }
        public function setSuperLogin($superLogin)
        {
            $this->superLogin=$superLogin;
        }
        public function getSuperMdp()
        {
            return($this->superMdp);
        }
        public function setSuperMdp($superMdp)
        {
            $this->superMdp=$superMdp;
        }
        public function getNomResp()
        {
            return($this->nomResp);
        }
        public function setNomResp($nomResp)
        {
            $this->nomResp=$nomResp;
        }
        public function getPrenomResp()
        {
            return($this->prenomResp);
        }
        public function setPrenomResp($prenomResp)
        {
            $this->prenomResp=$prenomResp;
        }
        public function getCoteMax()
        {
            return($this->coteMax);
        }
        public function setCoteMax($coteMax)
        {
            $this->coteMax=$coteMax;
        }
        public function getLogo()
        {
            return($this->logo);
        }
        public function setLogo($logo)
        {
            $this->logo=$logo;
        }
        public function getTailleLogoHaut()
        {
            return($this->tailleLogoHaut);
        }
        public function setTailleLogoHaut($tailleLogoHaut)
        {
            $this->tailleLogoHaut=$tailleLogoHaut;
        }
        public function getTailleLogoLarge()
        {
            return($this->tailleLogoLarge);
        }
        public function setTailleLogoLarge($tailleLogoLarge)
        {
            $this->tailleLogoLarge=$tailleLogoLarge;
        }
        public function getTailleLogoNivHaut()
        {
            return($this->tailleLogoNivHaut);
        }
        public function setTailleLogoNivHaut($tailleLogoNivHaut)
        {
            $this->tailleLogoNivHaut=$tailleLogoNivHaut;
        }
        public function getTailleLogoNivLarge()
        {
            return($this->tailleLogoNivLarge);
        }
        public function setTailleLogoNivLarge($tailleLogoNivLarge)
        {
            $this->tailleLogoNivLarge=$tailleLogoNivLarge;
        }
        public function getTailleLogoMembreHaut()
        {
            return($this->tailleLogoMembreHaut);
        }
        public function setTailleLogoMembreHaut($tailleLogoMembreHaut)
        {
            $this->tailleLogoMembreHaut=$tailleLogoMembreHaut;
        }
        public function getTailleLogoMembreLarge()
        {
            return($this->tailleLogoMembreLarge);
        }
        public function setTailleLogoMembreLarge($tailleLogoMembreLarge)
        {
            $this->tailleLogoMembreLarge=$tailleLogoMembreLarge;
        }
        public function getLibNiveau()
        {
            return($this->libNiveau);
        }
        public function setLibNiveau($libNiveau)
        {
            $this->libNiveau=$libNiveau;
        }
        public function getLibExamen()
        {
            return($this->libExamen);
        }
        public function setLibExamen($libExamen)
        {
            $this->libExamen=$libExamen;
        }
        public function getLibCompetence()
        {
            return($this->libCompetence);
        }
        public function setLibCompetence($libCompetence)
        {
            $this->libCompetence=$libCompetence;
        }
        public function getLibNiveaux()
        {
            return($this->libNiveaux);
        }
        public function setLibNiveaux($libNiveaux)
        {
            $this->libNiveaux=$libNiveaux;
        }
        public function getLibExamens()
        {
            return($this->libExamens);
        }
        public function setLibExamens($libExamens)
        {
            $this->libExamens=$libExamens;
        }
        public function getLibCompetences()
        {
            return($this->libCompetences);
        }
        public function setLibCompetences($libCompetences)
        {
            $this->libCompetences=$libCompetences;
        }

        /* fin get & set */

    }
?>
