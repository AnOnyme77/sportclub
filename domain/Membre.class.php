<?php
/* Autheur : Hainaut Jérôme
 * creer le 17/02/2014
 * */
class Membre
{
	public static $FORMATDATE = "Y-m-d"; //format date
	public static $FORMATIMESTAMP = "Y-m-d H:i:s"; //format datetime
	public static $FORMATDATEPRES = "d/m/Y"; //format datetime
	public static $FORMATHEUREPRES = "H:i:s"; //format datetime
	public static $LOGO = "/images/profilsMembres/defaultMember.jpg"; //chemin de l'image par default (tjrs mettre Parser::getChemin(). devant...)
 	   	
    private $idMembre; 
    private $nom;
    private $prenom;
    private $sexe; 
    private $dateNaissance;//dateTime
    private $telFixe; //String 25 caract
    private $telGsm; //String 25 caract
    private $adresse; //Adresse
    private $fonction; // M, A, P
    private $statut; // F (banni), T (actif)
    private $email;
    private $mdp; //en heu hasher ca!
    private $dateInscription;//dateTime
    private $logo; 
    private $supprime; //F (non), T (adios)
    private $niveau; //int
    private $nameSection;
    
    /*Constructeur*/
    public function __construct(){}

	/* méthode */
	
	/* cree le fichier barcode.png */
	public function eanIdMembre(){
		if(!file_exists(Parser::getCheminRacine().'/images/barcode/'.$this->getIdMembre().'.png')){
			// instanciation
			$bc = new Barcode(); 
			// Le code a générer
			$bc->setCode($this->getIdMembre());
			// Type de code : EAN, UPC, C39...
			$bc->setType('EAN');
			// taille de l'image (hauteur, largeur, zone calme)
			$bc->setSize(120, 300, 10);
			// Texte sous les barres :
			//    'AUTO' : affiche la valeur du codes barres
			//    'texte a afficher' : affiche un texte libre sous les barres
			$bc->setText('AUTO');
			// Si elle est appelée, cette méthode désactive
			// l'impression du Type de code (EAN, C128...)
			$bc->hideCodeType();
			// Couleurs des Barres, et du Fond
			$bc->setColors('#000000', '#FFFFFF');
			// Type de fichier
			$bc->setFiletype('PNG');
			// envoie l'image dans un fichier
			 $bc->writeBarcodeFile(Parser::getCheminRacine().'/images/barcode/'.$this->getIdMembre().'.png');
		}
	}
	
	public static function genererMDP ($longueur = 10){
	    $mdp = "";
	 
	    // Définir tout les caractères possibles dans le mot de passe
	    $possible = "12346789azertyuiopmlkjhgfdsqwxcvbnAZERTGFDSQWXCVBNHYUJKIOLMP";
	 
	    $longueurMax = strlen($possible);
	 
	    if ($longueur > $longueurMax*3) {
	        $longueur = $longueurMax;
	    }
	    $i = 0;
	    while ($i < $longueur) {
	        $caractere = substr($possible, mt_rand(0, $longueurMax-1), 1);
	 
	        // vérifier si le caractère est déjà utilisé dans $mdp plus de 3 fois
	        if (substr_count($mdp, $caractere)<=3) {
	            $mdp .= $caractere;
	            $i++;
	        }
	    }
	    return $mdp;
	}
	
	public function getAge(){
		return $this->dateNaissance->diff(new DateTime("now"))->format("%Y");
	}
	
	public function getAnneeInscription(){
		return $this->dateInscription->diff(new DateTime("now"))->format("%Y");
	}
	
	public function getDateInscriptionFormat(){
		return $this->dateInscription->format(self::$FORMATDATEPRES);
	}
	
	public function getDateNaissanceFormat(){
		return $this->dateNaissance->format(self::$FORMATDATEPRES);
	}
	
	public function equal($membre){
		$egal = true;
		if(is_object($membre) && get_class($this) == get_class($membre))
			foreach ( $this as $key => $value ) {
	       		if( $value !=$membre->$key) $egal=false;
			}
		else $egal = false;
		return 	$egal;
	}
	
	
	public function verifMDP($antMDPverif, $newMDP, $newMDPverif){
		$mdpOk=false;
		if(( (!isset($antMDPverif) || $antMDPverif== "") && 
			(!isset($newMDP) || $newMDP == "") && (!isset($newMDPverif) || $newMDPverif == "")) ||
			((isset($this->mdp) && isset($antMDPverif) && $this->mdp == $antMDPverif &&
				isset($newMDP) && $newMDP != "" && isset($newMDPverif) && $newMDP==$newMDPverif))){
			$mdpOk=true;
			$this->mdp=(isset($newMDP) && $newMDP != "")?$newMDP:$this->mdp;
		}
		return $mdpOk;
	}
	
    /* get & set */
    public function getIdMembre()
    {
        return($this->idMembre);
    }
    public function setIdMembre($idMembre)
    {
        $this->idMembre=$idMembre;
    }
    public function getNom()
    {
        return($this->nom);
    }
    public function setNom($nom)
    {
        $this->nom=$nom;
    }
    public function getPrenom()
    {
        return($this->prenom);
    }
    public function setPrenom($prenom)
    {
        $this->prenom=$prenom;
    }
    public function getSexe()
    {
        return($this->sexe);
    }
    public function setSexe($sexe)
    {
        $this->sexe=$sexe;
    }
    public function getDateNaissance()
    {
        return($this->dateNaissance);
    }
    public function setDateNaissance($dateNaissance)
    {
    	if( is_string($dateNaissance)) $dateNaissance = new DateTime($dateNaissance);
        $this->dateNaissance=$dateNaissance;
    }
    public function setDateNaissanceDetail($jour, $mois, $annee){
    	$this->dateNaissance= new DateTime("$annee-$mois-$jour");
    }
    public function getTelFixe()
    {
        return($this->telFixe);
    }
    public function setTelFixe($telFixe)
    {
        $this->telFixe=$telFixe;
    }
    public function getTelGsm()
    {
        return($this->telGsm);
    }
    public function setTelGsm($telGsm)
    {
        $this->telGsm=$telGsm;
    }
    public function getAdresse()
    {
        return($this->adresse);
    }
    public function setAdresse($adr_rue)
    {
        $this->adresse=$adr_rue;
    }
    public function getFonction()
    {
        return($this->fonction);
    }
    public function setFonction($fonction)
    {
        $this->fonction=$fonction;
    }
    public function getStatut()
    {
        return($this->statut);
    }
    public function setStatut($statut)
    {
        $this->statut=$statut;
    }
    public function getEmail()
    {
        return($this->email);
    }
    public function setEmail($email)
    {
        $this->email=$email;
    }
    public function getMdp()
    {
        return($this->mdp);
    }
    public function setMdp($mdp)
    {
        $this->mdp=$mdp;
    }
    public function getDateInscription()
    {
        return($this->dateInscription);
    }
    public function setDateInscription($dateInscription)
    {
    	if( is_string($dateInscription)) $dateInscription = new DateTime($dateInscription);
        $this->dateInscription=$dateInscription;
    }
    public function setDateInscriptionDetail($jour, $mois, $annee){
    	$this->dateInscription= new DateTime("$annee-$mois-$jour");
    }
    public function getLogo()
    {
        return($this->logo);
    }
    public function setLogo($logo)
    {
        $this->logo=$logo;
    }
    public function getSupprime()
    {
        return($this->supprime);
    }
    public function setSupprime($supprime)
    {
        $this->supprime=$supprime;
    }
    public function getNiveau()
    {
        return($this->niveau);
    }
    public function setNiveau($niveau)
    {
        $this->niveau=$niveau;
    }
    public function getNameSection()
    {
        return($this->nameSection);
    }
    public function setNameSection($nameSection)
    {
        $this->nameSection=$nameSection;
    }

    /* fin get & set */

}
