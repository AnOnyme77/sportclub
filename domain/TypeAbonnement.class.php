<?php
    class TypeAbonnement
    {
        private $libelle;
        private $prix;
        private $duree;
        private $nbUnite;
        private $cours;
        private $supprime;

        /* get & set */

        public function getLibelle()
        {
            return($this->libelle);
        }
        public function setLibelle($libelle)
        {
            $this->libelle=$libelle;
        }
        public function getPrix()
        {
            return($this->prix);
        }
        public function setPrix($prix)
        {
            $this->prix=$prix;
        }
        public function getDuree()
        {
            return($this->duree);
        }
        public function setDuree($duree)
        {
            $this->duree=$duree;
        }
        public function getNbUnite()
        {
            return($this->nbUnite);
        }
        public function setNbUnite($nbUnite)
        {
            $this->nbUnite=$nbUnite;
        }
        public function getCours()
        {
            return($this->cours);
        }
        public function setCours($cours)
        {
            $this->cours=$cours;
        }
        public function getSupprime()
        {
            return($this->supprime);
        }
        public function setSupprime($supprime)
        {
            $this->supprime=$supprime;
        }

        /* fin get & set */

    }
?>
