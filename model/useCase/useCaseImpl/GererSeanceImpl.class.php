<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
class GererSeanceImpl implements GererSeance{
    
    private $daoSeance;
    private $daoMembre;
    private $daoNiveau;
    
    public function GererSeanceImpl(){
        $this->daoSeance=DaoFactory::getInstance()->get("SeanceDao");
        $this->daoNiveau=DaoFactory::getInstance()->get("NiveauDao");
        $this->daoMembre=DaoFactory::getInstance()->get("MembreDao");
    }
    
    public function getCours($bundle){
        $tab=$this->daoSeance->getCours();
        
        
        if(is_array($tab)){
            $i=0;
            $tabSortie=array();
            for($i=0;$i<count($tab);$i++){
                $niveau=strtoupper($tab[$i][0]).substr($tab[$i],1);
                if(!in_array($niveau,$tabSortie))
                    $tabSortie[$tab[$i]]=$niveau;
            }
            natsort($tabSortie);
            $bundle->put(Bundle::$LISTE,$tabSortie);
            $bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
        }
    }
    
    public function ajouterSeance($bundle){
        $seance=$bundle->get(Bundle::$SEANCE);
        if(!is_null($seance) and is_object($seance)){
            if($this->daoMembre->getMembre($seance->getIdProf())!=null){
                $retour=$this->daoSeance->ajouterSeance($seance);
                if($retour!=false){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$ADDOK);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
        }
        return($retour);
    }
    public function modifierSeance($bundle){
        $seance=$bundle->get(Bundle::$SEANCE);
        if(!is_null($seance) and is_object($seance)){
            if($this->daoMembre->getMembre($seance->getIdProf())!=null){
                if($this->daoSeance->modifierSeance($seance)){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$MODOK);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$MODKO);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
        }
    }
    public function supprimerSeance($bundle){
        $seance=$bundle->get(Bundle::$SEANCE);
        if(!is_null($seance) and is_object($seance)){
            if($this->daoMembre->getMembre($seance->getIdProf())!=null){
                if($this->daoSeance->supprimerSeance($seance)){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$DELOK);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$DELKO);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
        }
    }
    public function listerSeance($bundle){
        $liste=$this->daoSeance->listerSeances();
        if(is_array($liste)){
            $bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTOK);
            $bundle->put(Bundle::$LISTE,$liste);
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
        }
    }
    public function betterListerSeance($bundle){
        $liste=$this->daoSeance->betterListerSeances();
        if(is_array($liste)){
            $listeSeances=array();
            $listeMembres=array();
            foreach($liste as $ligne){
                $seance=new Seance();
				$seance->setCours($ligne["cours"]);
				$seance->setNoSeance($ligne["noSeance"]);
				$seance->setDateCours($ligne["dateCours"]);
				$seance->setNbMembreMax($ligne["nbMembreMax"]);
				$seance->setIdProf($ligne["idProf"]);
				$listeSeances[]=$seance;
                
                $membre=new Membre();
                $membre->setNom($ligne["nom"]);
                $membre->setPrenom($ligne["prenom"]);
                $membre->setIdMembre($ligne["idProf"]);
                $listeMembres[]=$membre;
            }
            
            $listeRetour=array();
            $listeRetour["seances"]=$listeSeances;
            $listeRetour["membres"]=$listeMembres;
            
            $bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTOK);
            $bundle->put(Bundle::$LISTE,$listeRetour);
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
        }
    }
    public function getSeance($bundle){
        $seance=$bundle->get(Bundle::$SEANCE);
        if(!is_null($seance) and is_object($seance)){
            $retour=$this->daoSeance->getSeance($seance);
            if($retour!=false){
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
                $bundle->put(Bundle::$SEANCE,$retour);
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$MODKO);
            }

        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
        }
    }
}
?>