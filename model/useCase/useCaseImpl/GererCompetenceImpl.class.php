<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

class GererCompetenceImpl implements GererCompetence{
    
    private $daoCompetence;
    private $daoNiveau;
    private $daoFactory;
    
    public function GererCompetenceImpl(){
        $this->daoFactory=DaoFactory::getInstance();
        $this->daoCompetence=$this->daoFactory->get("CompetenceDao");
        $this->daoNiveau=$this->daoFactory->get("NiveauDao");
    }
    
    public function getCompetenceFromNiveau($bundle){
        $niveau=$bundle->get(Bundle::$NIVEAU);
        
        if($this->daoNiveau->getNiveau($niveau->getNiveau(),$niveau->getNameSection())!=false){
            $liste=$this->daoCompetence->getFromNiveau($niveau);
            if(is_array($liste)){
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
                $bundle->put(Bundle::$LISTE,$liste);
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
                Logger::getInstance()->logify($this,Bundle::$GETKO);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
        }
    }
    
    public function ajouterCompetence($bundle){
        $competence=$bundle->get(Bundle::$COMPETENCE);
        
        $niveau= new Niveau();
        $niveau->setNiveau($competence->getNiveau());
        $niveau->setNameSection($competence->getNameSection());
        
        if($this->daoNiveau->getNiveau($niveau->getNiveau(),$niveau->getNameSection())!=false){
            if($this->daoCompetence->ajouterCompetence($competence)){
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$ADDOK);
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$ADDKO);
                Logger::getInstance()->logify($this,Bundle::$ADDKO);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
        }
    }
    
    public function modifierCompetence($bundle){
        $competence=$bundle->get(Bundle::$COMPETENCE);
        $niveau= new Niveau();
        $niveau->setNiveau($competence->getNiveau());
        $niveau->setNameSection($competence->getNameSection());
        
        if($this->daoNiveau->getNiveau($niveau->getNiveau(),$niveau->getNameSection())!=false){
            if($this->daoCompetence->getCompetence($competence)!=false){
                if($this->daoCompetence->modifierCompetence($competence)){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$MODOK);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$MODKO);
                    Logger::getInstance()->logify($this,Bundle::$MODKO);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
        }
    }
    public function supprimerCompetence($bundle){
        $competence=$bundle->get(Bundle::$COMPETENCE);
        
        $niveau= new Niveau();
        $niveau->setNiveau($competence->getNiveau());
        $niveau->setNameSection($competence->getNameSection());
        
        if($this->daoNiveau->getNiveau($niveau->getNiveau(),$niveau->getNameSection())!=false){
            if($this->daoCompetence->supprimerCompetence($competence)){
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$DELOK);
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$DELKO);
                Logger::getInstance()->logify($this,Bundle::$DELKO);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
        }
    }
    
    public function listerCompetence($bundle){
        $liste=$this->daoCompetence->listerCompetences();
        if($liste!=false){
            $bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTOK);
            $bundle->put(Bundle::$LISTE,$liste);
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
            Logger::getInstance()->logify($this,Bundle::$LISTKO);
        }
    }
    
    public function getCompetence($bundle){
        $competence=$bundle->get(Bundle::$COMPETENCE);
        
        $niveau= new Niveau();
        $niveau->setNiveau($competence->getNiveau());
        $niveau->setNameSection($competence->getNameSection());
        
        if($this->daoNiveau->getNiveau($niveau->getNiveau(),$niveau->getNameSection())!=false){
            $competenceRetour=$this->daoCompetence->getCompetence($competence);
            if($competenceRetour!=false){
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
                $bundle->put(Bundle::$COMPETENCE,$competenceRetour);
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
                Logger::getInstance()->logify($this,Bundle::$GETKO);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
        }
    }
}
?>