<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Hainaut Jérôme
 * Team : Dev4u
 * créé le 25/04/2014 - modifée le 25/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
class GererExamenImpl implements GererExamen{
    
    private $dao;
    
    public function GererExamenImpl(){
        $this->dao=DaoFactory::getInstance()->get("ExamenDao");
	}

    public function getExamen($bundle){
		$operationReussie = false;
		$message = "";
    	$examen = $bundle->get(Bundle::$EXAMEN);
        $examenObject = $this->dao->getExamen($examen);
        if($examenObject!=null){   
        	$message = Bundle::$GETOK;
        	$operationReussie = true;
        }
        else{
           $message = Bundle::$GETKO; 
        }
        $bundle->put(Bundle::$OPERATION_REUSSIE,$operationReussie);
        $bundle->put(Bundle::$MESSAGE,$message);
        $bundle->put(Bundle::$EXAMEN,$examenObject);
    }
    
	public function getExamensNiveau($bundle){
		$operationReussie = false;
		$message = "";
    	$examen = $bundle->get(Bundle::$EXAMEN);
        $listeExamen = $this->dao->getExamenMembre($examen);
        if(is_array($listeExamen)){   
        	$message = Bundle::$GETOK;
        	$operationReussie = true;
        }
        else{
           $message =Bundle::$GETKO; 
        }
        $bundle->put(Bundle::$OPERATION_REUSSIE,$operationReussie);
        $bundle->put(Bundle::$MESSAGE,$message);
        $bundle->put(Bundle::$LISTE,$listeExamen);
    }
	
    public function modifierExamen($bundle){
        $operationReussie=false;
 		$message="";
        $examen=$bundle->get(Bundle::$EXAMEN);
		$libExam = explode(" ",strtolower(unserialize($_SESSION["oParametres"])->getLibExamen()));
        
        if (get_class($examen) != "Examen") {
           $message = Bundle::$NOTMOD; 
			
		}else{
			if($examen->getCote() < 0){
				$message = "Cote négative";
			}else {
				if($this->dao->getExamen($examen) == null){
		           $message = Bundle::$NOTEXIST; 
				}else{
					$operationReussie = $this->dao->modifierExamen($examen);
	        		if($operationReussie){	
						$message = Bundle::$MODOK;
					} else {
						$message =Bundle::$MODKO;
	        		}
				}
			}
		}
        $bundle->put(Bundle::$OPERATION_REUSSIE,$operationReussie);
        $bundle->put(Bundle::$MESSAGE,$message);
    }
    
    public function ajouterExamen($bundle){
 		$operationReussie=false;
 		$message="";
        $examen=$bundle->get(Bundle::$EXAMEN);
		$libNiveau = explode(" ",strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau()));
        if (get_class($examen) != "Examen") {
			$message = Bundle::$NOTADD;
			
		}else{
			if($examen->getDatePassage() == null){
				$examen->setDatePassage("now");
			}
			
			if($examen->getDatePassage() > (new DateTime())){
				$message = "date de passage suppérieure à la date actuelle";
			}else if($examen->getIdMembre() == null ){
				$message = "Aucun membre correspondant";
			} else if($examen->getNameSection() == null){
				$message = "Section vide";
			}else if($examen->getNiveau() == null){
				$message = ucfirst($libNiveau)." vide";
			}else if($examen->getCote() < 0){
				$message = "Côte négative";
			}else {
				$gererMembre = new GererMembreImpl();
				$bundleAutre = new Bundle();
				$bundleAutre->put(Bundle::$IDMEMBRE, $examen->getIdMembre());
				$gererMembre->rechercherMembreID($bundleAutre);
				if(!$bundleAutre->get(Bundle::$OPERATION_REUSSIE) ){
					$message = "Membre inconnu";
				} else {
					$membreRecup=$bundleAutre->get(Bundle::$MEMBRE);
					$bundleAutre->put(Bundle::$IDMEMBRE, $examen->getIdProf());
					$gererMembre->rechercherMembreID($bundleAutre);
					if(!$bundleAutre->get(Bundle::$OPERATION_REUSSIE) ){
						$message = "Prof inconnu";
					} else{
						$membre = $bundleAutre->get(Bundle::$MEMBRE);
						if($membre->getFonction() == "M"){
							$message = "Champs spécifique aux professeurs";
						} else{
							$gererNiveau = new GererNiveauImpl();
							$gererNiveau->getNiveau($examen->getNiveau(),$examen->getNameSection(),$bundleAutre);
							if(!$bundleAutre->get(Bundle::$OPERATION_REUSSIE)){
								$message = $bundleAutre->get(Bundle::$MESSAGE);
							} else{
								if(session_status()!=PHP_SESSION_ACTIVE) session_start();
					            $parametres = unserialize($_SESSION["oParametres"]);
					            if($examen->getCote() > $parametres->getCoteMax()){
					            	$message = "Côte supérieure à la cote maximum";
					            }else{
					            	if($this->dao->getExamen($examen) !=null){
										$message =Bundle::$EXIST;
					            	}else{
					            		$operationReussie1 = $this->dao->ajouterExamen($examen);
										$membreRecup->setNiveau($examen->getNiveau());
										$membreRecup->setNameSection($examen->getNameSection());
										$bundleAutre->put(Bundle::$MEMBRE,$membreRecup);
										$gererMembre->modifierMembre($bundleAutre);
										$operationReussie2=$bundleAutre->get(Bundle::$OPERATION_REUSSIE);
										$operationReussie=$operationReussie1&&$operationReussie2;
					            		if($operationReussie){
											$message = Bundle::$ADDOK;
										} else {
											$message = Bundle::$ADDKO;
					            		}
					            	}
					            }
							}
						}
					}
				}
			}
		}
        $bundle->put(Bundle::$OPERATION_REUSSIE,$operationReussie);
        $bundle->put(Bundle::$MESSAGE,$message);
    }
    
    public function supprimerExamen($bundle){
        $operationReussie = false;
		$exam = $bundle->get(Bundle::$EXAMEN);
		$message = "";
		if ($exam == null) {
			$message =Bundle::$NOTEXIST;
		} else {
			$operationReussie = $this->dao->supprimerExamen($exam);
			if ($operationReussie) {
				$message =Bundle::$DELOK;
			} else {
				$message =Bundle::$DELKO;
			}
		}
		
		$bundle->put(Bundle::$OPERATION_REUSSIE, $operationReussie);
		$bundle->put(Bundle::$MESSAGE, $message);
	 		
    }
    
    public function listerExamen($bundle){
	 	$listeOk = true;
		$message = "";
		$liste = $this->dao->listerExamen();
		$libExam = explode(" ",strtolower(unserialize($_SESSION["oParametres"])->getLibExamen()));
		$libExams = strtolower(unserialize($_SESSION["oParametres"])->getLibExamens());			            		
		if (empty($liste)){
			$message = "Liste vide";
			$listeOk = false;
		}
		else if (count($liste) == 1)
			$message = "Il y a 1 ".$libExam[1];
		else
			$message = "Il y a ".count($liste)." ".$libExams;
		$bundle->put(Bundle::$OPERATION_REUSSIE, $listeOk);
		$bundle->put(Bundle::$MESSAGE, $message);
		$bundle->put(Bundle::$LISTE, $liste);
    }
}