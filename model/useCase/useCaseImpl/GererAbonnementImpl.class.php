<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 16/03/2014 - modifée le 30/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
class GererAbonnementImpl implements GererAbonnement
{
    private static $UNITEREST='/^[0-9]{11}$/';
    private static $CODEEAN13='/^[0-9]{13}$/';
	private $abonnementDao;
    private $typeAbonnementDao;
    private $daoFactory;
	
	public function GererAbonnementImpl()
	{
        $this->daoFactory = DaoFactory::getInstance();
		$this->abonnementDao = $this->daoFactory->get("AbonnementDao");
        $this->typeAbonnementDao = $this->daoFactory->get("TypeAbonnementDao");
	}
	
	public function scanMembre($bundle){
		$liste=$this->abonnementDao->scanMembre($bundle->get(Bundle::$IDMEMBRE));
		if(is_array($liste)){
			$listeAbo=array();
			$listeTypeAbo=array();
			$listeRetour=array();
			foreach($liste as $ligne){
				$abonnement=new Abonnement();
				$typeAbonnement = new TypeAbonnement();
				
				if(is_null($ligne["dateFin"]))
                    $abonnement->setDateFin(null);
                else
                    $abonnement->setDateFin(new DateTime($ligne["dateFin"]));
				$abonnement->setLibelle($ligne["libelle"]);
				$abonnement->setNbUniteRest($ligne["nbUniteRest"]);
				$abonnement->setDateAchat($ligne["dateAchat"]);
				$abonnement->setIdMembre($bundle->get(Bundle::$IDMEMBRE));
				
				$typeAbonnement->setCours($ligne["cours"]);
				$typeAbonnement->setNbUnite($ligne["nbUnite"]);
				
				$listeAbo[]=$abonnement;
				$listeTypeAbo[]=$typeAbonnement;
			}
			$listeRetour["abonnements"]=$listeAbo;
			$listeRetour["typeAbonnements"]=$listeTypeAbo;
			$bundle->put(Bundle::$LISTE,$listeRetour);
			$bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTOK);
		}
		else{
			$bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
            Logger::getInstance()->logify($this,Bundle::$LISTKO);
		}
	}

    public function getAbonnement($bundle)
    {
        $idMembre = $bundle->get(Bundle::$IDMEMBRE);
        $abo = $bundle->get(Bundle::$ABONNEMENT);
        $libelleAbo = $abo->getLibelle();
        $dateAchat = $abo->getDateAchat();

        $returnAbo = $this->abonnementDao->getAbonnement($idMembre,$libelleAbo,$dateAchat);

        if($returnAbo!=false)
        {
            if($returnAbo->getIdMembre()==$idMembre)
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
                $bundle->put(Bundle::$ABONNEMENT,$returnAbo);
            }
            else
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
            }
        }
        else
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
            Logger::getInstance()->logify($this,Bundle::$GETKO);
        }

        return($bundle);
    }

    public function decrementeAbonnementMembre($bundle){
        $abo = new Abonnement();
        $abo = $bundle->get(Bundle::$ABONNEMENT);
        $idMembre = $abo->getIdMembre();
        $libelle = $abo->getLibelle();
        $dateAchat = new DateTime($abo->getDateAchat());
        $abo = $this->abonnementDao->getAbonnement($idMembre, $libelle, $dateAchat);

        $erreur = $this->verifAbonnement($abo,"La décrémentation");
        if(!is_null($abo))
        {
            if($erreur == false)
            {
                if($abo->getNbUniteRest() > 0){
                    $abo->setNbUniteRest($abo->getNbUniteRest()-1);
                    if($abo->getNbUniteRest() > 0){
                        $this->abonnementDao->modifierAbonnement($abo);
   					    $abo = $bundle->put(Bundle::$ABONNEMENT,$abo);
                        $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                        $bundle->put(Bundle::$MESSAGE,Bundle::$DECOK);
                    }
                    elseif($abo->getNbUniteRest() == 0){
                        $this->abonnementDao->modifierAbonnement($abo);
    				    $abo = $bundle->put(Bundle::$ABONNEMENT,$abo);
                        $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                        $bundle->put(Bundle::$MESSAGE,Bundle::$DECOK." L'abonnement est clôturé.");
                    }
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$ABOKO);
                }
            }
            else
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$DECKO);
                Logger::getInstance()->logify($this,Bundle::$DECKO);
                if($erreur != false)
                    Logger::getInstance()->logify($this,"Vérification échouée.");
            }
        }
        else
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,"L'abonnement reçu en retour est invalide.");
            Logger::getInstance()->logify($this,"L'abonnement reçu en retour est invalide.");
        }

    }

	public function getAbonnementsMembre($bundle)
	{
        $idMembre = $bundle->get(Bundle::$IDMEMBRE);

        $abosMembre=$this->abonnementDao->getAbonnementsMembre($idMembre);
        if(count($abosMembre)>=0){
                if(count($abosMembre)==0){
                    $bundle->put(Bundle::$LISTE,$abosMembre);
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,"Liste vide.");
                }
                elseif(count($abosMembre)==1){
                    $bundle->put(Bundle::$LISTE,$abosMembre);
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,"Vous avez 1 abonnement.");
                }
                else{
                    $bundle->put(Bundle::$LISTE,$abosMembre);
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,"Vous avez ".count($abosMembre)." abonnements.");
                }
            }
        else
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
            Logger::getInstance()->logify($this,Bundle::$LISTKO);
        }
        return($bundle);
	}

    public function modifierAbonnement($bundle)
    {
        $abo = $bundle->get(Bundle::$ABONNEMENT);

        $typeAbo = new TypeAbonnement();
        $typeAbo->setLibelle($abo->getLibelle());
        if($this->typeAbonnementDao->getTypeAbonnement($typeAbo->getLibelle()))
        {
            $erreur = $this->verifAbonnement($abo,"La modification");
            if($erreur == false && $this->abonnementDao->modifierAbonnement($abo))
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$MODOK);
            }
            else
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$MODKO);
                Logger::getInstance()->logify($this,Bundle::$MODKO);
                if($erreur != false)
                    Logger::getInstance()->logify($this,"Vérification échouée.");
            }
        }
        else
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,"Le type d'abonnement lié à cet abonnement n'existe pas.");
            Logger::getInstance()->logify($this,"Le type d'abonnement n'existe pas.");
        }

        return($bundle);
    }
    
    public function ajouterAbonnement($bundle)
    {
        $abo = $bundle->get(Bundle::$ABONNEMENT);

        $typeAbo = new TypeAbonnement();
        $typeAbo->setLibelle($abo->getLibelle());

        if($this->typeAbonnementDao->getTypeAbonnement($typeAbo->getLibelle()))
        {
            $erreur = $this->verifAbonnement($abo,"L'ajout");
            if($erreur == false && $this->abonnementDao->ajouterAbonnement($abo))
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$ADDOK);
            }
            else
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$ADDKO);
                Logger::getInstance()->logify($this,Bundle::$ADDKO);
                if($erreur != false)
                    Logger::getInstance()->logify($this,"Vérification échouée.");
            }
        }
        else
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,"Le type d'abonnement lié à cet abonnement n'existe pas.");
            Logger::getInstance()->logify($this,"Le type d'abonnement n'existe pas.");
        }

        return($bundle);
    }

    public function supprimerAbonnement($bundle)
    {
        $abo = $bundle->get(Bundle::$ABONNEMENT);

        $typeAbo = new TypeAbonnement();
        $typeAbo->setLibelle($abo->getLibelle());

        if($this->typeAbonnementDao->getTypeAbonnement($typeAbo->getLibelle()))
        {
            if($this->abonnementDao->supprimerAbonnement($abo))
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$DELOK);
            }
            else
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$DELKO);
                Logger::getInstance()->logify($this,Bundle::$DELKO);
            }
        }
        else
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,"Le type d'abonnement lié à cet abonnement n'existe pas.");
            Logger::getInstance()->logify($this,"Le type d'abonnement n'existe pas.");
        }

        return($bundle);
    }

    public function listerAbonnements($bundle)
    {
        $liste=$this->abonnementDao->listerAbonnements();
        if(is_array($liste)){
            $listeAbos=array();
            $listeMembres=array();
            $listeRetour=array();
            foreach($liste as $ligne){
                $abonnement=new Abonnement();
                $abonnement->setIdMembre($ligne["idMembre"]);
                $abonnement->setLibelle($ligne["libelle"]);
                $abonnement->setDateDebut(new DateTime($ligne["dateDebut"]));
                $abonnement->setDateAchat(new DateTime($ligne["dateAchat"]));
                $abonnement->setNbUniteRest($ligne["nbUniteRest"]);
                $abonnement->setPrix($ligne["prix"]);
                if(is_null($ligne["dateFin"]))
                    $abonnement->setDateFin(null);
                else
                    $abonnement->setDateFin(new DateTime($ligne["dateFin"]));
                $listeAbos[]=$abonnement;
                
                $membre=new Membre();
                $membre->setNom($ligne["nom"]);
                $membre->setPrenom($ligne["prenom"]);
                $membre->setIdMembre($ligne["idMembre"]);
                $listeMembres[]=$membre;
            }
            
            $listeRetour["abonnements"]=$listeAbos;
            $listeRetour["membres"]=$listeMembres;
            
            if(count($listeAbos)>=0){
                if(count($listeAbos)==0){
                    $bundle->put(Bundle::$LISTE,$listeRetour);
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,"Liste vide.");
                }
                elseif(count($listeAbos)==1){
                    $bundle->put(Bundle::$LISTE,$listeRetour);
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,"Il y a 1 abonnement.");
                }
                else{
                    $bundle->put(Bundle::$LISTE,$listeRetour);
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,"Il y a ".count($listeAbos)." abonnements.");
                }
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
        }
    }

    /*
    *   Vérification des différents champs de l'abonnement 
    */

    //  IDMEMBRE
    public static function codeValidEAN13($code){
        $digits = $code;
        // les positions: 2, 4, 6, etc.
        $posPaire = $digits[1] + $digits[3] + $digits[5] + $digits[7] + $digits[9] + $digits[11];
        
        // les positions: 1, 3, 5, etc.
        $posImpaire = $digits[0] + $digits[2] + $digits[4] + $digits[6] + $digits[8] + $digits[10];

        $tot = $posPaire * 3 + $posImpaire;

        return ((ceil($tot/10))*10 - $tot);
    }
    
    public static function idMembreOk($code){ // vérifie que l'idMembre est correct
        return (preg_match(self::$CODEEAN13,$code) && self::codeValidEAN13($code) == $code[12]);
    }

    //  LIBELLE
    public function libelleOk($libelle){
        return (strlen($libelle)<=30);
    }

    //DATEDEBUT
    public static function checkValidDate($sDate) 
    {
        if (checkdate( $sDate->format("m"), $sDate->format("d"), $sDate->format("Y")))
            return true;
        else
            return false;
    }

    public function dateDebutOk($date)
    {
        return(self::checkValidDate($date));
    }

    //DATEACHAT
    public function dateAchatOk($timeStamp)
    {
        if (checkdate( $timeStamp->format("m"), $timeStamp->format("d"), $timeStamp->format("Y")))
            return true;
        else
            return false;
    }

    //NBUNITEREST
    public function nbUniteRestOk($nb)
    {
        return (preg_match(self::$UNITEREST,$nb));
    }

    //PRIX
    public function prixOk($prix)
    {
        return(filter_var($prix, FILTER_VALIDATE_FLOAT));
    }


    /*
    *   Fonction de vérification pour tous les champs de l'abonnement avant un ajout
    */

    private function verifAbonnement($abonnement, $fonction)
    {
        $message=false;

        /*
        *   Vérification si les champs sont remplis
        */

        if ($abonnement->getIdMembre()==null && $abonnement->getIdMembre()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque l'ID du membre dans l'abonnement";

        else if ($abonnement->getLibelle()==null && $abonnement->getLibelle()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque le libelle de l'abonnement";

        else if ($abonnement->getDateDebut()==null && $abonnement->getDateDebut()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque la date de début de l'abonnement";

        else if ($abonnement->getDateAchat()==null && $abonnement->getDateAchat()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque la date d'achat de l'abonnement";

        /* Aucune vérification sur le nombre d'unités restantes, ce chiffre dépend de tout. Il peut y être comme il peut ne pas exister */
        else if ($abonnement->getPrix()==null && $abonnement->getPrix()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque le prix de l'abonnement";
            
        /*
        *   Vérification si les champs semblent corrects
        */

        else if (!is_null($abonnement->getIdMembre()) && !self::idMembreOk($abonnement->getIdMembre()))
            $message = $fonction." n'a pas pu être réalisé. Le code d'identification du membre est incorrect";

        else if ($abonnement->getLibelle()==!null && !self::libelleOk($abonnement->getLibelle()))
            $message = $fonction." n'a pas pu être réalisé. Le libelle de l'abonnement est incorrect";

        else if ($abonnement->getDateDebut()==!null && !self::dateDebutOk($abonnement->getDateDebut()))
            $message = $fonction." n'a pas pu être réalisé. La date de début de l'abonnement est incorrecte";

        else if ($abonnement->getDateAchat()==!null && !self::dateAchatOk($abonnement->getDateAchat()))
            $message = $fonction." n'a pas pu être réalisé. La date d'achat de l'abonnement est incorrecte";

        else if ($abonnement->getPrix()==!null && !self::prixOk($abonnement->getPrix()))
            $message = $fonction." n'a pas pu être réalisé. Le prix de l'abonnement est incorrect";

        return $message;
    }

}


?>