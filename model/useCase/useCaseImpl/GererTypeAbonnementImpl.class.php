<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 07/04/2014 - modifée le 28/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
class GererTypeAbonnementImpl implements GererTypeAbonnement
{ 	
    private static $CODEEAN13='/^[0-9]{13}$/';
	private $typeAbonnementDao;
	
	public function GererTypeAbonnementImpl()
	{
		$this->typeAbonnementDao = DaoFactory::getInstance()->get("TypeAbonnementDao");
	}

    public function getTypeAbonnement($libelle, $bundle)
    {
        $typeAbonnement = $this->typeAbonnementDao->getTypeAbonnement($libelle);
        if($typeAbonnement!=false)
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
            $bundle->put(Bundle::$TYPEABONNEMENT,$typeAbonnement);
        }
        else
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
            Logger::getInstance()->logify($this,Bundle::$GETKO);
        }
        return($bundle);
    }

	public function ajouterTypeAbonnement($bundle)
    {
        $message="";
        $op = false;
        $retour = false;
        //On récupére le typeAbonnement qui se trouve dans le bundle
        $typeAbonnement=$bundle->get(Bundle::$TYPEABONNEMENT);
        //On le vérifie
        if(!is_null($typeAbonnement) and is_object($typeAbonnement))
        {
            //On lance la vérification des champs de l'objet.
            $verif = self::verifTypeAbonnement($typeAbonnement, "L'ajout");

            if($verif=="") //Si $verif == "" --> Ce qui veut dire qu'aucun message d'erreur n'a été enregistré
            {
                //On vérifie s'il n'existe déjà pas dans la base de données
                if(is_null($this->typeAbonnementDao->getTypeAbonnement($typeAbonnement->getLibelle()))){
                    $retour=$this->typeAbonnementDao->ajouterTypeAbonnement($typeAbonnement);
                }
                else{
                    $op =false;
                    $message = Bundle::$EXIST;
                    Logger::getInstance()->logify($this,$message);
                }
                
                if($retour!=false){
                    $op =true;
                    $message = Bundle::$ADDOK;
                }
                else{
                    $op =false;
                    $message = Bundle::$ADDKO;
                    Logger::getInstance()->logify($this,$message);
                }
            }
            else //Sinon si $verif != "", ce qui veut dire qu'on a une erreur. On l'enregistre.
            {
                $message = $verif;
                Logger::getInstance()->logify($this,$message);
            }
        }
        else
        {
            $op = false;
            $message = Bundle::$NOTEXIST;
            Logger::getInstance()->logify($this,$message);
        }

        $bundle->put(Bundle::$OPERATION_REUSSIE,$op);
        $bundle->put(Bundle::$MESSAGE,$message);

        return($bundle);
    }
    public function supprimerTypeAbonnement($bundle)
    {
        $typeAbonnement=$bundle->get(Bundle::$TYPEABONNEMENT);
        if(!is_null($typeAbonnement) and is_object($typeAbonnement))
        {
            $retour=$this->typeAbonnementDao->supprimerTypeAbonnement($typeAbonnement);
            if($retour!=false)
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$DELOK);
            }
            else
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$DELKO);
                Logger::getInstance()->logify($this,Bundle::$DELKO);
            }
        }
        else
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
        }
        return($bundle);
    }
    public function supprimerPhysTypeAbonnement($bundle)
    {
        $typeAbonnement=$bundle->get(Bundle::$TYPEABONNEMENT);
        if(!is_null($typeAbonnement) and is_object($typeAbonnement))
        {
            $retour=$this->typeAbonnementDao->supprimerPhysTypeAbonnement($typeAbonnement);
            if($retour!=false)
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                $bundle->put(Bundle::$MESSAGE,Bundle::$DELOK);
            }
            else
            {
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$DELKO);
                Logger::getInstance()->logify($this,Bundle::$DELKO);
            }
        }
        else
        {
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
            Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
        }
        return($bundle);
    }
    public function modifierTypeAbonnement($bundle)
    {
        $message="";
        $op = false;
        //On récupére le typeAbonnement qui se trouve dans le bundle
        $typeAbonnement=$bundle->get(Bundle::$TYPEABONNEMENT);
        //On le vérifie
        if(!is_null($typeAbonnement) and is_object($typeAbonnement))
        {
            //On lance la vérification des champs de l'objet comme lors de l'ajout.
            $verif = self::verifTypeAbonnement($typeAbonnement, "La modification");

            if($verif=="") //Si $verif == "" --> Ce qui veut dire qu'aucun message d'erreur n'a été enregistré
            {
                $retour=$this->typeAbonnementDao->modifierTypeAbonnement($typeAbonnement);
                if($retour!=false)
                {
                    $op =true;
                    $message = Bundle::$MODOK;
                }
                else
                {
                    $op =false;
                    $message = Bundle::$MODKO;
                    Logger::getInstance()->logify($this,Bundle::$MODKO);
                }
            }
            else //Sinon si $verif != "", ce qui veut dire qu'on a une erreur. On l'enregistre.
                $message = $verif;
        }
        else
        {
            $op = false;
            $message = Bundle::$NOTEXIST;
            Logger::getInstance()->logify($this, Bundle::$NOTEXIST);
        }

        $bundle->put(Bundle::$OPERATION_REUSSIE,$op);
        $bundle->put(Bundle::$MESSAGE,$message);

        return($bundle);
    }
    public function listerTypesAbonnement($bundle)
    {
	 	$listeOk = true;
		$message = "";
		$myListe = null;
        $myListe=$this->typeAbonnementDao->listerTypesAbonnement();
		if (empty($myListe)){
			$message = "Liste vide.";
			$listeOk = false;
		}
		else if (count($myListe) == 1)
			$message = "Il y a 1 type d'abonnement.";
		else
			$message = "Il y a ".count($myListe)." types d'abonnements.";
		$bundle->put(Bundle::$OPERATION_REUSSIE, $listeOk);
		$bundle->put(Bundle::$MESSAGE, $message);
            $bundle->put(Bundle::$LISTE,$myListe);

        return($bundle);
    }

    public function listerTypesAbonnementValides($bundle)
    {
        $listeOk = true;
        $message = "";
        $myListe = null;
        $myListe=$this->typeAbonnementDao->listerTypesAbonnementValides();
        if (empty($myListe)){
            $message = "Liste vide.";
            $listeOk = false;
        }
        else if (count($myListe) == 1)
            $message = "Il y a 1 type d'abonnement.";
        else
            $message = "Il y a ".count($myListe)." types d'abonnements.";
        $bundle->put(Bundle::$OPERATION_REUSSIE, $listeOk);
        $bundle->put(Bundle::$MESSAGE, $message);
            $bundle->put(Bundle::$LISTE,$myListe);

        return($bundle);
    }

    /* ---------------------------------------------------------------------------------------------------
    *   Différentes fonctions de vérification des différents champs de l'abonnement 
       ---------------------------------------------------------------------------------------------------*/

    //  LIBELLE
    public function libelleOk($libelle){
        return (strlen($libelle)<=30);
    }

    //PRIX
    public function prixOk($prix)
    {
        return(filter_var($prix, FILTER_VALIDATE_FLOAT));
    }

    //DUREE
    public function dureeOk($duree)
    {
        return($duree > -2);
    }

    //NBUNITE
    public function nbUniteOk($nb)
    {
        /* Le NbUnite doit etre égal à -1 ou >(STRICTEMENT) à 0 pour etre correct */
        if($nb == -1 || $nb > 0)
            return true;
        else
            return false;
    }

    //SUPPRIME
    public function supprimeOk($supprime)
    {
        $check = false;
        if($supprime == "F" || $supprime == "T")
            $check = true;

        return($check);
    }


    /*  ----------------------------------------------------------------------------------------
    *   Fonction de vérification pour TOUS les champs du type d'abonnement avant un ajout
        ----------------------------------------------------------------------------------------*/

    private function verifTypeAbonnement($typeAbonnement, $fonction)
    {
        $message="";

        /*
        *   Vérification si les champs sont remplis
        */

        if ($typeAbonnement->getLibelle()==null && $typeAbonnement->getLibelle()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque le libelle du type d'abonnement";

        else if ($typeAbonnement->getPrix()==null && $typeAbonnement->getPrix()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque le prix du type d'abonnement";

        else if ($typeAbonnement->getDuree()==null && $typeAbonnement->getDuree()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque la durée du type d'abonnement";

        /* Pour le nombre d'unité, il doit etre égal soit à :
        *   -1 : Si le type d'abonnement n'a pas de nombre d'unité
        *   !0 ou !null : Si le type d'abonnement a un nombre d'unité
        */
        else if ($typeAbonnement->getNbUnite()==null && $typeAbonnement->getNbUnite()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque le nombre d'unité du type d'abonnement";

        /* Aucune vérification sur les cours, ce champ dépend de tout. Il n'est pas obligatoire */

        else if ($typeAbonnement->getSupprime()==null && $typeAbonnement->getSupprime()=="")
            $message = $fonction." n'a pas pu être réalisé. Il manque le champ de suppression du type d'abonnement";

        /*
        *   Vérification si les champs semblent corrects ...
        */

        else if ($typeAbonnement->getLibelle()==!null && !self::libelleOk($typeAbonnement->getLibelle()))
            $message = $fonction." n'a pas pu être réalisé. Le libelle du type d'abonnement est incorrect";

        else if ($typeAbonnement->getPrix()==!null && !self::prixOk($typeAbonnement->getPrix()))
            $message = $fonction." n'a pas pu être réalisé. Le prix du type d'abonnement est incorrect";

        else if (!is_null($typeAbonnement->getDuree()) && !self::dureeOk($typeAbonnement->getDuree()))
            $message = $fonction." n'a pas pu être réalisé. La durée du type d'abonnement est incorrecte";

        /* nbUniteOk --> check si la valeur est ok, à regarder la fonction plus haut */
        else if ($typeAbonnement->getNbUnite()==!null && !self::nbUniteOk($typeAbonnement->getNbUnite()))
            $message = $fonction." n'a pas pu être réalisé. Le nombre d'unité du type abonnement est incorrect";

        else if ($typeAbonnement->getSupprime()==!null && !self::supprimeOk($typeAbonnement->getSupprime()))
            $message = $fonction." n'a pas pu être réalisé. Le champ supprimé du type d'abonnement est différent de F ou T";

        return $message;
    }

}


?>