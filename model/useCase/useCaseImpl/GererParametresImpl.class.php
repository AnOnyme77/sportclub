<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

class GererParametresImpl implements GererParametres{
    private $parametreDao;
    
    //constructeur
    public function GererParametresImpl(){
        $this->parametreDao=DaoFactory::getInstance()->get("ParametresDao");
    }
    
    //Fonction de récupération des paramètres
    public function getParametres(){
        $retour=false;
        $parametres=$this->parametreDao->getParametres();
        if($parametres!=false){
            if(session_status()!=PHP_SESSION_ACTIVE) session_start();
            $_SESSION["oParametres"]=serialize($parametres);
            $retour=true;
        }
        return($retour);
    }
    
    //Fonction de modification des paramètres
    public function modifierParametres($bundle){
        $parametre=$bundle->get(Bundle::$PARAMETRE);
        //On contact le DAO
        $retour=$this->parametreDao->modifierParametres($parametre);
        
        if($retour==true){
            $bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$MESSAGE,Bundle::$MODOK);
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$MODKO);
        }
        
        return($bundle);
    }
}

?>