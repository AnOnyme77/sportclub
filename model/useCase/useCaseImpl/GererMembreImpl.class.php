<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jérôme Hainaut
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
class GererMembreImpl implements GererMembre{
	private static $FONCTION='/^[MAP]$/';
	private static $CODEEAN13='/^[0-9]{13}$/';
 	private static $SEX = '/^[MF]$/';
 	private static $TEL = '/^[0-9]{3,25}$/'; 
 	private static $CP = '/^[0-9]{2,10}$/';
 	private static $EMAIL = '/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/';
 	private static $DATEFORMATFRANCAIS = '/^([0-3][0-9]})(/)([0-9]{2,2})(/)([0-3]{2,2})$/';
 	private static $STATUT = '/^[FT]$/';
 	private static $SUPPRIMER = '/^[FT]$/';
 	
	private $membreDao;
	
	function GererMembreImpl(){
		$this->membreDao = DaoFactory::getInstance()->get("MembreDao");
	}
	
	public function rechercherNomPrenom($bundle){
		$membre=$bundle->get(Bundle::$MEMBRE);
		$retour=false;
		$liste=$this->membreDao->rechercherNomPrenom($membre);
		if(is_array($liste)){
			$bundle->put(Bundle::$OPERATION_REUSSIE,true);
			$bundle->put(Bundle::$MESSAGE,Bundle::$SEARCHOK);
			$bundle->put(Bundle::$LISTE,$liste);
		}
		else{
			$bundle->put(Bundle::$OPERATION_REUSSIE,false);
			$bundle->put(Bundle::$MESSAGE,Bundle::$SEARCHKO);
		}
	}
	
 	public function ajouterMembre($bundle){
 		$ajoutReussi=false;
 		$message="";
 		$membre= $bundle->get(Bundle::$MEMBRE);
	 	if (get_class($membre) != "Membre") {
			$message = Bundle::$NOTEXIST;
			
		} else{
		
		// les champs qui on une valeur par défaut
			if(null==$membre->getIdMembre() || $membre->getIdMembre()==""){
				$membre->setIdMembre($this->geneIdMembre());
			}
			if(null==$membre->getFonction() || $membre->getFonction()==""){
				$membre->setFonction("M");
			}
			if(null==$membre->getStatut() || $membre->getStatut()==""){
				$membre->setStatut("F");
			}
			if(null==$membre->getSupprime() || $membre->getSupprime()==""){
				$membre->setSupprime("F");
			}
			if(null==$membre->getDateInscription()){
				$membre->setDateInscription("NOW");
			}
			if(null==$membre->getLogo()){
				$membre->setLogo(Membre::$LOGO);
			}
			if($membre->getNameSection() == "") $membre->setNameSection(null);
			if($membre->getNiveau() == "") $membre->setNiveau(null);
		// les différents champs obligatoires ici.
		
			if (null==$membre->getIdMembre() || $membre->getIdMembre()==""){
				$message = "L'ajout n'a pas pu être réalisé. valeur non autorisée pour le code du membre";
				Logger::getInstance()->logify($this,"(ajout) Il manque l'identifiant du membre");
			}else if (null==$membre->getNom() || $membre->getNom()==""){
				$message = "L'ajout n'a pas pu être réalisé. Il manque le nom du membre";
			} else if (null==$membre->getPrenom()|| $membre->getPrenom()=="") {
				$message = "L'ajout n'a pas pu être réalisé. Il manque le prénom du membre";
			} else if (null==$membre->getSexe() || $membre->getSexe()=="") {
				$message = "L'ajout n'a pas pu être réalisé. Il manque le sexe du membre";
			} else if (null==$membre->getDateNaissance()) {
				$message = "L'ajout n'a pas pu être réalisé. Il manque la date de naissance du membre";
			} else if (null==$membre->getAdresse()) {
				$message = "L'ajout n'a pas pu être réalisé. Il manque l'adresse";
		 	}else if (null==$membre->getAdresse()->getRue() || $membre->getAdresse()->getRue()=="") {
				$message = "L'ajout n'a pas pu être réalisé. Il manque la rue";
		 	}else if (null==$membre->getAdresse()->getVille() || $membre->getAdresse()->getVille()=="") {
				$message = "L'ajout n'a pas pu être réalisé. Il manque la ville";
			}else if (null==$membre->getAdresse()->getCodePostale() || $membre->getAdresse()->getRue()=="") {
				$message = "L'ajout n'a pas pu être réalisé. Il manque le code postal";
			} else if (null==$membre->getEmail() || $membre->getEmail()=="") {
				$message = "L'ajout n'a pas pu être réalisé. Il manque l'email du membre";
			} else if (null==$membre->getMdp() || $membre->getMdp()=="") {
				$message = "L'ajout n'a pas pu être réalisé. Il manque le mot de passe du membre";
			
	// si il y a des erreurs dans l'un des champs
			} else if (!self::idMembreOk($membre->getIdMembre())) {
				$message = "L'ajout n'a pas pu être réalisé. code d'identification du membre incorrect";
				Logger::getInstance()->logify($this,"(ajout) Code d'identification du membre incorrect : ".$membre->getIdMembre());
			} else if (!self::fonctionOk($membre->getFonction())) {
				$message = "L'ajout n'a pas pu être réalisé. valeur non autorisée pour la fonction du membre";
			}else if (!self::nomOk($membre->getNom())) {
				$message = "L'ajout n'a pas pu être réalisé. trop de caractères pour le nom du membre";
			}else if (!self::prenomOk($membre->getPrenom())) {
				$message = "L'ajout n'a pas pu être réalisé. trop de caractères pour le prénom du membre";
			}else if (!self::sexOk($membre->getSexe())) {
				$message = "L'ajout n'a pas pu être réalisé. mauvais code pour le sexe du membre";
			}else if (!self::telOk($membre->getTelFixe())) {
				$message = "L'ajout n'a pas pu être réalisé. mauvais numéro de téléphone fixe";
			}else if (!self::telOk($membre->getTelGsm())) {
				$message = "L'ajout n'a pas pu être réalisé. mauvais numéro de GSM";
			}else if (!self::rueOk($membre->getAdresse()->getRue())) {
				$message = "L'ajout n'a pas pu être réalisé. trop de caractères pour la rue du membre";
			}else if (!self::villeOk($membre->getAdresse()->getVille())) {
				$message = "L'ajout n'a pas pu être réalisé. trop de caractères pour la ville du membre";
			}else if (!self::paysOk($membre->getAdresse()->getPays())) {
				$message = "L'ajout n'a pas pu être réalisé. trop de caractères pour le pays du membre";
			}else if (!self::codePostalOk($membre->getAdresse()->getCodePostale())) {
				$message = "L'ajout n'a pas pu être réalisé. mauvais code postal";
			}else if (!self::statutOk($membre->getStatut())) {
				$message = "L'ajout n'a pas pu être réalisé. statut incorrect";
			}else if (!self::emailOk($membre->getEmail())) {
				$message = "L'ajout n'a pas pu être réalisé. email incorrect";
			}else if (!self::logoOk($membre->getLogo())) {
				$message = "L'ajout n'a pas pu être réalisé. chemin incorrect";
			}else if (!self::supprimerOk($membre->getSupprime())) {
				$message = "L'ajout n'a pas pu être réalisé. champs supprimer incorrect";
			}else {
				if ($this->membreDao->getMembre($membre->getIdMembre()) != null ) {
					$message = Bundle::$EXIST; //ne doit normalement jamais passée ici
				} else {
					$membreRef = $this->membreDao->getMembreEmail($membre->getEmail());
					if($membreRef != null){
						if($membreRef->getSupprime() == "F"){
							$message = "L'ajout n'a pas pu être réalisé. Cet Email est déjà utiliser.";
						}
						else{
							$membreRef->setEmail($membreRef->getIdMembre()."mailModifier.mod");
							$ajoutReussi = $this->membreDao->modifierMembre($membreRef);
							if ($ajoutReussi) {
								$ajoutReussi = $this->membreDao->ajouterMembre($membre);
								if ($ajoutReussi) {
									$message = Bundle::$ADDOK;
								} else {
									$message = Bundle::$ADDKO;
								}
							} else {
								$message = Bundle::$ADDKO;
							}
						}
					}
					else{
						$ajoutReussi = $this->membreDao->ajouterMembre($membre);
						if ($ajoutReussi) {
							$message = "Ajout réalisé avec succès";
						} else {
							$message = "L'ajout n'a pas pu être réalisé";
						}	
					}
				}
			}
		}
		$bundle->put(Bundle::$OPERATION_REUSSIE, $ajoutReussi);
		$bundle->put(Bundle::$MEMBRE, $membre);
		$bundle->put(Bundle::$MESSAGE, $message); 		
 	}
	 	
	 	
 	public function rechercherMembreID($bundle){  // rechercher un membre
	 	$operationReussie = false;
		$idMembre = $bundle->get(Bundle::$IDMEMBRE);
		$membre = $this->membreDao->getMembre($idMembre);
		$message = "";
		if (empty($membre) || $membre->getSupprime()=="T") {
			$message = Bundle::$NOTEXIST;
		} else {
			$operationReussie = true;
		}
		$bundle->put(Bundle::$IDMEMBRE, $idMembre);
		$bundle->put(Bundle::$OPERATION_REUSSIE, $operationReussie);
		$bundle->put(Bundle::$MEMBRE, $membre);
		$bundle->put(Bundle::$MESSAGE, $message);
 	}
 	
 	public function rechercherMembreEmail($bundle){  // rechercher un membre
	 	$operationReussie = false;
		$email = $bundle->get(Bundle::$EMAIL);
		$membre = $this->membreDao->getMembreEmail($email);
		$message = "";
		if (empty($membre) || $membre->getSupprime()=="T") {
			$message = Bundle::$NOTEXIST;
		} else {
			$operationReussie = true;
			$message = "Membre récupéré.";
		}
		$bundle->put(Bundle::$EMAIL, $email);
		$bundle->put(Bundle::$OPERATION_REUSSIE, $operationReussie);
		$bundle->put(Bundle::$MEMBRE, $membre);
		$bundle->put(Bundle::$MESSAGE, $message);
 	}
 	
 	public function listerMembre($bundle){
 		
	 	$listeOk = true;
		$message = "";
		$listeMembre = null;
		$listeMembre = $this->membreDao->listerMembre();
		if (empty($listeMembre)){
			$message = "Liste vide.";
			$listeOk = false;
		}
		else if (count($listeMembre) == 1)
			$message = "Il y a 1 membre.";
		else
			$message = "Il y a ".count($listeMembre)." membres.";
		$bundle->put(Bundle::$OPERATION_REUSSIE, $listeOk);
		$bundle->put(Bundle::$MESSAGE, $message);
		$bundle->put(Bundle::$LISTE, $listeMembre);
 	} 	
 	
 	public function listerMembreNonBan($bundle){
 		
	 	$listeOk = true;
		$message = "";
		$listeMembre = null;
		$listeMembre = $this->membreDao->listerMembreNonBan();
		if (empty($listeMembre)){
			$message = "Liste vide.";
			$listeOk = false;
		}
		else if (count($listeMembre) == 1)
			$message = "Il y a 1 membre.";
		else
			$message = "Il y a ".count($listeMembre)." membres.";
		$bundle->put(Bundle::$OPERATION_REUSSIE, $listeOk);
		$bundle->put(Bundle::$MESSAGE, $message);
		$bundle->put(Bundle::$LISTE, $listeMembre);
 	} 	
 	
 	public function supprimerMembre($bundle){
	 	$operationReussie = false;
		$idMembre = $bundle->get(Bundle::$IDMEMBRE);
		$membre = $this->membreDao->getMembre($idMembre);
		$message = "";
		if (empty($membre) || $membre->getSupprime()=="T") {
			$message = Bundle::$NOTEXIST;
		} else {
			$operationReussie = $this->membreDao->supprimerMembre($idMembre);
			if ($operationReussie) {
				$message = Bundle::$DELOK;
			} else {
				$message = Bundle::$DELKO;
			}
		}
		
		$bundle->put(Bundle::$OPERATION_REUSSIE, $operationReussie);
		$bundle->put(Bundle::$MESSAGE, $message);
 		
 	}
 	
 	
 	public function modifierMembre($bundle){
 		$modifReussi=false;
 		$message="";
 		$membre= $bundle->get(Bundle::$MEMBRE);
	 	if (get_class($membre) != "Membre") {
			$message = Bundle::$NOTEXIST;
			
//les différents champs obligatoires ici.
		} else{
			if (null==$membre->getIdMembre() || $membre->getIdMembre()==""){
				$message = "La modification n'a pas pu être réalisée. Ce membre n'existe pas";
				Logger::getInstance()->logify($this,"(modification) Il manque l'identifiant du membre");
			}else if (null==$membre->getNom() || $membre->getNom()==""){
				$message = "La modification n'a pas pu être réalisée. Il manque le nom du membre";
			} else if (null==$membre->getPrenom()|| $membre->getPrenom()=="") {
				$message = "La modification n'a pas pu être réalisée. Il manque le prénom du membre";
			} else if (null==$membre->getSexe() || $membre->getSexe()=="") {
				$message = "La modification n'a pas pu être réalisée. Il manque le sexe du membre";
			} else if (null==$membre->getDateNaissance()) {
				$message = "La modification n'a pas pu être réalisée. Il manque la date de naissance du membre";
			} else if (null==$membre->getDateInscription()) {
				$message = "La modification n'a pas pu être réalisée. Il manque la date d'inscription du membre";
			} else if (null==$membre->getAdresse()) {
				$message = "La modification n'a pas pu être réalisée. Il manque l'adresse";
		 	}else if (null==$membre->getAdresse()->getRue() || $membre->getAdresse()->getRue()=="") {
				$message = "La modification n'a pas pu être réalisée. Il manque la rue";
		 	}else if (null==$membre->getAdresse()->getVille() || $membre->getAdresse()->getVille()=="") {
				$message = "La modification n'a pas pu être réalisée. Il manque la ville";
			}else if (null==$membre->getAdresse()->getCodePostale() || $membre->getAdresse()->getRue()=="") {
				$message = "La modification n'a pas pu être réalisée. Il manque le code postal";
			} else if (null==$membre->getEmail() || $membre->getEmail()=="") {
				$message = "La modification n'a pas pu être réalisée. Il manque l'email du membre";
			} else if (null==$membre->getMdp() || $membre->getMdp()=="") {
				$message = "La modification n'a pas pu être réalisée. Il manque le mot de passe du membre";
			
	// si il y a des erreurs dans l'un des champs
			} else if (!self::idMembreOk($membre->getIdMembre())) {
				$message = "La modification n'a pas pu être réalisée. Ce membre n'existe pas";
				Logger::getInstance()->logify($this,"(modification) Code d'identification du membre incorrect : ".$membre->getIdMembre());
			} else if (!self::fonctionOk($membre->getFonction())) {
				$message = "La modification n'a pas pu être réalisée. valeur non autorisée pour la fonction du membre";
			}else if (!self::nomOk($membre->getNom())) {
				$message = "La modification n'a pas pu être réalisée. trop de caractère pour le nom du membre";
			}else if (!self::prenomOk($membre->getPrenom())) {
				$message = "La modification n'a pas pu être réalisée. trop de caractère pour le prénom du membre";
			}else if (!self::sexOk($membre->getSexe())) {
				$message = "La modification n'a pas pu être réalisée. mauvais code pour le sexe du membre";
			}else if (!self::telOk($membre->getTelFixe())) {
				$message = "La modification n'a pas pu être réalisée. mauvais numéro de téléphone fixe";
			}else if (!self::telOk($membre->getTelGsm())) {
				$message = "La modification n'a pas pu être réalisée. mauvais numéro de GSM";
			}else if (!self::rueOk($membre->getAdresse()->getRue())) {
				$message = "La modification n'a pas pu être réalisée. trop de caractère pour la rue du membre";
			}else if (!self::villeOk($membre->getAdresse()->getVille())) {
				$message = "La modification n'a pas pu être réalisée. trop de caractère pour la ville du membre";
			}else if (!self::paysOk($membre->getAdresse()->getPays())) {
				$message = "La modification n'a pas pu être réalisée. trop de caractère pour le pays du membre";
			}else if (!self::codePostalOk($membre->getAdresse()->getCodePostale())) {
				$message = "La modification n'a pas pu être réalisée. mauvais code postal";
			}else if (!self::statutOk($membre->getStatut())) {
				$message = "La modification n'a pas pu être réalisée. statut incorrect";
			}else if (!self::emailOk($membre->getEmail())) {
				$message = "La modification n'a pas pu être réalisée. email incorrect";
			}else if (!self::logoOk($membre->getLogo())) {
				$message = "La modification n'a pas pu être réalisée. chemin incorrect";
			}else{
				$membreDb = $this->membreDao->getMembre($membre->getIdMembre());
				if ( $membreDb == null || $membreDb->getSupprime()=="T") {
					$message = Bundle::$NOTEXIST; 	
				} else{
					$membreEmail = $this->membreDao->getMembreEmail($membre->getEmail());
					if($membreEmail!= null && $membreDb->getEmail() != $membre->getEmail()){
						if($membreEmail->getSupprime()=="F"){
							$message = "La modification n'a pas pu être réalisée. Cet Email est déjà utilisé.";
						}
						else{
							$membreEmail->setEmail($membreEmail->getIdMembre()."mailModifier.mod");
							$modifReussi = $this->membreDao->modifierMembre($membreEmail);
							if ($modifReussi) {
								$modifReussi = $this->membreDao->modifierMembre($membre);
								if ($modifReussi) {
									$message = Bundle::$MODOK;
								} else {
									$message = Bundle::$MODKO;
								}
							} else {
								$message = Bundle::$MODKO;
							}
						}
					}
					else{
						$modifReussi = $this->membreDao->modifierMembre($membre);
						if ($modifReussi) {
							$message = Bundle::$MODOK;
						} else {
							$message = Bundle::$MODKO;
						}
					}
				}
			}
		} 
			
		$bundle->put(Bundle::$OPERATION_REUSSIE, $modifReussi);
		$bundle->put(Bundle::$MESSAGE, $message); 	
 		
 	}
 	
 	/* crée un nouvelle idMembre */
 	public function geneIdMembre(){
 		$codeEAN13 = "";
 		$membre = $this->membreDao->getLastMembre();
 		if($membre == null){
 			$codeEAN13.="040000000000";
 			$codeEAN13.=self::codeValidEAN13($codeEAN13);
 		}
 		else if($membre->getIdMembre() < "0500000000000"){
 			$codeEAN13.= "0".(substr ( $membre->getIdMembre() , 0 , 12 )+1);
 			$codeEAN13.=self::codeValidEAN13($codeEAN13);
 		}
 		else{
 			$codeEAN13 = "erreur";
 		}
 		return $codeEAN13;
 	}
 	public static function codeValidEAN13($code){
 		$digits = $code;
		// les positions: 2, 4, 6, etc.
		$posPaire = $digits[1] + $digits[3] + $digits[5] + $digits[7] + $digits[9] + $digits[11];
		
		// les positions: 1, 3, 5, etc.
		$posImpaire = $digits[0] + $digits[2] + $digits[4] + $digits[6] + $digits[8] + $digits[10];

		$tot = $posPaire * 3 + $posImpaire;

		return (ceil($tot/10))*10 - $tot;
 	}
 	
 	/* vérification des différents champs du membre */

	public static function idMembreOk($code){ // vérifie que l'idMembre est correct
		return (preg_match(self::$CODEEAN13,$code) && self::codeValidEAN13($code) == $code[12]);
	}
	
	public static function fonctionOk($fct){
		return (preg_match(self::$FONCTION,$fct));
	}
	public static function nomOk($nom){
		return (strlen($nom)<=30);
	}
	public static function prenomOk($prenom){
		return (strlen($prenom)<=30);
	}
	public static function sexOk($sexe){
		return(preg_match(self::$SEX, $sexe));
	}
	public static function telOk($tel){
		return($tel==null || $tel =="" || preg_match(self::$TEL, $tel) );
	}
	public static function emailOk($email){
		return  (preg_match ( self::$EMAIL , $email ));
	}
	public static function codePostalOk($cp){
		return ( preg_match (  self::$CP, $cp ) );
	}
	public static function rueOk($rue){
		return (strlen($rue)<=50);
	}
	public static function villeOk($ville){
		return (strlen($ville)<=30);
	}
	public static function paysOk($pays){
		return (null==$pays || $pays=="" || strlen($pays)<=30);
	}
	public static function statutOk($statut){
		return (preg_match(self::$STATUT, $statut));
	}
	public static function logoOk($logo){
		return (null==$logo || $logo=="" || strlen($logo)<=50);
	}
	public static function supprimerOk($sup){
		return preg_match(self::$SUPPRIMER, $sup);
	}
}