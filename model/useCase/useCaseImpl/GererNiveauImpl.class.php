<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
    class GererNiveauImpl implements GererNiveau{
        
        private $dao;
        
        public function GererNiveauImpl(){
            $this->dao=DaoFactory::getInstance()->get("NiveauDao");
        }
        
        public function getNiveau($niveau,$nameSection,$bundle){
            $niveauObject = $this->dao->getNiveau($niveau,$nameSection);
            if($niveauObject!=false){
                if($niveauObject->getSupprime()=="F"){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
                    $bundle->put(Bundle::$NIVEAU,$niveauObject);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
                    Logger::getInstance()->logify($this,Bundle::$GETKO);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
                Logger::getInstance()->logify($this,Bundle::$GETKO);
            }
            return($bundle);
        }
        
        public function modifierNiveau($bundle){
            $niveau=$bundle->get(Bundle::$NIVEAU);
            if($this->dao->getNiveau($niveau->getNiveau(),$niveau->getNameSection())!=false){
                if($this->dao->modifierNiveau($niveau)){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$MODOK);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$MODKO);
                    Logger::getInstance()->logify($this,Bundle::$MODKO);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
                Logger::getInstance()->logify($this,Bundle::$GETKO);
            }
            return($bundle);
        }
        
        public function ajouterNiveau($bundle){
            $niveau=$bundle->get(Bundle::$NIVEAU);
            $returnedNiveau=$this->dao->getNiveau($niveau->getNiveau(),$niveau->getNameSection());
            if(is_bool($returnedNiveau)
               && $returnedNiveau==false){
                if($this->dao->ajouterNiveau($niveau)){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$ADDOK);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$ADDKO);
                    Logger::getInstance()->logify($this,Bundle::$ADDKO);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$EXIST);
                Logger::getInstance()->logify($this,Bundle::$EXIST);
            }
            return($bundle);
        }
        
        public function supprimerNiveau($bundle){
            $niveau=$bundle->get(Bundle::$NIVEAU);
            $niveau->setSupprime("V");
            if($this->dao->getNiveau($niveau->getNiveau(),$niveau->getNameSection())!=false){
                if($this->dao->modifierNiveau($niveau)){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$DELOK);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$DELKO);
                    Logger::getInstance()->logify($this,Bundle::$DELKO);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
                Logger::getInstance()->logify($this,Bundle::$GETKO);
            }
            return($bundle);
        }
        
        public function listerNiveau($bundle){
            $liste=$this->dao->listerNiveau();
            if(count($liste)>=0){
                if(count($liste)==0){
                    $bundle->put(Bundle::$LISTE,$liste);
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,"Liste vide.");
                }
                elseif(count($liste)==1){
                    $libNiveau = explode(" ",strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau()));
                    $bundle->put(Bundle::$LISTE,$liste);
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,"Il y a 1 ".$libNiveau[1].".");
                }
                else{
                    $bundle->put(Bundle::$LISTE,$liste);
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,"Il y a ".count($liste)." ".strtolower(unserialize($_SESSION["oParametres"])->getLibNiveaux()).".");
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
                Logger::getInstance()->logify($this,Bundle::$LISTKO);
            }
            return($bundle);
        }
    }
?>