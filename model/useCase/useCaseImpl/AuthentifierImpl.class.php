<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frederic
 * Team : Dev4u
 * cree le ??/??/2014 - modifee le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

class AuthentifierImpl{
    //REGEX
    private static $CODEEAN13='/^[0-9]{13}$/';
    private static $EMAIL = '/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/';

    //Mon membreDao
    private $membreDao;
    
    //Constructeur
    public function AuthentifierImpl()
    {
        $this->membreDao = DaoFactory::getInstance()->get("MembreDao");
    }
    
    //Ma fonction d'authentification (retourne le bundle, test -> facultatif, juste pour les tests)
    public function authentifier($bundle, $test=false)
    {
        $message="";
        $membre=null;
        $authentifier = $bundle->get(Bundle::$AUTHENTIFIER);
        $bundle->put(Bundle::$OPERATION_REUSSIE,false);
        //Verifier si la classe de l'objet authentifier est bien Authentifier.
        if(get_class($authentifier) != 'Authentifier')
        {
            $message = "L'authentification n'a pas pu etre effectuee (Authentification erronnee).";
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
        }
        else
        {
            //On verifie si l'email est bien un e-mail valable et si le mdp est pas vide
            //if(ereg(self::$EMAIL,$authentifier->getEmail()) && $authentifier->getMdp() != "")
            if($authentifier->getMdp() != "")
            {
                //echo(unserialize($_SESSION["oParametres"])->getSuperLogin());
                if(sha1($authentifier->getEmail())==unserialize($_SESSION["oParametres"])->getSuperLogin() and
                   sha1($authentifier->getMdp())==unserialize($_SESSION["oParametres"])->getSuperMdp()){
                    $membre = new Membre();
                    $membre->setAdresse(new Adresse("Street Root","1000","Bruxelles","Belgique"));
                    $membre->setDateInscription(new DateTime("2011-03-05 06:06:06"));
                    $membre->setEmail("dev4u@gmail.com");
                    $membre->setNom("Root");
                    $membre->setPrenom("root");
                    $membre->setSexe("M");
                    $membre->setDateNaissance(new DateTime("1993-05-15"));
                    $membre->setFonction("A");
                    $membre->setMdp("jesuisroot");
                    $membre->setLogo("chemin/isanzo.jpg");
                    $membre->setSupprime("F");
                    $membre->setStatut("F");
                }
                else{
                    //Si ok, on authentifie, si a Žchoue, checkLogin enverra un message d'erreur et un membre null.
                    $membre = $this->membreDao->checkLogin($authentifier);   
                }
                //On vŽrifie si le membre n'est pas null.
                if($membre != null)
                {
                    //Verification des differents criteres pour la connexion :
                    //Statut doit etre e True et Supprime Ã  False.
                    if($membre->getStatut() == "F" && $membre->getSupprime() == "F")
                    {
                        //Si tout est ok, on passe le membre dans les variables de session pour la connexion.
                        if(empty($_SESSION))
                            session_start();
                        $_SESSION['oMembre'] = serialize($membre);
                        if($test)
                            $message = "Connexion OK";
                        else
                            $message = "ok";
                            
                        $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    }
                    else
                    {
                        $message = Bundle::$CONKO;
                        $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    }
                }
                else
                {
                    $message = Bundle::$CONKO;
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                }
                    
            }
            else
            {
                $message = Bundle::$CONKO;
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            }
        }
        
        $bundle->put(Bundle::$MESSAGE,$message);
        return $bundle;
    }
}



?>
