<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

class GererInscriptionImpl implements GererInscription{
    
    private $daoInscription;
    private $daoMembre;
    private $daoSeance;
    
    public function GererInscriptionImpl(){
        $this->daoInscription=DaoFactory::getInstance()->get("InscriptionDao");
        $this->daoMembre=DaoFactory::getInstance()->get("MembreDao");
        $this->daoSeance=DaoFactory::getInstance()->get("SeanceDao");
    }
    
    public function getNombre($bundle){
        $seance = $bundle->get(Bundle::$SEANCE);
        if(!is_null($seance)){
            if($this->daoSeance->getSeance($seance)!=false){
                $nombre=$this->daoInscription->getNombre($seance);
                if(is_numeric($nombre)){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
                    $bundle->put(Bundle::$ENTIER,$nombre);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$ERROR);
                    Logger::getInstance()->logify($this,Bundle::$ERROR);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
            Logger::getInstance()->logify($this,Bundle::$GETKO);
        }
    }
    
    public function getFromSeance($bundle){
        $seance = $bundle->get(Bundle::$SEANCE);
        if(!is_null($seance)){
            if($this->daoSeance->getSeance($seance)!=false){
                $liste=$this->daoInscription->getFromSeance($seance);
                if(is_array($liste)){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
                    $bundle->put(Bundle::$LISTE,$liste);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$ERROR);
                    Logger::getInstance()->logify($this,Bundle::$ERROR);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
            }
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
            Logger::getInstance()->logify($this,Bundle::$GETKO);
        }
    }
    
    public function ajouterInscription($bundle){
        $inscription=$bundle->get(Bundle::$INSCRIPTION);
        if($inscription!=null){
            
            $membre= new Membre();
            $membre->setIdMembre($inscription->getIdMembre());
            
            $seance= new Seance();
            $seance->setNoSeance($inscription->getNoSeance());
            
            if($this->daoMembre->getMembre($membre->getIdMembre())!=null and
               $this->daoSeance->getSeance($seance)!=false){
                if($this->daoInscription->getInscription($inscription)==false){
                    if($this->daoInscription->ajouterInscription($inscription)){
                        $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                        $bundle->put(Bundle::$MESSAGE,Bundle::$ADDOK);
                    }
                    else{
                        $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                        $bundle->put(Bundle::$MESSAGE,Bundle::$ADDKO);
                        Logger::getInstance()->logify($this,Bundle::$ADDKO);
                    }
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,"Vous êtes déjà inscrit à ce cours");
                    Logger::getInstance()->logify($this,"Vous êtes déjà inscrit à ce cours");
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
            }
            
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$ADDKO);
            Logger::getInstance()->logify($this,Bundle::$ADDKO);
        }
    }
    public function modifierInscription($bundle){
        $inscription=$bundle->get(Bundle::$INSCRIPTION);
        if($inscription!=null){
            
            $membre= new Membre();
            $membre->setIdMembre($inscription->getIdMembre());
            
            $seance= new Seance();
            $seance->setNoSeance($inscription->getNoSeance());
            
            if($this->daoMembre->getMembre($membre->getIdMembre())!=null and
               $this->daoSeance->getSeance($seance)!=false){
                if($this->daoInscription->getInscription($inscription)!=false){
                    if($this->daoInscription->modifierInscription($inscription)){
                        $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                        $bundle->put(Bundle::$MESSAGE,Bundle::$MODOK);
                    }
                    else{
                        $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                        $bundle->put(Bundle::$MESSAGE,Bundle::$MODKO);
                        Logger::getInstance()->logify($this,Bundle::$MODKO);
                    }
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                    Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
            }
            
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$MODKO);
            Logger::getInstance()->logify($this,Bundle::$MODKO);
        }
    }
    public function supprimerInscription($bundle){
        $inscription=$bundle->get(Bundle::$INSCRIPTION);
        if($inscription!=null){
            
            $membre= new Membre();
            $membre->setIdMembre($inscription->getIdMembre());
            
            $seance= new Seance();
            $seance->setNoSeance($inscription->getNoSeance());
            
            if($this->daoMembre->getMembre($membre->getIdMembre())!=null and
               $this->daoSeance->getSeance($seance)!=false){
                if($this->daoInscription->getInscription($inscription)!=false){
                    if($this->daoInscription->supprimerInscription($inscription)){
                        $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                        $bundle->put(Bundle::$MESSAGE,Bundle::$DELOK);
                    }
                    else{
                        $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                        $bundle->put(Bundle::$MESSAGE,Bundle::$DELKO);
                        Logger::getInstance()->logify($this,Bundle::$DELKO);
                    }
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                    Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
            }
            
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$DELOK);
            Logger::getInstance()->logify($this,Bundle::$DELOK);
        }
    }
    public function marquerPresent($bundle){
        $inscription=$bundle->get(Bundle::$INSCRIPTION);
        if($inscription!=null){
            $inscription->setPresent("T");
            $bundle->put(Bundle::$INSCRIPTION,$inscription);
            
            $this->modifierInscription($bundle);
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,"L'inscription reçue est invalide");
            Logger::getInstance()->logify($this,"L'inscription reçue est invalide");
        }
    }
    public function listerInscriptions($bundle){
        $liste=$this->daoInscription->listerInscription();
        if($liste!=false){
            $bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$LISTE,$liste);
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
            Logger::getInstance()->logify($this,Bundle::$LISTKO);
        }
    }
    
    public function betterListerInscriptions($bundle){
        $liste=$this->daoInscription->betterListerInscription();
        if($liste!=false){
            $listeCompte=array();
            $listeProf=array();
            $listeSeance=array();
            foreach($liste as $ligne){
                $listeCompte[]=$ligne["compte"];
                
                $prof=new Membre();
                $prof->setIdMembre($ligne["idProf"]);
                $prof->setNom($ligne["nom"]);
                $prof->setPrenom($ligne["prenom"]);
                $listeProf[]=$prof;
                
                $seance=new Seance();
                $seance->setIdProf($ligne["idProf"]);
                $seance->setCours($ligne["cours"]);
                $seance->setDateCours($ligne["dateCours"]);
                $seance->setNoSeance($ligne["noSeance"]);
                $seance->setNbMembreMax($ligne["nbMembreMax"]);
                $listeSeance[]=$seance;
            }
            
            $listeRetour=array();
            $listeRetour["prof"]=$listeProf;
            $listeRetour["seance"]=$listeSeance;
            $listeRetour["compte"]=$listeCompte;
            
            $bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$LISTE,$listeRetour);
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
            Logger::getInstance()->logify($this,Bundle::$LISTKO);
        }
    }
    
    public function getPresenceEleve($bundle){
        $inscription=new Inscription();
        $inscription->setIdMembre($bundle->get(Bundle::$IDMEMBRE));
        $liste=$this->daoInscription->getPresenceEleve($inscription);
        if($liste!=false){
            $listeCompte=array();
            $listeProf=array();
            $listeSeance=array();
            $listePresence=array();
            foreach($liste as $ligne){
                $listeCompte[]=$ligne["compte"];
                
                $prof=new Membre();
                $prof->setIdMembre($ligne["idProf"]);
                $prof->setNom($ligne["nom"]);
                $prof->setPrenom($ligne["prenom"]);
                $listeProf[]=$prof;
                
                $seance=new Seance();
                $seance->setIdProf($ligne["idProf"]);
                $seance->setCours($ligne["cours"]);
                $seance->setDateCours($ligne["dateCours"]);
                $seance->setNoSeance($ligne["noSeance"]);
                $seance->setNbMembreMax($ligne["nbMembreMax"]);
                $listeSeance[]=$seance;
                
                if($ligne["present"]==1){
                    $listePresence[]=true;
                }
                else{
                    $listePresence[]=false;
                }
            }
            
            $listeRetour=array();
            $listeRetour["prof"]=$listeProf;
            $listeRetour["seance"]=$listeSeance;
            $listeRetour["compte"]=$listeCompte;
            $listeRetour["presence"]=$listePresence;
            
            $bundle->put(Bundle::$OPERATION_REUSSIE,true);
            $bundle->put(Bundle::$LISTE,$listeRetour);
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$LISTKO);
            Logger::getInstance()->logify($this,Bundle::$LISTKO);
        }
    }
    public function getInscription($bundle){
        $inscription=$bundle->get(Bundle::$INSCRIPTION);
        if($inscription!=null){
            
            $membre= new Membre();
            $membre->setIdMembre($inscription->getIdMembre());
            
            $seance= new Seance();
            $seance->setNoSeance($inscription->getNoSeance());
            
            if($this->daoMembre->getMembre($membre->getIdMembre())!=null and
               $this->daoSeance->getSeance($seance)!=false){
                $retour=$this->daoInscription->getInscription($inscription);
                if($retour!=false){
                    $bundle->put(Bundle::$OPERATION_REUSSIE,true);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$GETOK);
                }
                else{
                    $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                    $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                    Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
                }
            }
            else{
                $bundle->put(Bundle::$OPERATION_REUSSIE,false);
                $bundle->put(Bundle::$MESSAGE,Bundle::$NOTEXIST);
                Logger::getInstance()->logify($this,Bundle::$NOTEXIST);
            }
            
        }
        else{
            $bundle->put(Bundle::$OPERATION_REUSSIE,false);
            $bundle->put(Bundle::$MESSAGE,Bundle::$GETKO);
            Logger::getInstance()->logify($this,Bundle::$GETKO);
        }
    }
}
?>