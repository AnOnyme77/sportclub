<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Hainaut Jérôme
 * Team : Dev4u
 * créé le 25/04/2014 - modifée le 25/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
interface GererExamen{
    public function getExamen($bundle);
    public function modifierExamen($bundle);
    public function ajouterExamen($bundle);
    public function supprimerExamen($bundle);
    public function listerExamen($bundle);
}