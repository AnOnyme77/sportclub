<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
interface GererSeance{
    public function ajouterSeance($bundle);
    public function modifierSeance($bundle);
    public function supprimerSeance($bundle);
    public function listerSeance($bundle);
    public function getSeance($bundle);
}
?>