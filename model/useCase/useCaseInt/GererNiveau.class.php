<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
    interface GererNiveau{
        public function getNiveau($niveau,$namesection,$bundle);
        public function modifierNiveau($bundle);
        public function ajouterNiveau($bundle);
        public function supprimerNiveau($bundle);
        public function listerNiveau($bundle);
    }
?>