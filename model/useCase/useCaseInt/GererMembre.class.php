<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jérôme Hainaut
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

 interface GererMembre{
 	public function ajouterMembre($bundle);
 	public function rechercherMembreID($bundle);
 	public function rechercherMembreEmail($bundle);
 	public function listerMembre($bundle);
 	public function supprimerMembre($bundle);
 	public function modifierMembre($bundle);
 	public function geneIdMembre();
 	public static function codeValidEAN13($code);
 	public static function idMembreOk($code);
	public static function fonctionOk($fct);
	public static function nomOk($nom);
	public static function prenomOk($prenom);
	public static function sexOk($sexe);
	public static function telOk($tel);
	public static function emailOk($email);
	public static function codePostalOk($cp);
	public static function rueOk($rue);
	public static function villeOk($ville);
	public static function paysOk($pays);
	public static function statutOk($statut);
	public static function logoOk($logo);
	public static function supprimerOk($sup);
 }
