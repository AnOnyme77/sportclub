<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
    interface GererCompetence{
        public function ajouterCompetence($bundle);
        public function modifierCompetence($bundle);
        public function supprimerCompetence($bundle);
        public function listerCompetence($bundle);
        public function getCompetence($bundle);
    }
?>