<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

 interface GererAbonnement
 {
 	public function getAbonnement($bundle);
    public function getAbonnementsMembre($bundle);
    public function modifierAbonnement($bundle);
    public function ajouterAbonnement($bundle);
    public function supprimerAbonnement($bundle);
    public function listerAbonnements($bundle);
 }