<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
interface GererInscription{
    public function ajouterInscription($bundle);
    public function modifierInscription($bundle);
    public function supprimerInscription($bundle);
    public function marquerPresent($bundle);
    public function listerInscriptions($bundle);
    public function getInscription($bundle);
}
?>