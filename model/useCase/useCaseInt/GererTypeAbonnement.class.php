<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

 interface GererTypeAbonnement
 {
    public function ajouterTypeAbonnement($bundle);
    public function supprimerTypeAbonnement($bundle);
    public function supprimerPhysTypeAbonnement($bundle);
    public function modifierTypeAbonnement($bundle);
    public function listerTypesAbonnement($bundle);
    public function listerTypesAbonnementValides($bundle);
 }