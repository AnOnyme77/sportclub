<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 29/04/2014 - modifée le 29/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function testAjout($idMembre, $libelleTypeAbo)
{
    $useCase=new GererAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("AbonnementDao");
    $daoTypeAbo = DaoFactory::getInstance()->get("TypeAbonnementDao");

    $typeAbo = getObjectTypeAbonnement();
    $daoTypeAbo->ajouterTypeAbonnement($typeAbo);

    $abo = new Abonnement();
    $abo = getObjectAbonnement($idMembre,$libelleTypeAbo);
    
    $dao->supprimerAbonnement($abo);

    $bundle =  new Bundle();
    $bundle->put(Bundle::$ABONNEMENT,$abo);
    
    $bundle=$useCase->ajouterAbonnement($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE))
    {
        echo("Ajout Abonnement: OK\n");
        $abo2=$dao->getAbonnement($abo->getIdMembre(), $abo->getLibelle(), $abo->getDateAchat());
        
        if($abo2->getIdMembre()!=$abo->getIdMembre())
                echo("ajoutAbonnement idMembre différents\n");
            else
                echo("ajoutAbonnement idMembre: OK\n");

        if($abo2->getLibelle()!=$abo->getLibelle())
                echo("ajoutAbonnement Libelles différents\n");
            else
                echo("ajoutAbonnement Libelles: OK\n");

        if($abo2->getDateDebut()!=$abo->getDateDebut())
                echo("ajoutAbonnement dates de début différentes\n");
            else
                echo("ajoutAbonnement dates de début: OK\n");

        if($abo2->getDateAchat()!=$abo->getDateAchat())
                echo("ajoutAbonnement dates d'achat différentes\n");
            else
                echo("ajoutAbonnement dates d'achat: OK\n");

        if($abo2->getNbUniteRest()!=$abo->getNbUniteRest())
                echo("ajoutAbonnement Nombre d'unités différents\n");
            else
                echo("ajoutAbonnement Nombre d'unités: OK\n");

        if($abo2->getPrix()!=$abo->getPrix())
                echo("ajoutAbonnement Prix différents\n");
            else
                echo("ajoutAbonnement Prix: OK\n");

        if($abo2->getDateFin()!=$abo->getDateFin())
                echo("ajoutAbonnement dates de fin différentes\n");
            else
                echo("ajoutAbonnement dates de fin: OK\n");
    }
    else
        echo("Ajout de l'abonnement échoué \n");

}

function testSupprimer($idMembre,$libelleTypeAbo)
{
    $useCase=new GererAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("AbonnementDao");
    
    $abo = new Abonnement();
    $abo = getObjectAbonnement($idMembre,$libelleTypeAbo);

    $dao->supprimerAbonnement($abo);
    $dao->ajouterAbonnement($abo);
    
    $bundle =  new Bundle();
    $bundle->put(Bundle::$ABONNEMENT,$abo);
    
    $bundle=$useCase->supprimerAbonnement($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE))
        echo("Suppression Abonnement: OK\n");
    else
        echo("Suppression Abonnement échouée");

    //On supprime aussi le typeAbonnement
    $daoTypeAbo = DaoFactory::getInstance()->get("TypeAbonnementDao");
    $typeAbo = getObjectTypeAbonnement();
    $daoTypeAbo->supprimerPhysTypeAbonnement($typeAbo);
}

function testModifier($idMembre,$libelleTypeAbo,$aboModif)
{
    $useCase=new GererAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("AbonnementDao");

    $abo = new Abonnement();
    $abo = getObjectAbonnement($idMembre,$libelleTypeAbo);
    
    $dao->supprimerAbonnement($abo);
    $dao->ajouterAbonnement($abo);

    $bundle =  new Bundle();
    $bundle->put(Bundle::$ABONNEMENT,$aboModif);
    
    $bundle=$useCase->modifierAbonnement($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE))
    {
        echo("Modification Abonnement: OK\n");
        $abo2=$dao->getAbonnement($abo->getIdMembre(), $abo->getLibelle(), $abo->getDateAchat());
        
        if($abo2->getIdMembre()!=$abo->getIdMembre())
                echo("modifierAbonnement idMembre différents\n");
            else
                echo("modifierAbonnement idMembre: OK\n");

        if($abo2->getLibelle()!=$abo->getLibelle())
                echo("modifierAbonnement Libelles différents\n");
            else
                echo("modifierAbonnement Libelles: OK\n");

        if($abo2->getDateDebut()!=$abo->getDateDebut())
                echo("modifierAbonnement dates de début différentes\n");
            else
                echo("modifierAbonnement dates de début: OK\n");

        if($abo2->getDateAchat()!=$abo->getDateAchat())
                echo("modifierAbonnement dates d'achat différentes\n");
            else
                echo("modifierAbonnement dates d'achat: OK\n");

        if($abo2->getNbUniteRest()!=$abo->getNbUniteRest())
                echo("modifierAbonnement Nombre d'unités différents\n");
            else
                echo("modifierAbonnement Nombre d'unités: OK\n");

        if($abo2->getPrix()!=$abo->getPrix())
                echo("modifierAbonnement Prix différents\n");
            else
                echo("modifierAbonnement Prix: OK\n");

        if($abo2->getDateFin()!=$abo->getDateFin())
                echo("modifierAbonnement dates de fin différentes\n");
            else
                echo("modifierAbonnement dates de fin: OK\n");
    }
    else
        echo("Ajout de l'abonnement échoué \n");
}

function testGetOneAbonnement($idMembre,$libelleTypeAbo)
{
    $useCase=new GererAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("AbonnementDao");
    
    $abo = new Abonnement();
    $abo = getObjectAbonnement($idMembre,$libelleTypeAbo);
    
    if($dao->supprimerAbonnement($abo) && $dao->ajouterAbonnement($abo))
    {

        $bundle =  new Bundle();
        $bundle->put(Bundle::$ABONNEMENT,$abo);
        $bundle->put(Bundle::$IDMEMBRE,$abo->getIdMembre());
        $bundle=$useCase->getAbonnement($bundle);
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
        {
            
            echo("Récupération d'un abonnement: OK\n");
            $abo2=$bundle->get(Bundle::$ABONNEMENT);
            
            if($abo2->getIdMembre()!=$abo->getIdMembre())
                echo("getAbonnement idMembre différents\n");
            else
                echo("getAbonnement idMembre: OK\n");

            if($abo2->getLibelle()!=$abo->getLibelle())
                    echo("getAbonnement Libelles différents\n");
                else
                    echo("getAbonnement Libelles: OK\n");

            if($abo2->getDateDebut()!=$abo->getDateDebut())
                    echo("getAbonnement dates de début différentes\n");
                else
                    echo("getAbonnement dates de début: OK\n");

            if($abo2->getDateAchat()!=$abo->getDateAchat())
                    echo("getAbonnement dates d'achat différentes\n");
                else
                    echo("getAbonnement dates d'achat: OK\n");

            if($abo2->getNbUniteRest()!=$abo->getNbUniteRest())
                    echo("getAbonnement Nombre d'unités différents\n");
                else
                    echo("getAbonnement Nombre d'unités: OK\n");

            if($abo2->getPrix()!=$abo->getPrix())
                    echo("getAbonnement Prix différents\n");
                else
                    echo("getAbonnement Prix: OK\n");

            if($abo2->getDateFin()!=$abo->getDateFin())
                    echo("getAbonnement dates de fin différentes\n");
                else
                    echo("getAbonnement dates de fin: OK\n");
            }
            else
                echo("Récupération échouée");
    }
    else{
        echo("Récupération échouée");
    }
    
}

function testGetAbonnementsMembre($idMembre,$libelleTypeAbo)
{
    $useCase=new GererAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("AbonnementDao");
    
    $abo = new Abonnement();
    $abo = getObjectAbonnement($idMembre,$libelleTypeAbo);
    
    $bundle=new Bundle();
    
    $dao->supprimerAbonnement($abo);
    if($dao->ajouterAbonnement($abo)){
        echo("RE-Ajout pour le listage Abonnements d'un membre: OK\n");
        $bundle->put(Bundle::$IDMEMBRE,$abo->getIdMembre());
        $bundle=$useCase->getAbonnementsMembre($bundle);
        $liste=$bundle->get(Bundle::$LISTE);
        if(count($liste)>=1){
            echo("Compte: OK\n");
            $i=0;
            $trouve=false;
            while($i<count($liste) && !$trouve){
                if($liste[$i]->getIdMembre()==$abo->getIdMembre())
                    $trouve=true;
                $i++;
            }
            if($trouve){
                echo("Listing Abonnements d'un membre OK\n");
                if($dao->supprimerAbonnement($abo)==1) echo("Nettoyage OK\n");
                else echo("Nettoyage échoué\n");
            }
            else{
                echo("Listing échoué\n");
            }
        }
        else{
            echo("Compte échoué\n");
        }
        
    }
    else{
        echo("Ajout échoué\n");
    }
}

function testlister($idMembre,$libelleTypeAbo)
{
    $useCase=new GererAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("AbonnementDao");
    
    $abo = new Abonnement();
    $abo = getObjectAbonnement($idMembre,$libelleTypeAbo);
    
    $bundle=new Bundle();
    
    $dao->supprimerAbonnement($abo);
    if($dao->ajouterAbonnement($abo)){
        echo("RE-Ajout pour le listage Abonnement: OK\n");
        
        $bundle=$useCase->listerAbonnements($bundle);
        $liste=$bundle->get(Bundle::$LISTE);
        if(count($liste)>=1){
            echo("Compte: OK\n");
            $i=0;
            $trouve=false;
            while($i<count($liste) && !$trouve){
                if($liste[$i]->getIdMembre()==$abo->getIdMembre())
                    $trouve=true;
                $i++;
            }
            if($trouve){
                echo("Listing Abonnements OK\n");
                if($dao->supprimerAbonnement($abo)==1) echo("Nettoyage OK\n");
                else echo("Nettoyage échoué\n");
            }
            else{
                echo("Listing échoué\n");
            }
        }
        else{
            echo("Compte échoué\n");
        }
        
    }
    else{
        echo("Ajout échoué\n");
    }
}

function getObjectAbonnement($idMembre,$libelleTypeAbo)
{
    $abo=new Abonnement();
    $abo->setIdMembre($idMembre);
    $abo->setLibelle($libelleTypeAbo);
    $abo->setDateDebut(new DateTime("2014-01-01"));
    $abo->setDateAchat(new DateTime("2014-01-01 10:00:00"));
    $abo->setNbUniteRest(null);
    $abo->setPrix(284.3);
    $abo->setDateFin(new DateTime("2015-01-01"));

    return ($abo);
}

function getObjectAbonnement2($idMembre, $libelleTypeAbo)
{
    $abo=new Abonnement();
    $abo->setIdMembre($idMembre);
    $abo->setLibelle($libelleTypeAbo);
    $abo->setDateDebut(new DateTime("2014-01-01"));
    $abo->setDateAchat(new DateTime("2014-01-01 10:00:00"));
    $abo->setNbUniteRest(null);
    $abo->setPrix(984.3);
    $abo->setDateFin(new DateTime("2015-01-01"));

    return ($abo);
}

function supprimerMembre($membre)
{
    date_default_timezone_set('Europe/Paris');

    $daoMem = DaoFactory::getInstance()->get("MembreDao");

    if($daoMem->supprimerPhysMembre($membre->getIdMembre()))
        echo "Membre Supp : OK\n";
}

function getObjetMembre(){
    date_default_timezone_set('Europe/Paris');
    $mem = new Membre();
    $mem->setAdresse(new Adresse("Bois Du Prince 131","5640","Mettet","Belgique"));
    $mem->setDateInscription(new DateTime("2011-03-05 06:06:06"));
    $mem->setEmail("exemple@gmail.com");
    $mem->setNom("Jamar");
    $mem->setPrenom("Frederic");
    $mem->setSexe("M");
    $mem->setDateNaissance(new DateTime("1993-05-15"));
    $mem->setFonction("M");
    $mem->setMdp("jesuisfred");
    $mem->setLogo("chemin/isanzo.jpg");
    $mem->setSupprime("F");
    $mem->setStatut("F");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $mem);
    $gererMembre->ajouterMembre($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "exemple@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        $membre=null;
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
            $membre=$bundle->get(Bundle::$MEMBRE);
        else
            echo($bundle->get(Bundle::$MESSAGE));
    }
    else
        echo($bundle->get(Bundle::$MESSAGE));
    
    return($membre);
}

function getObjectTypeAbonnement()
{
    $typeAbo=new TypeAbonnement();
    $typeAbo->setLibelle("Abonnement test");
    $typeAbo->setPrix(299);
    $typeAbo->setDuree(365);
    $typeAbo->setNbUnite(-1);
    $typeAbo->setCours("Tous les cours admis");
    $typeAbo->setSupprime("F");

    return ($typeAbo);
}

$membre=getObjetMembre();
$idMembre = $membre->getIdMembre();
$typeAbo = getObjectTypeAbonnement();
$libelleTypeAbo = $typeAbo->getLibelle();
$abo2 = getObjectAbonnement2($idMembre, $libelleTypeAbo);


testAjout($idMembre,$libelleTypeAbo);
testGetOneAbonnement($idMembre,$libelleTypeAbo);
testModifier($idMembre,$libelleTypeAbo,$abo2);
testLister($idMembre,$libelleTypeAbo);
testGetAbonnementsMembre($idMembre,$libelleTypeAbo);
testSupprimer($idMembre,$libelleTypeAbo);
supprimerMembre($membre);

?>