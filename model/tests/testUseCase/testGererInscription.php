<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");


function supprimerAnonyme($membre)
{
    date_default_timezone_set('Europe/Paris');

    $daoMem = DaoFactory::getInstance()->get("MembreDao");

    if($daoMem->supprimerPhysMembre($membre->getIdMembre()))
        echo "Anonyme Supp : OK\n";
}

function getObjetAnonyme(){
    date_default_timezone_set('Europe/Paris');
    $mem = new Membre();
    $mem->setAdresse(new Adresse("Bois Du Prince 131","5640","Mettet","Belgique"));
    $mem->setDateInscription(new DateTime("2011-03-05 06:06:06"));
    $mem->setEmail("anonyme77@gmail.com");
    $mem->setNom("Jamar");
    $mem->setPrenom("Frederic");
    $mem->setSexe("M");
    $mem->setDateNaissance(new DateTime("1993-05-15"));
    $mem->setFonction("M");
    $mem->setMdp("jesuisfred");
    $mem->setLogo("chemin/isanzo.jpg");
    $mem->setSupprime("F");
    $mem->setStatut("F");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $mem);
    $gererMembre->ajouterMembre($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "anonyme77@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        $membre=null;
        if($bundle->get(Bundle::$OPERATION_REUSSIE)){
            $membre=$bundle->get(Bundle::$MEMBRE);
            echo("Création du membre OK\n");
        }
        else
            echo($bundle->get(Bundle::$MESSAGE));
    }
    else
        echo($bundle->get(Bundle::$MESSAGE));
    
    return($membre);
}
function getObjectSeance($membre){
    $daoSeance=DaoFactory::getInstance()->get("SeanceDao");
    
    $seance=new Seance();
    $seance->setCours("Jujutsu");
    $seance->setDateCours("1993-05-15");
    $seance->setIdProf($membre->getIdMembre());
    $seance->setNbMembreMax(15);
    $seance->setNoSeance(20);
    
    $idSeance=$daoSeance->ajouterSeance($seance);
    if($idSeance){
        echo("Ajout de la seance OK\n");
    }
    else{
        echo("Ajout de la seance echoue\n");
    }
    return($idSeance);
}
function supprimerSeance($idSeance){
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    $seance=new Seance();
    $seance->setNoSeance($idSeance);
    if($dao->supprimerSeance($seance)){
        echo("Suppression Seance OK\n");
    }
    else{
        echo("Suppression de la seance echoue\n");
    }
}
function getObjetInscription($membre,$idSeance){
    $inscription=new Inscription();
    $inscription->setNoSeance($idSeance);
    $inscription->setIdmembre($membre->getIdMembre());
    $inscription->setPresent('F');
    return($inscription);
}

function inscriptionEgales($inscription1, $inscription2){
    if($inscription1->getNoSeance()==$inscription2->getNoSeance()){
        echo("Numéro seance OK\n");
    }
    else{
        echo("Numéro de séance différents\n");
    }
    if($inscription1->getIdMembre()==$inscription2->getIdMembre()){
        echo("IdMembre OK\n");
    }
    else{
        echo("IdMembre différents\n");
    }
    if($inscription1->getPresent()==$inscription2->getPresent()){
        echo("Presence OK\n");
    }
    else{
        echo("Presence différents\n");
    }
}

function testAjout($inscription){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $useCase=new GererInscriptionImpl();
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$INSCRIPTION,$inscription);
    
    $useCase->ajouterInscription($bundle);
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Ajout OK\n");
        $inscriptionRetour=$dao->getInscription($inscription);
        if($inscriptionRetour!=false){
            echo("Confirmation OK\n");
            inscriptionEgales($inscription,$inscriptionRetour);
        }
        else{
            echo("Confirmation échouée\n");
        }
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
    
}

function testSupprimer($inscription){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $useCase=new GererInscriptionImpl();
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$INSCRIPTION,$inscription);
    
    $useCase->supprimerInscription($bundle);
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Ajout OK\n");
        $inscriptionRetour=$dao->getInscription($inscription);
        if($inscriptionRetour==false){
            echo("Confirmation OK\n");
        }
        else{
            echo("Confirmation échouée\n");
        }
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
}

function testModifier($inscription){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $useCase=new GererInscriptionImpl();
    
    $inscription->setPresent("T");
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$INSCRIPTION,$inscription);
    
    $useCase->modifierInscription($bundle);
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Modification OK\n");
        $inscriptionRetour=$dao->getInscription($inscription);
        if($inscriptionRetour!=false){
            echo("Confirmation OK\n");
            inscriptionEgales($inscription,$inscriptionRetour);
        }
        else{
            echo("Confirmation échouée\n");
        }
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
}

function testMarquerPresent($inscription){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $useCase=new GererInscriptionImpl();
    
    $inscription->setPresent("T");
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$INSCRIPTION,$inscription);
    
    $useCase->marquerPresent($bundle);
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Présent OK\n");
        $inscriptionRetour=$dao->getInscription($inscription);
        if($inscriptionRetour!=false){
            echo("Confirmation OK\n");
            inscriptionEgales($inscription,$inscriptionRetour);
        }
        else{
            echo("Confirmation échouée\n");
        }
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
}

function testListing(){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $useCase=new GererInscriptionImpl();
    
    
    $bundle=new Bundle();
    
    $useCase->listerInscriptions($bundle);
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Listing OK\n");
        $liste=$bundle->get(Bundle::$LISTE);
        foreach($liste as $inscription){
            $inscriptionRetour=$dao->getInscription($inscription);
            if($inscriptionRetour!=false){
                echo("Confirmation OK\n");
                inscriptionEgales($inscription,$inscriptionRetour);
            }
            else{
                echo("Confirmation échouée\n");
            }
        }
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
}

function testGet($inscription){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $useCase=new GererInscriptionImpl();
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$INSCRIPTION,$inscription);
    
    $useCase->getInscription($bundle);
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Récupération OK\n");
        $inscriptionRetour=$dao->getInscription($inscription);
        if($inscriptionRetour!=false){
            echo("Confirmation OK\n");
            inscriptionEgales($inscription,$inscriptionRetour);
        }
        else{
            echo("Confirmation échouée\n");
        }
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
}
function getNombre($idSeance){
    $seance=new Seance();
    $seance->setNoSeance($idSeance);
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$SEANCE,$seance);
    
    $useCase=new GererInscriptionImpl();
    
    $useCase->getNombre($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Nombre OK\n");
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
}

function getFromSeance($idSeance){
    $seance=new Seance();
    $seance->setNoSeance($idSeance);
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$SEANCE,$seance);
    
    $useCase=new GererInscriptionImpl();
    
    $useCase->getFromSeance($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Liste inscription OK\n");
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
}
$membre=getObjetAnonyme();
$idSeance=getObjectSeance($membre);

$inscription=getObjetInscription($membre,$idSeance);
testAjout($inscription);
getNombre($idSeance);
getFromSeance($idSeance);
testModifier($inscription);
testMarquerPresent($inscription);
testListing();
testGet($inscription);
testSupprimer($inscription);

supprimerSeance($idSeance);
supprimerAnonyme($membre)
?>