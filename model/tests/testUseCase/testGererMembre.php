<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Jérôme Hainaut
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

date_default_timezone_set('Europe/Paris');
$membreDao = DaoFactory::getInstance()->get("MembreDao");


function verifMemeMembre($membre1, $membre2, $messagePerso){
	if($membre1->getAdresse()->getRue() == $membre2->getAdresse()->getRue())
		echo("<br/>".$messagePerso."getRue OK \n");
	else
		echo("<br/>".$messagePerso."getRue fail \n");
	if($membre1->getAdresse()->getPays() == $membre2->getAdresse()->getPays())
		echo("<br/>".$messagePerso."getPays OK \n");
	else
		echo("<br/>".$messagePerso."getPays fail \n");
	if($membre1->getAdresse()->getCodePostale() == $membre2->getAdresse()->getCodePostale())
		echo("<br/>".$messagePerso."getCodePostale OK \n");
	else
		echo("<br/>".$messagePerso."getCodePostale fail \n");
	if($membre1->getAdresse()->getVille() == $membre2->getAdresse()->getVille())
		echo("<br/>".$messagePerso."getVille OK \n");
	else
		echo("<br/>".$messagePerso."getVille fail \n");
	if($membre1->getDateInscription() == $membre2->getDateInscription())
		echo("<br/>".$messagePerso."getDateInscription OK \n");
	else
		echo("<br/>".$messagePerso."getDateInscription fail \n");
	if($membre1->getEmail() == $membre2->getEmail())
		echo("<br/>".$messagePerso."getEmail OK \n");
	else
		echo("<br/>".$messagePerso."getEmail fail \n");
	if($membre1->getIdMembre() == $membre2->getIdMembre())
		echo("<br/>".$messagePerso."getIdMembre OK \n");
	else
		echo("<br/>".$messagePerso."getIdMembre fail \n");
	if($membre1->getNom() == $membre2->getNom())
		echo("<br/>".$messagePerso."getNom OK \n");
	else
		echo("<br/>".$messagePerso."getNom fail \n");
	if($membre1->getPrenom() == $membre2->getPrenom())
		echo("<br/>".$messagePerso."getPrenom OK \n");
	else
		echo("<br/>".$messagePerso."getPrenom fail \n");
	if($membre1->getSexe() == $membre2->getSexe())
		echo("<br/>".$messagePerso."getSexe OK \n");
	else
		echo("<br/>".$messagePerso."getSexe fail \n");
	if($membre1->getDateNaissance() == $membre2->getDateNaissance())
		echo("<br/>".$messagePerso."getDateNaissance OK \n");
	else
		echo("<br/>".$messagePerso."getDateNaissance fail \n");
	if($membre1->getFonction() == $membre2->getFonction())
		echo("<br/>".$messagePerso."getFonction OK \n");
	else
		echo("<br/>".$messagePerso."getFonction fail \n");
	if($membre1->getMdp() == $membre2->getMdp())
		echo("<br/>".$messagePerso."getMdp OK \n");
	else
		echo("<br/>".$messagePerso."getMdp fail \n");
	if($membre1->getLogo() == $membre2->getLogo())
		echo("<br/>".$messagePerso."getLogo OK \n");
	else
		echo("<br/>".$messagePerso."getLogo fail \n");
	if($membre1->getSupprime() == $membre2->getSupprime())
		echo("<br/>".$messagePerso."getSupprime OK \n");
	else
		echo("<br/>".$messagePerso."getSupprime fail \n");
	if($membre1->getStatut() == $membre2->getStatut())
		echo("<br/>".$messagePerso."getStatut OK \n");
	else
		echo("<br/>".$messagePerso."getStatut fail \n");
}


$gererMembre = new GererMembreImpl(); //classe GererMembre
$bundle = new Bundle();
$membres = array();//liste de membre
$i=0;
for(;$i<4;$i++){
	$membres[$i]= new Membre();
	$idM = $gererMembre->geneIdMembre(); //génère L'ID mais ne prérenplit pas le membre avec.
	$membres[$i]->setEmail("mail".$idM."@mail.com");
	$membres[$i]->setAdresse(new Adresse("rueduTest","5621","belleVille","Belgique"));
	$membres[$i]->setDateInscription(new DateTime("2011-03-".($i+2)." 06:06:06"));
	$membres[$i]->setNom("nom".$i);
	$membres[$i]->setPrenom("testPrénom".$i);
	$membres[$i]->setSexe("M");
	$membres[$i]->setDateNaissance(new DateTime("1990-02-11"));
	$membres[$i]->setFonction("M");
	$membres[$i]->setMdp("mdp".$i);
	$membres[$i]->setLogo("chemin/logo");
	$membres[$i]->setSupprime("F");
	$membres[$i]->setStatut("F");
	$bundle->put(Bundle::$MEMBRE, $membres[$i]);
	$gererMembre->ajouterMembre($bundle);
	if($bundle->get(Bundle::$OPERATION_REUSSIE)){
		echo("<br/>ajoutMembre ".$membres[$i]->getIdMembre()." OK\n");	
	}
	else{
		echo("<br/>ajoutMembre : ".$bundle->get(Bundle::$MESSAGE)." \n");
	}
}
// ajout membre avec l'email d'un autre.

$membresupl= new Membre();
$idM = $gererMembre->geneIdMembre();
$membresupl->setIdMembre($idM);
$membresupl->setEmail($membres[0]->getEmail());
$membresupl->setAdresse(new Adresse("rueduTest","5621","belleVille","Belgique"));
$membresupl->setDateInscription(new DateTime("2011-03-".($i+2)." 06:06:06"));
$membresupl->setNom("nom".$i);
$membresupl->setPrenom("testPrénom".$i);
$membresupl->setSexe("M");
$membresupl->setDateNaissance(new DateTime("1990-02-11"));
$membresupl->setFonction("M");
$membresupl->setMdp("mdp".$i);
$membresupl->setLogo("chemin/logo");
$membresupl->setSupprime("F");
$membresupl->setStatut("F");
$bundle->put(Bundle::$MEMBRE, $membresupl);
$gererMembre->ajouterMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>ajoutMembre avec email autre ".$membresupl->getIdMembre()." \n");	
}
else{
	echo("<br/>ajoutMembre avec email autre: ".$bundle->get(Bundle::$MESSAGE)." OK \n");
}

//rechercher Membre sur ID
$bundle->vider();
$bundle->put(Bundle::$IDMEMBRE, $membres[1]->getIdMembre());
$gererMembre->rechercherMembreID($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>rechercherMembreID OK\n");	
	$membre=$bundle->get(Bundle::$MEMBRE);
	verifMemeMembre($membre,$membres[1],"rechercherMembreID ");
}
else{
	echo("<br/>rechercherMembreID : ".$bundle->get(Bundle::$MESSAGE)." \n");
}

//rechercher Membre sur Email
$bundle->vider();
$bundle->put(Bundle::$EMAIL, $membres[1]->getEmail());
$gererMembre->rechercherMembreEmail($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>rechercherMembreEmail OK\n");	
	$membre=$bundle->get(Bundle::$MEMBRE);
	verifMemeMembre($membre,$membres[1],"rechercherMembreEmail ");
}
else{
	echo("<br/>L145 rechercherMembreEmail : ".$bundle->get(Bundle::$MESSAGE)." \n");
}

//modifier Membre
$membres[1]->setNom("hyolllloooo");
$bundle->vider();
$bundle->put(Bundle::$MEMBRE, $membres[1]);
$gererMembre->modifierMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>modifierMembre OK\n");	
}
else{
	echo("<br/>modifierMembre : ".$bundle->get(Bundle::$MESSAGE)." \n");
}


//lister Membre
$bundle->vider();
$gererMembre->listerMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>listerMembre OK\n");	
	$membresListe=$bundle->get(Bundle::$LISTE);
	for($i=0;$i<5;$i++){
		//verifMemeMembre($membres[$i],$membresListe[$i],"listerMembre ".$i." " );
	}
}
else{
	echo("<br/>isterMembre : ".$bundle->get(Bundle::$MESSAGE)." \n");
}


//supprimer Membre
$bundle->vider();
$bundle->put(Bundle::$IDMEMBRE, $membres[1]->getIdMembre());
$gererMembre->supprimerMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>supprimerMembre OK\n");	
}
else{
	echo("<br/>supprimerMembre : ".$bundle->get(Bundle::$MESSAGE)." \n");
}

//rechercher membre supprimer
$bundle->vider();
$bundle->put(Bundle::$IDMEMBRE, $membres[1]->getIdMembre());
$gererMembre->rechercherMembreID($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>rechercherMembreID fail\n");	
}
else{
	echo("<br/>rechercherMembreID : ".$bundle->get(Bundle::$MESSAGE)." OK \n");
}

//modifier Membre avec email d'un autre mais supprimer
$membreOld = $membres[0];
$membreOld->setEmail($membres[1]->getEmail());
$bundle->vider();
$bundle->put(Bundle::$MEMBRE, $membreOld);
$gererMembre->modifierMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>modifierMembre meme email membre sup OK\n");	
}
else{
	echo("<br/>modifierMembre d'email sup : ".$bundle->get(Bundle::$MESSAGE)." \n");
}

//modifier Membre supprimer
$membreOld = $membres[1];
$membreOld->setNom("hyolllloooffffo");
$bundle->vider();
$bundle->put(Bundle::$MEMBRE, $membreOld);
$gererMembre->modifierMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>modifierMembre fail\n");	
}
else{
	echo("<br/>modifierMembre : ".$bundle->get(Bundle::$MESSAGE)." OK \n");
}

//supprimer Membre déja supprimer
$bundle->vider();
$bundle->put(Bundle::$IDMEMBRE, $membres[1]->getIdMembre());
$gererMembre->supprimerMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>supprimerMembre fail\n");	
}
else{
	echo("<br/>supprimerMembre : ".$bundle->get(Bundle::$MESSAGE)." OK \n");
}

//supprimer un autre Membre
$bundle->vider();
$bundle->put(Bundle::$IDMEMBRE, $membres[3]->getIdMembre());
$gererMembre->supprimerMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>supprimerMembre OK\n");	
}
else{
	echo("<br/>supprimerMembre : ".$bundle->get(Bundle::$MESSAGE)." \n");
}
//ajout d'un membre avec email d'un supp

$membresupl= null;
$membresupl= new Membre();
$idM = $gererMembre->geneIdMembre();
$membresupl->setEmail($membres[3]->getEmail());
$membresupl->setAdresse(new Adresse("rueduTest","5621","belleVille","Belgique"));
$membresupl->setDateInscription(new DateTime("2011-03-".($i+2)." 06:06:06"));
$membresupl->setNom("nom".$i);
$membresupl->setPrenom("testPrénom".$i);
$membresupl->setSexe("M");
$membresupl->setDateNaissance(new DateTime("1990-02-11"));
$membresupl->setMdp("mdp".$i);
$membresupl->setLogo("chemin/logo");
$bundle->put(Bundle::$MEMBRE, $membresupl);
$gererMembre->ajouterMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>ajoutMembre avec email autre supp".$membresupl->getIdMembre()." OK \n");	
}
else{
	echo("<br/>ajoutMembre avec email autre supp: ".$bundle->get(Bundle::$MESSAGE)." \n");
}

//modifier Membre avec email d'un autre
$membreOld = $membres[1];
$membreOld->setEmail($membres[2]->getEmail());
$bundle->vider();
$bundle->put(Bundle::$MEMBRE, $membreOld);
$gererMembre->modifierMembre($bundle);
if($bundle->get(Bundle::$OPERATION_REUSSIE)){
	echo("<br/>modifierMembre meme email fail \n");	
}
else{
	echo("<br/>modifierMembre meme email : ".$bundle->get(Bundle::$MESSAGE)." OK \n");
}



$membreDao = DaoFactory::getInstance()->get("MembreDao");
foreach ( $membres as $membre ) {
	$membreDao->supprimerPhysMembre($membre->getIdMembre());      
}
$membreDao->supprimerPhysMembre($membresupl->getIdMembre());
