<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Fr�d�ric
 * Team : Dev4u
 * cr�� le ??/??/2014 - modif�e le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");
function getObjetAnonyme(){
    date_default_timezone_set('Europe/Paris');
    $mem = new Membre();
    $mem->setAdresse(new Adresse("Bois Du Prince 131","5640","Mettet","Belgique"));
    $mem->setDateInscription(new DateTime("2011-03-05 06:06:06"));
    $mem->setEmail("anonyme77@gmail.com");
    $mem->setNom("Jamar");
    $mem->setPrenom("Frederic");
    $mem->setSexe("M");
    $mem->setDateNaissance(new DateTime("1993-05-15"));
    $mem->setFonction("M");
    $mem->setMdp("jesuisfred");
    $mem->setLogo("chemin/isanzo.jpg");
    $mem->setSupprime("F");
    $mem->setStatut("T");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $mem);
    $gererMembre->ajouterMembre($bundle);
    
    $membre=null;
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "anonyme77@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
            $membre=$bundle->get(Bundle::$MEMBRE);
        else
            echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
    else
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    
    return($membre);
}
function testAuthentifier($membre1)
{
    if(empty($_SESSION))
        session_start();
    $test = true;
    $useCase = new AuthentifierImpl();
    $bundle = new Bundle();
    
    //Dao membre
    $membreDao = DaoFactory::getInstance()->get("MembreDao");
    
    
    //V�rification de l'ajout du membre, Si r�ussi on passe au login.
    if($membre1!=null)
    {
        echo("Ajout: OK\n");
        $authentifier = new Authentifier($membre1->getEmail(),$membre1->getMdp());
        $bundle->put(Bundle::$AUTHENTIFIER,$authentifier);
        $bundle2 = $useCase->authentifier($bundle);
        $membre2 = unserialize($_SESSION["oMembre"]);
        if($bundle2->get(Bundle::$OPERATION_REUSSIE))
        {
            if($membre2 != null)
            {
            	$messagePerso= "authentifier ";
                    if($membre1->getAdresse()->getRue() == $membre2->getAdresse()->getRue())
                            echo($messagePerso."getRue OK \n");
                    else
                            echo($messagePerso."getRue fail \n");
                    if($membre1->getAdresse()->getPays() == $membre2->getAdresse()->getPays())
                            echo($messagePerso."getPays OK \n");
                    else
                            echo($messagePerso."getPays fail \n");
                    if($membre1->getAdresse()->getCodePostale() == $membre2->getAdresse()->getCodePostale())
                            echo($messagePerso."getCodePostale OK \n");
                    else
                            echo($messagePerso."getCodePostale fail \n");
                    if($membre1->getAdresse()->getVille() == $membre2->getAdresse()->getVille())
                            echo($messagePerso."getVille OK \n");
                    else
                            echo($messagePerso."getVille fail \n");
                    if($membre1->getDateInscription() == $membre2->getDateInscription())
                            echo($messagePerso."getDateInscription OK \n");
                    else
                            echo($messagePerso."getDateInscription fail \n");
                    if($membre1->getEmail() == $membre2->getEmail())
                            echo($messagePerso."getEmail OK \n");
                    else
                            echo($messagePerso."getEmail fail \n");
                    if($membre1->getIdMembre() == $membre2->getIdMembre())
                            echo($messagePerso."getIdMembre OK \n");
                    else
                            echo($messagePerso."getIdMembre fail \n");
                    if($membre1->getNom() == $membre2->getNom())
                            echo($messagePerso."getNom OK \n");
                    else
                            echo($messagePerso."getNom fail \n");
                    if($membre1->getPrenom() == $membre2->getPrenom())
                            echo($messagePerso."getPrenom OK \n");
                    else
                            echo($messagePerso."getPrenom fail \n");
                    if($membre1->getSexe() == $membre2->getSexe())
                            echo($messagePerso."getSexe OK \n");
                    else
                            echo($messagePerso."getSexe fail \n");
                    if($membre1->getDateNaissance() == $membre2->getDateNaissance())
                            echo($messagePerso."getDateNaissance OK \n");
                    else
                            echo($messagePerso."getDateNaissance fail \n");
                    if($membre1->getFonction() == $membre2->getFonction())
                            echo($messagePerso."getFonction OK \n");
                    else
                            echo($messagePerso."getFonction fail \n");
                    if($membre1->getMdp() == $membre2->getMdp())
                            echo($messagePerso."getMdp OK \n");
                    else
                            echo($messagePerso."getMdp fail \n");
                    if($membre1->getLogo() == $membre2->getLogo())
                            echo($messagePerso."getLogo OK \n");
                    else
                            echo($messagePerso."getLogo fail \n");
                    if($membre1->getSupprime() == $membre2->getSupprime())
                            echo($messagePerso."getSupprime OK \n");
                    else
                            echo($messagePerso."getSupprime fail \n");
                    if($membre1->getStatut() == $membre2->getStatut())
                            echo($messagePerso."getStatut OK \n");
                    else
                            echo($messagePerso."getStatut fail \n");
            }
            else
                echo "Connexion FAIL !\n";
            
        }
        else{
            echo($bundle2->get(Bundle::$MESSAGE)."\n");
        }
        
        //Suppression physique du membre1
        if($membreDao->supprimerPhysMembre($membre1->getIdMembre()))
                echo "Suppression OK\n";
        else
                echo "Suppression fail\n";
    }
    else
        echo "Ajout fail\n";
    
}
$membre=getObjetAnonyme();
date_default_timezone_set('Europe/Paris');
testAuthentifier($membre);
?>
