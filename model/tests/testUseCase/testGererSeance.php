<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function testGetCours(){
    $useCase=new GererSeanceImpl();
    $bundle = new Bundle();
    $useCase->getCours($bundle);
    if($bundle->get(Bundle::$OPERATION_REUSSIE) and is_array($bundle->get(Bundle::$LISTE))){
        echo("Listing des cours OK\n");
    }
    else{
        echo("Listing des cours échoué\n");
    }
}
function seancesEgale($seance1,$seance2){
    if($seance1->getNoSeance()==$seance2->getNoSeance()){
        echo("Numéro de séance: OK\n");
    }
    else{
        echo("Numéro de séance différents\n");
    }
    
    if($seance1->getCours()==$seance2->getCours()){
        echo("Cours: OK\n");
    }
    else{
        echo("Cours différents\n");
    }
    
    if($seance1->getDateCours()==$seance2->getDateCours()){
        echo("Date Cours: OK\n");
    }
    else{
        echo("Date Cours différents\n");
    }
    
    if($seance1->getNbMembreMax()==$seance2->getNbMembreMax()){
        echo("Nombre de membre: OK\n");
    }
    else{
        echo("Nombre de membres différents\n");
    }
    
    if($seance1->getIdProf()==$seance2->getIdProf()){
        echo("Id Prof: OK\n");
    }
    else{
        echo("Id Prof différents\n");
    }
}

function supprimerAnonyme($membre)
{
    date_default_timezone_set('Europe/Paris');

    $daoMem = DaoFactory::getInstance()->get("MembreDao");

    if($daoMem->supprimerPhysMembre($membre->getIdMembre()))
        echo "Anonyme Supp : OK\n";
}

function getObjetAnonyme(){
    date_default_timezone_set('Europe/Paris');
    $mem = new Membre();
    $mem->setAdresse(new Adresse("Bois Du Prince 131","5640","Mettet","Belgique"));
    $mem->setDateInscription(new DateTime("2011-03-05 06:06:06"));
    $mem->setEmail("anonyme77@gmail.com");
    $mem->setNom("Jamar");
    $mem->setPrenom("Frederic");
    $mem->setSexe("M");
    $mem->setDateNaissance(new DateTime("1993-05-15"));
    $mem->setFonction("M");
    $mem->setMdp("jesuisfred");
    $mem->setLogo("chemin/isanzo.jpg");
    $mem->setSupprime("F");
    $mem->setStatut("F");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $mem);
    $gererMembre->ajouterMembre($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "anonyme77@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        $membre=null;
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
            $membre=$bundle->get(Bundle::$MEMBRE);
        else
            echo($bundle->get(Bundle::$MESSAGE));
    }
    else
        echo($bundle->get(Bundle::$MESSAGE));
    
    return($membre);
}
function testAjout($membre){
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    $useCase=new GererSeanceImpl();
    
    $seance=new Seance();
    $seance->setCours("Drague");
    $seance->setDateCours("2012-04-02");
    $seance->setNbMembreMax(15);
    $seance->setIdProf($membre->getIdMembre());
    
    $bundle= new Bundle();
    $bundle->put(Bundle::$SEANCE,$seance);
    
    $id=$useCase->ajouterSeance($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Ajout OK\n");
        $seance->setNoSeance($id);
        if($dao->getSeance($seance)!=false){
            echo("Verification OK\n");
        }
        else{
            echo($bundle->get(Bundle::$MESSAGE));
        }
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
    return($id);
}

function testRecuperation($id,$membre){
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    $useCase=new GererSeanceImpl();
    
    $seance=new Seance();
    $seance->setNoSeance($id);
    $seance->setCours("Drague");
    $seance->setDateCours("2012-04-02");
    $seance->setNbMembreMax(15);
    $seance->setIdProf($membre->getIdMembre());
    
    $bundle= new Bundle();
    $bundle->put(Bundle::$SEANCE,$seance);
    
    $useCase->getSeance($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Récupération OK\n");
        $seanceRetour=$bundle->get(Bundle::$SEANCE);
        seancesEgale($seance,$seanceRetour);
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
}
function testModification($id,$membre){
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    $useCase=new GererSeanceImpl();
    
    $seance=new Seance();
    $seance->setNoSeance($id);
    $seance->setCours("embrasser");
    $seance->setDateCours("2012-05-02");
    $seance->setNbMembreMax(25);
    $seance->setIdProf($membre->getIdMembre());
    
    $bundle= new Bundle();
    $bundle->put(Bundle::$SEANCE,$seance);
    
    $useCase->modifierSeance($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Modification OK\n");
        $seanceRetour=$dao->getSeance($seance);
        seancesEgale($seance,$seanceRetour);
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
}
function testLister(){
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    $useCase=new GererSeanceImpl();
    
    $seance=new Seance();
    
    $bundle= new Bundle();
    
    $useCase->listerSeance($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Listing OK\n");
        $liste=$bundle->get(Bundle::$LISTE);
        if($liste!=false){
            echo("Récupération de la liste OK\n");
            foreach($liste as $seance){
                $seanceRetour=$dao->getSeance($seance);
                seancesEgale($seance,$seanceRetour);
            }
        }
        else{
            echo("La récupération de la liste a échoué\n");
        }
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
}
function testSupprimer($id,$membre){
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    $useCase=new GererSeanceImpl();
    
    $seance=new Seance();
    $seance->setNoSeance($id);
    $seance->setIdProf($membre->getIdMembre());
    
    $bundle= new Bundle();
    $bundle->put(Bundle::$SEANCE,$seance);
    
    $useCase->supprimerSeance($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Suppression OK\n");
        if($dao->getSeance($seance)==false){
            echo("Confirmation de suppression OK\n");
        }
        else{
            echo("Confirmation de suppressin échouée\n");
        }
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
}
$membre=getObjetAnonyme();
$id=testAjout($membre);
testRecuperation($id,$membre);
testModification($id,$membre);
testLister();
testGetCours();
testSupprimer($id,$membre);
supprimerAnonyme($membre);
?>