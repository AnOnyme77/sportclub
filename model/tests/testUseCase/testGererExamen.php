<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Hainaut
 * Team : Dev4u
 * créé le 25/04/2014 - modifée le 25/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");


function examenEgale($examen1,$examen2){
    if($examen1->getDatePassage()==$examen2->getDatePassage()){
        echo("getDatePassage : OK\n");
    }
    else{
        echo("getDatePassage différents\n");
    }
    
    if($examen1->getCote()==$examen2->getCote()){
        echo("getCote: OK\n");
    }
    else{
        echo("getCote différents\n");
    }
    
    if($examen1->getCommentaire()==$examen2->getCommentaire()){
        echo("getCommentaire : OK\n");
    }
    else{
        echo("getCommentaire différents\n");
    }
    
    if($examen1->getNiveau()==$examen2->getNiveau()){
        echo("getNiveau: OK\n");
    }
    else{
        echo("getNiveau différents\n");
    }
    
    if($examen1->getIdProf()==$examen2->getIdProf()){
        echo("Id Prof: OK\n");
    }
    else{
        echo("Id Prof différents\n");
    }
    
    if($examen1->getIdMembre()==$examen2->getIdMembre()){
        echo("getIdMembre: OK\n");
    }
    else{
        echo("getIdMembre différents\n");
    }
}

function getObjetMembre($i){
    date_default_timezone_set('Europe/Paris');
    $membre = new Membre();
    $membre->setAdresse(new Adresse("rue","5600","ville","Belgique"));
    $membre->setDateInscription(new DateTime("2012-03-07 06:07:06"));
    $membre->setEmail("membreExamen".$i."@gmail.com");
    $membre->setNom("Membre".$i);
    $membre->setPrenom("Prenom".$i);
    $membre->setSexe("M");
    $membre->setDateNaissance(new DateTime("1990-11-02"));
    $membre->setFonction("P");
    $membre->setMdp("superMotDePasse");
    $membre->setLogo("image/toto.jpg");
    $membre->setSupprime("F");
    $membre->setStatut("F");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $membre);
    $gererMembre->ajouterMembre($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "membreExamen".$i."@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        $membre=null;
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
            $membre=$bundle->get(Bundle::$MEMBRE);
        else
            echo($bundle->get(Bundle::$MESSAGE));
    }
    else
        echo($bundle->get(Bundle::$MESSAGE));
    
    return($membre);
}

function supprimerMembre($membre)
{
    date_default_timezone_set('Europe/Paris');

    $daoMem = DaoFactory::getInstance()->get("MembreDao");

    if($daoMem->supprimerPhysMembre($membre->getIdMembre()))
        echo "Membre Supp : OK\n";
}

function testListing(){
	$gererExamen = new GererExamenImpl();
	
    $bundle=new Bundle();
    
    $gererExamen->listerExamen($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Récupération de la liste: OK\n");
    }
    else{
        echo("Récupération de la liste échouée\n");
    }
}

function getAjoutNiveau(){
    $niveau=new Niveau();
    
    $niveau->setNiveau("1");
    $niveau->setLibelle("testBleu");
    $niveau->setNameSection("orange");
    $niveau->setLogo("caca");
    $niveau->setSupprime("F");
    
    $dao=new NiveauDaoImpl();
    $dao->supprimerNiveau($niveau);
    if($dao->ajouterNiveau($niveau)){
        echo("Ajout: OK\n");
    }
    else{
        echo("Ajout: échoué\n");
    }
    return $niveau;
}

function testSuppressionNiveau(){
    $niveau=new Niveau();
    
    $niveau->setNiveau("1");
    $niveau->setLibelle("testBleu");
    $niveau->setNameSection("orange");
    $niveau->setLogo("caca");
    $niveau->setSupprime("V");
    
    $dao=new NiveauDaoImpl();
    if($dao->supprimerNiveau($niveau)==1){
        echo("Suppression: OK\n");
    }
    else{
        echo("Suppression: échouée\n");
    }
}

function testAjout($membre1, $membre2, $niveau){
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("un comentaire inutil");
    $examen->setCote(0);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
  
    $dao=new ExamenDaoImpl();
    $dao->supprimerExamen($examen);
    
    $gererExamen = new GererExamenImpl();
	
    $bundle=new Bundle();
    $bundle->put(Bundle::$EXAMEN, $examen);
    $gererExamen->ajouterExamen($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Ajout: OK\n");
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
    
}

function testSuppression($membre1, $membre2, $niveau){
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("un comentaire inutil");
    $examen->setCote(0);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
    
     $gererExamen = new GererExamenImpl();
	
    $bundle=new Bundle();
    $bundle->put(Bundle::$EXAMEN, $examen);
    $gererExamen->supprimerExamen($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Suppression: OK\n");
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
    
}

function testRecuperation($membre1, $membre2, $niveau){
    $ok=true;
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("un comentaire inutil");
    $examen->setCote(0);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
    
    $dao=new ExamenDaoImpl();
    $dao->supprimerExamen($examen);
    $dao->ajouterExamen($examen);
    $retour=$dao->getExamen($examen);
    
    $gererExamen=new GererExamenImpl();
    $bundle= new Bundle();
    $bundle->put(Bundle::$EXAMEN,$examen);
    
    $gererExamen->getExamen($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Récupération OK\n");
        $examenRetour=$bundle->get(Bundle::$EXAMEN);
        examenEgale($examen,$examenRetour);
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
}

function testRecuperationNiveau($membre1, $membre2, $niveau){
    $ok=true;
    $examen=new Examen();
    
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    
    $gererExamen=new GererExamenImpl();
    $bundle= new Bundle();
    $bundle->put(Bundle::$EXAMEN,$examen);
    
    $gererExamen->getExamensNiveau($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE) and is_array($bundle->get(Bundle::$LISTE))){
        echo("Récupération liste OK\n");
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
}
function testModification($membre1, $membre2, $niveau){
    
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("modifier");
    $examen->setCote(7);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
    
 	$gererExamen=new GererExamenImpl();
    $bundle= new Bundle();
    $bundle->put(Bundle::$EXAMEN,$examen);
    
    $gererExamen->modifierExamen($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Modification OK\n");
    }
    else{
        echo($bundle->get(Bundle::$MESSAGE));
    }
}
$gererParam = new GererParametresImpl();
$gererParam->getParametres();
$membre1 = getObjetMembre(1);
$membre2 = getObjetMembre(2);
$niveau = getAjoutNiveau();
testAjout($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testListing();
testRecuperation($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testRecuperationNiveau($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testModification($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testSuppression($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testSuppressionNiveau();
supprimerMembre($membre1);
supprimerMembre($membre2);
?>