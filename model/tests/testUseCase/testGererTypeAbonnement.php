<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 28/04/2014 - modifée le 28/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function getObjectTypeAbonnement()
{
    $typeAbo=new TypeAbonnement();
    $typeAbo->setLibelle("Abonnement test");
    $typeAbo->setPrix(299);
    $typeAbo->setDuree(365);
    $typeAbo->setNbUnite(-1);
    $typeAbo->setCours("Tous les cours admis");
    $typeAbo->setSupprime("F");

    return ($typeAbo);
}

function getObjectTypeAbonnement2()
{
    $typeAbo=new TypeAbonnement();
    $typeAbo->setLibelle("Abonnement test");
    $typeAbo->setPrix(329);
    $typeAbo->setDuree(364);
    $typeAbo->setNbUnite(1000);
    $typeAbo->setCours("Tous les cours admis sauf Yoga");
    $typeAbo->setSupprime("F");

    return ($typeAbo);
}

function testGetTypeAbonnement()
{
    $useCase=new GererTypeAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("TypeAbonnementDao");
    
    $typeAbo = new TypeAbonnement();
    $typeAbo = getObjectTypeAbonnement();

    if($dao->supprimerPhysTypeAbonnement($typeAbo) && $dao->ajouterTypeAbonnement($typeAbo))
    {
    
        $bundle =  new Bundle();
        $bundle=$useCase->getTypeAbonnement($typeAbo->getLibelle(),$bundle);
        
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
        {
            echo("Récupération: OK\n");
            $typeAbo2=$bundle->get(Bundle::$TYPEABONNEMENT);
            
            if($typeAbo2->getLibelle()!=$typeAbo->getLibelle())
                echo("getTypeAbonnement Libelles différents\n");
            else
                echo("getTypeAbonnement Libelles: OK\n");
                
            if($typeAbo2->getPrix()!=$typeAbo->getPrix())
                echo("getTypeAbonnement Prix différents\n");
            else
                echo("getTypeAbonnement Prix: OK\n");

            if($typeAbo2->getDuree()!=$typeAbo->getDuree())
                echo("getTypeAbonnement Durées différents\n");
            else
                echo("getTypeAbonnement Durées: OK\n");

            if($typeAbo2->getNbUnite()!=$typeAbo->getNbUnite())
                echo("getTypeAbonnement NbUnites différents\n");
            else
                echo("getTypeAbonnement NbUnites: OK\n");

            if($typeAbo2->getCours()!=$typeAbo->getCours())
                echo("getTypeAbonnement Cours différents\n");
            else
                echo("getTypeAbonnement Cours: OK\n");

            if($typeAbo2->getSupprime()!=$typeAbo->getSupprime())
                echo("getTypeAbonnement Supprimés différents\n");
            else
                echo("getTypeAbonnement Supprimés: OK\n");
        }
        else
            echo("getTypeAbonnement Récupération échouée\n");
    }
    else{
        echo("getTypeAbonnement Récupération échouée\n");
    }
    
}
function testSupprimerPhys()
{
    $useCase=new GererTypeAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("TypeAbonnementDao");
    
    $typeAbo = new TypeAbonnement();
    $typeAbo = getObjectTypeAbonnement();

    $dao->supprimerPhysTypeAbonnement($typeAbo);
    $dao->ajouterTypeAbonnement($typeAbo);
    
    $bundle =  new Bundle();
    $bundle->put(Bundle::$TYPEABONNEMENT,$typeAbo);
    
    $bundle=$useCase->supprimerPhysTypeAbonnement($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE))
        echo("Suppression: OK\n");
    else
        echo("Suppression échouée");
}
function testAjouter()
{
    $useCase=new GererTypeAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("TypeAbonnementDao");
    
    $typeAbo = new TypeAbonnement();
    $typeAbo = getObjectTypeAbonnement();
    
    $dao->supprimerPhysTypeAbonnement($typeAbo);

    $bundle =  new Bundle();
    $bundle->put(Bundle::$TYPEABONNEMENT,$typeAbo);
    
    $bundle=$useCase->ajouterTypeAbonnement($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE))
    {
        echo("Ajout: OK\n");
        $typeAbo2=$dao->getTypeAbonnement($typeAbo->getLibelle());
        
        if($typeAbo2->getLibelle()!=$typeAbo->getLibelle())
                echo("ajoutTypeAbonnement Libelles différents\n");
            else
                echo("ajoutTypeAbonnement Libelles: OK\n");
                
            if($typeAbo2->getPrix()!=$typeAbo->getPrix())
                echo("ajoutTypeAbonnement Prix différents\n");
            else
                echo("ajoutTypeAbonnement Prix: OK\n");

            if($typeAbo2->getDuree()!=$typeAbo->getDuree())
                echo("ajoutTypeAbonnement Durées différents\n");
            else
                echo("ajoutTypeAbonnement Durées: OK\n");

            if($typeAbo2->getNbUnite()!=$typeAbo->getNbUnite())
                echo("ajoutTypeAbonnement NbUnites différents\n");
            else
                echo("ajoutTypeAbonnement NbUnites: OK\n");

            if($typeAbo2->getCours()!=$typeAbo->getCours())
                echo("ajoutTypeAbonnement Cours différents\n");
            else
                echo("ajoutTypeAbonnement Cours: OK\n");

            if($typeAbo2->getSupprime()!=$typeAbo->getSupprime())
                echo("ajoutTypeAbonnement Supprimés différents\n");
            else
                echo("ajoutTypeAbonnement Supprimés: OK\n");
    }
    else
        echo("Ajout échoué \n");
}
function testModifier()
{
    $useCase=new GererTypeAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("TypeAbonnementDao");
    
    //Premier Type Abonnement
    $typeAbo = new TypeAbonnement();
    $typeAbo = getObjectTypeAbonnement();

    //Deuxieme Type Abonnement pour la modification
    $typeAbo2 = new TypeAbonnement();
    $typeAbo2 = getObjectTypeAbonnement2();

    //Nettoyage ... et ajout du PREMIER Type Abonnement
    $dao->supprimerPhysTypeAbonnement($typeAbo);
    $dao->ajouterTypeAbonnement($typeAbo);

    $bundle = new Bundle();
    $bundle->put(Bundle::$TYPEABONNEMENT,$typeAbo2);
    //Modification de l'abonnement 1 en 2.
    $bundle=$useCase->modifierTypeAbonnement($bundle);

    if($bundle->get(Bundle::$OPERATION_REUSSIE))
    {
        echo "Modification OK \n";
        $typeAbo = $dao->getTypeAbonnement($typeAbo2->getLibelle());
        $typeAbo2 = $bundle->get(Bundle::$TYPEABONNEMENT);

        if($typeAbo2->getLibelle()!=$typeAbo->getLibelle())
            echo("modifierTypeAbonnement Libelles différents\n");
        else
            echo("modifierTypeAbonnement Libelles: OK\n");
            
        if($typeAbo2->getPrix()!=$typeAbo->getPrix())
            echo("modifierTypeAbonnement Prix différents\n");
        else
            echo("modifierTypeAbonnement Prix: OK\n");

        if($typeAbo2->getDuree()!=$typeAbo->getDuree())
            echo("modifierTypeAbonnement Durées différents\n");
        else
            echo("modifierTypeAbonnement Durées: OK\n");

        if($typeAbo2->getNbUnite()!=$typeAbo->getNbUnite())
            echo("modifierTypeAbonnement NbUnites différents\n");
        else
            echo("modifierTypeAbonnement NbUnites: OK\n");

        if($typeAbo2->getCours()!=$typeAbo->getCours())
            echo("modifierTypeAbonnement Cours différents\n");
        else
            echo("modifierTypeAbonnement Cours: OK\n");

        if($typeAbo2->getSupprime()!=$typeAbo->getSupprime())
            echo("modifierTypeAbonnement Supprimés différents\n");
        else
            echo("modifierTypeAbonnement Supprimés: OK\n");
    }
    else
        echo "Modification échouée \n";
}
function testLister()
{
    $useCase=new GererTypeAbonnementImpl();
    $dao = DaoFactory::getInstance()->get("TypeAbonnementDao");
    
    $typeAbo = new TypeAbonnement();
    $typeAbo = getObjectTypeAbonnement();

    $bundle = new Bundle();
    $dao->supprimerPhysTypeAbonnement($typeAbo);
    if($dao->ajouterTypeAbonnement($typeAbo))
    {
        echo("Lister OK\nlisterTypesAbonnement Ajout: OK\n");
        
        $bundle=$useCase->listerTypesAbonnement($bundle);
        $liste=$bundle->get(Bundle::$LISTE);
        if(count($liste)>=1){
            echo("listerTypesAbonnement Compte: OK\n");
            $i=0;
            $trouve=false;
            while($i<count($liste) && !$trouve){
                if($liste[$i]->getLibelle()==$typeAbo->getLibelle())
                    $trouve=true;
                $i++;
            }
            if($trouve){
                echo("listerTypesAbonnement Listing OK\n");
                if($dao->supprimerPhysTypeAbonnement($typeAbo)==1) 
                    echo("listerTypesAbonnement Nettoyage OK\n");
                else 
                    echo("listerTypesAbonnement Nettoyage échoué\n");
            }
            else{
                echo("listerTypesAbonnement Listing échoué\n");
            }
        }
        else{
            echo("listerTypesAbonnement Compte échoué\n");
        }
        
    }
    else{
        echo("listerTypesAbonnement Ajout échoué\n");
    }

}

testAjouter();
testGetTypeAbonnement();
testModifier();
testLister();
testSupprimerPhys();

?>