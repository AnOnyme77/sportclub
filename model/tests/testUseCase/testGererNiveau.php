<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function testGetNiveau(){
    $useCase=new GererNiveauImpl();
    $dao = DaoFactory::getInstance()->get("NiveauDao");
    
    $niveau=new Niveau();
    $niveau->setLibelle("test de niveau");
    $niveau->setLogo("logoLOL");
    $niveau->setNameSection("masupersection");
    $niveau->setNiveau(42);
    $niveau->setSupprime("F");
    
    if($dao->supprimerNiveau($niveau) && $dao->ajouterNiveau($niveau)){
    
        $bundle =  new Bundle();
        
        $bundle=$useCase->getNiveau($niveau->getNiveau(),$niveau->getNameSection(),$bundle);
        
        if($bundle->get(Bundle::$OPERATION_REUSSIE)){
            
            echo("Récupération: OK\n");
            $niveau2=$bundle->get(Bundle::$NIVEAU);
            
            if($niveau2->getNiveau()!=$niveau->getNiveau())
                echo("Niveau échoué\n");
            else
                echo("Niveau: OK\n");
                
            if($niveau2->getLibelle()!=$niveau->getLibelle())
                echo("Libellé échoué\n");
            else
                echo("Libellé: OK\n");
            
            if($niveau2->getNameSection()!=$niveau->getNameSection())
                echo("NameSection échoué\n");
            else
                echo("NameSection: OK\n");
            
            if($niveau2->getLogo()!=$niveau->getLogo())
                echo("Logo échoué\n");
            else
                echo("Logo: OK\n");
                
            if($niveau2->getSupprime()!=$niveau->getSupprime())
                echo("Supprime échoué\n");
            else
                echo("Supprime: OK\n");
        }
        else
            echo("Récupération échouée");
    }
    else{
        echo("Récupération échouée");
    }
    
}
function testSupprimerNiveau(){

    $useCase=new GererNiveauImpl();
    $dao = DaoFactory::getInstance()->get("NiveauDao");
    
    $niveau=new Niveau();
    $niveau->setLibelle("test de niveau");
    $niveau->setLogo("logoLOL");
    $niveau->setNameSection("masupersection");
    $niveau->setNiveau(42);
    $niveau->setSupprime("F");
    
    $dao->supprimerNiveau($niveau);
    
    $dao->ajouterNiveau($niveau);
    
    $bundle =  new Bundle();
    $bundle->put(Bundle::$NIVEAU,$niveau);
    
    $bundle=$useCase->supprimerNiveau($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE))
        echo("Suppression: OK\n");
    else
        echo("Suppression échouée");
    
}
function testAjouter(){
    $useCase=new GererNiveauImpl();
    $dao = DaoFactory::getInstance()->get("NiveauDao");
    
    $niveau=new Niveau();
    $niveau->setLibelle("test de niveau");
    $niveau->setLogo("logoLOL");
    $niveau->setNameSection("masupersection");
    $niveau->setNiveau(42);
    $niveau->setSupprime("F");
    
    $dao->supprimerNiveau($niveau);
    
    $bundle =  new Bundle();
    $bundle->put(Bundle::$NIVEAU,$niveau);
    
    $bundle=$useCase->ajouterNiveau($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Ajout: OK\n");
        $niveau2=$dao->getNiveau($niveau->getNiveau(),$niveau->getNameSection());
        
        if($niveau2->getNiveau()!=$niveau->getNiveau())
            echo("Niveau échoué\n");
        else
            echo("Niveau: OK\n");
            
        if($niveau2->getLibelle()!=$niveau->getLibelle())
            echo("Libellé échoué\n");
        else
            echo("Libellé: OK\n");
        
        if($niveau2->getNameSection()!=$niveau->getNameSection())
            echo("NameSection échoué\n");
        else
            echo("NameSection: OK\n");
        
        if($niveau2->getLogo()!=$niveau->getLogo())
            echo("Logo échoué\n");
        else
            echo("Logo: OK\n");
            
        if($niveau2->getSupprime()!=$niveau->getSupprime())
            echo("Supprime échoué\n");
        else
            echo("Supprime: OK\n");
    }
    else{
        echo("Ajout échouée");
    }
}
function testModifier(){
    $useCase= new GererNiveauImpl();
    $dao=DaoFactory::getInstance()->get("NiveauDao");
    
    $niveau=new Niveau();
    $niveau->setLibelle("test de niveau");
    $niveau->setLogo("logoLOL");
    $niveau->setNameSection("masupersection");
    $niveau->setNiveau(42);
    $niveau->setSupprime("F");
    
    $dao->supprimerNiveau($niveau);
    $dao->ajouterNiveau($niveau);
    
    $niveau2=new Niveau();
    $niveau2->setLibelle("test de niveau lol");
    $niveau2->setLogo("logoLOLo");
    $niveau2->setNameSection("masupersection");
    $niveau2->setNiveau(42);
    $niveau2->setSupprime("F");
    
    $bundle= new Bundle();
    $bundle->put(Bundle::$NIVEAU,$niveau2);
    $bundle=$useCase->modifierNiveau($bundle);
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Modification: OK\n");
        $niveau=$dao->getNiveau($niveau2->getNiveau(),$niveau2->getNameSection());
        $niveau2=$bundle->get(Bundle::$NIVEAU);
            
        if($niveau2->getNiveau()!=$niveau->getNiveau())
            echo("Niveau échoué\n");
        else
            echo("Niveau: OK\n");
            
        if($niveau2->getLibelle()!=$niveau->getLibelle())
            echo("Libellé échoué\n");
        else
            echo("Libellé: OK\n");
        
        if($niveau2->getNameSection()!=$niveau->getNameSection())
            echo("NameSection échoué\n");
        else
            echo("NameSection: OK\n");
        
        if($niveau2->getLogo()!=$niveau->getLogo())
            echo("Logo échoué\n");
        else
            echo("Logo: OK\n");
            
        if($niveau2->getSupprime()!=$niveau->getSupprime())
            echo("Supprime échoué\n");
        else
            echo("Supprime: OK\n");
    }
    else{
        echo("Modification échouée\n");
    }
}
function testLister(){
    $useCase= new GererNiveauImpl();
    $dao=DaoFactory::getInstance()->get("NiveauDao");
    
    $niveau=new Niveau();
    $niveau->setLibelle("test de niveau");
    $niveau->setLogo("logoLOL");
    $niveau->setNameSection("masupersection");
    $niveau->setNiveau(42);
    $niveau->setSupprime("F");
    
    $bundle=new Bundle();
    
    $dao->supprimerNiveau($niveau);
    if($dao->ajouterNiveau($niveau)){
        echo("Ajout: OK\n");
        
        $bundle=$useCase->listerNiveau($bundle);
        $liste=$bundle->get(Bundle::$LISTE);
        if(count($liste)>=1){
            echo("Compte: OK\n");
            $i=0;
            $trouve=false;
            while($i<count($liste) && !$trouve){
                if($liste[$i]->getNiveau()==$niveau->getNiveau()
                   && $liste[$i]->getNameSection() == $niveau->getNameSection())$trouve=true;
                $i++;
            }
            if($trouve){
                echo("Listing OK\n");
                if($dao->supprimerNiveau($niveau)==1) echo("Nettoyage OK\n");
                else echo("Nettoyage échoué\n");
            }
            else{
                echo("Listing échoué\n");
            }
        }
        else{
            echo("Compte échoué\n");
        }
        
    }
    else{
        echo("Ajout échoué\n");
    }
    
    
}
testSupprimerNiveau();
testGetNiveau();
testAjouter();
testModifier();
testLister();
?>