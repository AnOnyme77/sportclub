<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function getObjetNiveau(){
    $niveau = new Niveau();
    $niveau->setNiveau(77);
    $niveau->setNameSection("anonyme");
    $niveau->setLibelle("fineau");
    $niveau->setLogo("Mabite");
    $niveau->setSupprime("F");
    
    return($niveau);
}
function ajouterNiveau(){
    $daoNiveau=DaoFactory::getInstance()->get("NiveauDao");
    $daoNiveau->ajouterNiveau(getObjetNiveau());
}
function supprimerNiveau(){
    $daoNiveau=DaoFactory::getInstance()->get("NiveauDao");
    $daoNiveau->supprimerNiveau(getObjetNiveau());
}

function competenceEgales($competence1,$competence2){
    if($competence1->getNiveau()==$competence2->getNiveau()){
        echo("Niveau: OK\n");
    }
    else{
        echo("Niveau différents");
    }
    
    if($competence1->getNameSection()==$competence2->getNameSection()){
        echo("NameSection: OK\n");
    }
    else{
        echo("NameSection différents");
    }
    if($competence1->getTitre()==$competence2->getTitre()){
        echo("Titre: OK\n");
    }
    else{
        echo("Titre différents");
    }
    if($competence1->getDetail()==$competence2->getDetail()){
        echo("Detail: OK\n");
    }
    else{
        echo("Detail différents");
    }
}

function testAjout(){
    $useCase=new GererCompetenceImpl();
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $competence=new Competence();
    $competence->setNiveau(77);
    $competence->setNameSection("anonyme");
    $competence->setDetail("faire le fineau");
    $competence->setTitre("fineauder");
    
    $daoCompetence->supprimerCompetence($competence);
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$COMPETENCE,$competence);
    
    $useCase->ajouterCompetence($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Ajout: OK\n");
        $competenceRetour=$daoCompetence->getCompetence($competence);
        if($competenceRetour!=false){
            competenceEgales($competence,$competenceRetour);
        }
        else{
            echo("Aucune compétence retournée\n");
        }
    }
    else{
        echo(Bundle::$MESSAGE);
    }
    
}

function testModifier(){
    $useCase=new GererCompetenceImpl();
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $competence=new Competence();
    $competence->setNiveau(77);
    $competence->setNameSection("anonyme");
    $competence->setDetail("faire le fineau comme un truand");
    $competence->setTitre("fineauder");
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$COMPETENCE,$competence);
    
    $useCase->modifierCompetence($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Modification: OK\n");
        $competenceRetour=$daoCompetence->getCompetence($competence);
        if($competenceRetour!=false){
            competenceEgales($competence,$competenceRetour);
        }
        else{
            echo("Aucune compétence retournée\n");
        }
    }
    else{
        echo(Bundle::$MESSAGE);
    }
}

function testRecuperation(){
    $useCase=new GererCompetenceImpl();
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $competence=new Competence();
    $competence->setNiveau(77);
    $competence->setNameSection("anonyme");
    $competence->setDetail("faire le fineau comme un truand");
    $competence->setTitre("fineauder");
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$COMPETENCE,$competence);
    
    $useCase->getCompetence($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Récupération: OK\n");
        $competenceRetour=$daoCompetence->getCompetence($competence);
        if($competenceRetour!=false){
            competenceEgales($competence,$competenceRetour);
        }
        else{
            echo("Aucune compétence retournée\n");
        }
    }
    else{
        echo(Bundle::$MESSAGE);
    }
}
function testGetFromNiveau(){
    $useCase=new GererCompetenceImpl();
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $niveau=new Niveau();
    $niveau->setNiveau(77);
    $niveau->setNameSection("anonyme");
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$NIVEAU,$niveau);
    
    $useCase->getCompetenceFromNiveau($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Récupération: OK\n");
        $liste=$bundle->get(Bundle::$LISTE);
        foreach($liste as $competence){
            $competenceRetour=$daoCompetence->getCompetence($competence);
            if($competenceRetour!=false){
                competenceEgales($competence,$competenceRetour);
            }
            else{
                echo("Aucune compétence retournée\n");
            }
        }
    }
    else{
        echo(Bundle::$MESSAGE);
    }
}
function testLister(){
    $useCase=new GererCompetenceImpl();
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $bundle=new Bundle();
    
    $useCase->listerCompetence($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Listing: OK\n");
        $liste=$bundle->get(Bundle::$LISTE);
        
        foreach($liste as $competence){
            $competenceRetour=$daoCompetence->getCompetence($competence);
            if($competenceRetour!=false){
                competenceEgales($competence,$competenceRetour);
            }
            else{
                echo("Aucune compétence retournée\n");
            }
        }
    }
    else{
        echo(Bundle::$MESSAGE);
    }
}

function testSupprimer(){
    $useCase=new GererCompetenceImpl();
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $competence=new Competence();
    $competence->setNiveau(77);
    $competence->setNameSection("anonyme");
    $competence->setDetail("faire le fineau comme un truand");
    $competence->setTitre("fineauder");
    
    $bundle=new Bundle();
    $bundle->put(Bundle::$COMPETENCE,$competence);
    
    $useCase->supprimerCompetence($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        echo("Suppression: OK\n");
    }
    else{
        echo(Bundle::$MESSAGE);
    }
}
ajouterNiveau();
testAjout();
testModifier();
testRecuperation();
testGetFromNiveau();
testLister();
testSupprimer();
supprimerNiveau();
?>