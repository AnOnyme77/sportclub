<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Jérôme Hainaut
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

echo("test MembreDaoImpl OK");

function verifCheckLogin()
{
	//Dao membre
	$membreDao = DaoFactory::getInstance()->get("MembreDao");
	
	//Création d'un membre (merci jerome)
	$membre1= new Membre(null);
	$membre1->setAdresse(new Adresse("rue Bois Du Prince 131","5640","Mettet","Belgique"));
	$membre1->setDateInscription(new DateTime("2014-03-05 06:06:06"));
	$membre1->setEmail("isanzo@me.com");
	$membre1->setIdMembre("9234567890249");
	$membre1->setNom("iSanzo");
	$membre1->setPrenom("iSanzo");
	$membre1->setSexe("M");
	$membre1->setDateNaissance(new DateTime("1991-02-11"));
	$membre1->setFonction("M");
	$membre1->setMdp("LOL");
	$membre1->setLogo("nean");
	$membre1->setSupprime("F");
	$membre1->setStatut("T");
	
	$messagePerso = "";
	
	//Vérification de l'ajout du membre, Si réussi on passe au login.
	echo "------- OK -> Test pour checkLogin\n";
	if($membreDao->ajouterMembre($membre1))
	{
		echo "Ajout OK \n";
		$authentifier = new Authentifier($membre1->getEmail(),$membre1->getMdp());
		$membre2 = $membreDao->checkLogin($authentifier);
		verifMemeMembre($membre1, $membre2, "verif checkLogin : ");
		
		//Suppression du membre1
		if($membreDao->supprimerMembre($membre1->getIdMembre()))
			echo "Suppression OK\n";
		else
			echo "Suppression fail\n";
		
	}
	else
	    echo "Ajout fail\n";
	    
	
	
	
}	

function verifMemeMembre($membre1, $membre2, $messagePerso){
	if($membre1->getAdresse()->getRue() == $membre2->getAdresse()->getRue())
		echo($messagePerso."getRue OK \n");
	else
		echo($messagePerso."getRue fail \n");

	if($membre1->getAdresse()->getPays() == $membre2->getAdresse()->getPays())
		echo($messagePerso."getPays OK \n");
	else
		echo($messagePerso."getPays fail \n");

	if($membre1->getAdresse()->getCodePostale() == $membre2->getAdresse()->getCodePostale())
		echo($messagePerso."getCodePostale OK \n");
	else
		echo($messagePerso."getCodePostale fail \n");

	if($membre1->getAdresse()->getVille() == $membre2->getAdresse()->getVille())
		echo($messagePerso."getVille OK \n");
	else
		echo($messagePerso."getVille fail \n");

	if($membre1->getDateInscription() == $membre2->getDateInscription())
		echo($messagePerso."getDateInscription OK \n");
	else
		echo($messagePerso."getDateInscription fail \n");

	if($membre1->getEmail() == $membre2->getEmail())
		echo($messagePerso."getEmail OK \n");
	else
		echo($messagePerso."getEmail fail \n");

	if($membre1->getIdMembre() == $membre2->getIdMembre())
		echo($messagePerso."getIdMembre OK \n");
	else
		echo($messagePerso."getIdMembre fail \n");

	if($membre1->getNom() == $membre2->getNom())
		echo($messagePerso."getNom OK \n");
	else
		echo($messagePerso."getNom fail \n");

	if($membre1->getPrenom() == $membre2->getPrenom())
		echo($messagePerso."getPrenom OK \n");
	else
		echo($messagePerso."getPrenom fail \n");

	if($membre1->getSexe() == $membre2->getSexe())
		echo($messagePerso."getSexe OK \n");
	else
		echo($messagePerso."getSexe fail \n");

	if($membre1->getDateNaissance() == $membre2->getDateNaissance())
		echo($messagePerso."getDateNaissance OK \n");
	else
		echo($messagePerso."getDateNaissance fail \n");

	if($membre1->getFonction() == $membre2->getFonction())
		echo($messagePerso."getFonction OK \n");
	else
		echo($messagePerso."getFonction fail \n");

	if($membre1->getMdp() == $membre2->getMdp())
		echo($messagePerso."getMdp OK \n");
	else
		echo($messagePerso."getMdp fail \n");

	if($membre1->getLogo() == $membre2->getLogo())
		echo($messagePerso."getLogo OK \n");
	else
		echo($messagePerso."getLogo fail \n");

	if($membre1->getSupprime() == $membre2->getSupprime())
		echo($messagePerso."getSupprime OK \n");
	else
		echo($messagePerso."getSupprime fail \n");
	
	if($membre1->getStatut() == $membre2->getStatut())
		echo($messagePerso."getStatut OK \n");
	else
		echo($messagePerso."getStatut fail \n");
}


date_default_timezone_set('Europe/Paris');//pq faut-il faire ca???

$membreDao = DaoFactory::getInstance()->get("MembreDao");

$membresAjouter = array();
for($i=0;$i<5;$i++){
//ajout de 5 membres
	$membre= new Membre(null);
	$membre->setAdresse(new Adresse("rueduTest","cpduTest","belleVille","Belgique"));
	$membre->setDateInscription(new DateTime("2011-03-05 06:06:06"));
	$membre->setEmail("monemail".$i."@mail.mail");
	$membre->setIdMembre("1234567890"+$i);
	$membre->setNom("nom".$i);
	$membre->setPrenom("testPrénom".$i);
	$membre->setSexe("M");
	$membre->setDateNaissance(new DateTime("1990-02-11"));
	$membre->setFonction("M");
	$membre->setMdp("mdp".$i);
	$membre->setLogo("chemin/logo");
	$membre->setSupprime("F");
	$membre->setStatut("F");
	
	$membresAjouter[$i]=$membre;
	if($membreDao->ajouterMembre($membre)){
		echo("membre: ".$membre->getIdMembre()." ajoutée OK \n");
	}
	else{
		echo("membre: ".$membre->getIdMembre()." existe déjà OK \n");
	}
}


$membre1=$membreDao->getMembre("1234567892");
verifMemeMembre($membre1, $membresAjouter[2], "getMembre ");

// modification
//$membresAjouter[2]->setDateInscription(new DateTime("2014-02-05 06:06:06"));
$membresAjouter[2]->setEmail("sgsfdgrsgl@mail.mail");
$membresAjouter[2]->setNom("tornom1");
$membresAjouter[2]->setPrenom("coucou");
$membresAjouter[2]->setSexe("M");
$membresAjouter[2]->setDateNaissance(new DateTime("1991-02-11"));
$membresAjouter[2]->setFonction("M");
$membresAjouter[2]->setMdp("mdptrop");
$membresAjouter[2]->setLogo("chemin/logoTroll");
$membresAjouter[2]->setSupprime("F");
$membresAjouter[2]->setStatut("F");

if($membreDao->modifierMembre($membresAjouter[2])){
	echo("membre modifier OK");
}
else{
	echo("membre non modifier");
}

$membre1=$membreDao->getMembre("1234567892");
verifMemeMembre($membre1, $membresAjouter[2], "ModifierMembre ");//

$membre1=$membreDao->getMembreEmail("sgsfdgrsgl@mail.mail");
verifMemeMembre($membre1, $membresAjouter[2], "getMembreEmail ");

/*
$membres = $membreDao->listerMembre();//lister les membres
for($i=0;$i<5;$i++){
	verifMemeMembre($membres[$i], $membresAjouter[$i], "listerMembre membre".$i." ");

}*/
			

if($membreDao->supprimerMembre("1234567893")){
	echo("membre: ".$membre->getIdMembre()." supprimer OK \n");
}
else{
	echo("membre: ".$membre->getIdMembre()." déjà supprimer OK \n");
}

verifCheckLogin();

// affichage le dernier membre 
$membre1=$membreDao->getLastMembre();
verifMemeMembre($membre1, $membreDao->getMembre("9234567890249"), "getLastMembre ");
	

for ( $i=0; $i < 5; $i++ ) {
	$membreDao->supprimerPhysMembre("1234567890"+$i);	
}      
$membreDao->supprimerPhysMembre("9234567890249");
