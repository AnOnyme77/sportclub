<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Hainaut
 * Team : Dev4u
 * créé le 22/04/2014 - modifée le 22/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function getObjetMembre($i){
    date_default_timezone_set('Europe/Paris');
    $membre = new Membre();
    $membre->setAdresse(new Adresse("rue","5600","ville","Belgique"));
    $membre->setDateInscription(new DateTime("2012-03-07 06:07:06"));
    $membre->setEmail("membreExamen".$i."@gmail.com");
    $membre->setNom("Membre".$i);
    $membre->setPrenom("Prenom".$i);
    $membre->setSexe("M");
    $membre->setDateNaissance(new DateTime("1990-11-02"));
    $membre->setFonction("M");
    $membre->setMdp("superMotDePasse");
    $membre->setLogo("image/toto.jpg");
    $membre->setSupprime("F");
    $membre->setStatut("F");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $membre);
    $gererMembre->ajouterMembre($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "membreExamen".$i."@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        $membre=null;
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
            $membre=$bundle->get(Bundle::$MEMBRE);
        else
            echo($bundle->get(Bundle::$MESSAGE));
    }
    else
        echo($bundle->get(Bundle::$MESSAGE));
    
    return($membre);
}

function supprimerMembre($membre)
{
    date_default_timezone_set('Europe/Paris');

    $daoMem = DaoFactory::getInstance()->get("MembreDao");

    if($daoMem->supprimerPhysMembre($membre->getIdMembre()))
        echo "Membre Supp : OK\n";
}


function testListing(){
  
    $examen=new Examen();
    $dao=DaoFactory::getInstance()->get("ExamenDao");
    
    $liste=$dao->listerExamen();
    if($liste!=null){
        echo("Récupération de la liste: OK\n");
    }
    else{
        echo("Récupération de la liste échouée\n");
    }
}
function getAjoutNiveau(){
    $niveau=new Niveau();
    
    $niveau->setNiveau("1");
    $niveau->setLibelle("testBleu");
    $niveau->setNameSection("orange");
    $niveau->setLogo("caca");
    $niveau->setSupprime("V");
    
    $dao=new NiveauDaoImpl();
    $dao->supprimerNiveau($niveau);
    if($dao->ajouterNiveau($niveau)){
        echo("Ajout: OK\n");
    }
    else{
        echo("Ajout: échoué\n");
    }
    return $niveau;
}

function testSuppressionNiveau(){
    $niveau=new Niveau();
    
    $niveau->setNiveau("1");
    $niveau->setLibelle("testBleu");
    $niveau->setNameSection("orange");
    $niveau->setLogo("caca");
    $niveau->setSupprime("V");
    
    $dao=new NiveauDaoImpl();
    if($dao->supprimerNiveau($niveau)==1){
        echo("Suppression: OK\n");
    }
    else{
        echo("Suppression: échouée\n");
    }
}

function testAjout($membre1, $membre2, $niveau){
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("un comentaire inutil");
    $examen->setCote(20);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
    
    $dao=new ExamenDaoImpl();
    $dao->supprimerExamen($examen);
    if($dao->ajouterExamen($examen)){
        echo("Ajout: OK\n");
    }
    else{
        echo("Ajout: échoué\n");
    }
}

function testSuppression($membre1, $membre2, $niveau){
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("un comentaire inutil");
    $examen->setCote(20);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
    
    $dao=new ExamenDaoImpl();
    if($dao->supprimerExamen($examen)){
        echo("Suppression: OK\n");
    }
    else{
        echo("Suppression: échouée\n");
    }
}
function testRecuperationExamen($membre1, $membre2, $niveau){
    $ok=true;
    $examen=new Examen();
    
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    
    $dao=new ExamenDaoImpl();
    //$dao->supprimerExamen($examen);
    //$dao->ajouterExamen($examen);
    $listeRetour=$dao->getExamenMembre($examen);
    
    if(is_array($listeRetour)){
        foreach($listeRetour as $retour){
            if($retour->getNameSection()!=$examen->getNameSection())
                $ok=false;
            
            if($retour->getNiveau()!=$examen->getNiveau())
                $ok=false;
            
            if($retour->getIdMembre()!=$examen->getIdMembre())
                $ok=false;
            
            if($ok){
                echo("Récupération liste: OK\n");
            }
            else{
                echo("Récupération liste échouée\n");
            }
        }
    }
    else{
        echo("Récupération échouée\n");
    }
}
function testRecuperation($membre1, $membre2, $niveau){
    $ok=true;
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("un comentaire inutil");
    $examen->setCote(20);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
    
    $dao=new ExamenDaoImpl();
    $dao->supprimerExamen($examen);
    $dao->ajouterExamen($examen);
    $retour=$dao->getExamen($examen);
    
    if($retour!=null){
        if($retour->getDatePassage()!=$examen->getDatePassage())
            $ok=false;
            
        if($retour->getCommentaire()!=$examen->getCommentaire())
            $ok=false;
        
        if($retour->getNameSection()!=$examen->getNameSection())
            $ok=false;
        
        if($retour->getCote()!=$examen->getCote())
            $ok=false;
        
        if($retour->getNiveau()!=$examen->getNiveau())
            $ok=false;
        
        if($retour->getIdMembre()!=$examen->getIdMembre())
            $ok=false;
        
        if($retour->getIdProf()!=$examen->getIdProf())
            $ok=false;
        
        if($ok){
            echo("Récupération: OK\n");
        }
        else{
            echo("Récupération échouée\n");
        }
    }
    else{
        echo("Récupération échouée\n");
    }
}
function testModification($membre1, $membre2, $niveau){
    $ok=true;
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("un comentaire inutil");
    $examen->setCote(20);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
    
    $dao=new ExamenDaoImpl();
    $dao->supprimerExamen($examen);
    $dao->ajouterExamen($examen);
    
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("modifier");
    $examen->setCote(7);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
    
    if($dao->modifierExamen($examen)){
    	$retour=$dao->getExamen($examen);
        if($retour->getCote()!=7)
            $ok=false;
            
        if($retour->getCommentaire()!="modifier")
            $ok=false;
          
        if($ok){
            echo("Modification: OK\n");
        }
        else{
            echo("Modification échouée\n");
        }
    }
    else{
        echo("Modification échouée\n");
    }
}
function testRecherche($membre1, $membre2, $niveau){
    $ok=false;
    $examen=new Examen();
    
    $examen->setDatePassage("2013-02-11");
    $examen->setCommentaire("un comentaire inutil");
    $examen->setCote(20);
    $examen->setNiveau($niveau->getNiveau());
    $examen->setNameSection($niveau->getNameSection());
    $examen->setIdMembre($membre1);
    $examen->setIdProf($membre2);
    
    $dao=new ExamenDaoImpl();
    
    $retour=$dao->rechercherExamen($examen);
    
    if($retour!=null){
        echo("Recherche OK\n");
        foreach($retour as $examen2){
            if($examen->getDatePassage()==$examen2->getDatePassage() and
               $examen->getCommentaire()==$examen2->getCommentaire() and
           	   $examen->getIdMembre()==$examen2->getIdMembre() and
               $examen->getIdProf()==$examen2->getIdProf() and
               $examen->getCote()==$examen2->getCote() and
               $examen->getNameSection()==$examen2->getNameSection() and
               $examen->getNiveau()==$examen2->getNiveau()) $ok=true;
        }
        if($ok) echo("Confirmation OK\n");
        else echo("Confirmation échouée");
    }
    else{
        echo("Recherche infructueuse\n");
    }
}
$membre1 = getObjetMembre(1);
$membre2 = getObjetMembre(2);
$niveau = getAjoutNiveau();
testAjout($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testListing();
testRecuperation($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testRecuperationExamen($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testRecherche($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testModification($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testSuppression($membre1->getIdMembre(), $membre2->getIdMembre(), $niveau);
testSuppressionNiveau();
supprimerMembre($membre1);
supprimerMembre($membre2);
?>