<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

date_default_timezone_set('Europe/Paris');


/*
*   Fonctions pour les tests :
*/
function checkTypeAbo($typeAbo1,$typeAbo2){
    if($typeAbo1->getLibelle()==$typeAbo2->getLibelle())
        echo("Libellés Type Abonnement: OK\n");
    else
        echo("Libellés Type Abonnement différents\n");
    
    if($typeAbo1->getPrix()==$typeAbo2->getPrix())
        echo("Prix Type Abonnement: OK\n");
    else
        echo("Prix Type Abonnement différents\n");

    if($typeAbo1->getDuree()==$typeAbo2->getDuree())
        echo("Durées Type Abonnement: OK\n");
    else
        echo("Durées Type Abonnement différentes\n");

    if($typeAbo1->getNbUnite()==$typeAbo2->getNbUnite())
        echo("NbUnite Type Abonnement: OK\n");
    else
        echo("NbUnite Type Abonnement différents\n");

    if($typeAbo1->getCours()==$typeAbo2->getCours())
        echo("Cours Type Abonnement: OK\n");
    else
        echo("Cours Type Abonnement différents\n");

    if($typeAbo1->getSupprime()==$typeAbo2->getSupprime())
        echo("Supprimés Type Abonnement: OK\n");
    else
        echo("Supprimés Type Abonnement différents\n");

}

function testGetTypeAbonnement()
{
    $dao= DaoFactory::getInstance()->get("TypeAbonnementDao");

    $typeAbo = new TypeAbonnement();
    $typeAbo = $dao->getTypeAbonnement("Abonnement 1an");

    if($typeAbo != null)
        echo("Liste d'un type d'abonnement : OK\n");
    else
        echo("Liste d'un type d'abonnement: échoué\n");
}

function testListerTypesAbonnement()
{
    $typeAbo = new TypeAbonnement();
    $dao= DaoFactory::getInstance()->get("TypeAbonnementDao");

    $listeTypeAbo = $dao->listerTypesAbonnement();
    if($listeTypeAbo!=null){
        echo("Récupération de la liste: OK\n");
        foreach($listeTypeAbo as $typeAboRetour){
            $typeAbo->setLibelle($typeAboRetour->getLibelle());
            $typeAbo=$dao->getTypeAbonnement($typeAbo->getLibelle());
            if($typeAbo!=false){
                echo("Récupération du type d'abonnement OK\n");
                checkTypeAbo($typeAbo,$typeAboRetour);
            }
            else{
                echo("Récupération du type d'abonnement échouée\n");
            }
        }
    }
    else{
        echo("Récupération de la liste échouée\n");
    }
}

function testAjouterTypeAbonnement()
{
    $typeAbo = new TypeAbonnement();
    $dao = DaoFactory::getInstance()->get("TypeAbonnementDao");

    $typeAbo->setLibelle("Abonnement 6mois");
    $typeAbo->setPrix(162.0);
    $typeAbo->setDuree(182);
    $typeAbo->setNbUnite(null);
    $typeAbo->setCours("Cours de spinning");
    $typeAbo->setSupprime("F");

    if($dao->ajouterTypeAbonnement($typeAbo))
        echo "Ajout du type d'abonnement OK\n";
    else
        echo "Ajout du type d'abonnement échoué\n";
}

function testSupprimerPhysTypeAbonnement()
{
    $typeAbo = new TypeAbonnement();
    $dao = DaoFactory::getInstance()->get("TypeAbonnementDao");

    $typeAbo->setLibelle("Abonnement 6mois");
    $typeAbo->setPrix(162.0);
    $typeAbo->setDuree(182);
    $typeAbo->setNbUnite(null);
    $typeAbo->setCours("Cours de spinning");
    $typeAbo->setSupprime("F");

    if($dao->supprimerPhysTypeAbonnement($typeAbo))
        echo "Suppression du type d'abonnement OK\n";
    else
        echo "Suppression du type d'abonnement échoué\n";
}

function testSupprimerTypeAbonnement()
{
    $typeAbo = new TypeAbonnement();
    $dao = DaoFactory::getInstance()->get("TypeAbonnementDao");

    $typeAbo->setLibelle("Abonnement 6mois");
    $typeAbo->setPrix(162.0);
    $typeAbo->setDuree(182);
    $typeAbo->setNbUnite(null);
    $typeAbo->setCours("Cours de spinning");
    $typeAbo->setSupprime("F");

    if($dao->supprimerTypeAbonnement($typeAbo))
        echo "Suppression logique du type d'abonnement OK\n";
    else
        echo "Suppression logique du type d'abonnement échoué\n";
}

function testModifierTypeAbonnement()
{
    $typeAbo = new TypeAbonnement();
    $dao = DaoFactory::getInstance()->get("TypeAbonnementDao");

    $typeAbo->setLibelle("Abonnement 6mois");
    $typeAbo->setPrix(162.0);
    $typeAbo->setDuree(182);
    $typeAbo->setNbUnite(null);
    $typeAbo->setCours("Cours de Yoga, Cours de Spinning");
    $typeAbo->setSupprime("F");

    if($dao->modifierTypeAbonnement($typeAbo))
        echo "Modification du type d'abonnement OK\n";
    else
        echo "Modification du type d'abonnement échoué\n";
}

/*
*   Début des tests :
*/
echo "----- Debut des tests TypeAbonnementDao OK -----\n";
testGetTypeAbonnement();
testListerTypesAbonnement();
testAjouterTypeAbonnement();
testModifierTypeAbonnement();
testSupprimerTypeAbonnement();
testSupprimerPhysTypeAbonnement();
echo "----- Fin des tests TypeAbonnementDao OK -----\n";

?>