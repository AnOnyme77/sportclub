<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function supprimerAnonyme($membre)
{
    date_default_timezone_set('Europe/Paris');

    $daoMem = DaoFactory::getInstance()->get("MembreDao");

    if($daoMem->supprimerPhysMembre($membre->getIdMembre()))
        echo "Anonyme Supp : OK\n";
}

function getObjetAnonyme(){
    date_default_timezone_set('Europe/Paris');
    $mem = new Membre();
    $mem->setAdresse(new Adresse("Bois Du Prince 131","5640","Mettet","Belgique"));
    $mem->setDateInscription(new DateTime("2011-03-05 06:06:06"));
    $mem->setEmail("anonyme77@gmail.com");
    $mem->setNom("Jamar");
    $mem->setPrenom("Frederic");
    $mem->setSexe("M");
    $mem->setDateNaissance(new DateTime("1993-05-15"));
    $mem->setFonction("M");
    $mem->setMdp("jesuisfred");
    $mem->setLogo("chemin/isanzo.jpg");
    $mem->setSupprime("F");
    $mem->setStatut("F");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $mem);
    $gererMembre->ajouterMembre($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "anonyme77@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        $membre=null;
        if($bundle->get(Bundle::$OPERATION_REUSSIE)){
            $membre=$bundle->get(Bundle::$MEMBRE);
            echo("Création du membre OK\n");
        }
        else
            echo($bundle->get(Bundle::$MESSAGE));
    }
    else
        echo($bundle->get(Bundle::$MESSAGE));
    
    return($membre);
}
function getObjectSeance($membre){
    $daoSeance=DaoFactory::getInstance()->get("SeanceDao");
    
    $seance=new Seance();
    $seance->setCours("Jujutsu");
    $seance->setDateCours("1993-05-15");
    $seance->setIdProf($membre->getIdMembre());
    $seance->setNbMembreMax(15);
    $seance->setNoSeance(20);
    
    $idSeance=$daoSeance->ajouterSeance($seance);
    if($idSeance){
        echo("Ajout de la seance OK\n");
    }
    else{
        echo("Ajout de la seance echoue\n");
    }
    return($idSeance);
}
function supprimerSeance($idSeance){
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    $seance=new Seance();
    $seance->setNoSeance($idSeance);
    if($dao->supprimerSeance($seance)){
        echo("Suppression Seance OK\n");
    }
    else{
        echo("Suppression de la seance echoue\n");
    }
}
function getObjetInscription($membre,$idSeance){
    $inscription=new Inscription();
    $inscription->setNoSeance($idSeance);
    $inscription->setIdmembre($membre->getIdMembre());
    $inscription->setPresent('F');
    return($inscription);
}

function inscriptionEgales($inscription1, $inscription2){
    if($inscription1->getNoSeance()==$inscription2->getNoSeance()){
        echo("Numéro seance OK\n");
    }
    else{
        echo("Numéro de séance différents\n");
    }
    if($inscription1->getIdMembre()==$inscription2->getIdMembre()){
        echo("IdMembre OK\n");
    }
    else{
        echo("IdMembre différents\n");
    }
    if($inscription1->getPresent()==$inscription2->getPresent()){
        echo("Presence OK\n");
    }
    else{
        echo("Presence différents\n");
    }
}

function testAjout($inscription){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    if($dao->ajouterInscription($inscription)){
        echo("Ajout OK\n");
        $inscriptionRetour=$dao->getInscription($inscription);
        if($inscriptionRetour!=false){
            echo("Inscription Retour OK\n");
            inscriptionEgales($inscription,$inscriptionRetour);
        }
        else{
            echo("Inscription retour échouée");
        }
    }
    else{
        echo("Ajout échoué\n");
    }
}
function testGet($inscription){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $inscriptionRetour=$dao->getInscription($inscription);
    if($inscriptionRetour!=false){
        echo("Récupération OK\n");
        inscriptionEgales($inscription,$inscriptionRetour);
    }
    else{
        echo("Récupération échouée\n");
    }
}
function testSupprimer($inscription){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    if($dao->supprimerInscription($inscription)){
        echo("Suppression OK\n");
        $inscriptionRetour=$dao->getInscription($inscription);
        if($inscriptionRetour==false){
            echo("Inscription Retour OK\n");
        }
        else{
            echo("Inscription retour échouée");
        }
    }
    else{
        echo("Suppression échoué\n");
    }
}
function testModifier($inscription){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $inscription->setPresent("T");
    if($dao->modifierInscription($inscription)){
        echo("Modification OK\n");
        $inscriptionRetour=$dao->getInscription($inscription);
        if($inscriptionRetour!=false){
            echo("Inscription Retour OK\n");
            inscriptionEgales($inscription,$inscriptionRetour);
        }
        else{
            echo("Inscription retour échouée");
        }
    }
    else{
        echo("Modification échoué\n");
    }
}

function testLister(){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $retour=$dao->listerInscription();
    if($retour!=false){
        echo("Listing OK\n");
        foreach($retour as $inscription){
            $inscriptionRetour=$dao->getInscription($inscription);
            if($inscriptionRetour!=false){
                echo("Inscription Retour OK\n");
                inscriptionEgales($inscription,$inscriptionRetour);
            }
            else{
                echo("Inscription retour échouée");
            }
        }
    }
    else{
        echo("Listing échoué\n");
    }
}

function testGetNombre($idSeance){
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    
    $seance=new Seance();
    $seance->setNoSeance($idSeance);
    $retour=$dao->getNombre($seance);
    if($retour!=false and is_numeric($retour)){
        echo("Nombre OK\n");
    }
    else{
        echo("Nombre échoué\n");
    }
}

function testGetFromSeance($idSeance){
    $seance = new Seance();
    $seance->setNoSeance($idSeance);
    $dao=DaoFactory::getInstance()->get("InscriptionDao");
    $liste=$dao->getFromSeance($seance);
    if(is_array($liste)){
        echo("Récupération liste OK\n");
    }
    else{
        echo("Récupération liste échouée\n");
    }
}
$membre=getObjetAnonyme();
$idSeance=getObjectSeance($membre);

$inscription=getObjetInscription($membre,$idSeance);
testAjout($inscription);
testGet($inscription);
testGetNombre($idSeance);
testGetFromSeance($idSeance);
testModifier($inscription);
testLister();
testSupprimer($inscription);

supprimerSeance($idSeance);
supprimerAnonyme($membre)
?>