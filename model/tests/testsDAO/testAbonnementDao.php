<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

date_default_timezone_set('Europe/Paris');

function checkAbo($abo1,$abo2)
{
    if($abo1->getIdMembre()==$abo2->getIdMembre())
        echo("idMembre de l'abonnement: OK\n");
    else
        echo("idMembre de l'abonnement différents\n");

    if($abo1->getLibelle()==$abo2->getLibelle())
        echo("Libellés de l'abonnement: OK\n");
    else
        echo("Libellés de l'abonnement différents\n");
    
    if($abo1->getDateDebut()==$abo2->getDateDebut())
        echo("Dates Début de l'abonnement: OK\n");
    else
        echo("Dates Début de l'abonnement différents\n");

    if($abo1->getDateAchat()==$abo2->getDateAchat())
        echo("Dates d'achat de l'abonnement: OK\n");
    else
        echo("Dates d'achat de l'abonnement différentes\n");

    if($abo1->getNbUniteRest()==$abo2->getNbUniteRest())
        echo("NbUniteRest de l'abonnement: OK\n");
    else
        echo("NbUniteRest de l'abonnement différents\n");

    if($abo1->getPrix()==$abo2->getPrix())
        echo("Prix de l'abonnement: OK\n");
    else
        echo("Prix de l'abonnement différents\n");

    if($abo1->getDateFin()==$abo2->getDateFin())
        echo("Dates de fin de l'abonnement: OK\n");
    else
        echo("Dates de fin de l'abonnement différents\n");
}

function testGetAbonnement($membre)
{
    $dao= DaoFactory::getInstance()->get("AbonnementDao");

    if($dao->getAbonnement($membre->getIdMembre(), "Abonnement 1an", "2014-01-01 10:00:00"))
    {
        echo("Liste d'un abonnement : OK\n");
    }
    else
    {
        echo("Liste d'un abonnement: échoué\n");
    }
}

function testListerAbonnements($membre)
{
    $abo = new Abonnement();
    $dao= DaoFactory::getInstance()->get("AbonnementDao");

    $listeAbo = $dao->listerAbonnements();
    if($listeAbo!=false){
        echo("Récupération de la liste: OK\n");
        foreach($listeAbo as $aboRetour){
            $abo->setLibelle($aboRetour->getLibelle());
            $abo->setIdMembre($membre->getIdMembre());
            $abo->setDateAchat($aboRetour->getDateAchat());
            $abo=$dao->getAbonnement($abo->getIdMembre(),$abo->getLibelle(),$abo->getDateAchat());
            if($abo!=false){
                echo("Récupération d'abonnement OK\n");
                checkAbo($abo,$aboRetour);
            }
            else{
                echo("Récupération d'abonnement échouée\n");
            }
        }
    }
    else{
        echo("Récupération de la liste des abonnements échouée\n");
    }
}

function testAjouterAbonnement($membre)
{
    $dao= DaoFactory::getInstance()->get("AbonnementDao");
    $abo = new Abonnement();
    
    $abo->setIdMembre($membre->getIdMembre());
    $abo->setLibelle("Abonnement 1an");
    $abo->setDateDebut(new DateTime("2014-01-01"));
    $abo->setDateAchat(new DateTime("2014-01-01 10:00:00"));
    $abo->setNbUniteRest(null);
    $abo->setPrix(284.3);
    $abo->setDateFin(new DateTime("2015-01-01"));
    
    if($dao->ajouterAbonnement($abo))
    {
        echo("Ajout Abonnement: OK\n");
    }
    else
    {
        echo("Ajout Abonnement: échoué\n");
    }
}

function testModifierAbonnement($membre)
{
    $dao= DaoFactory::getInstance()->get("AbonnementDao");
    $abo = new Abonnement();

    $abo->setIdMembre($membre->getIdMembre());
    $abo->setLibelle("Abonnement 1an");
    $abo->setDateDebut(new DateTime("2014-01-01"));
    $abo->setDateAchat(new DateTime("2014-01-01 10:00:00"));
    $abo->setNbUniteRest(null);
    $abo->setPrix(270.3);
    $abo->setDateFin(new DateTime("2015-01-01"));
    
    if($dao->modifierAbonnement($abo))
    {
        echo("Modification Abonnement: OK\n");
    }
    else
    {
        echo("Modification Abonnement: échoué\n");
    }
}

function testGetAbonnementMembre($membre)
{
    $dao= DaoFactory::getInstance()->get("AbonnementDao");

    if($dao->getAbonnementsMembre($membre->getIdMembre()))
    {
        echo("Liste Abonnements d'un membre: OK\n");
    }
    else
    {
        echo("Liste Abonnements d'un membre: échoué\n");
    }
}

function testSupprimerAbonnement($membre)
{
    $abo=new Abonnement();
    
    $abo->setIdMembre($membre->getIdMembre());
    $abo->setLibelle("Abonnement 1an");
    
    $dao=new AbonnementDaoImpl();
    if($dao->supprimerAbonnement($abo)==1){
        echo("Suppression: OK\n");
    }
    else{
        echo("Suppression: échouée\n");
    }
}

/* ----- OBJET MEMBRE SANZO POUR LE TEST -----*/

function supprimerAnonyme($membre)
{
    date_default_timezone_set('Europe/Paris');

    $daoMem = DaoFactory::getInstance()->get("MembreDao");

    if($daoMem->supprimerPhysMembre($membre->getIdMembre()))
        echo "Anonyme Supp : OK\n";
}

function getObjetAnonyme(){
    date_default_timezone_set('Europe/Paris');
    $mem = new Membre();
    $mem->setAdresse(new Adresse("Bois Du Prince 131","5640","Mettet","Belgique"));
    $mem->setDateInscription(new DateTime("2011-03-05 06:06:06"));
    $mem->setEmail("anonyme77@gmail.com");
    $mem->setNom("Jamar");
    $mem->setPrenom("Frederic");
    $mem->setSexe("M");
    $mem->setDateNaissance(new DateTime("1993-05-15"));
    $mem->setFonction("M");
    $mem->setMdp("jesuisfred");
    $mem->setLogo("chemin/isanzo.jpg");
    $mem->setSupprime("F");
    $mem->setStatut("F");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $mem);
    $gererMembre->ajouterMembre($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "anonyme77@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        $membre=null;
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
            $membre=$bundle->get(Bundle::$MEMBRE);
        else
            echo($bundle->get(Bundle::$MESSAGE));
    }
    else
        echo($bundle->get(Bundle::$MESSAGE));
    
    return($membre);
}
echo "----- Debut des tests AbonnementDao OK -----\n";
$membre=getObjetAnonyme();
testAjouterAbonnement($membre);
testListerAbonnements($membre);
testModifierAbonnement($membre);
testGetAbonnementMembre($membre);
testGetAbonnement($membre);
testSupprimerAbonnement($membre);
supprimerAnonyme($membre);
echo "----- Fin des tests AbonnementDao OK -----\n";

?>