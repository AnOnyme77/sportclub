<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function testGetParametres(){
    $parametresDao=new ParametresDaoImpl();
    $parametres=$parametresDao->getParametres();
    if(is_bool($parametres) and $parametres==false)
        echo("Lecture: echec\n");
    else{
        $valeurFichier=$parametresDao->parser(ParametresDaoImpl::getfichierParametres());
        echo("Lecture: OK\n");
        
        if($valeurFichier["Parametres"]["nomEntreprise"]==$parametres->getNomEntreprise())
            echo("nomEntreprise: OK\n");
        else
            echo("nomEntreprise: echec\n");
            
        if($valeurFichier["Parametres"]["numTel"]==$parametres->getNumTel())
            echo("numTel: OK\n");
        else
            echo("numTel: echec\n");
        
        if($valeurFichier["Parametres"]["rue"]==$parametres->getAdresseEntreprise()->getRue())
            echo("Rue: OK\n");
        else
            echo("Rue: echec\n");
            
        if($valeurFichier["Parametres"]["cp"]==$parametres->getAdresseEntreprise()->getCodePostale())
            echo("CodePostale: OK\n");
        else
            echo("CodePostale: echec\n");
            
        if($valeurFichier["Parametres"]["ville"]==$parametres->getAdresseEntreprise()->getVille())
            echo("Ville: OK\n");
        else
            echo("Ville: echec\n");
        
        if($valeurFichier["Parametres"]["pays"]==$parametres->getAdresseEntreprise()->getPays())
            echo("Pays: OK\n");
        else
            echo("Pays: echec\n");
        
        if($valeurFichier["Parametres"]["superLogin"]==$parametres->getSuperLogin())
            echo("superLogin: OK\n");
        else
            echo("superLogin: echec\n");
        
        if($valeurFichier["Parametres"]["superMdp"]==$parametres->getSuperMdp())
            echo("superMdp: OK\n");
        else
            echo("superMdp: echec\n");
    
        if($valeurFichier["Parametres"]["nomResp"]==$parametres->getNomResp())
            echo("nomResp: OK\n");
        else
            echo("nomResp: echec\n");
            
        if($valeurFichier["Parametres"]["prenomResp"]==$parametres->getPrenomResp())
            echo("prenomResp: OK\n");
        else
            echo("prenomResp: echec\n");
        
        if($valeurFichier["Parametres"]["coteMax"]==$parametres->getCoteMax())
            echo("coteMax: OK\n");
        else
            echo("coteMax: echec\n");
        
        if($valeurFichier["Parametres"]["logo"]==$parametres->getLogo())
            echo("logo: OK\n");
        else
            echo("logo: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoHaut"]==$parametres->getTailleLogoHaut())
            echo("tailleLogoHaut: OK\n");
        else
            echo("tailleLogoHaut: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoLarge"]==$parametres->getTailleLogoLarge())
            echo("tailleLogoLarge: OK\n");
        else
            echo("tailleLogoLarge: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoNivHaut"]==$parametres->getTailleLogoNivHaut())
            echo("tailleLogoNivHaut: OK\n");
        else
            echo("tailleLogoNivHaut: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoNivLarge"]==$parametres->getTailleLogoNivLarge())
            echo("tailleLogoNivLarge: OK\n");
        else
            echo("tailleLogoNivLarge: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoMembreHaut"]==$parametres->getTailleLogoMembreHaut())
            echo("tailleLogoMembreHaut: OK\n");
        else
            echo("tailleLogoMembreHaut: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoMembreLarge"]==$parametres->getTailleLogoMembreLarge())
            echo("tailleLogoMembreLarge: OK\n");
        else
            echo("tailleLogoMembreLarge echec\n");
        
        if($valeurFichier["Parametres"]["libNiveau"]==$parametres->getLibNiveau())
            echo("libNiveau: OK\n");
        else
            echo("libNiveau: echec\n");
        
        if($valeurFichier["Parametres"]["libExamen"]==$parametres->getLibExamen())
            echo("libExamen: OK\n");
        else
            echo("libExamen: echec\n");
        
        if($valeurFichier["Parametres"]["libCompetence"]==$parametres->getLibCompetence())
            echo("libCompetence: OK\n");
        else
            echo("libCompetence: echec\n");
        
    }
}




function testModifierParametres(){
    $parametresDao=new ParametresDaoImpl();
    $parametres=new Parametre();
    
    
    $parametres->setNomEntreprise("nomEntreprise");
    $parametres->setNumTel("numTel");
    $parametres->setAdresseEntreprise(new Adresse("rue","cp",
                                                          "ville","pays"));
    $parametres->setSuperLogin("superLogin");
    $parametres->setSuperMdp("superMdp");
    $parametres->setNomResp("nomResp");
    $parametres->setPrenomResp("prenomResp");
    $parametres->setCoteMax("coteMax");
    $parametres->setLogo("logo");
    $parametres->setTailleLogoHaut("tailleLogoHaut");
    $parametres->setTailleLogoLarge("tailleLogoLarge");
    $parametres->setTailleLogoNivHaut("tailleLogoNivHaut");
    $parametres->setTailleLogoNivLarge("tailleLogoNivLarge");
    $parametres->setTailleLogoMembreHaut("tailleLogoMembreHaut");
    $parametres->setTailleLogoMembreLarge("tailleLogoMembreLarge");
    $parametres->setLibNiveau("libNiveau");
    $parametres->setLibExamen("libExamen");
    $parametres->setLibCompetence("libCompetence");
    
    if($parametresDao->modifierParametres($parametres)){
        echo("Réécriture: OK\n");
        $valeurFichier=$parametresDao->parser(ParametresDaoImpl::getfichierParametres());
        echo("Lecture: OK\n");
        
        if($valeurFichier["Parametres"]["nomEntreprise"]==$parametres->getNomEntreprise())
            echo("nomEntreprise: OK\n");
        else
            echo("nomEntreprise: echec\n");
            
        if($valeurFichier["Parametres"]["numTel"]==$parametres->getNumTel())
            echo("numTel: OK\n");
        else
            echo("numTel: echec\n");

        if($valeurFichier["Parametres"]["rue"]==$parametres->getAdresseEntreprise()->getRue())
            echo("Rue: OK\n");
        else
            echo("Rue: echec\n");
            
        if($valeurFichier["Parametres"]["cp"]==$parametres->getAdresseEntreprise()->getCodePostale())
            echo("CodePostale: OK\n");
        else
            echo("CodePostale: echec\n");
            
        if($valeurFichier["Parametres"]["ville"]==$parametres->getAdresseEntreprise()->getVille())
            echo("Ville: OK\n");
        else
            echo("Ville: echec\n");
        
        if($valeurFichier["Parametres"]["pays"]==$parametres->getAdresseEntreprise()->getPays())
            echo("Pays: OK\n");
        else
            echo("Pays: echec\n");
        
        if($valeurFichier["Parametres"]["superLogin"]==$parametres->getSuperLogin())
            echo("superLogin: OK\n");
        else
            echo("superLogin: echec\n");
        
        if($valeurFichier["Parametres"]["superMdp"]==$parametres->getSuperMdp())
            echo("superMdp: OK\n");
        else
            echo("superMdp: echec\n");
    
        if($valeurFichier["Parametres"]["nomResp"]==$parametres->getNomResp())
            echo("nomResp: OK\n");
        else
            echo("nomResp: echec\n");
            
        if($valeurFichier["Parametres"]["prenomResp"]==$parametres->getPrenomResp())
            echo("prenomResp: OK\n");
        else
            echo("prenomResp: echec\n");
        
        if($valeurFichier["Parametres"]["coteMax"]==$parametres->getCoteMax())
            echo("coteMax: OK\n");
        else
            echo("coteMax: echec\n");
        
        if($valeurFichier["Parametres"]["logo"]==$parametres->getLogo())
            echo("logo: OK\n");
        else
            echo("logo: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoHaut"]==$parametres->getTailleLogoHaut())
            echo("tailleLogoHaut: OK\n");
        else
            echo("tailleLogoHaut: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoLarge"]==$parametres->getTailleLogoLarge())
            echo("tailleLogoLarge: OK\n");
        else
            echo("tailleLogoLarge: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoNivHaut"]==$parametres->getTailleLogoNivHaut())
            echo("tailleLogoNivHaut: OK\n");
        else
            echo("tailleLogoNivHaut: echec\n");
        if($valeurFichier["Parametres"]["tailleLogoNivLarge"]==$parametres->getTailleLogoNivLarge())
            echo("tailleLogoNivLarge: OK\n");
        else
            echo("tailleLogoNivLarge: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoMembreHaut"]==$parametres->getTailleLogoMembreHaut())
            echo("tailleLogoMembreHaut: OK\n");
        else
            echo("tailleLogoMembreHaut: echec\n");
        
        if($valeurFichier["Parametres"]["tailleLogoMembreLarge"]==$parametres->getTailleLogoMembreLarge())
            echo("tailleLogoMembreLarge: OK\n");
        else
            echo("tailleLogoMembreLarge echec\n");
        
        if($valeurFichier["Parametres"]["libNiveau"]==$parametres->getLibNiveau())
            echo("libNiveau: OK\n");
        else
            echo("libNiveau: echec\n");
        
        if($valeurFichier["Parametres"]["libExamen"]==$parametres->getLibExamen())
            echo("libExamen: OK\n");
        else
            echo("libExamen: echec\n");
        
        if($valeurFichier["Parametres"]["libCompetence"]==$parametres->getLibCompetence())
            echo("libCompetence: OK\n");
        else
            echo("libCompetence: echec\n");
    }
    else{
        echo("Réécriture: echec\n");
    }
    
}


testGetParametres();
testModifierParametres();
?>