<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function testListingSection(){
    $connexion=DaoFactory::getInstance()->getConnexion();
    
    $liste=DaoFactory::getInstance()->get("NiveauDao")->getSections();
    if(is_array($liste)){
        echo("Listing des sections OK\n");
    }
    else{
        echo("Listing des sections échoué");
    }
    
    
}

function testListing(){
    
    $connexion=DaoFactory::getInstance()->getConnexion();
    $requete="select * from NIVEAU where niveau=? and nameSection=?";
    
    $dao=new NiveauDaoImpl();
    $tab=$dao->listerNiveau();
    if(count($tab)>=0){
        echo("Listing: OK\n");
        $i=0;
        $noError=true;
        while($i<count($tab) and $noError){
            try{
                $ps=$connexion->prepare($requete);
                $ps->execute(array($tab[$i]->getNiveau(),$tab[$i]->getNameSection()));
                if($rep=$ps->fetch()){
                    if($rep["niveau"]!=$tab[$i]->getNiveau())
                        $noError=false;
                    if($rep["libelle"]!=$tab[$i]->getLibelle())
                        $noError=false;
                    if($rep["nameSection"]!=$tab[$i]->getNameSection())
                        $noError=false;
                    if($rep["logo"]!=$tab[$i]->getLogo())
                        $noError=false;
                    if($rep["supprime"]!=$tab[$i]->getSupprime())
                        $noError=false;
                }
                else{
                    $noError=false;
                }
                
                $rep=null;
                $ps=null;
            }
            catch(Exception $ex){
                echo($elem->getNiveau()." ".$elem->getNameSection()." récupération échouée\n");
            }
            
            $i++;
        }
        if($noError){
            echo("Valeurs: OK\n");
        }
        else{
            echo("Valeurs: test echoué\n");
        }
    }
}

function testAjout(){
    $niveau=new Niveau();
    
    $niveau->setNiveau("1");
    $niveau->setLibelle("testBleu");
    $niveau->setNameSection("orange");
    $niveau->setLogo("caca");
    $niveau->setSupprime("V");
    
    $dao=new NiveauDaoImpl();
    $dao->supprimerNiveau($niveau);
    if($dao->ajouterNiveau($niveau)){
        echo("Ajout: OK\n");
    }
    else{
        echo("Ajout: échoué\n");
    }
}

function testSuppression(){
    $niveau=new Niveau();
    
    $niveau->setNiveau("1");
    $niveau->setLibelle("testBleu");
    $niveau->setNameSection("orange");
    $niveau->setLogo("caca");
    $niveau->setSupprime("V");
    
    $dao=new NiveauDaoImpl();
    if($dao->supprimerNiveau($niveau)==1){
        echo("Suppression: OK\n");
    }
    else{
        echo("Suppression: échouée\n");
    }
}

function testRecuperation(){
    $ok=true;
    $niveau=new Niveau();
    
    $niveau->setNiveau("1");
    $niveau->setLibelle("testBleu");
    $niveau->setNameSection("orange");
    $niveau->setLogo("caca");
    $niveau->setSupprime("V");
    
    $dao=new NiveauDaoImpl();
    $dao->supprimerNiveau($niveau);
    $dao->ajouterNiveau($niveau);
    $retour=$dao->getNiveau($niveau->getNiveau(),$niveau->getNameSection());
    
    if($retour!=false){
        if($retour->getNiveau()!=$niveau->getNiveau())
            $ok=false;
            
        if($retour->getLibelle()!=$niveau->getLibelle())
            $ok=false;
        
        if($retour->getNameSection()!=$niveau->getNameSection())
            $ok=false;
            
        if($retour->getLogo()!=$niveau->getLogo())
            $ok=false;
        
        if($retour->getSupprime()!=$niveau->getSupprime())
            $ok=false;
            
        if($ok){
            echo("Récupération: OK\n");
        }
        else{
            echo("Récupération échouée\n");
        }
    }
    else{
        echo("Récupération échouée\n");
    }
}
function testModification(){
    $ok=true;
    $niveau=new Niveau();
    
    $niveau->setNiveau("1");
    $niveau->setLibelle("testBleu");
    $niveau->setNameSection("orange");
    $niveau->setLogo("caca");
    $niveau->setSupprime("V");
    
    $dao=new NiveauDaoImpl();
    $dao->supprimerNiveau($niveau);
    $dao->ajouterNiveau($niveau);
    
    $niveau->setNiveau("1");
    $niveau->setLibelle("testBleuet");
    $niveau->setNameSection("orange");
    $niveau->setLogo("prout");
    $niveau->setSupprime("F");
    
    if($dao->modifierNiveau($niveau)){
        if("1"!=$niveau->getNiveau())
            $ok=false;
            
        if("testBleuet"!=$niveau->getLibelle())
            $ok=false;
        
        if("orange"!=$niveau->getNameSection())
            $ok=false;
            
        if("prout"!=$niveau->getLogo())
            $ok=false;
        
        if("F"!=$niveau->getSupprime())
            $ok=false;
            
        if($ok){
            echo("Modification: OK\n");
        }
        else{
            echo("Modification échouée\n");
        }
    }
    else{
        echo("Modification échouée\n");
    }
}
function testRecherche(){
    $ok=false;
    $niveau=new Niveau();
    
    $niveau->setNiveau("1");
    $niveau->setNameSection("orange");
    $niveau->setLibelle("testBleu");
    $niveau->setLogo("caca");
    $niveau->setSupprime("V");
    
    $dao=new NiveauDaoImpl();
    
    $niveau->setNameSection(null);
    $retour=$dao->rechercherNiveau($niveau);
    
    if($retour!=false){
        echo("Recherche OK\n");
        foreach($retour as $niveau2){
            if($niveau->getLibelle()==$niveau2->getLibelle() and
               $niveau->getNiveau()==$niveau2->getNiveau() and
               $niveau->getLogo()==$niveau2->getLogo() and
               $niveau->getNameSection()==$niveau2->getNameSection() and
               $niveau->getSupprime()==$niveau2->getSupprime()) $ok=true;
        }
        if($ok) echo("Confirmation OK\n");
        else echo("Confirmation échouée");
    }
    else{
        echo("Recherche infructueuse\n");
    }
}
testAjout();
testListing();
testListingSection();
testRecuperation();
testRecherche();
testModification();
testSuppression();
?>