<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function competenceEgales($competence1,$competence2){
    if($competence1->getNiveau()==$competence2->getNiveau()){
        echo("Niveau: OK\n");
    }
    else{
        echo("Niveau différents");
    }
    
    if($competence1->getNameSection()==$competence2->getNameSection()){
        echo("NameSection: OK\n");
    }
    else{
        echo("NameSection différents");
    }
    if($competence1->getTitre()==$competence2->getTitre()){
        echo("Titre: OK\n");
    }
    else{
        echo("Titre différents");
    }
    if($competence1->getDetail()==$competence2->getDetail()){
        echo("Detail: OK\n");
    }
    else{
        echo("Detail différents");
    }
}
function getObjetNiveau(){
    $niveau = new Niveau();
    $niveau->setNiveau(77);
    $niveau->setNameSection("anonyme");
    $niveau->setLibelle("fineau");
    $niveau->setLogo("Mabite");
    $niveau->setSupprime("F");
    
    return($niveau);
}
function ajouterNiveau(){
    $daoNiveau=DaoFactory::getInstance()->get("NiveauDao");
    $daoNiveau->ajouterNiveau(getObjetNiveau());
}
function supprimerNiveau(){
    $daoNiveau=DaoFactory::getInstance()->get("NiveauDao");
    $daoNiveau->supprimerNiveau(getObjetNiveau());
}
function testRecuperation(){
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
        
    $competence= new Competence();

    $competence->setNiveau(77);
    $competence->setNameSection("anonyme");
    $competence->setTitre("fineauder");
    $competence->setDetail("faire le fineau");
    
    $competenceRetourne=$daoCompetence->getCompetence($competence);
    if($competenceRetourne!=false){
        echo("Récupération de la competence: OK\n");
        competenceEgales($competence,$competenceRetourne);
    }
    else{
        echo("Récupération de la competence échoué\n");
    }

}

function testAjout(){
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $competence=new Competence();
    $competence->setNiveau(getObjetNiveau()->getNiveau());
    $competence->setNameSection(getObjetNiveau()->getNameSection());
    $competence->setTitre("fineauder");
    $competence->setDetail("faire le fineau");
    
    if($daoCompetence->ajouterCompetence($competence)){
        echo("Ajout de la compétence: OK\n");
        $competenceRetour=$daoCompetence->getCompetence($competence);
        
        competenceEgales($competence,$competenceRetour);
    }
    else{
        echo("Ajout de la compétence échoué\n");
    }
    
}

function testSupprimer(){
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $competence=new Competence();
    $competence->setNiveau(getObjetNiveau()->getNiveau());
    $competence->setNameSection(getObjetNiveau()->getNameSection());
    $competence->setTitre("fineauder");
    $competence->setDetail("faire le fineau");
    
    if($daoCompetence->supprimerCompetence($competence)){
        echo("Suppression de la compétence: OK\n");
        if($daoCompetence->getCompetence($competence)==false){
            echo("Confirmation de suppression OK\n");
        }
        else{
            echo("Confirmation de la suppression\n");
        }
    }
    else{
        echo("Suppression échouée\n");
    }
}
function testModifier(){
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $competence=new Competence();
    $competence->setNiveau(getObjetNiveau()->getNiveau());
    $competence->setNameSection(getObjetNiveau()->getNameSection());
    $competence->setTitre("fineauder");
    $competence->setDetail("faire le fineau de course");
    
    if($daoCompetence->modifierCompetence($competence)){
        echo("Modification OK\n");
        $competenceRetour=$daoCompetence->getCompetence($competence);
        competenceEgales($competence,$competenceRetour);
    }
    else{
        echo("Modification échouée\n");
    }
    
}
function testLister(){
    $i=1;
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $liste=$daoCompetence->listerCompetences();
    if($liste!=false){
        echo("Listing des compétences OK\n");
        foreach($liste as $niveau){
            $niveauRetour=$daoCompetence->getCompetence($niveau);
            if($niveauRetour!=false){
                echo("Récupération $i OK\n");
                competenceEgales($niveau,$niveauRetour);
            }
        }
    }
    else{
        echo("Listing des compétences échoué\n");
    }
}
function testGetFromNiveau(){
    $daoCompetence=DaoFactory::getInstance()->get("CompetenceDao");
    
    $liste=$daoCompetence->getFromNiveau(getObjetNiveau());
    if($liste!=false){
        echo("Récupération par le niveau: OK\n");
        foreach($liste as $niveau){
            $niveauRetour=$daoCompetence->getCompetence($niveau);
            if($niveauRetour!=false){
                echo("Récupération $i OK\n");
                competenceEgales($niveau,$niveauRetour);
            }
        }
    }
    else{
        echo("La récupération par le niveau a échoué\n");
    }
}
ajouterNiveau();
testAjout();
testRecuperation();
testModifier();
testLister();
testGetFromNiveau();
testSupprimer();
supprimerNiveau();
?>