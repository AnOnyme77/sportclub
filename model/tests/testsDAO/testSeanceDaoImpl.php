<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
 
define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");
function testListingCours(){
    
    $liste=DaoFactory::getInstance()->get("SeanceDao")->getCours();
    if(is_array($liste)){
        echo("Listing des cours OK\n");
    }
    else{
        echo("Listing des cours échoué");
    }
    
    
}

function seancesEgale($seance1,$seance2){
    if($seance1->getNoSeance()==$seance2->getNoSeance()){
        echo("Numéro de séance: OK\n");
    }
    else{
        echo("Numéro de séance différents\n");
    }
    
    if($seance1->getCours()==$seance2->getCours()){
        echo("Cours: OK\n");
    }
    else{
        echo("Cours différents\n");
    }
    
    if($seance1->getDateCours()==$seance2->getDateCours()){
        echo("Date Cours: OK\n");
    }
    else{
        echo("Date Cours différents\n");
    }
    
    if($seance1->getNbMembreMax()==$seance2->getNbMembreMax()){
        echo("Nombre de membre: OK\n");
    }
    else{
        echo("Nombre de membres différents\n");
    }
    
    if($seance1->getIdProf()==$seance2->getIdProf()){
        echo("Id Prof: OK\n");
    }
    else{
        echo("Id Prof différents\n");
    }
}

function supprimerAnonyme($membre)
{
    date_default_timezone_set('Europe/Paris');

    $daoMem = DaoFactory::getInstance()->get("MembreDao");

    if($daoMem->supprimerPhysMembre($membre->getIdMembre()))
        echo "Anonyme Supp : OK\n";
}

function getObjetAnonyme(){
    date_default_timezone_set('Europe/Paris');
    $mem = new Membre();
    $mem->setAdresse(new Adresse("Bois Du Prince 131","5640","Mettet","Belgique"));
    $mem->setDateInscription(new DateTime("2011-03-05 06:06:06"));
    $mem->setEmail("anonyme77@gmail.com");
    $mem->setNom("Jamar");
    $mem->setPrenom("Frederic");
    $mem->setSexe("M");
    $mem->setDateNaissance(new DateTime("1993-05-15"));
    $mem->setFonction("M");
    $mem->setMdp("jesuisfred");
    $mem->setLogo("chemin/isanzo.jpg");
    $mem->setSupprime("F");
    $mem->setStatut("F");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $mem);
    $gererMembre->ajouterMembre($bundle);
    
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "anonyme77@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        $membre=null;
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
            $membre=$bundle->get(Bundle::$MEMBRE);
        else
            echo($bundle->get(Bundle::$MESSAGE));
    }
    else
        echo($bundle->get(Bundle::$MESSAGE));
    
    return($membre);
}
function ajouterSeance($membre){
    $seance=new Seance();
    $seance->setNoSeance(10);
    $seance->setCours("Jujutsu");
    $seance->setDateCours("2013-04-12");
    $seance->setIdProf($membre->getIdMembre());
    $seance->setNbMembreMax(14);
    
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    $retour=$dao->ajouterSeance($seance);
    if($retour){
        echo("Ajout séance: OK\n");
        $seance->setNoSeance($retour);
        $seanceRetour=$dao->getSeance($seance);
        seancesEgale($seance,$seanceRetour);
    }
    else{
        echo("Ajout Séance échoué\n");
    }
    return($retour);
}

function getSeance($id,$membre){
    $seance=new Seance();
    $seance->setNoSeance($id);
    $seance->setCours("Jujutsu");
    $seance->setDateCours("2013-04-12");
    $seance->setIdProf($membre->getIdMembre());
    $seance->setNbMembreMax(14);
    
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    $seanceRetour=$dao->getSeance($seance);
    if($seanceRetour!=false){
        echo("Récupération séance: OK\n");
        seancesEgale($seance,$seanceRetour);
    }
    else{
        echo("Récupération Séance échoué\n");
    } 
}

function modifierSeance($id,$membre){
    $seance=new Seance();
    $seance->setNoSeance($id);
    $seance->setCours("Jujutsa");
    $seance->setDateCours("2013-04-12");
    $seance->setIdProf($membre->getIdMembre());
    $seance->setNbMembreMax(25);
    
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    if($dao->modifierSeance($seance)){
        echo("Modification séance: OK\n");
        $seanceRetour=$dao->getSeance($seance);
        if($seanceRetour!=false){
            echo("Récupération séance OK\n");
            seancesEgale($seance,$seanceRetour);
        }
        else{
            echo("Le récupération de la séance a échoué\n");    
        }
    }
    else{
        echo("Modification Séance échoué\n");
    } 
}

function listerSeances(){
    $seance=new Seance();
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    
    $liste=$dao->listerSeances();
    if($liste!=null){
        echo("Récupération de la liste: OK\n");
        foreach($liste as $seanceRetour){
            $seance->setNoSeance($seanceRetour->getNoSeance());
            $seance=$dao->getSeance($seance);
            if($seance!=false){
                echo("Récupération de la séance OK\n");
                seancesEgale($seance,$seanceRetour);
            }
            else{
                echo("Récupération de la séance échouée\n");
            }
        }
    }
    else{
        echo("Récupération de la liste échouée\n");
    }
}

function supprimerSeance($id){
    $seance=new Seance();
    $seance->setNoSeance($id);
    
    $dao=DaoFactory::getInstance()->get("SeanceDao");
    
    if($dao->supprimerSeance($seance)){
        echo("Suppression OK\n");
        if($dao->getSeance($seance)==false){
            echo("Confirmation OK\n");
        }
        else{
            echo("Confirmation échouée\n");
        }
    }
    else{
        echo("Suppression échouée\n");
    }
    
}
$membre=getObjetAnonyme();
$id=ajouterSeance($membre);
getSeance($id,$membre);
modifierSeance($id,$membre);
listerSeances();
testListingCours();
supprimerSeance($id);
supprimerAnonyme($membre);
?>