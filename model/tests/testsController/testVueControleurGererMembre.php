<?php

/* ---------------------------------------------------------------------------------------------------
 * Author : Jérôme Hainaut
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

/**
 * rem: quand tu changes de page (ou que tu clique sur enregistrer) tu repasses par la classe de routage 
 * 		qui te renvoie au bonne endrois
 * 
 * Quand on fait un retour, il faut rafraichir la page.
 * Comment gérer l'ajout d'un membre? (si 2 administrateurs veulent ajouter un nouveau membre,
 * 		le 2eume risque d'avoir un prob)
 * 
 * gérer le mot de passe (a l'ajout, en générer un et possiblilité a l'admin d'en recrée un)
 * 
 * si on n'oublie un champs, ne pas retournée une erreur mais réafficher le membre et dire ce qu'il faut ajouter.
 * 
 * vérifier que le chemin du logo est correct (bonne extention, etc)
 */

if(empty($_SESSION)) session_start();

date_default_timezone_set('Europe/Paris');//pq faut-il faire ca???
define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

$membre = new Membre();
	$membre->setAdresse(new Adresse("rueduTest","cpduTest","belleVille","Belgique"));
	$membre->setDateInscription(new DateTime("2010-03-05 06:06:06"));
	$membre->setEmail("admin@mail.mail");
	$membre->setIdMembre(1234567890);
	$membre->setNom("nom");
	$membre->setPrenom("testPrénom");
	$membre->setSexe("M");
	$membre->setDateNaissance(new DateTime("1990-02-11"));
	$membre->setFonction("A");
	$membre->setMdp("mdp");
	$membre->setLogo("chemin/logo");
	$membre->setSupprime("F");
	$membre->setStatut("F");

$_SESSION["oMembre"]=serialize($membre);


class Routeur extends Controller{

 	private $ctrlRechercherMembre; //mettre une varialbe par controleur
	private $ctrlAjouterMembre;
	private $ctrlGererParametre;
	
	public function __construct() {
    	$this->ctrlRechercherMembre = new ControllerRechercherMembre();
    	$this->ctrlAjouterMembre = new ControllerAjouterMembre();
		$this->ctrlGererParametre = new ControllerParametre();
	}

  // Traite une requête entrante
  public function routerRequete() {
    try {
      if (isset($_GET['page'])) {
        if ($_GET['page'] == 'rechercherMembre') {
              $this->ctrlRechercherMembre->listerMembre();
        }else if($_GET['page'] == 'ajouterMembre'){
        	if(isset($_GET['id'])){
        		if(isset($_POST['enreg'])){
        			$this->ctrlAjouterMembre->modifierMembre($_GET['id']);
        		}else if(isset($_POST['supp'])){
        			$this->ctrlAjouterMembre->supprimerMembre($_GET['id']);
        		}else{
        			$this->ctrlAjouterMembre->afficherMembre($_GET['id']);
        		}
        	}
        	else{
        		if(isset($_POST['ajout'])){
        			$this->ctrlAjouterMembre->ajouterMembre();
        		}else{
        			$this->ctrlAjouterMembre->afficherMembre();
        		}
        	}
        }else if($_GET["page"]=="gererParametre"){
			$this->ctrlGererParametre->afficherParametres();
		}
		else
          throw new Exception("Action non valide");
      }
	  
      else {  // aucune action définie : affichage de l'accueil
        echo("returner a la page d'acceuil");
      }
    }
    catch (Exception $e) {
    	self::erreur($e->getMessage());
    }
  }
}

$routeur = new Routeur();
$routeur->routerRequete();

?>
