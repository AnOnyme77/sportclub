<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");

function recupererParametres(){
    $controller = new ControllerParametre();
    $bundle=$controller->getParametres();
    
    if($bundle==true){
        echo "Récupération: OK\n";
        if(session_status()!=PHP_SESSION_ACTIVE)
            session_start();
        if(isset($_SESSION["oParametres"])){
            echo("Objet en session: OK\n");
            $dao = new ParametresDaoImpl();
            $valeurFichier=$dao->parser(ParametresDaoImpl::getfichierParametres());
            
            if($valeurFichier["Parametres"]["nomEntreprise"]==unserialize($_SESSION["oParametres"])->getNomEntreprise())
                echo("nomEntreprise: OK\n");
            else
                echo("nomEntreprise: echec\n");
                
            if($valeurFichier["Parametres"]["numTel"]==unserialize($_SESSION["oParametres"])->getNumTel())
                echo("numTel: OK\n");
            else
                echo("numTel: echec\n");
            
            if($valeurFichier["Parametres"]["rue"]==unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getRue())
                echo("Rue: OK\n");
            else
                echo("Rue: echec\n");
                
            if($valeurFichier["Parametres"]["cp"]==unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getCodePostale())
                echo("CodePostale: OK\n");
            else
                echo("CodePostale: echec\n");
                
            if($valeurFichier["Parametres"]["ville"]==unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getVille())
                echo("Ville: OK\n");
            else
                echo("Ville: echec\n");
            
            if($valeurFichier["Parametres"]["pays"]==unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getPays())
                echo("Pays: OK\n");
            else
                echo("Pays: echec\n");
        
            if($valeurFichier["Parametres"]["superLogin"]==unserialize($_SESSION["oParametres"])->getSuperLogin())
                echo("superLogin: OK\n");
            else
                echo("superLogin: echec\n");
            
            if($valeurFichier["Parametres"]["superMdp"]==unserialize($_SESSION["oParametres"])->getSuperMdp())
                echo("superMdp: OK\n");
            else
                echo("superMdp: echec\n");
        
            if($valeurFichier["Parametres"]["nomResp"]==unserialize($_SESSION["oParametres"])->getNomResp())
                echo("nomResp: OK\n");
            else
                echo("nomResp: echec\n");
                
            if($valeurFichier["Parametres"]["prenomResp"]==unserialize($_SESSION["oParametres"])->getPrenomResp())
                echo("prenomResp: OK\n");
            else
                echo("prenomResp: echec\n");
            
            if($valeurFichier["Parametres"]["coteMax"]==unserialize($_SESSION["oParametres"])->getCoteMax())
                echo("coteMax: OK\n");
            else
                echo("coteMax: echec\n");
            
            if($valeurFichier["Parametres"]["logo"]==unserialize($_SESSION["oParametres"])->getLogo())
                echo("logo: OK\n");
            else
                echo("logo: echec\n");
            
            if($valeurFichier["Parametres"]["tailleLogoHaut"]==unserialize($_SESSION["oParametres"])->getTailleLogoHaut())
                echo("tailleLogoHaut: OK\n");
            else
                echo("tailleLogoHaut: echec\n");
            
            if($valeurFichier["Parametres"]["tailleLogoLarge"]==unserialize($_SESSION["oParametres"])->getTailleLogoLarge())
                echo("tailleLogoLarge: OK\n");
            else
                echo("tailleLogoLarge: echec\n");
            
            if($valeurFichier["Parametres"]["tailleLogoNivHaut"]==unserialize($_SESSION["oParametres"])->getTailleLogoNivHaut())
                echo("tailleLogoNivHaut: OK\n");
            else
                echo("tailleLogoNivHaut: echec\n");
            
            if($valeurFichier["Parametres"]["tailleLogoNivLarge"]==unserialize($_SESSION["oParametres"])->getTailleLogoNivLarge())
                echo("tailleLogoNivLarge: OK\n");
            else
                echo("tailleLogoNivLarge: echec\n");
            
            if($valeurFichier["Parametres"]["tailleLogoMembreHaut"]==unserialize($_SESSION["oParametres"])->getTailleLogoMembreHaut())
                echo("tailleLogoMembreHaut: OK\n");
            else
                echo("tailleLogoMembreHaut: echec\n");
            
            if($valeurFichier["Parametres"]["tailleLogoMembreLarge"]==unserialize($_SESSION["oParametres"])->getTailleLogoMembreLarge())
                echo("tailleLogoMembreLarge: OK\n");
            else
                echo("tailleLogoMembreLarge echec\n");
            
            if($valeurFichier["Parametres"]["libNiveau"]==unserialize($_SESSION["oParametres"])->getLibNiveau())
                echo("libNiveau: OK\n");
            else
                echo("libNiveau: echec\n");
            
            if($valeurFichier["Parametres"]["libExamen"]==unserialize($_SESSION["oParametres"])->getLibExamen())
                echo("libExamen: OK\n");
            else
                echo("libExamen: echec\n");
            
            if($valeurFichier["Parametres"]["libCompetence"]==unserialize($_SESSION["oParametres"])->getLibCompetence())
                echo("libCompetence: OK\n");
            else
                echo("libCompetence: echec\n");
        }
    }
    else{
        echo "Récupération échouée\n";
    }
}
function modifierParametres(){
    $controller = new ControllerParametre();
    $controller->modifierParametres("nomEntreprise",
                                        "numTel",
                                        "rue","cp","ville","pays",
                                        "superLogin",
                                        "superMdp",
                                        "nomResp",
                                        "prenomResp",
                                        "coteMax",
                                        "logo",
                                        "tailleLogoHaut",
                                        "tailleLogoLarge",
                                        "tailleLogoNivHaut",
                                        "tailleLogoNivLarge",
                                        "tailleLogoMembreHaut",
                                        "tailleLogoMembreLarge",
                                        "libNiveau",
                                        "libExamen",
                                        "libCompetence",true);
}
recupererParametres();
//modifierParametres();
?>