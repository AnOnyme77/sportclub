<?php
//testPythonOK

/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Fr�d�ric
 * Team : Dev4u
 * cr�� le ??/??/2014 - modif�e le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
define('ROOTWAY',"../../..");
require_once(ROOTWAY."/outils/autoloader.php");
date_default_timezone_set('Europe/Paris');//pq faut-il faire ca???

function getObjetAnonyme(){
    date_default_timezone_set('Europe/Paris');
    $mem = new Membre();
    $mem->setAdresse(new Adresse("Bois Du Prince 131","5640","Mettet","Belgique"));
    $mem->setDateInscription(new DateTime("2011-03-05 06:06:06"));
    $mem->setEmail("anonyme77@gmail.com");
    $mem->setNom("Jamar");
    $mem->setPrenom("Frederic");
    $mem->setSexe("M");
    $mem->setDateNaissance(new DateTime("1993-05-15"));
    $mem->setFonction("M");
    $mem->setMdp("jesuisfred");
    $mem->setLogo("chemin/isanzo.jpg");
    $mem->setSupprime("F");
    $mem->setStatut("F");
    
    $gererMembre = new GererMembreImpl();
    $bundle = new Bundle();
    
    $bundle->put(Bundle::$MEMBRE, $mem);
    $gererMembre->ajouterMembre($bundle);
    
    $membre=null;
    if($bundle->get(Bundle::$OPERATION_REUSSIE)){
        $bundle->put(Bundle::$EMAIL, "anonyme77@gmail.com");
        $gererMembre->rechercherMembreEmail($bundle);
        
        if($bundle->get(Bundle::$OPERATION_REUSSIE))
            $membre=$bundle->get(Bundle::$MEMBRE);
        else
            echo($bundle->get(Bundle::$MESSAGE)."\n");
    }
    else
        echo($bundle->get(Bundle::$MESSAGE)."\n");
    
    return($membre);
}

function testControllerAuthentifier()
{
    $controller = new ControllerAuthentifier();
    
    //Ajout d'un membre
    //Dao membre
    $membreDao = DaoFactory::getInstance()->get("MembreDao");

    $membre1=getObjetAnonyme();
    
    if($membre1!=null)
    {
        echo("Ajout: OK\n");
        //Verification du login
        echo $controller->authentifier($membre1->getEmail(),$membre1->getMdp(), true); //True -> mode test
        //Suppression physique du membre1
        if($membreDao->supprimerPhysMembre($membre1->getIdMembre()))
                echo "Suppression OK\n";
        else
                echo "Suppression fail\n";
    
    }
    else
        echo "Ajout �echou�e";
    
}
testControllerAuthentifier();

?>