<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jérôme Hainaut - Jamar Frédéric
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
interface MembreDao{
	public function ajouterMembre($membre);
	public function getMembre($idMembre); 
	public function listerMembre();
	public function supprimerMembre($idMembre);
	public function modifierMembre($membre);
	public function getMembreEmail($email); 
	public function getLastMembre();
	public function checkLogin($authentifier);
	
}