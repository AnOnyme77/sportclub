<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Hainaut Jérôme
 * Team : Dev4u
 * créé le 22/04/2014 - modifée le 22/04/2014
 *  -----------------------------------------------------------------------------------------------------*/
interface ExamenDao{
    //Interface pour la gestion en base de donnée des niveau
    public function listerExamen();
    public function getExamen($examen);
    public function ajouterExamen($examen);
    public function modifierExamen($examen);
    public function supprimerExamen($examen);
}
