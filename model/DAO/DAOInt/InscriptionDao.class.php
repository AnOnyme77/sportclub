<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
interface InscriptionDao{
    public function ajouterInscription($presence);
    public function modifierInscription($presence);
    public function supprimerInscription($presence);
    public function listerInscription();
    public function getInscription($presence);
}
?>