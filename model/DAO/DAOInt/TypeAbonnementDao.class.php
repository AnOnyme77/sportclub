<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 02/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
    interface TypeAbonnementDao
    {
    	public function getTypeAbonnement($libelle);
        public function ajouterTypeAbonnement($typeAbonnement);
        public function supprimerTypeAbonnement($typeAbonnement);
        public function supprimerPhysTypeAbonnement($typeAbonnement);
        public function modifierTypeAbonnement($typeAbonnement);
        public function listerTypesAbonnement();
        public function listerTypesAbonnementValides();
    }
?>