<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
    interface AbonnementDao
    {
    	public function getAbonnement($idMembre, $libelle, $dateAchat);
        public function ajouterAbonnement($abonnement);
        public function supprimerAbonnement($abonnement);
        public function modifierAbonnement($abonnement);
        public function listerAbonnements();
        public function getAbonnementsMembre($idMembre);
    }
?>