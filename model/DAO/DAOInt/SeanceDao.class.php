<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
interface SeanceDao{
    // Interface pour la gestion en base de données des Séances
    public function ajouterSeance($seance);
    public function modifierSeance($seance);
    public function supprimerSeance($seance);
    public function listerSeances();
    public function getSeance($seance);
}
?>