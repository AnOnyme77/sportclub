<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
interface NiveauDao{
    //Interface pour la gestion en base de donnée des niveau
    public function listerNiveau();
    public function getNiveau($niveau,$nameSection);
    public function ajouterNiveau($niveau);
    public function modifierNiveau($niveau);
    public function supprimerNiveau($niveau);
}
?>