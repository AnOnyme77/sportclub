<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
interface ParametresDao{
    //interface du paramètres dao
    public function getParametres();
    public function modifierParametres($parametres);
    public function initialiserParametres();
}
?>