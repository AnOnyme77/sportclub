<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
abstract class Parser {
    // Interface pour les parser.
    private static $NOMRACINE = "ProjetPHP";
    private static $NOMRACINEAPACHE = NULL;
    private static $configStructure="config/structure.ini";
    public abstract function parser($fichierConfiguration);
    
    // Méthode statique pour le calcul de chemin à partir de la racine du projet
    public static function getCheminRacine(){
		$complet=explode("/",realpath(null));
        $i=0;
        $nbrElement=count($complet);
        for($i=0;$i<count($complet) and $complet[$nbrElement-$i-1]!=self::$NOMRACINE;$i++);
        $racine=implode("/",array_slice($complet,0,($nbrElement-$i)));
        return($racine);
	}
	
	//implique $_SERVER
	public static function getChemin(){
		$racine = "";
		if($_SERVER != null){
			if($_SERVER["PHP_SELF"] != null)
				$complet=explode("/",$_SERVER["PHP_SELF"]);
			else if($_SERVER["REQUEST_URI"])
				$complet=explode("/",$_SERVER["REQUEST_URI"]);
			else if($_SERVER["SCRIPT_NAME"])
				$complet=explode("/",$_SERVER["SCRIPT_NAME"]);
				
	        $i=0;
	        $nbrElement=count($complet);
	        for($i=0;$i<count($complet) and $complet[$nbrElement-$i-1]!=self::getCheminApache();$i++);
	        $racine=implode("/",array_slice($complet,0,($nbrElement-$i)));
		}
		return($racine);
	}
    
	public static function getCheminApache(){
        if(self::$NOMRACINEAPACHE==NULL){
            $parserStructure=new ParserStructure();
            $config=$parserStructure->parser(self::$configStructure);
            self::$NOMRACINEAPACHE=$config->getRacineApache();
        }
        return(self::$NOMRACINEAPACHE);
	}
}

?>