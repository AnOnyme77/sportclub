<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
interface CompetenceDao{
    // Interface pour la gestion en base de données des compétences
    public function ajouterCompetence($competence);
    public function modifierCompetence($competence);
    public function supprimerCompetence($competence);
    public function listerCompetences();
    public function getCompetence($competence);
}
?>