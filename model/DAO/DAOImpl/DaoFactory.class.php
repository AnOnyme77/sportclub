<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Dev4u
 * Team : Dev4u
 * créé le 08/02/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class DaoFactory {
	
	//Cette classe sert de façade afin de dispenser les DAOs à la demande
	private static $_instance;
//	private static $_fichierConfig = "config/config.ini";
	private static $_fichierConfig = "config/configAnon.ini";
	private static $_nbConnexion=5;
	private $connexionActive;
	private $connexions=array();
	private $persistance;

	private function __construct() {
		$parserConfig = new ParserConfig();
		$this->persistance = $parserConfig->parser(self::$_fichierConfig);
		$this->connexionActive=0;
		$this->createConnexions();
	}

	private function __clone() {
		Logger::getInstance()->logify($this,"Essai de clone de la DaoFactory");
	}
	
	private function createConnexions(){
		$i=0;
		while($i<self::$_nbConnexion){
			$objetPDO = null;
			$dsn = $this->persistance->getTypeDB() . ":host=" .
					$this->persistance->getUrlDB() .
					";dbname=" . $this->persistance->getBase();
				
			try{
				$objetPDO = new PDO($dsn, $this->persistance->getUtilisateurDB(), $this->persistance->getMdpDB(),
									array(PDO::ATTR_PERSISTENT=>$this->persistance->getPersistant()));
				
				$objetPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$objetPDO->query("set names utf8");
			}
			catch (PDOException $e) {
				Logger::getInstance()->logify($this,"Erreur !: " . $e->getMessage() . "\n");
				print "Erreur !: " . $e->getMessage() . "<br/>";
				die();
			}
			$this->connexions[$i]=$objetPDO;
			$i++;
		}
	}
	
	//Cette classe implémente le pattern singleton
	//Cette méthode retourne le singleton généré au chargement de la classe
	public static function getInstance() {
		if (true === is_null(self :: $_instance)) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	//Cette méthode  crée et et renvoit un objet php servant de connexion à la base de données
	public function getConnexion() {
		
		$objetPDO=null;
		do{
		$numero=$this->connexionActive;
		$objetPDO=$this->connexions[$numero];
		
		if($objetPDO==null)
			Logger::getInstance()->logify($this,"Erreur au chargement de l'objet pdo");
			
		$numero++;
		$numero=$numero%self::$_nbConnexion;
		$this->connexionActive=$numero;
		}while($objetPDO==null);
		return $objetPDO;
	}
	
	//Méthode réflexive permettant de retourner un dao à la demande
	public function get($dao){
		try{
			if(class_exists($dao."Impl")){
				$nomComplet=$dao."Impl";
				return(new $nomComplet());
			}
			else{
				Logger::getInstance()->logify($this,"Le chargement de la class: ".$dao."Impl"." est impossible");
				throw new Exception("Chargement impossible");
			}
		}
		catch(Exception $e){
			echo("Erreur: ".$e->getMessage());
		}
	}
	
}
?>
