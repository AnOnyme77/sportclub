<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 02/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
    class TypeAbonnementDaoImpl implements TypeAbonnementDao
    {
    	private static $LISTER= "SELECT * from TYPEABONNEMENT";
        private static $LISTERVALIDES= "SELECT * from TYPEABONNEMENT WHERE supprime = 'F'";
    	private static $AJOUT= "INSERT INTO TYPEABONNEMENT(libelle,prix,duree,nbUnite,cours,supprime) 
                                                    VALUES(:libelle,:prix,:duree,:nbUnite,:cours,:supprime)";
    	private static $SUPPRIMERPHYS= "DELETE from TYPEABONNEMENT where libelle=?";
        private static $SUPPRIMER = "UPDATE TYPEABONNEMENT SET supprime='T' WHERE libelle=?";
    	private static $GET= "SELECT * from TYPEABONNEMENT WHERE libelle=:libelle";
    	private static $MODIFIER= "UPDATE TYPEABONNEMENT SET prix=:prix, duree=:duree, nbUnite=:nbUnite, 
                                                            cours=:cours, supprime=:supprime WHERE libelle=:libelle";
    	
        function TypeAbonnementDaoImpl(){
            /* Constructeur vide */
        }

        public function getTypeAbonnement($libelle)
        {
            $connexion=null;
            $ps=null;
            $rep=null;
            $typeAbo=null;

            try
            {
                $connexion = DaoFactory::getInstance()->getConnexion();
                $ps=$connexion->prepare(self::$GET);
                $ps->bindParam(':libelle', $libelle, PDO::PARAM_STR);
                $ps->execute();

                if($rep=$ps->fetch())
                {
                    $typeAbo = new TypeAbonnement();
                    $typeAbo->setLibelle($rep["libelle"]);
                    $typeAbo->setPrix($rep["prix"]);
                    $typeAbo->setDuree($rep["duree"]);
                    $typeAbo->setNbUnite($rep["nbUnite"]);
                    $typeAbo->setCours($rep["cours"]);
                    $typeAbo->setSupprime($rep["supprime"]);
                }
                else
                    Logger::getInstance()->logify($this,"Le type d'abonnement n'existe pas");
            }
            catch (Exception $ex) {
                print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
                Logger::getInstance()->logify($this,"Erreur dans les reqêtes de listage");
                die();
            }
            self::cloturer($ps, $connexion);

            return($typeAbo);
        }

    	public function listerTypesAbonnement()
        {
        	$typeAbonnements = array();
        	$connexion=null;
            $ps=null;
            $rep=null;

            try{
                $connexion = DaoFactory::getInstance()->getConnexion();
                $ps=$connexion->prepare(self::$LISTER);
                $ps->execute();
                
                while($rep=$ps->fetch()){
                    $typeAbo=new TypeAbonnement();
                    $typeAbo->setLibelle($rep["libelle"]);
                    $typeAbo->setPrix($rep["prix"]);
                    $typeAbo->setDuree($rep["duree"]);
                    $typeAbo->setNbUnite($rep["nbUnite"]);
                    $typeAbo->setCours($rep["cours"]);
                    $typeAbo->setSupprime($rep["supprime"]);
                    
                    $typeAbonnements[]=$typeAbo;
                }
            }
            catch (Exception $ex) {
    			print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
     	    	die();
    		}
    		self::cloturer($ps, $connexion);

        	return($typeAbonnements);
        }

        public function listerTypesAbonnementValides()
        {
            $typeAbonnements = array();
            $connexion=null;
            $ps=null;
            $rep=null;

            try{
                $connexion = DaoFactory::getInstance()->getConnexion();
                $ps=$connexion->prepare(self::$LISTERVALIDES);
                $ps->execute();
                
                while($rep=$ps->fetch()){
                    $typeAbo=new TypeAbonnement();
                    $typeAbo->setLibelle($rep["libelle"]);
                    $typeAbo->setPrix($rep["prix"]);
                    $typeAbo->setDuree($rep["duree"]);
                    $typeAbo->setNbUnite($rep["nbUnite"]);
                    $typeAbo->setCours($rep["cours"]);
                    $typeAbo->setSupprime($rep["supprime"]);
                    
                    $typeAbonnements[]=$typeAbo;
                }
            }
            catch (Exception $ex) {
                print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
                die();
            }
            self::cloturer($ps, $connexion);

            return($typeAbonnements);
        }

        public function ajouterTypeAbonnement($typeAbonnement)
        {
            $ajoutOk=false;
            $connexion=null;
            $ps=null;

            try{
                $connexion=DaoFactory::getInstance()->getConnexion();
                $ps = $connexion->prepare(self::$AJOUT);

                $temp = $typeAbonnement->getLibelle();
                $ps->bindParam(':libelle',      $temp,      PDO::PARAM_STR);
                unset($temp);
                $temp = $typeAbonnement->getPrix();
                $ps->bindParam(':prix',         $temp,         PDO::PARAM_INT);
                unset($temp);
                $temp = $typeAbonnement->getDuree();
                $ps->bindParam(':duree',        $temp,        PDO::PARAM_INT);
                unset($temp);
                $temp = $typeAbonnement->getNbUnite();
                $ps->bindParam(':nbUnite',      $temp,      PDO::PARAM_INT);
                unset($temp);
                $temp = $typeAbonnement->getCours();
                $ps->bindParam(':cours',        $temp,        PDO::PARAM_STR);
                unset($temp);
                $temp = $typeAbonnement->getSupprime();
                $ps->bindParam(':supprime',     $temp,     PDO::PARAM_STR);
                unset($temp);
                
                $ajoutOk = $ps->execute();
                
            } catch (Exception $ex) {
                print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
                die();
            }
                self::cloturer($ps, $connexion);
            return($ajoutOk);
        }
        public function supprimerPhysTypeAbonnement($typeAbonnement)
        {
        	$connexion=null;
            $ps=null;
            $rep=null;
            try{
                $connexion=DaoFactory::getInstance()->getConnexion();
                $ps=$connexion->prepare(self::$SUPPRIMERPHYS);
                $ps->execute(array($typeAbonnement->getLibelle()));
                $count=$ps->rowCount();
            }
            catch(Exception $ex){
                print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
     	    	die();
            }
            self::cloturer($ps,$connexion);
            return($count);
        }
        public function supprimerTypeAbonnement($typeAbonnement)
        {
            $connexion=null;
            $ps=null;
            $rep=null;
            try{
                $connexion=DaoFactory::getInstance()->getConnexion();
                $ps=$connexion->prepare(self::$SUPPRIMER);
                $ps->execute(array($typeAbonnement->getLibelle()));
                $count=$ps->rowCount();
            }
            catch(Exception $ex){
                print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
                die();
            }
            self::cloturer($ps,$connexion);
            return($count);
        }
        public function modifierTypeAbonnement($typeAbonnement)
        {
        	$modifOk=false;
            $connexion=null;
            $ps=null;

            try{
                $connexion=DaoFactory::getInstance()->getConnexion();
                $ps=$connexion->prepare(self::$MODIFIER);
                
                $temp = $typeAbonnement->getLibelle();
                $ps->bindParam(':libelle',      $temp,      PDO::PARAM_STR);
                unset($temp);
                $temp = $typeAbonnement->getPrix();
                $ps->bindParam(':prix',         $temp,         PDO::PARAM_INT);
                unset($temp);
                $temp = $typeAbonnement->getDuree();
                $ps->bindParam(':duree',        $temp,        PDO::PARAM_INT);
                unset($temp);
                $temp = $typeAbonnement->getNbUnite();
                $ps->bindParam(':nbUnite',      $temp,      PDO::PARAM_INT);
                unset($temp);
                $temp = $typeAbonnement->getCours();
                $ps->bindParam(':cours',        $temp,        PDO::PARAM_STR);
                unset($temp);
                $temp = $typeAbonnement->getSupprime();
                $ps->bindParam(':supprime',     $temp,     PDO::PARAM_STR);
                unset($temp);
                
                $modifOk = $ps->execute();
                
            } catch (Exception $ex) {
                print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
                die();
            }
                self::cloturer($ps, $connexion);
            return($modifOk);
        }
        
        private function cloturer($prep, $con, $rep=null) {
            try{
                $prep->closeCursor();
            }catch(Exception $e ){
                $prep = null;
            }
            $con= null;
            $rep=null;
        }

    }
?>