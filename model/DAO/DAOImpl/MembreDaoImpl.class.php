<?php

/* ---------------------------------------------------------------------------------------------------
 * Author : Jérôme Hainaut - Jamar Frédéric - Evrard Laurent
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

class MembreDaoImpl implements MembreDao {
	private static $GET = "SELECT * FROM MEMBRE WHERE idMembre = :idMembre";
	private static $GETAUTH = "SELECT * FROM MEMBRE WHERE email = :email AND mdp = :mdp AND supprime='F'";
	private static $GETEMAIL = "SELECT * FROM MEMBRE WHERE email = :email";
	private static $GETLAST = "SELECT * FROM MEMBRE order by idMembre desc limit 1";

	private static $LISTER = "SELECT * FROM MEMBRE WHERE supprime='F' ORDER BY idMembre";
	private static $LISTERNONBAN = "SELECT * FROM MEMBRE WHERE supprime='F' and statut = 'F' ORDER BY idMembre";
	private static $SUPPRIMER = "UPDATE MEMBRE SET supprime='T' where idMembre=:idMembre";
	private static $SUPPRIMERPHYS = "DELETE FROM MEMBRE where idMembre=:idMembre";

	private static $AJOUT = "INSERT INTO MEMBRE (idMembre, nom, prenom, sexe, adr_pays, adr_ville, adr_rue, adr_cp, dateNaissance, fonction, statut, mdp, email, telFix, telGsm, dateInscription, logo, supprime, niveau, nameSection) VALUES (:idMembre, :nom, :prenom, :sexe, :adr_pays, :adr_ville, :adr_rue, :adr_cp, :dateNaissance, :fonction, :statut, :mdp, :email, :telFix, :telGsm, :dateInscription, :logo, :supprime, :niveau, :nameSection)";

	private static $MAJ = "UPDATE MEMBRE SET nom =:nom, prenom =:prenom, sexe =:sexe, adr_pays =:adr_pays,adr_ville=:adr_ville,adr_rue=:adr_rue, adr_cp=:adr_cp, dateInscription=:dateInscription,  dateNaissance=:dateNaissance, fonction=:fonction, statut=:statut, mdp=:mdp, email=:email, telFix=:telFix, telGsm=:telGsm, logo=:logo, niveau=:niveau,nameSection =:nameSection where idMembre=:idMembre";

	private static $RECHNOMPREN = "select * from MEMBRE where nom like :nom and prenom like :prenom and statut='F'";

	function MembreDaoImpl() {
	}

	private function cloturer($prep, $con, $rep = null) {
		try {
			$prep->closeCursor();
		} catch (Exception $e) {
			$prep = null;
		}
		$con = null;
		$rep = null;
	}

	public function rechercherNomPrenom($membre) {
		$ajoutReussi = false;
		$con = null; //connection
		$ps = null; //reponse sql
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $RECHNOMPREN);

			$ps->execute(array (
				":nom"=>"%".$membre->getNom()."%",
				":prenom"=>"%".$membre->getPrenom()."%"
			));

			$ajoutReussi = array ();
			while ($rep = $ps->fetch()) {
				$membre = new Membre();
				$membre->setAdresse(new Adresse($rep['adr_rue'], $rep['adr_cp'], $rep['adr_ville'], $rep['adr_pays']));
				$membre->setDateInscription(new DateTime($rep['dateInscription']));
				$membre->setEmail($rep['email']);
				$membre->setIdMembre($rep['idMembre']);
				$membre->setNom($rep['nom']);
				$membre->setPrenom($rep['prenom']);
				$membre->setSexe($rep['sexe']);
				$membre->setDateNaissance(new DateTime($rep['dateNaissance']));
				$membre->setFonction($rep['fonction']);
				$membre->setMdp($rep['mdp']);
				$membre->setLogo($rep['logo']);
				$membre->setSupprime($rep['supprime']);
				$membre->setStatut($rep['statut']);
				$membre->setTelFixe($rep['telFix']);
				$membre->setTelGsm($rep['telGsm']);
				$membre->setNiveau($rep['niveau']);
				$membre->setNameSection($rep['nameSection']);

				$ajoutReussi[] = $membre;
			}

		} catch (Exception $ex) {
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con);
		return $ajoutReussi;
	}

	public function ajouterMembre($membre) {
		$ajoutReussi = false;
		$con = null; //connection
		$ps = null; //reponse sql
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $AJOUT);
			
			$temp = $membre->getIdMembre();
			$ps->bindParam(':idMembre', $temp, PDO :: PARAM_INT);
			unset($temp); $temp = $membre->getNom();
			$ps->bindParam(':nom', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getPrenom();
			$ps->bindParam(':prenom', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getSexe();
			$ps->bindParam(':sexe', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getAdresse()->getPays();
			$ps->bindParam(':adr_pays', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getAdresse()->getVille();
			$ps->bindParam(':adr_ville', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getAdresse()->getRue();
			$ps->bindParam(':adr_rue', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getAdresse()->getCodePostale();
			$ps->bindParam(':adr_cp', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getDateNaissance()->format(Membre :: $FORMATDATE);
			$ps->bindParam(':dateNaissance', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getFonction();
			$ps->bindParam(':fonction', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getStatut();
			$ps->bindParam(':statut', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getMdp();
			$ps->bindParam(':mdp', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getEmail();
			$ps->bindParam(':email', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getTelFixe();
			$ps->bindParam(':telFix', $temp, PDO :: PARAM_INT);
			unset($temp); $temp = $membre->getTelGsm();
			$ps->bindParam(':telGsm', $temp, PDO :: PARAM_INT);
			unset($temp); $temp = $membre->getDateInscription()->format(Membre :: $FORMATIMESTAMP);
			$ps->bindParam(':dateInscription', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getLogo();
			$ps->bindParam(':logo', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getSupprime();
			$ps->bindParam(':supprime', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getNiveau();
			$ps->bindParam(':niveau', $temp, PDO :: PARAM_INT);
			unset($temp); $temp = $membre->getNameSection();
			$ps->bindParam(':nameSection',$temp, PDO :: PARAM_STR);

			$ajoutReussi = $ps->execute();

		} catch (Exception $ex) {
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con);
		return $ajoutReussi;
	}

	public function getMembre($idMembre) {
		$membre = null;
		$rep = null; //reponse sql
		$con = null; //connection
		$ps = null; //pour la requete
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $GET);
			$ps->bindParam(':idMembre', $idMembre, PDO :: PARAM_INT);
			$ps->execute();

			if ($rep = $ps->fetch()) {
				$membre = new Membre();
				$membre->setAdresse(new Adresse($rep['adr_rue'], $rep['adr_cp'], $rep['adr_ville'], $rep['adr_pays']));
				$membre->setDateInscription(new DateTime($rep['dateInscription']));
				$membre->setEmail($rep['email']);
				$membre->setIdMembre($rep['idMembre']);
				$membre->setNom($rep['nom']);
				$membre->setPrenom($rep['prenom']);
				$membre->setSexe($rep['sexe']);
				$membre->setDateNaissance(new DateTime($rep['dateNaissance']));
				$membre->setFonction($rep['fonction']);
				$membre->setMdp($rep['mdp']);
				$membre->setLogo($rep['logo']);
				$membre->setSupprime($rep['supprime']);
				$membre->setStatut($rep['statut']);
				$membre->setTelFixe($rep['telFix']);
				$membre->setTelGsm($rep['telGsm']);
				$membre->setNiveau($rep['niveau']);
				$membre->setNameSection($rep['nameSection']);
			}

		} catch (Exception $ex) {
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con, $rep);
		return $membre;
	}

	public function listerMembre() {
		$liste = array ();
		$con = null;
		$ps = null;
		$rep = null;
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $LISTER);
			$ps->execute();

			$i = 0;
			while ($rep = $ps->fetch()) {
				$membre = new Membre();
				$membre->setAdresse(new Adresse($rep['adr_rue'], $rep['adr_cp'], $rep['adr_ville'], $rep['adr_pays']));
				$membre->setDateInscription($rep['dateInscription']);
				$membre->setEmail($rep['email']);
				$membre->setIdMembre($rep['idMembre']);
				$membre->setNom($rep['nom']);
				$membre->setPrenom($rep['prenom']);
				$membre->setSexe($rep['sexe']);
				$membre->setDateNaissance($rep['dateNaissance']);
				$membre->setFonction($rep['fonction']);
				$membre->setMdp($rep['mdp']);
				$membre->setLogo($rep['logo']);
				$membre->setSupprime($rep['supprime']);
				$membre->setStatut($rep['statut']);
				$membre->setTelFixe($rep['telFix']);
				$membre->setTelGsm($rep['telGsm']);
				$membre->setNiveau($rep['niveau']);
				$membre->setNameSection($rep['nameSection']);

				$liste[$i] = $membre;
				$i++;
			}
		} catch (Exception $ex) {
			echo ("Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con, $rep);
		return $liste;
	}
	
	public function listerMembreNonBan() {
		$liste = array ();
		$con = null;
		$ps = null;
		$rep = null;
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $LISTERNONBAN);
			$ps->execute();

			$i = 0;
			while ($rep = $ps->fetch()) {
				$membre = new Membre();
				$membre->setAdresse(new Adresse($rep['adr_rue'], $rep['adr_cp'], $rep['adr_ville'], $rep['adr_pays']));
				$membre->setDateInscription($rep['dateInscription']);
				$membre->setEmail($rep['email']);
				$membre->setIdMembre($rep['idMembre']);
				$membre->setNom($rep['nom']);
				$membre->setPrenom($rep['prenom']);
				$membre->setSexe($rep['sexe']);
				$membre->setDateNaissance($rep['dateNaissance']);
				$membre->setFonction($rep['fonction']);
				$membre->setMdp($rep['mdp']);
				$membre->setLogo($rep['logo']);
				$membre->setSupprime($rep['supprime']);
				$membre->setStatut($rep['statut']);
				$membre->setTelFixe($rep['telFix']);
				$membre->setTelGsm($rep['telGsm']);
				$membre->setNiveau($rep['niveau']);
				$membre->setNameSection($rep['nameSection']);

				$liste[$i] = $membre;
				$i++;
			}
		} catch (Exception $ex) {
			echo ("Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con, $rep);
		return $liste;
	}

	public function supprimerMembre($idMembre) {
		$suppressionReussie = false;
		$con = null; //connection
		$ps = null;
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $SUPPRIMER);
			$ps->bindParam(':idMembre', $idMembre, PDO :: PARAM_INT);
			$ps->execute();
			$nb = $ps->rowCount();
			$suppressionReussie = $nb == 1;
		} catch (Exception $ex) {
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con);

		return $suppressionReussie;
	}

	public function supprimerPhysMembre($idMembre) {
		$suppressionReussie = false;
		$con = null; //connection
		$ps = null;
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $SUPPRIMERPHYS);
			$ps->bindParam(':idMembre', $idMembre, PDO :: PARAM_INT);
			$ps->execute();
			$nb = $ps->rowCount();
			$suppressionReussie = $nb == 1;
		} catch (Exception $ex) {
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con);

		return $suppressionReussie;
	}

	public function modifierMembre($membre) {
		$modifReussit = false;
		$con = null; //connection
		$ps = null; //reponse sql
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $MAJ);

			$temp = $membre->getIdMembre();
			$ps->bindParam(':idMembre', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getNom();
			$ps->bindParam(':nom', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getPrenom();
			$ps->bindParam(':prenom', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getSexe();
			$ps->bindParam(':sexe', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getAdresse()->getPays();
			$ps->bindParam(':adr_pays', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getAdresse()->getVille();
			$ps->bindParam(':adr_ville', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getAdresse()->getRue();
			$ps->bindParam(':adr_rue', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getAdresse()->getCodePostale();
			$ps->bindParam(':adr_cp', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getDateNaissance()->format(Membre :: $FORMATDATE);
			$ps->bindParam(':dateNaissance', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getDateInscription()->format(Membre :: $FORMATIMESTAMP);
			$ps->bindParam(':dateInscription', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getFonction();
			$ps->bindParam(':fonction', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getStatut();
			$ps->bindParam(':statut', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getMdp();
			$ps->bindParam(':mdp', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getEmail();
			$ps->bindParam(':email', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getTelFixe();
			$ps->bindParam(':telFix', $temp, PDO :: PARAM_INT);
			unset($temp); $temp = $membre->getTelGsm();
			$ps->bindParam(':telGsm', $temp, PDO :: PARAM_INT);
			unset($temp); $temp = $membre->getLogo();
			$ps->bindParam(':logo', $temp, PDO :: PARAM_STR);
			unset($temp); $temp = $membre->getNiveau();
			$ps->bindParam(':niveau', $temp, PDO :: PARAM_INT);
			unset($temp); $temp = $membre->getNameSection();
			$ps->bindParam(':nameSection', $temp, PDO :: PARAM_STR);

			$modifReussit = $ps->execute();

		} catch (Exception $ex) {
			echo ("Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con);
		return $modifReussit;
	}

	public function getMembreEmail($email) {
		$membre = null;
		$rep = null; //reponse sql
		$con = null; //connection
		$ps = null; //pour la requete
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $GETEMAIL);
			$ps->bindParam(':email', $email, PDO :: PARAM_STR);
			$ps->execute();

			if ($rep = $ps->fetch()) {
				$membre = new Membre();
				$membre->setAdresse(new Adresse($rep['adr_rue'], $rep['adr_cp'], $rep['adr_ville'], $rep['adr_pays']));
				$membre->setDateInscription(new DateTime($rep['dateInscription']));
				$membre->setEmail($email);
				$membre->setIdMembre($rep['idMembre']);
				$membre->setNom($rep['nom']);
				$membre->setPrenom($rep['prenom']);
				$membre->setSexe($rep['sexe']);
				$membre->setDateNaissance(new DateTime($rep['dateNaissance']));
				$membre->setFonction($rep['fonction']);
				$membre->setMdp($rep['mdp']);
				$membre->setLogo($rep['logo']);
				$membre->setSupprime($rep['supprime']);
				$membre->setStatut($rep['statut']);
				$membre->setTelFixe($rep['telFix']);
				$membre->setTelGsm($rep['telGsm']);
				$membre->setNiveau($rep['niveau']);
				$membre->setNameSection($rep['nameSection']);
			}

		} catch (Exception $ex) {
			echo("Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con, $rep);
		return $membre;
	}

	public function getLastMembre() {
		$membre = null;
		$rep = null; //reponse sql
		$con = null; //connection
		$ps = null; //pour la requete
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $GETLAST);
			$ps->execute();

			if ($rep = $ps->fetch()) {
				$membre = new Membre();
				$membre->setAdresse(new Adresse($rep['adr_rue'], $rep['adr_cp'], $rep['adr_ville'], $rep['adr_pays']));
				$membre->setDateInscription(new DateTime($rep['dateInscription']));
				$membre->setEmail($rep['email']);
				$membre->setIdMembre($rep['idMembre']);
				$membre->setNom($rep['nom']);
				$membre->setPrenom($rep['prenom']);
				$membre->setSexe($rep['sexe']);
				$membre->setDateNaissance(new DateTime($rep['dateNaissance']));
				$membre->setFonction($rep['fonction']);
				$membre->setMdp($rep['mdp']);
				$membre->setLogo($rep['logo']);
				$membre->setSupprime($rep['supprime']);
				$membre->setStatut($rep['statut']);
				$membre->setTelFixe($rep['telFix']);
				$membre->setTelGsm($rep['telGsm']);
				$membre->setNiveau($rep['niveau']);
				$membre->setNameSection($rep['nameSection']);
			}

		} catch (Exception $ex) {
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con, $rep);
		return $membre;
	}

	public function checkLogin($authentifier) {
		$membre = null;
		$mail =null;
		$mdp = null;
		$rep = null;
		$con = null;
		$ps = null;
		try {
			$con = DaoFactory :: getInstance()->getConnexion();
			$ps = $con->prepare(self :: $GETAUTH);
			$mail = $authentifier->getEmail();
			$mdp = $authentifier->getMdp();
			$ps->bindParam(':email', $mail, PDO :: PARAM_STR);
			$ps->bindParam(':mdp', $mdp, PDO :: PARAM_STR);
			$ps->execute();

			if ($rep = $ps->fetch()) {
				$membre = new Membre();
				$membre->setAdresse(new Adresse($rep['adr_rue'], $rep['adr_cp'], $rep['adr_ville'], $rep['adr_pays']));
				$membre->setDateInscription(new DateTime($rep['dateInscription']));
				$membre->setEmail($rep['email']);
				$membre->setIdMembre($rep['idMembre']);
				$membre->setNom($rep['nom']);
				$membre->setPrenom($rep['prenom']);
				$membre->setSexe($rep['sexe']);
				$membre->setDateNaissance(new DateTime($rep['dateNaissance']));
				$membre->setFonction($rep['fonction']);
				$membre->setMdp($rep['mdp']);
				$membre->setLogo($rep['logo']);
				$membre->setSupprime($rep['supprime']);
				$membre->setStatut($rep['statut']);
				$membre->setTelFixe($rep['telFix']);
				$membre->setTelGsm($rep['telGsm']);
				$membre->setNiveau($rep['niveau']);
				$membre->setNameSection($rep['nameSection']);
			}

		} catch (Exception $erreur) {
			Logger :: getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>" . $ex->getLine());
		}
		self :: cloturer($ps, $con, $rep);
		return $membre;
	}
}