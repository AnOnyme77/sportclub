<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class SeanceDaoImpl implements SeanceDao{
    
    // Requête SQL
    private static $AJOUT="insert into SEANCE(cours,dateCours, nbMembreMax,idProf) values(?,?,?,?)";
	private static $GETIDENTIFIANT="select noSeance from SEANCE where cours=? and dateCours=? and nbMembreMax=? and idProf=?";
    private static $MODIF="update SEANCE set cours=?,dateCours=?,nbMembreMax=?,idProf=? where noSeance=?";
    private static $SUPPRIMER="delete from SEANCE where noSeance=?";
    private static $LISTER="select * from SEANCE";
    private static $GET="select * from SEANCE where noSeance=?";
	private static $GETCOURS="select distinct(cours) from SEANCE";
	private static $BETTERGETCOURS="select distinct(nameSection) as 'cours' from NIVEAU where supprime='F' union select distinct(cours) from SEANCE union select distinct(cours) from TYPEABONNEMENT where supprime='F' and cours!=''";
	private static $BETTERLISTER="select noSeance,cours,dateCours,nbMembreMax,idProf,nom,prenom from SEANCE,MEMBRE where SEANCE.idProf=MEMBRE.idMEMBRE";
    
	public function getCours(){
		$section=false;
		$connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion = DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$BETTERGETCOURS);
            $ps->execute();
            
			$section=array();
            while($rep=$ps->fetch()){
                $section[]=$rep["cours"];
            }
        }
        catch (Exception $ex) {
			print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
 	    	die();
		}
		self::cloturer($ps, $connexion);
        
        return($section);
	}
	
    public function ajouterSeance($seance){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$AJOUT);

            $ps->execute(array($seance->getCours(),
                               $seance->getDateCours(),
                               $seance->getNbMembreMax(),
                               $seance->getIdProf()));
			try{
				$ps->closeCursor();
			}catch(Exception $e ){
				$ps = null;
				Logger::getInstance()->logify($this,"L'ajout de la séance a échoué");
			}
			
			$ps = $con->prepare(self::$GETIDENTIFIANT);

            $ps->execute(array($seance->getCours(),
                               $seance->getDateCours(),
                               $seance->getNbMembreMax(),
                               $seance->getIdProf()));
			
			while($donnees=$ps->fetch())
				$retour=$donnees["noSeance"];
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
    public function modifierSeance($seance){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$MODIF);

            $ps->execute(array($seance->getCours(),
							   $seance->getDateCours(),
							   $seance->getNbMembreMax(),
							   $seance->getIdProf(),
							   $seance->getNoSeance()));
			
			$retour=true;
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
    public function supprimerSeance($seance){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$SUPPRIMER);

            $ps->execute(array($seance->getNoSeance()));
			$retour=true;
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
	
    public function listerSeances(){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$LISTER);

            $ps->execute();
			
			$retour=array();
			while($donnees=$ps->fetch()){
				$seance=new Seance();
				$seance->setCours($donnees["cours"]);
				$seance->setNoSeance($donnees["noSeance"]);
				$seance->setDateCours($donnees["dateCours"]);
				$seance->setNbMembreMax($donnees["nbMembreMax"]);
				$seance->setIdProf($donnees["idProf"]);
				$retour[]=$seance;
			}
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
	public function betterListerSeances(){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$BETTERLISTER);

            $ps->execute();
			
			$retour=array();
			while($donnees=$ps->fetch()){
				$retour[]=$donnees;
			}
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
    public function getSeance($seance){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$GET);

            $ps->execute(array($seance->getNoSeance()));
			if($donnees=$ps->fetch()){
				$retour=new Seance();
				$retour->setNoSeance($donnees["noSeance"]);
				$retour->setCours($donnees["cours"]);
				$retour->setDateCours($donnees["dateCours"]);
				$retour->setNbMembreMax($donnees["nbMembreMax"]);
				$retour->setIdProf($donnees["idProf"]);
			}
			else{
				Logger::getInstance()->logify($this,"Aucune séance retournée");
			}
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
    
    //Cette méthode cloture les connexions àa la base de données
    private function cloturer($prep, $con, $rep=null) {
		try{
			$prep->closeCursor();
		}catch(Exception $e ){
			$prep = null;
		}
		$con= null;
		$rep=null;
	}
}
?>