<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
    class Persistance
    {
        //Cette classe contient les informations nécessaires à la connexion à la db
        private $urlDB;
        private $utilisateurDB;
        private $mdpDB;
        private $typeDB;
        private $base;
        private $persistant;
        
        /*Constructeur*/
        public function __construct($url,$utilisateur,$mdp,$base,$type,$persistant){
            $this->urlDB=$url;
            $this->utilisateurDB=$utilisateur;
            $this->mdpDB=$mdp;
            $this->typeDB=$type;
            $this->base=$base;
            $this->persistant=$persistant;
        }

        /* get & set */
        public function getPersistant(){
            return($this->persistant);
        }
        public function setPersistant($persistant){
            $this->persistant=$persistant;
        }
        public function getUrlDB()
        {
            return($this->urlDB);
        }
        public function setUrlDB($urlDB)
        {
            $this->urlDB=$urlDB;
        }
        public function getUtilisateurDB()
        {
            return($this->utilisateurDB);
        }
        public function setUtilisateurDB($utilisateurDB)
        {
            $this->utilisateurDB=$utilisateurDB;
        }
        public function getMdpDB()
        {
            return($this->mdpDB);
        }
        public function setMdpDB($mdpDB)
        {
            $this->mdpDB=$mdpDB;
        }
        public function getTypeDB()
        {
            return($this->typeDB);
        }
        public function setTypeDB($typeDB)
        {
            $this->typeDB=$typeDB;
        }
        public function getBase()
        {
            return($this->base);
        }
        public function setBase($base)
        {
            $this->base=$base;
        }

        /* fin get & set */

    }
?>
