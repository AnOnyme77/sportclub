<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class InscriptionDaoImpl implements InscriptionDao{
    
    private static $AJOUT="insert into INSCRIPTION(noSeance,idMembre,present) values(?,?,?)";
    private static $MODIF="update INSCRIPTION set present=? where noSeance=? and idMembre=?";
    private static $SUPPRIMER="delete from INSCRIPTION where noSeance=? and idMembre=?";
    private static $LISTER="select * from INSCRIPTION";
    private static $GET="select * from INSCRIPTION where noSeance=? and idMembre=?";
	private static $GETNOMBRE="select count(*) from INSCRIPTION where noSeance=?";
	private static $GETFROMSEANCE="select * from INSCRIPTION where noSeance=?";
    private static $BETTERLISTER="select SEANCE.noSeance,cours,dateCours,(select count(*) from INSCRIPTION where INSCRIPTION.noSeance=SEANCE.noSeance) as 'compte',nom,prenom,SEANCE.idProf,nbMembreMax from SEANCE left outer join INSCRIPTION on  INSCRIPTION.noSeance = SEANCE.noSeance inner join MEMBRE on MEMBRE.idMembre = SEANCE.idProf";
    private static $PRESENCEELEVE="select SEANCE.noSeance,cours,dateCours,(select count(*) from INSCRIPTION where INSCRIPTION.noSeance=SEANCE.noSeance) as 'compte',nom,prenom,SEANCE.idProf,nbMembreMax,(select count(*) from INSCRIPTION where INSCRIPTION.idMembre=? and INSCRIPTION.noSeance=SEANCE.noSeance) as present from SEANCE left outer join INSCRIPTION on  INSCRIPTION.noSeance = SEANCE.noSeance inner join MEMBRE on MEMBRE.idMembre = SEANCE.idProf;";
	private $connexion;
    
    public function InscriptionDaoImpl(){
        $this->connexion=DaoFactory::getInstance()->getConnexion();
    }
    
	public function getPresenceEleve($inscription){
		$retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$PRESENCEELEVE);
			$ps->execute(array($inscription->getIdMembre()));
			
			$retour=array();
			while($donnees=$ps->fetch()){
				$retour[]=$donnees;
			}
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de suppression");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
	}
	
	public function getNombre($seance){
		$retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$GETNOMBRE);

            $ps->execute(array($seance->getNoSeance()));
			
			if($donnees=$ps->fetch()){
				$retour = $donnees[0];
			}
			else{
				Logger::getInstance()->logify($this,"Aucune inscription trouvée");
			}
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de suppression");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
	}
	
	public function getFromSeance($seance){
		$retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$GETFROMSEANCE);

            $ps->execute(array($seance->getNoSeance()));
			
			$retour=array();
			while($donnees=$ps->fetch()){
				$inscription=new Inscription();
				$inscription->setIdMembre($donnees["idMembre"]);
				$inscription->setNoSeance($donnees["noSeance"]);
				$inscription->setPresent($donnees["present"]);
				$retour[] = $inscription;
			}

            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de suppression");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
	}
	
    public function ajouterInscription($presence){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$AJOUT);

            $ps->execute(array($presence->getNoSeance(),
                               $presence->getIdMembre(),
                               $presence->getPresent()));
			
			$retour=true;
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
    public function modifierInscription($presence){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$MODIF);

            $ps->execute(array($presence->getPresent(),
							   $presence->getNoSeance(),
                               $presence->getIdMembre()));
			
			$retour=true;
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
    public function supprimerInscription($presence){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$SUPPRIMER);

            $ps->execute(array($presence->getNoSeance(),
                               $presence->getIdMembre()));
			
			$retour=true;
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de suppression");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
    public function listerInscription(){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$LISTER);
			$ps->execute();
			
			$retour=array();
			
			while($donnees=$ps->fetch()){
				$insc = new Inscription();
				$insc->setNoSeance($donnees["noSeance"]);
				$insc->setIdMembre($donnees["idMembre"]);
				$insc->setPresent($donnees["present"]);
				$retour[]=$insc;
			}
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de suppression");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
	public function betterListerInscription(){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$BETTERLISTER);
			$ps->execute();
			
			$retour=array();
			
			while($donnees=$ps->fetch()){
				$retour[]=$donnees;
			}
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de suppression");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
    public function getInscription($presence){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$GET);

            $ps->execute(array($presence->getNoSeance(),
                               $presence->getIdMembre()));
			
			if($donnees=$ps->fetch()){
				$retour = new Inscription();
				$retour->setNoSeance($donnees["noSeance"]);
				$retour->setIdMembre($donnees["idMembre"]);
				$retour->setPresent($donnees["present"]);
			}
			else{
				Logger::getInstance()->logify($this,"Aucune inscription trouvée");
			}
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de suppression");
            die();
        }
        self::cloturer($ps, $con);
		return $retour;
    }
	
	    //Cette méthode cloture les connexions àa la base de données
    private function cloturer($prep, $con, $rep=null) {
		try{
			$prep->closeCursor();
		}catch(Exception $e ){
			$prep = null;
		}
		$con= null;
		$rep=null;
	}
}
?>