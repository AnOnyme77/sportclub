<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Hainaut Jérôme
 * Team : Dev4u
 * créé le 22/04/2014 - modifée le 22/04/2014
 -----------------------------------------------------------------------------------------------------*/
class ExamenDaoImpl implements ExamenDao{
    //Implémente la gestion des Examen
	
	//Requête SQL
    private static $LISTER="select * from EXAMEN";
    private static $AJOUT="insert into EXAMEN(datePassage,cote,commentaire,niveau,nameSection,idMembre,idProf) values(:datePassage,:cote,:commentaire,:niveau,:nameSection,:idMembre,:idProf)";
    private static $SUPPRIMER="delete from EXAMEN where idMembre=:idMembre and nameSection=:nameSection and niveau=:niveau and datePassage=:datePassage";
	private static $GET="select * from EXAMEN where idMembre=:idMembre and nameSection=:nameSection and niveau=:niveau and datePassage=:datePassage";
	private static $MODIFIER="update EXAMEN set cote=:cote,commentaire=:commentaire where idMembre=:idMembre and nameSection=:nameSection and niveau=:niveau and datePassage=:datePassage";
	private static $RECHERCHE="select * from EXAMEN where datePassage like '%:datePassage%' and cote like '%:cote%' and commentaire like '%:commentaire%' and niveau like '%:niveau%' and nameSection like '%:nameSection%' and idMembre like '%:idMembre%' and idProf like '%:idProf%' ";
    private static $GETEXAMENS="select * from EXAMEN where idMembre=? and nameSection=? and niveau=?";
	
    public function listerExamen(){
        $examens=array();
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion = DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$LISTER);
            $ps->execute();
            
            while($rep=$ps->fetch()){
                $examen=new Examen();
                $examen->setDatePassage($rep["datePassage"]);
                $examen->setCommentaire($rep["commentaire"]);
                $examen->setCote($rep["cote"]);
                $examen->setNiveau($rep["niveau"]);
                $examen->setNameSection($rep["nameSection"]);
                $examen->setIdMembre($rep["idMembre"]);
                $examen->setIdProf($rep["idProf"]);
                
                $examens[]=$examen;
            }
        }
        catch (Exception $ex) {
			echo("Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
			Logger::getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
		}
		self::cloturer($ps, $connexion);
        
        return($examens);
    }
    public function getExamen($examen){
		$retour=null;
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$GET);
            
			$temp = $examen->getDatePassage()->format(Membre::$FORMATIMESTAMP);
            $ps->bindParam(':datePassage', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getNiveau();
            $ps->bindParam(':niveau', $temp, PDO::PARAM_INT);
            unset($temp); $temp = $examen->getNameSection();
            $ps->bindParam(':nameSection', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getIdMembre();
            $ps->bindParam(':idMembre', $temp, PDO::PARAM_STR);
            unset($temp);

            $ps->execute();
            if($rep=$ps->fetch()){
				$retour=new Examen();
                $retour->setDatePassage($rep["datePassage"]);
                $retour->setCommentaire($rep["commentaire"]);
                $retour->setCote($rep["cote"]);
                $retour->setNiveau($rep["niveau"]);
                $retour->setNameSection($rep["nameSection"]);
                $retour->setIdMembre($rep["idMembre"]);
                $retour->setIdProf($rep["idProf"]);
                
            }
			else{
				Logger::getInstance()->logify($this,"Récupération impossible: datePassage:".$examen->getDatePassage()->format(Membre::$FORMATIMESTAMP)." Niveau: ". $examen->getNiveau().' nameSection: '. $examen->getNameSection().' idMembre: '. $examen->getIdMembre());
			}
        }
        catch(Exception $ex){
			echo("Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
			Logger::getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
        }
        self::cloturer($ps,$connexion);
        return($retour);
    }
    
	public function getExamenMembre($examen){
		$listeRetour=null;
		$retour=null;
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$GETEXAMENS);
			
            $ps->execute(array($examen->getIdMembre(),$examen->getNameSection(),$examen->getNiveau()));
			$listeRetour=array();
            while($rep=$ps->fetch()){
				$retour=new Examen();
                $retour->setDatePassage($rep["datePassage"]);
                $retour->setCommentaire($rep["commentaire"]);
                $retour->setCote($rep["cote"]);
                $retour->setNiveau($rep["niveau"]);
                $retour->setNameSection($rep["nameSection"]);
                $retour->setIdMembre($rep["idMembre"]);
                $retour->setIdProf($rep["idProf"]);
				
				$listeRetour[]=$retour;
            }
        }
        catch(Exception $ex){
			echo("Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
			Logger::getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
        }
        self::cloturer($ps,$connexion);
        return($listeRetour);
    }
	
    public function ajouterExamen($examen){
        $ajoutReussi=false;
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$AJOUT);
            
            $temp = $examen->getDatePassage()->format(Membre::$FORMATIMESTAMP);
			$ps->bindParam(':datePassage', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getNiveau();
			$ps->bindParam(':niveau', $temp, PDO::PARAM_INT);
            unset($temp); $temp = $examen->getNameSection();
			$ps->bindParam(':nameSection', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getIdMembre();
			$ps->bindParam(':idMembre', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getIdProf();
			$ps->bindParam(':idProf', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getCote();
			$ps->bindParam(':cote', $temp, PDO::PARAM_INT);
            unset($temp); $temp = $examen->getCommentaire();
			$ps->bindParam(':commentaire', $temp, PDO::PARAM_STR);
			unset($temp); 

            $ajoutReussi=$ps->execute();
        }
        catch(Exception $ex){
			echo("Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
			Logger::getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
        }
        self::cloturer($ps,$connexion);
        return($ajoutReussi);
    }
	
    public function modifierExamen($examen){
        $modifReussit=false;
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$MODIFIER);

            $temp = $examen->getCote();
            $ps->bindParam(':cote', $temp, PDO::PARAM_INT);
            unset($temp); $temp = $examen->getCommentaire();
            $ps->bindParam(':commentaire', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getIdMembre();
            $ps->bindParam(':idMembre', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getNameSection();
            $ps->bindParam(':nameSection', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getNiveau();
            $ps->bindParam(':niveau', $temp, PDO::PARAM_INT);
            $temp = $examen->getDatePassage()->format(Membre::$FORMATIMESTAMP);
            $ps->bindParam(':datePassage', $temp, PDO::PARAM_STR);
            unset($temp);
			
            $modifReussit=$ps->execute();
           		
        }
        catch(Exception $ex){
			echo("Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
			Logger::getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
        }
        self::cloturer($ps,$connexion);
        return($modifReussit);
    }
	
    public function supprimerExamen($examen){
		$suppressionReussie = false;
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$SUPPRIMER);
            
			$temp = $examen->getDatePassage()->format(Membre::$FORMATIMESTAMP);
            $ps->bindParam(':datePassage', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getNiveau();
            $ps->bindParam(':niveau', $temp, PDO::PARAM_INT);
            unset($temp); $temp = $examen->getNameSection();
            $ps->bindParam(':nameSection', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getIdMembre();
            $ps->bindParam(':idMembre', $temp, PDO::PARAM_STR);
            unset($temp);
			
            $ps->execute();
			$suppressionReussie = $ps->rowCount()==1;
        }
        catch(Exception $ex){
			echo("Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
			Logger::getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
        }
        
		return $suppressionReussie;
    }
    
	public function rechercherExamen($examen){
		$retour=null;
		$connexion=null;
		$retour=false;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$RECHERCHE);
            
			$temp = $examen->getDatePassage()->format(Membre::$FORMATIMESTAMP);
            $ps->bindParam(':datePassage', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getNiveau();
            $ps->bindParam(':niveau', $temp, PDO::PARAM_INT);
            unset($temp); $temp = $examen->getNameSection();
            $ps->bindParam(':nameSection', $temp, PDO::PARAM_STR);
            unset($temp); $temp = $examen->getIdMembre();
            $ps->bindParam(':idMembre', $temp, PDO::PARAM_STR);
            unset($temp);
			
            $ps->execute();
			$retour=array();
			while($rep=$ps->fetch()){
				$exam=new Examen();
                $exam->setDatePassage($rep["datePassage"]);
                $exam->setCommentaire($rep["commentaire"]);
                $exam->setCote($rep["cote"]);
                $exam->setNiveau($rep["niveau"]);
                $exam->setNameSection($rep["nameSection"]);
                $exam->setIdMembre($rep["idMembre"]);
                $exam->setIdProf($rep["idProf"]);

				$retour[]=$exam;
			}
        }
        catch(Exception $ex){
			echo("Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
			Logger::getInstance()->logify($this, "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine());
        }
        return $retour;
	}
	
	//Cloture la connexion à la db
    private function cloturer($prep, $con, $rep=null) {
		try{
			$prep->closeCursor();
		}catch(Exception $e ){
			$prep = null;
		}
		$con= null;
		$rep=null;
	}
}