<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class ParserLogger extends Parser{
    
    public function __construct(){}
    
    public function parser($fichierConfiguration){
        $config=null;
        if(file_exists(Parser::getCheminRacine()."/".$fichierConfiguration)){
            $myIniFile = parse_ini_file (Parser::getCheminRacine()."/".$fichierConfiguration,TRUE);
            $active=($myIniFile["Configuration"]["active"]);
            $config= new ConfigLogger();
            $config->setActif($active);
        }
        else{
            echo("Le fichier de configuration du logger n'existe pas");
        }
        return($config);
    }
}
?>