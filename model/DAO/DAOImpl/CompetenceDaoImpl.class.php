<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class CompetenceDaoImpl implements CompetenceDao{
    // Classe gérant les compétences dans la base de donnée
    
    // Requête SQL
    private static $AJOUT="insert into COMPETENCE(niveau,nameSection,titre, detail) values(?,?,?,?)";
    private static $MODIF="update COMPETENCE set detail=? where niveau=? and nameSection=? and titre=?";
    private static $SUPPRIMER="delete from COMPETENCE where niveau=? and nameSection=? and titre=?";
    private static $LISTER="select * from COMPETENCE";
    private static $GET="select * from COMPETENCE where niveau=? and nameSection=? and titre=?";
    private static $GETFROMNIVEAU="select * from COMPETENCE where niveau=? and nameSection=?";
    
    public function getFromNiveau($niveau){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
		try {
			$con = DaoFactory::getInstance()->getConnexion();
			$ps = $con->prepare(self::$GETFROMNIVEAU);

            $ps->execute(array($niveau->getNiveau(),$niveau->getNameSection()));
            
            $retour=array();
            
            while($donnees=$ps->fetch()){
                $competence=new Competence();
                $competence->setNiveau($donnees["niveau"]);
                $competence->setNameSection($donnees["nameSection"]);
                $competence->setTitre($donnees["titre"]);
                $competence->setDetail($donnees["detail"]);
                
                $retour[]=$competence;
            }
		} catch (Exception $ex) {
			print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de listing");
 	    	die();
		}
			self::cloturer($ps, $con);
		return $retour;
    }
    
    // Méthode d'ajout des compétences
    public function ajouterCompetence($competence){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
        
        $daoNiveau=DaoFactory::getInstance()->get("NiveauDao");
        
        $niveau=new Niveau();
        $niveau->setNameSection($competence->getNameSection());
        $niveau->setNiveau($competence->getNiveau());
        
        if($daoNiveau->getNiveau($niveau->getNiveau(),$niveau->getNameSection())!=false){
            try {
                $con = DaoFactory::getInstance()->getConnexion();
                $ps = $con->prepare(self::$AJOUT);
    
                $ps->execute(array($competence->getNiveau(),
                                   $competence->getNameSection(),
                                   $competence->getTitre(),
                                   $competence->getDetail()));
                $retour=true;
                
            } catch (Exception $ex) {
                print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
                Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
                die();
            }
            self::cloturer($ps, $con);
        }
		return $retour;
    }
    public function modifierCompetence($competence){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
           
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$MODIF);
            
            $ps->execute(array($competence->getDetail(),
                               $competence->getNiveau(),
                               $competence->getNameSection(),
                               $competence->getTitre()));
            $retour=true;
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de modification");
            die();
        }
        self::cloturer($ps, $con);

		return $retour;
    }
    public function supprimerCompetence($competence){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
           
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$SUPPRIMER);
              
            $ps->execute(array($competence->getNiveau(),
                               $competence->getNameSection(),
                               $competence->getTitre()));
            $retour=true;
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de suppression");
            die();
        }
        self::cloturer($ps, $con);

		return $retour;
    }
    public function listerCompetences(){
        $retour=false;
		$con = null;//connection
		$ps = null; //reponse sql
		try {
			$con = DaoFactory::getInstance()->getConnexion();
			$ps = $con->prepare(self::$LISTER);

            $ps->execute(array());
            
            $retour=array();
            
            while($donnees=$ps->fetch()){
                $competence=new Competence();
                $competence->setNiveau($donnees["niveau"]);
                $competence->setNameSection($donnees["nameSection"]);
                $competence->setTitre($donnees["titre"]);
                $competence->setDetail($donnees["detail"]);
                
                $retour[]=$competence;
            }
		} catch (Exception $ex) {
			print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de listing");
 	    	die();
		}
			self::cloturer($ps, $con);
		return $retour;
    }
    public function getCompetence($competence){
        $retour=new Competence();
		$con = null;//connection
		$ps = null; //reponse sql
		try {
			$con = DaoFactory::getInstance()->getConnexion();
			$ps = $con->prepare(self::$GET);

            $ps->execute(array($competence->getNiveau(),
                               $competence->getNameSection(),
                               $competence->getTitre()));
            
            if($donnees=$ps->fetch()){
                $retour->setNiveau($donnees["niveau"]);
                $retour->setNameSection($donnees["nameSection"]);
                $retour->setTitre($donnees["titre"]);
                $retour->setDetail($donnees["detail"]);
            }
            else{
                $retour=false;
            }
		} catch (Exception $ex) {
			print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes de recupération");
 	    	die();
		}
			self::cloturer($ps, $con);
		return $retour;
    }
    
    //Cette méthode cloture les connexions àa la base de données
    private function cloturer($prep, $con, $rep=null) {
		try{
			$prep->closeCursor();
		}catch(Exception $e ){
			$prep = null;
		}
		$con= null;
		$rep=null;
	}
}
?>