<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
    class ParserConfig extends Parser{
        //Cette classe permet de lire et retourner un objet permettant l'accès à la base de données
        public function __construct(){}
        
        public function parser($cheminFichier){
            $cheminRacine=Parser::getCheminRacine();
            $myIniFile = parse_ini_file ($cheminRacine."/".$cheminFichier,TRUE);
            $urlDB=($myIniFile["Serveur BDD"]["urlDB"]);
            $utilisateurDB=($myIniFile["Serveur BDD"]["utilisateurDB"]);
            $mdpDB=($myIniFile["Serveur BDD"]["mdpDB"]);
            $base=($myIniFile["Serveur BDD"]["base"]);
            $typeDB=($myIniFile["Serveur BDD"]["typeDB"]);
            if(isset($myIniFile["Configuration"]["persistant"]))
                $persistant=($myIniFile["Configuration"]["persistant"]);
            else
                $persistant=true;
            return(new Persistance($urlDB,$utilisateurDB,
                                   $mdpDB,$base,$typeDB,$persistant));
        }
    }
?>