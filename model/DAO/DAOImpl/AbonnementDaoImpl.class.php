<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

    class AbonnementDaoImpl implements AbonnementDao
    {
	private static $LISTER= "SELECT * from ABONNEMENT";
	private static $AJOUT= "INSERT INTO ABONNEMENT(idMembre,libelle,dateDebut,dateAchat,nbUniteRest,prix,dateFin) VALUES(:idMembre,:libelle,:dateDebut,:dateAchat,:nbUniteRest,:prix,:dateFin)";
	private static $SUPPRIMER= "DELETE from ABONNEMENT where idMembre=? and libelle=?";
	private static $GET= "SELECT * from ABONNEMENT where idMembre=:idMembre and libelle=:libelle and dateAchat=:dateAchat";
	private static $GETABOMEMBRE= "SELECT * from ABONNEMENT where idMembre=:idMembre";
	private static $MODIFIER= "UPDATE ABONNEMENT SET dateDebut=:dateDebut,nbUniteRest=:nbUniteRest,prix=:prix,dateFin=:dateFin
                                                    WHERE idMembre=:idMembre and libelle=:libelle and dateAchat=:dateAchat";
    private static $BETTERLISTER="select ABONNEMENT.idMembre,libelle,dateDebut,dateAchat,nbUniteRest,prix,dateFin,nom,prenom from ABONNEMENT,MEMBRE where ABONNEMENT.idMembre=MEMBRE.idMembre";
	private static $SCAN="select ABONNEMENT.libelle,dateFin,TYPEABONNEMENT.cours,TYPEABONNEMENT.nbUnite,MEMBRE.idMembre,ABONNEMENT.nbUniteRest,ABONNEMENT.dateAchat from ABONNEMENT,MEMBRE,TYPEABONNEMENT where
							ABONNEMENT.libelle=TYPEABONNEMENT.libelle and ABONNEMENT.idMembre=MEMBRE.idMembre and ABONNEMENT.nbUniteRest !=0 and (dateFin>=CURRENT_DATE or dateFin IS NULL) and ABONNEMENT.idMembre=? order by TYPEABONNEMENT.cours";

	
    function AbonnementDaoImpl(){}
	
	public function scanMembre($idMembre){
		$connexion=null;
        $ps=null;
        $rep=null;
        $abo=false;

        try
        {
            $connexion = DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$SCAN);
            
			$abo=array();
            $ps->execute(array($idMembre));
            while($rep=$ps->fetch()){
                $abo[]=$rep;
            }
        }
        catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            die();
        }
        self::cloturer($ps, $connexion);

        return($abo);
	}

    public function getAbonnement($idMembre, $libelle, $dateAchat)
    {
        $connexion=null;
        $ps=null;
        $rep=null;
        $abo=false;

        try
        {
            $connexion = DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$GET);

            $temp=$idMembre;
            $ps->bindParam(':idMembre', $temp, PDO::PARAM_INT);
            unset($temp); $temp=$libelle;
            $ps->bindParam(':libelle', $temp, PDO::PARAM_STR);
            unset($temp); $temp=$dateAchat->format(Abonnement::$FORMATIMESTAMP);
            $ps->bindParam(':dateAchat', $temp, PDO::PARAM_STR);
            unset($temp);
            
            $ps->execute();
            if($rep=$ps->fetch())
            {
                $abo=new Abonnement();
                $abo->setIdMembre($rep["idMembre"]);
                $abo->setLibelle($rep["libelle"]);
                $abo->setDateDebut(new DateTime($rep["dateDebut"]));
                $abo->setDateAchat(new DateTime($rep["dateAchat"]));
                $abo->setNbUniteRest($rep["nbUniteRest"]);
                $abo->setPrix($rep["prix"]);
                if(is_null($rep["dateFin"]))
                    $abo->setDateFin(null);
                else
                    $abo->setDateFin(new DateTime($rep["dateFin"]));
            }
        }
        catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            die();
        }
        self::cloturer($ps, $connexion);

        return($abo);
    }

    public function listerAbonnements()
    {
        $retour=false;
        $con = null;
        $ps = null;
        
        try {
            $con = DaoFactory::getInstance()->getConnexion();
            $ps = $con->prepare(self::$BETTERLISTER);

            $ps->execute();
            //Tableau de retour des données
            $retour=array();
            while($donnees=$ps->fetch()){
                //On enregistre chaque record dans le tableau.
                //C'est le useCase qui s'occupera de générer les objets
                $retour[]=$donnees;
            }
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            Logger::getInstance()->logify($this,"Erreur dans les reqêtes d'ajout");
            die();
        }
        self::cloturer($ps, $con);
        return $retour;
    }

    public function ajouterAbonnement($abonnement)
    {
        $ajoutOk=false;
        $connexion=null;
        $ps=null;

        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps = $connexion->prepare(self::$AJOUT);

            $temp = $abonnement->getIdMembre();
            $ps->bindParam(':idMembre',     $temp,  PDO::PARAM_INT);
            
            unset($temp); $temp = $abonnement->getLibelle();
            $ps->bindParam(':libelle',      $temp,  PDO::PARAM_STR);

            unset($temp); $temp = $abonnement->getDateDebut()->format(Abonnement::$FORMATDATE);
            $ps->bindParam(':dateDebut',    $temp,  PDO::PARAM_STR);

            unset($temp); $temp = $abonnement->getDateAchat()->format(Abonnement::$FORMATIMESTAMP);
            $ps->bindParam(':dateAchat',    $temp,  PDO::PARAM_STR);

            unset($temp); $temp = $abonnement->getNbUniteRest();
            $ps->bindParam(':nbUniteRest',  $temp,  PDO::PARAM_INT);

            unset($temp); $temp = $abonnement->getPrix();
            $ps->bindParam(':prix',         $temp,  PDO::PARAM_INT);

            unset($temp); 
            if(!is_null($abonnement->getDateFin())){
                $temp = $abonnement->getDateFin()->format(Abonnement::$FORMATDATE);
                $ps->bindParam(':dateFin',      $temp,  PDO::PARAM_STR);
            }
            else{
                $temp = null;
                $ps->bindParam(':dateFin',      $temp,  PDO::PARAM_INT);
            } 
            unset($temp);
            
            $ajoutOk = $ps->execute();
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            die();
        }
            self::cloturer($ps, $connexion);
        return($ajoutOk);
    }
    public function supprimerAbonnement($abonnement)
    {
    	$connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$SUPPRIMER);
            $ps->execute(array($abonnement->getIdMembre(),
                               $abonnement->getLibelle()));
            $count=$ps->rowCount();
        }
        catch(Exception $ex){
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
 	    	die();
        }
        self::cloturer($ps,$connexion);
        return($count);
    }
    public function modifierAbonnement($abonnement)
    {
    	$modifOk=false;
        $connexion=null;
        $ps=null;

        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$MODIFIER);
            
            $temp = $abonnement->getIdMembre();
            $ps->bindParam(':idMembre',     $temp,  PDO::PARAM_INT);
            
            unset($temp); $temp = $abonnement->getLibelle();
            $ps->bindParam(':libelle',      $temp,  PDO::PARAM_STR);

            unset($temp); $temp = $abonnement->getDateDebut()->format(Abonnement::$FORMATDATE);
            $ps->bindParam(':dateDebut',    $temp,  PDO::PARAM_STR);

            unset($temp); $temp = $abonnement->getDateAchat()->format(Abonnement::$FORMATIMESTAMP);
            $ps->bindParam(':dateAchat',    $temp,  PDO::PARAM_STR);

            unset($temp); $temp = $abonnement->getNbUniteRest();
            $ps->bindParam(':nbUniteRest',  $temp,  PDO::PARAM_INT);

            unset($temp); $temp = $abonnement->getPrix();
            $ps->bindParam(':prix',         $temp,  PDO::PARAM_INT);

            unset($temp); 
            if(!is_null($abonnement->getDateFin())){
                $temp = $abonnement->getDateFin()->format(Abonnement::$FORMATDATE);
                $ps->bindParam(':dateFin',      $temp,  PDO::PARAM_STR);
            }
            else{
                $temp = null;
                $ps->bindParam(':dateFin',      $temp,  PDO::PARAM_INT);
            } 
            unset($temp);
            
            $modifOk = $ps->execute();
            
        } catch (Exception $ex) {
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
            die();
        }
            self::cloturer($ps, $connexion);
        return($modifOk);
    }
    public function getAbonnementsMembre($idMembre)
    {
    	$abonnements = array();
    	$connexion=null;
        $ps=null;
        $rep=null;

        try{
            $connexion = DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$GETABOMEMBRE);

            $ps->bindParam(':idMembre', $idMembre, PDO::PARAM_INT);
            
            $ps->execute();
            
            while($rep=$ps->fetch()){
                $abo=new Abonnement();
                $abo->setIdmembre($rep["idMembre"]);
                $abo->setLibelle($rep["libelle"]);
                $abo->setDateDebut(new DateTime($rep["dateDebut"]));
                $abo->setDateAchat(new DateTime($rep["dateAchat"]));
                $abo->setNbUniteRest($rep["nbUniteRest"]);
                $abo->setPrix($rep["prix"]);
                if(is_null($rep["dateFin"]))
                    $abo->setDateFin(null);
                else
                    $abo->setDateFin(new DateTime($rep["dateFin"]));
                $abonnements[]=$abo;
            }
        }
        catch (Exception $ex) {
			print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
 	    	die();
		}
		self::cloturer($ps, $connexion);

    	return($abonnements);
    }
    
    private function cloturer($prep, $con, $rep=null) {
        try{
            $prep->closeCursor();
        }catch(Exception $e ){
            $prep = null;
        }
        $con= null;
        $rep=null;
    }

    }
?>