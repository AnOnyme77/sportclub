<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 30/05/2014
 -----------------------------------------------------------------------------------------------------*/
class ParametresDaoImpl extends Parser implements ParametresDao{
    //Implémentation de la gestion du fichier de parametres
    private static $fichierConfig = "config/parametres.ini";
    //private static $fichierConfig = "config/parametresLocal.ini";
    private static $enteteConfig= "Parametres";
    
    public function __construct(){}
    
    //Parse le fichier de parametre et retourne un tableau de celui ci
    public function parser($cheminFichier){
        if(!file_exists($cheminFichier)){
            Logger::getInstance()->logify($this,"Le fichier de paramètres est introuvable");
            throw new Exception("Le fichier de paramètres est introuvable");
        }
        
        $myIniFile=parse_ini_file ($cheminFichier,TRUE);
        
        return($myIniFile);
    }
    
    //Retourne le chemin complet vers le fichier de parametres
    public static function getfichierParametres(){
        return(Parser::getCheminRacine()."/".self::$fichierConfig);
    }
    
    //Retourne un objet de type parametres
    public function getParametres(){
        $parametres=new Parametre;
        try{
            $config=$this->parser($this->getCheminRacine()."/".self::$fichierConfig);
            
            $parametres->setNomEntreprise($config["Parametres"]["nomEntreprise"]);
            $parametres->setNumTel($config["Parametres"]["numTel"]);
            $parametres->setAdresseEntreprise(new Adresse($config["Parametres"]["rue"],$config["Parametres"]["cp"],
                                                          $config["Parametres"]["ville"],$config["Parametres"]["pays"]));
            $parametres->setSuperLogin($config["Parametres"]["superLogin"]);
            $parametres->setSuperMdp($config["Parametres"]["superMdp"]);
            $parametres->setNomResp($config["Parametres"]["nomResp"]);
            $parametres->setPrenomResp($config["Parametres"]["prenomResp"]);
            $parametres->setCoteMax($config["Parametres"]["coteMax"]);
            $parametres->setLogo($config["Parametres"]["logo"]);
            $parametres->setTailleLogoHaut($config["Parametres"]["tailleLogoHaut"]);
            $parametres->setTailleLogoLarge($config["Parametres"]["tailleLogoLarge"]);
            $parametres->setTailleLogoNivHaut($config["Parametres"]["tailleLogoNivHaut"]);
            $parametres->setTailleLogoNivLarge($config["Parametres"]["tailleLogoNivLarge"]);
            $parametres->setTailleLogoMembreHaut($config["Parametres"]["tailleLogoMembreHaut"]);
            $parametres->setTailleLogoMembreLarge($config["Parametres"]["tailleLogoMembreLarge"]);
            $parametres->setLibNiveau($config["Parametres"]["libNiveau"]);
            $parametres->setLibExamen($config["Parametres"]["libExamen"]);
            $parametres->setLibCompetence($config["Parametres"]["libCompetence"]);
            $parametres->setLibNiveaux($config["Parametres"]["libNiveaux"]);
            $parametres->setLibExamens($config["Parametres"]["libExamens"]);
            $parametres->setLibCompetences($config["Parametres"]["libCompetences"]);
        }
        catch(Exception $e){
            echo($e->getMessage());
            $parametres=false;
        }
        Logger::getInstance()->logify($this,"Lecture du fichier de parametres");
        return($parametres);
    }
    
    public function modifierParametres($parametres){
        $retour=false;
        $fichier=fopen("parametres.ini","w");
        
        if($fichier==false)
            throw new Exception("Erreur à l'ouverture du fichier");
        
        fwrite($fichier,"[".self::$enteteConfig."]\n");
        fwrite($fichier,"nomEntreprise=".$parametres->getNomEntreprise()."\n");
        fwrite($fichier,"numTel=".$parametres->getNumTel()."\n");
        fwrite($fichier,"rue=".$parametres->getAdresseEntreprise()->getRue()."\n");
        fwrite($fichier,"cp=".$parametres->getAdresseEntreprise()->getCodePostale()."\n");
        fwrite($fichier,"ville=".$parametres->getAdresseEntreprise()->getVille()."\n");
        fwrite($fichier,"pays=".$parametres->getAdresseEntreprise()->getPays()."\n");
        fwrite($fichier,"superLogin=".$parametres->getSuperLogin()."\n");
        fwrite($fichier,"superMdp=".$parametres->getSuperMdp()."\n");
        fwrite($fichier,"nomResp=".$parametres->getNomResp()."\n");
        fwrite($fichier,"prenomResp=".$parametres->getPrenomResp()."\n");
        fwrite($fichier,"coteMax=".$parametres->getCoteMax()."\n");
        fwrite($fichier,"logo=".$parametres->getLogo()."\n");
        fwrite($fichier,"tailleLogoHaut=".$parametres->getTailleLogoHaut()."\n");
        fwrite($fichier,"tailleLogoLarge=".$parametres->getTailleLogoLarge()."\n");
        fwrite($fichier,"tailleLogoNivHaut=".$parametres->getTailleLogoNivHaut()."\n");
        fwrite($fichier,"tailleLogoNivLarge=".$parametres->getTailleLogoNivLarge()."\n");
        fwrite($fichier,"tailleLogoMembreHaut=".$parametres->getTailleLogoMembreHaut()."\n");
        fwrite($fichier,"tailleLogoMembreLarge=".$parametres->getTailleLogoMembreLarge()."\n");
        fwrite($fichier,"libNiveau=".$parametres->getLibNiveau()."\n");
        fwrite($fichier,"libExamen=".$parametres->getLibExamen()."\n");
        fwrite($fichier,"libCompetence=".$parametres->getLibCompetence()."\n");
        fwrite($fichier,"libNiveaux=".$parametres->getLibNiveaux()."\n");
        fwrite($fichier,"libExamens=".$parametres->getLibExamens()."\n");
        fwrite($fichier,"libCompetences=".$parametres->getLibCompetences()."\n");
        fclose($fichier);
        
        if(rename("parametres.ini",parent::getCheminRacine()."/".self::$fichierConfig))
            $retour=true;
        
        Logger::getInstance()->logify($this,"Modification du fichier de paramètres");
        return($retour);
    }
    
    public function initialiserParametres(){
        
    }
}

?>