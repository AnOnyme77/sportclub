<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Laurent Evrard
 * Team : Dev4u
 * créé le ??/??/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class NiveauDaoImpl implements NiveauDao{
    //Implémente la gestion des niveau
	
	//Requête SQL
    private static $LISTER="select * from NIVEAU where supprime='F' order by niveau";
    private static $AJOUT="insert into NIVEAU(niveau,libelle,nameSection,logo,supprime) values(?,?,?,?,?)";
    private static $SUPPRIMER="delete from NIVEAU where niveau=? and nameSection=?";
	private static $GET="select * from NIVEAU where niveau=? and nameSection=?";
	private static $MODIFIER="update NIVEAU set niveau=?, libelle=?, nameSection=?, logo=?, supprime=? where niveau=? and nameSection=?";
	private static $RECHERCHE="select * from NIVEAU where niveau like '%?%' and nameSection like '%?%' and libelle like '%?%' and logo like '%?%'";
    private static $GETSECTION="select distinct(nameSection) from NIVEAU  where supprime='F'";
	
	public function getSections(){
		$section=false;
		$connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion = DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$GETSECTION);
            $ps->execute();
            
			$section=array();
            while($rep=$ps->fetch()){
                $section[]=$rep["nameSection"];
            }
        }
        catch (Exception $ex) {
			print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
 	    	die();
		}
		self::cloturer($ps, $connexion);
        
        return($section);
	}
	
    public function listerNiveau(){
        $niveaux=array();
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion = DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$LISTER);
            $ps->execute();
            
            while($rep=$ps->fetch()){
                $niv=new Niveau();
                $niv->setNiveau($rep["niveau"]);
                $niv->setLibelle($rep["libelle"]);
                $niv->setNameSection($rep["nameSection"]);
                $niv->setLogo($rep["logo"]);
                $niv->setSupprime($rep["supprime"]);
                
                $niveaux[]=$niv;
            }
        }
        catch (Exception $ex) {
			print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
 	    	die();
		}
		self::cloturer($ps, $connexion);
        
        return($niveaux);
    }
    public function getNiveau($niveau,$nameSection){
		$retour=new Niveau();
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$GET);
            $ps->execute(array($niveau,
							   $nameSection));
            if($rep=$ps->fetch()){
				$retour->setNiveau($rep["niveau"]);
				$retour->setLibelle($rep["libelle"]);
				$retour->setNameSection($rep["nameSection"]);
				$retour->setLogo($rep["logo"]);
				$retour->setSupprime($rep["supprime"]);
            }
			else{
				$retour=false;
				Logger::getInstance()->logify($this,"Récupération impossible: niveau:".$niveau." section: ".$nameSection);
			}
        }
        catch(Exception $ex){
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
 	    	die();
        }
        self::cloturer($ps,$connexion);
        return($retour);
    }
    public function ajouterNiveau($niveau){
        $retour=false;
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$AJOUT);
            $ps->execute(array($niveau->getNiveau(),
                               $niveau->getLibelle(),
                               $niveau->getNameSection(),
                               $niveau->getLogo(),
                               $niveau->getSupprime()));
            $compte=$ps->rowCount();
            if($compte==1){
                $retour=true;
            }
			else{
				Logger::getInstance()->logify($this,"Ajout du niveau impossible niveau: ".$niveau->getNiveau()." section: ".$niveau->getNaleSection());
			}
        }
        catch(Exception $ex){
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
 	    	die();
        }
        self::cloturer($ps,$connexion);
        return($retour);
    }
	
    public function modifierNiveau($niveau){
        $retour=false;
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$MODIFIER);
            $ps->execute(array($niveau->getNiveau(),
                               $niveau->getLibelle(),
                               $niveau->getNameSection(),
                               $niveau->getLogo(),
                               $niveau->getSupprime(),
							   $niveau->getNiveau(),
							   $niveau->getNameSection()));
            $compte=$ps->rowCount();
            if($compte==1){
                $retour=true;
            }
			else{
				Logger::getInstance()->logify($this,"Modification impossible: ".$compte." lignes modifiées, niveau:".$niveau->getNiveau()." section: ".$niveau->getNameSection());
			}
        }
        catch(Exception $ex){
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
 	    	die();
        }
        self::cloturer($ps,$connexion);
        return($retour);
    }
	
    public function supprimerNiveau($niveau){
        $connexion=null;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$SUPPRIMER);
            $ps->execute(array($niveau->getNiveau(),
                               $niveau->getNameSection()));
            $compte=$ps->rowCount();
        }
        catch(Exception $ex){
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
			Logger::getInstance()->logify($this,"Suppression impossible: niveau:".$niveau->getNiveau()." section: ".$niveau->getNameSection());
 	    	die();
        }
        self::cloturer($ps,$connexion);
        return($compte);
    }
    
	public function rechercherNiveau($niveau){
		$connexion=null;
		$retour=false;
        $ps=null;
        $rep=null;
        try{
            $connexion=DaoFactory::getInstance()->getConnexion();
            $ps=$connexion->prepare(self::$RECHERCHE);
            $ps->execute(array(($niveau->getNiveau()!=null?$niveau->getNiveau():''),
                               ($niveau->getNameSection()!=null?$niveau->getNameSection():''),
							   ($niveau->getLibelle()!=null?$niveau->getLibelle():''),
							   ($niveau->getLogo()!=null?$niveau->getLogo():'')));
			if($ps->rowCount()>0){
				$retour=array();
				while($rep=$ps->fetch()){
					$niveau=new Niveau();
					$niveau->setNiveau($rep["niveau"]);
					$niveau->setLibelle($rep["libelle"]);
					$niveau->setNameSection($rep["nameSection"]);
					$niveau->setLogo($rep["logo"]);
					$niveau->setSupprime($rep["supprime"]);
					
					$retour[]=$niveau;
				}
			}
			else{
				Logger::getInstance()->logify($this,"Aucun enregistrement retourné");
			}
        }
        catch(Exception $ex){
            print "Erreur !: " . $ex->getMessage() . "<br/>".$ex->getLine();
			Logger::getInstance()->logify($this,"Recherche impossible: niveau:".$niveau->getNiveau()." section: ".$niveau->getNameSection());
 	    	die();
        }
        self::cloturer($ps,$connexion);
        return($retour);
	}
	
	//Cloture la connexion à la db
    private function cloturer($prep, $con, $rep=null) {
		try{
			$prep->closeCursor();
		}catch(Exception $e ){
			$prep = null;
		}
		$con= null;
		$rep=null;
	}
}
?>