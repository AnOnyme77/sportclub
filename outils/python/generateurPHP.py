#!/usr/bin/python
# -*-coding:utf-8 -*

import string

def genGetters(var,fichier):
    if(len(var)>0):
        nom=string.capitalize(var[0])+var[1:]
        fichier.write("        public function get"+str(nom)+"()\n")
        fichier.write("        {\n")
        fichier.write("            return($this->"+str(var)+");\n")
        fichier.write("        }\n")

def genSetters(var,fichier):
    if(len(var)>0):
        nom=string.capitalize(var[0])+var[1:]
        fichier.write("        public function set"+str(nom)+"($"+str(var)+")\n")
        fichier.write("        {\n")
        fichier.write("            $this->"+str(var)+"=$"+str(var)+";\n")
        fichier.write("        }\n")

def genDeclaration(var,fichier):
    fichier.write("        private $"+str(var)+";\n")

def getInfos():
    infos={}
    infos["classe"]=raw_input("Entrez le nom (emplacement) de la classe: ")
    
    infos["variables"]=[]
    variable=raw_input("Nom de la variable (!END pour terminer): ")
    while variable!="!END":
        infos["variables"].append(variable)
        variable=raw_input("Nom de la variable (!END pour terminer): ")
    return(infos)
    
infos=getInfos()
fichier=open(infos["classe"]+".class.php","w")
fichier.write("<?php\n")
fichier.write("    class "+infos["classe"]+"\n")
fichier.write("    {\n")
for elem in infos["variables"]:
    genDeclaration(elem,fichier)
fichier.write("\n        /* get & set */\n\n")
for elem in infos["variables"]:
    genGetters(elem,fichier)
    genSetters(elem,fichier)
fichier.write("\n        /* fin get & set */\n\n")
fichier.write("    }\n")
fichier.write("?>\n")
