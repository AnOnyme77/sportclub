#!/usr/bin/python
# -*-coding:utf-8 -*

import os

def tester(ok,ko):
    for f in os.listdir("."):
        if(os.path.isfile(str(f))):
            foud=False
            fichier=open(str(f),"r")
            if("//testPythonOK" in fichier.read()):
                fichier.close()
                sortie=os.popen("php "+str(f))
                for line in sortie:
                    if("ok" in line.lower()):
                        ok.append(str(f)+": "+line)
                    else:
                        ko.append(str(f)+": "+line)
                    
            else:
                fichier.close()
        else:
            old=os.getcwd()
            os.chdir(f)
            print "Entree: "+f
            ok,ko=tester(ok,ko)
            print "Sortie: "+f
            os.chdir(old)
    return(ok,ko)

chemin="../../model/tests/"
old=os.getcwd()

ok=[]
ko=[]
os.chdir(chemin)

ok,ko=tester(ok,ko)
print "\t\tTests OK"
if(len(ok)!=0):
    for line in ok:
        print line
else:
    print "Aucun test n a reussi"

print "\t\t Tests KO"
if(len(ko)!=0):
    for line in ko:
        print line
else:
    print "Aucune erreur dans les tests"

print "\n\t\t"+"------------------------------------------------"
print "\n\t\t"+str(len(ok))+" tests réussi(s) sur "+str(len(ok)+len(ko))+" passé(s)"
print "\n\t\t"+str(100*float(len(ok))/float(len(ok)+len(ko)))+"% des tests ont réussis"


os.chdir(old)