<?php
    /* ---------------------------------------------------------------------------------------------------
    * Author : Evrard Laurent
    * Team : Dev4u
    * créé le 03/04/2014 - modifée le 17/04/2014
    -----------------------------------------------------------------------------------------------------*/
    
    /*On supprime les anciens autoloader*/
    spl_autoload_register(null, false);
    
    /*On définit les extensions que l'on veut charger automatiquement*/
    spl_autoload_extensions('.php, .class.php, .lib.php, .ini');
    
    /*On définit nos loader*/
    function domainLoader($className){
        $filename=$className.".class.php";
        $way=ROOTWAY."/domain/".$filename;
        if(!file_exists($way))
            return false;
        require_once($way);
    }
    
    function controllerLoader($className){
        $filename=$className.".class.php";
        $way=ROOTWAY."/controller/".$filename;
        if(!file_exists($way))
            return false;
        require_once($way);
    }
    function DAOImplLoader($className){
        $filename=$className.".class.php";
        $way=ROOTWAY."/model/DAO/DAOImpl/".$filename;
        if(!file_exists($way))
            return false;
        require_once($way);
    }
    function DAOIntLoader($className){
        $filename=$className.".class.php";
        $way=ROOTWAY."/model/DAO/DAOInt/".$filename;
        if(!file_exists($way))
            return false;
        require_once($way);
    }
    function useCaseImplLoader($className){
        $filename=$className.".class.php";
        $way=ROOTWAY."/model/useCase/useCaseImpl/".$filename;
        if(!file_exists($way))
            return false;
        require_once($way);
    }
    function useCaseIntLoader($className){
        $filename=$className.".class.php";
        $way=ROOTWAY."/model/useCase/useCaseInt/".$filename;
        if(!file_exists($way))
            return false;
        require_once($way);
    }
    function configurationLoader($className){
        $filename=$className.".ini";
        $way=ROOTWAY."/config/".$filename;
        if(!file_exists($way))
            return false;
        require_once($way);
    }
    
    function viewLoader($className){
        $filename=$className.".class.php";
        $way=ROOTWAY."/view/".$filename;
        if(!file_exists($way))
            return false;
        require_once($way);
    }
    
    /*On enregistre nos loaders*/
    spl_autoload_register("domainLoader");
    spl_autoload_register("controllerLoader");
    spl_autoload_register("DAOImplLoader");
    spl_autoload_register("DAOIntLoader");
    spl_autoload_register("useCaseImplLoader");
    spl_autoload_register("useCaseIntLoader");
    spl_autoload_register("configurationLoader");
    spl_autoload_register("viewLoader");
    
?>