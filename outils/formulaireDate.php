<div>
<select name="<?= $id ?>jour" id="<?= $id ?>jour">
    <?php
        $i=1;
        if(!isset($jour)){
        	$jour = date('j');
		}
        for($i=1;$i<=31;$i++)
        {
            if($i!=$jour)
                echo("<option value='".$i."'>".$i."</option>");
            else
                echo("<option value='".$i."' selected='selected'>".$i."</option>");
        }
    ?>
</select>
<select name="<?= $id ?>mois" id="<?= $id ?>mois">
    <?php
        $tabMois=array("Janvier","Février","Mars","Avril","Mai","Juin","Juillet",
                       "Août","Septembre","Octobre","Novembre","Décembre");
        $i=1;
        if(!isset($mois)){
        	$mois=date('m');
		}
        for($i=1;$i<=12;$i++)
        {
            if($i!=$mois)
                echo("<option value='".$i."'>".$tabMois[($i-1)]."</option>");
            else
                echo("<option value='".$i."' selected='selected'>".$tabMois[($i-1)]."</option>");
        }
    ?>
</select>
<select name="<?= $id ?>annee" id="<?= $id ?>annee">
    <?php
        if(!isset($annee)){
        	$annee=date("Y");
		}
        
        for($i=($annee-100);$i<=date("Y")+100;$i++)
        {
            if($i!=$annee)
                echo("<option value='".$i."'>".$i."</option>");
            else
                echo("<option value='".$i."' selected='selected'>".$i."</option>");
        }
    ?>
</select>
</div>