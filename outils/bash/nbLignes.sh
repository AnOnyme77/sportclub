#!/bin/bash
somme=0
liste=$(find ../../ -name "*.php" -exec  wc -l {} \; | cut -d"." -f1)
for elem in $liste
do
	let "somme = $somme + $elem"
done

liste=$(find ../../ -name "*.js" -exec  wc -l {} \; | cut -d"." -f1)
for elem in $liste
do
	let "somme = $somme + $elem"
done

liste=$(find ../../ -name "*.py" -exec  wc -l {} \; | cut -d"." -f1)
for elem in $liste
do
	let "somme = $somme + $elem"
done

echo "Le projet contient actuellement: $somme lignes de code"
