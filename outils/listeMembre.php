<?php
if(isset($liste) and is_array($liste) and count($liste)>0){
    echo("<br/><table>");
    echo("<tr>");
        echo("<th>Nom</th>");
        echo("<th>Prenom</th>");
        echo("<th>Email</th>");
        echo("<th>Status</th>");
        echo("<th></th>");
    echo("</tr>");
    foreach($liste as $membre){
        echo("<tr>");
        echo("<td>".$membre->getNom()."</td>");
        echo("<td>".$membre->getPrenom()."</td>");
        echo("<td>".$membre->getEmail()."</td>");
        switch($membre->getFonction()){
            case "A":
                echo("<td>Administrateur</td>");
                break;
            case "P":
                echo("<td>Professeur</td>");
                break;
            case "M":
                echo("<td>Membre</td>");
                break;
		}
        echo("<td><input type='submit' value='Selectionner' onclick='retourner(\"".$membre->getIdMembre()."\",\"".$champs."\")'/></td>");
        echo("</tr>");
    }
    echo("</table>");
}
else{
    echo("<p>Aucun membre ne correspond à votre recherche</p>");
}
?>