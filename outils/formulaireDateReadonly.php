<div>
<select name="<?= $id ?>jour" id="<?= $id ?>jour">
    <?php
        $i=1;
        if(!isset($jour)){
        	$jour = date('j');
		}
        echo("<option disabled='disabled' value='".$jour."' selected='selected'>".$jour."</option>");

    ?>
</select>
<select name="<?= $id ?>mois" id="<?= $id ?>mois">
    <?php
        $tabMois=array("Janvier","Février","Mars","Avril","Mai","Juin","Juillet",
                       "Août","Septembre","Octobre","Novembre","Décembre");
        $i=1;
        if(!isset($mois)){
        	$mois=date('m');
		}
        echo("<option disabled='disabled' value='".$mois."' selected='selected'>".$tabMois[($mois-1)]."</option>");
    ?>
</select>
<select name="<?= $id ?>annee" id="<?= $id ?>annee">
    <?php
        if(!isset($annee)){
        	$annee=date("Y");
		}
        echo("<option disabled='disabled' value='".$annee."' selected='selected'>".$annee."</option>");
    ?>
</select>
</div>