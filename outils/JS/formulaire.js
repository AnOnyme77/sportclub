/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
function formHeure(suite){
    
    var auj = new Date();
    var div = document.createElement("div");
    
    var selectHeure = document.createElement("select");
    selectHeure.setAttribute("id","heure"+suite);
    
    var actuel=1;
    for(actuel=0;actuel<=23;actuel++){
        optionHeure=document.createElement("option");
        optionHeure.setAttribute("value",actuel);
        if (actuel==auj.getHours()) {
            optionHeure.setAttribute("selected","selected");
        }
        
        texte=document.createTextNode(actuel);
        
        optionHeure.appendChild(texte);
        
        selectHeure.appendChild(optionHeure);
    }
    
    var selectMinute = document.createElement("select");
    selectMinute.setAttribute("id","minute"+suite);
    
    var actuel=1;
    for(actuel=0;actuel<=59;actuel++){
        optionMinute=document.createElement("option");
        optionMinute.setAttribute("value",actuel);
        if (actuel==auj.getMinutes()) {
            optionMinute.setAttribute("selected","selected");
        }
        
        texte=document.createTextNode(actuel);
        
        optionMinute.appendChild(texte);
        
        selectMinute.appendChild(optionMinute);
    }
    
    div.appendChild(selectHeure);
    
    div.appendChild(selectMinute);
    
    return(div);
}
function formDate(suite) {
    var auj = new Date();
    var div = document.createElement("div");
    
    var selectJour = document.createElement("select");
    selectJour.setAttribute("id","jour"+suite);
    
    var actuel=1;
    var texte;
    var optionJour;
    var optionMois;
    for(actuel=1;actuel<=31;actuel++){
        optionJour=document.createElement("option");
        optionJour.setAttribute("value",actuel);
        if (actuel==auj.getDate()) {
            optionJour.setAttribute("selected","selected");
        }
        
        texte=document.createTextNode(actuel);
        
        optionJour.appendChild(texte);
        
        selectJour.appendChild(optionJour);
    }
    
    var selectMois = document.createElement("select");
    selectMois.setAttribute("id","mois"+suite);
    var mois=["Janvier","Février","Mars","Avril","Mai","Juin","Juillet",
                       "Août","Septembre","Octobre","Novembre","Décembre"];
    for(actuel=1;actuel<=12;actuel++){
        
        optionMois=document.createElement("option");
        optionMois.setAttribute("value",actuel);
        if (actuel==auj.getMonth()) {
            optionMois.setAttribute("selected","selected");
        }
        texte=document.createTextNode(mois[actuel-1]);
        
        optionMois.appendChild(texte);
        
        selectMois.appendChild(optionMois);
    }
    
    var selectAnnee = document.createElement("select");
    selectAnnee.setAttribute("id","annee"+suite);
    
    for(actuel=auj.getFullYear()-100;actuel<=auj.getFullYear()+30;actuel++){
        
        optionAnnee=document.createElement("option");
        optionAnnee.setAttribute("value",actuel);
        if (actuel==auj.getFullYear()) {
            optionAnnee.setAttribute("selected","selected");
        }
        
        texte=document.createTextNode(actuel);
        
        optionAnnee.appendChild(texte);
        
        selectAnnee.appendChild(optionAnnee);
    }
    
    div.appendChild(selectJour);
    
    div.appendChild(selectMois);
    
    div.appendChild(selectAnnee);
    
    return(div);
}