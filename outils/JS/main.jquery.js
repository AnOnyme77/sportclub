$(document).ready(function()
{
	$("#sortClient").dynatable(
	{
		features:
		{
			paginate: true,
			recordCount: true,
			perPageSelect: true
		},
		
		inputs: {
			queryEvent: 'blur change keyup',
			paginationPrev: 'Précédente',
			paginationNext: 'Suivante',
			recordCountText: '| Affichage '
		},
		
		
		dataset: {
			perPageDefault: 3,
			perPageOptions: [3,6,9,12],
		},
		      
		params: {
			records: 'résultats'
		},
	});
});