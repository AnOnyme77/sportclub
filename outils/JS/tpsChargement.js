/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
function logify(page,temps) {
    var xhr=getXMLHttpRequest();

    nom = page.split("/");
    nom = nom[nom.length - 1];
    nom = nom.substr(0, nom.lastIndexOf("."));
    nom = nom.replace(new RegExp("(%20|_|-)", "g"), "");
    
    var requete="responsetime/log/"+nom+"/"+temps;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    
    xhr.send(null);
}