/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try { xhr = new ActiveXObject("Msxml2.XMLHTTP"); }
            catch(e) { xhr = new ActiveXObject("Microsoft.XMLHTTP"); }
        }
        else xhr = new XMLHttpRequest();
    }
    else{
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest..."); return null;
    }
    return xhr;
}