this.zoom_image = function(){
    xOffset = 10;
    yOffset = 30;
    $(".zoom").hover(function(e){
        $("body").append("<p id='zoom'><img width='350px' src='"+ this.src +"'alt='Visualisation image' /></p>");
        $("#zoom")
        .css("top",(e.pageY - xOffset) + "px")
        .css("left",(e.pageX + yOffset) + "px")
        .fadeIn("slow");
        },
        function(){
            $("#zoom").remove();
            });
            $("a.zoom").mousemove(function(e){
                $("#zoom")
            .css("top",(e.pageY - xOffset) + "px")
            .css("left",(e.pageX + yOffset) + "px");
        });
};
$(document).ready(function(){
zoom_image();
})