<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 05/05/2014 - modifée le 29/05/2014
 -----------------------------------------------------------------------------------------------------*/

class ControllerTypeAbonnement extends Controller
{
    private $useCase;
    
    public function ControllerTypeAbonnement()
    {
        parent::Controller();
        $this->useCase = new GererTypeAbonnementImpl();
        $this->useCaseSeance= new GererSeanceImpl();
    }

    public function actionDefault()
    {
        $this->listerTypeAbonnement();
    }
    
    public function ajouterTypeAbonnement()
    {
       if(in_array($this->fonction,array("A"))){
            $this->useCaseSeance->getCours($this->bundle);
            $vue = new Vue("administrateurs/typeAbonnementAjout");
            $vue->generer(array("ajout"=>true,"listeCours"=>$this->bundle->get(Bundle::$LISTE)),$this->fonction);
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }

    public function ajoutTypeAbonnement($libelle=null,$prix=null,$duree=null,$nbUnite=null,$cours=null,$supprime=null)
    {
        if($libelle!=null && $prix!=null && $supprime!=null && ($duree != -1 || $nbUnite != -1)){
            $objetTypeAbo= new TypeAbonnement();
            $objetTypeAbo->setLibelle($libelle);
            $objetTypeAbo->setPrix($prix);
            $objetTypeAbo->setDuree($duree);
            $objetTypeAbo->setNbUnite($nbUnite);
            $objetTypeAbo->setCours($cours);
            $objetTypeAbo->setSupprime("F");
            
            $this->bundle->put(Bundle::$TYPEABONNEMENT,$objetTypeAbo);
            $this->useCase->ajouterTypeAbonnement($this->bundle);
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été remplis");
            echo("Veuillez remplir tous les champs.");
        }
    }

    public function modiferTypeAbonnement($libelle=null)
    {
        if(in_array($this->fonction,array("A"))){
            $vue = new Vue("administrateurs/typeAbonnementAjout");
            $this->useCase->getTypeAbonnement($libelle,$this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $vue->generer(array("ajout"=>false,
                                "typeAbonnement"=>$this->bundle->get(Bundle::$TYPEABONNEMENT)),$this->fonction);
                }
                else{
                    self::erreur($this->bundle->get($this->bundle->get(Bundle::$MESSAGE)));
                }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }

    public function modificationTypeAbonnement($libelle=null,$prix=null,$duree=null,$nbUnite=null,$cours=null,$supprime=null)
    {
        if($libelle!=null && $prix!=null && $duree!=null && $supprime!=null){
            $objetTypeAbo= new TypeAbonnement();
            $objetTypeAbo->setLibelle($libelle);
            $objetTypeAbo->setPrix($prix);
            $objetTypeAbo->setDuree($duree);
            $objetTypeAbo->setNbUnite($nbUnite);
            if(is_null($cours)) $cours="";
            $objetTypeAbo->setCours($cours);
            $objetTypeAbo->setSupprime("F");
            
            $this->bundle->put(Bundle::$TYPEABONNEMENT,$objetTypeAbo);
            $this->useCase->modifierTypeAbonnement($this->bundle);
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été remplis");
            echo("Veuillez remplir tous les champs.");
        }
    }

    public function renouvellementTypeAbonnement($libelle=null)
    {
        if($libelle!=null){
            $objetTypeAbo= new TypeAbonnement();
            $this->useCase->getTypeAbonnement($libelle,$this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $objetTypeAbo = $this->bundle->get(Bundle::$TYPEABONNEMENT);
                $objetTypeAbo->setSupprime("F");
                $this->bundle->put(Bundle::$TYPEABONNEMENT,$objetTypeAbo);
                $this->useCase->modifierTypeAbonnement($this->bundle);
                if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                    echo($this->bundle->get(Bundle::$MESSAGE));
                }
                else
                    echo("Renouvellement du type d'abonnement effectué avec succès.");
            }
        }
        else{
            Logger::getInstance()->logify($this,"Impossible de récupérer le libelle du type d'abonnement.");
            echo("Impossible de récupérer le libelle du type d'abonnement.");
        }
    }

    public function listerTypeAbonnement()
    {
       if(in_array($this->fonction,array("A","P","M"))){
            $this->useCase->listerTypesAbonnement($this->bundle);
            $vue = new Vue("administrateurs/typeAbonnement");
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                    "liste"=>$this->bundle->get(Bundle::$LISTE),
                                    "fonction"=>unserialize($_SESSION["oMembre"])->getFonction()),
                              $this->fonction);
            }
            else{
                $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE)),$this->fonction);
            }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }

    public function suppressionTypeAbonnement($libelle=null){
        if($libelle!=null){
            $this->useCase->getTypeAbonnement($libelle,$this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $this->useCase->supprimerTypeAbonnement($this->bundle);
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
            else{
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
        }
        else{
            Logger::getInstance()->logify($this,"Paramètres incorrects");
            echo("Les paramètres reçus sont incorrects");
        }
    }

    public function suppressionPhysTypeAbonnement($libelle=null){
        if($libelle!=null){
            $this->useCase->getTypeAbonnement($libelle,$this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $this->useCase->supprimerPhysTypeAbonnement($this->bundle);
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
            else{
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
        }
        else{
            Logger::getInstance()->logify($this,"Paramètres incorrects");
            echo("Les paramètres reçus sont incorrects");
        }
    }
}



?>
