<?php
/*require_once("../view/Vue.class.php");*/

/** Condition pour tout les controlleurs.
 * 
 * Ce qu'il faut savoir l'url doit etre de la forme index.php?page=nomdevotrecontroller&action=fctalancer
 * 		nomdevotrecontroller doit etre le nom de votre classe sans le mot "Controller" 
 * 			(expl: recherchermembre pour la classe ControllerRechercherMembre)
 * 		&action=fctalancer n'est pas obligatoir et dans ce cas la, c'est la fct actionDefault qui serra executer
 * 		fctalancer est la fonction que vous voulez lancer.
 * 
 * rem: pas besoin de faire attention a la casse Oo (j'ai tester...)
 * 
 */
abstract class Controller{
	protected static $INDEXMEMBRESESSION="oMembre";
    protected $fonction=null;
	protected $bundle;
	
	function Controller(){
		self::setFonction();
		$this->bundle = new Bundle();
	}
	
	/**cette fonction permet d'avoir l'action a réaliser par défaut
	 */
	abstract function actionDefault ();
	
	//donne la fonction du membre connecter
	public function setFonction(){
        if(session_status()!=PHP_SESSION_ACTIVE) session_start();
		if(isset($_SESSION[self::$INDEXMEMBRESESSION])){
            $this->fonction = unserialize($_SESSION[self::$INDEXMEMBRESESSION])->getFonction();
		}
		else{
			$this->fonction = "I";
		}
	}
	
	//lancer cette fct si une erreur est rencontrer
	public function erreur($message){
    	$vue = new Vue('erreurs/erreur');
    	$vue->generer(array('message' => $message), $this->fonction);
    }
    
    public function getBundle(){
    	return $this->bundle;
    }
    
}
