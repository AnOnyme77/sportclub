<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class ControllerParametre extends Controller
{
    //Controller des parametres
	
	//Liste des autorisations
    private static $AUTHORIZEDROLE=array("A");
    
	//Objet use case
    private $useCase;
    
	//Constructeur
    public function ControllerParametre(){
		//On contacte la classe mère pour exécuter son contructeur
    	parent::Controller();
		
		//On crée le UC
        $this->useCase= new GererParametresImpl();
    }
    
	//Création de la vue
    public function afficherParametres(){
        if(in_array($this->fonction,self::$AUTHORIZEDROLE)){
            $vue = new Vue("administrateurs/gererParametres");
            $reussi=$this->getParametres(true);
            $vue->generer(array("lectureOk"=>$reussi),$this->fonction);
        }
    }
	
	public function changerLogo(){
        if(isset($_FILES["image"])){
			$lien=Uploader::getInstance()->upload(Uploader::$IMAGE,Uploader::$PARAMETRES,$_FILES["image"]);
            echo($lien);
            $parametre=unserialize($_SESSION["oParametres"]);
            $parametre->setLogo($lien);
            $this->bundle->put(Bundle::$PARAMETRE,$parametre);
            $this->useCase->modifierParametres($this->bundle,true);
            
            header("Location: ".Parser::getChemin()."/parametres");
		}
    }
	
	public function actionDefault(){
		$this->afficherParametres();
	}
    
	//Retourne les paramètres lus dans le fichier
	//Le parametre optionnel représente une obligation de relecture dans le fichier
    public function getParametres($forceRequest=false){
        $retour=false;
        if(session_status()!=PHP_SESSION_ACTIVE)
            session_start();
        if(!isset($_SESSION["oParametres"]) || $forceRequest==true){
            $recup=$this->useCase->getParametres();
            if($recup){
                if(isset($_SESSION["oParametres"]))
                    $retour=true;
            }
        }
        else{
            $retour=true;
        }
        return($retour);
    }
    
	//Fonction de modification des parametres
    /*
    *   POST ?
    */
    public function modifierParametres($test=false){
        $parametres= new Parametre();
		
		if(isset($_POST["nomEntreprise"]) and $_POST["nomEntreprise"]!="")$parametres->setNomEntreprise($_POST["nomEntreprise"]);

		if(isset($_POST["numTel"]) and $_POST["numTel"]!="")$parametres->setNumTel($_POST["numTel"]);
		
		if(isset($_POST["rue"]) and $_POST["rue"]!=""and 
			isset($_POST["cp"]) and $_POST["cp"]!="" and
			isset($_POST["ville"]) and $_POST["ville"]!="" and
			isset($_POST["pays"]) and $_POST["pays"]!="")$parametres->setAdresseEntreprise(new Adresse($_POST["rue"],$_POST["cp"],$_POST["ville"],$_POST["pays"]));

		if(isset($_POST["nomResp"]) and $_POST["nomResp"]!="")$parametres->setNomResp($_POST["nomResp"]);
		
		if(isset($_POST["prenomResp"]) and $_POST["prenomResp"]!="")$parametres->setPrenomResp($_POST["prenomResp"]);
		
		if(isset($_POST["coteMax"]) and $_POST["coteMax"]!="")$parametres->setCoteMax($_POST["coteMax"]);
		
		if(isset($_POST["logo"]) and $_POST["logo"]!="")$parametres->setLogo($_POST["logo"]);
		
		if(isset($_POST["libNiveau"]) and $_POST["libNiveau"]!="")$parametres->setLibNiveau($_POST["libNiveau"]);
		
		if(isset($_POST["libExamen"]) and $_POST["libExamen"]!="")$parametres->setLibExamen($_POST["libExamen"]);
		
		if(isset($_POST["libCompetence"]) and $_POST["libCompetence"]!="")$parametres->setLibCompetence($_POST["libCompetence"]);

        if(isset($_POST["libNiveaux"]) and $_POST["libNiveaux"]!="")$parametres->setLibNiveaux($_POST["libNiveaux"]);
        
        if(isset($_POST["libExamens"]) and $_POST["libExamens"]!="")$parametres->setLibExamens($_POST["libExamens"]);
        
        if(isset($_POST["libCompetences"]) and $_POST["libCompetences"]!="")$parametres->setLibCompetences($_POST["libCompetences"]);
		
        $parametres->setSuperLogin(unserialize($_SESSION["oParametres"])->getSuperLogin());
        $parametres->setLogo(unserialize($_SESSION["oParametres"])->getLogo());
        $parametres->setSuperMdp(unserialize($_SESSION["oParametres"])->getSuperMdp());
		$parametres->setTailleLogoHaut(unserialize($_SESSION["oParametres"])->getTailleLogoHaut());
        $parametres->setTailleLogoLarge(unserialize($_SESSION["oParametres"])->getTailleLogoLarge());
        $parametres->setTailleLogoNivHaut(unserialize($_SESSION["oParametres"])->getTailleLogoNivHaut());
        $parametres->setTailleLogoNivLarge(unserialize($_SESSION["oParametres"])->getTailleLogoNivLarge());
        $parametres->setTailleLogoMembreHaut(unserialize($_SESSION["oParametres"])->getTailleLogoMembreHaut());
        $parametres->setTailleLogoMembreLarge(unserialize($_SESSION["oParametres"])->getTailleLogoMembreLarge());
        $bundle= new Bundle();
        $bundle->put(Bundle::$PARAMETRE,$parametres);
        
        $bundleRetour=$this->useCase->modifierParametres($bundle);
        
        if($test)
            echo("Modification: OK\n");
        else
            echo($bundleRetour->get(Bundle::$MESSAGE));
    }
    
}

?>