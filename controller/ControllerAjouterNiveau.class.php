<?php
class ControllerAjouterNiveau extends Controller{
    
    private static $AUTHORIZEDROLE=array("A","P");
    private $useCase;
    
    public function ControllerAjouterNiveau(){
        parent::Controller();
        $this->bundle= new Bundle();
        $this->useCase=new GererNiveauImpl();
    }
    public function ajouterNiveau(){
        if(in_array($this->fonction,self::$AUTHORIZEDROLE)){
            $niveau =  new Niveau();
            if(isset($_POST["niveau"]))$niveau->setNiveau($_POST["niveau"]);
            if(isset($_POST["section"]))$niveau->setNameSection($_POST["section"]);
            
            $niveau->setLibelle($_POST["libelle"]);
            $niveau->setLogo($_POST["monfichier"]);
            
            $this->bundle->vider();
            $this->bundle->put(Bundle::$NIVEAU,$niveau);
            
            $this->useCase->ajouterNiveau($this->bundle);
            
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Erreur d'autorisation");
            self::erreur("Vous n'avez pas l'autorisation d'accéder à cette page");
        }
    }
    public function supprimerNiveau(){
        if(in_array($this->fonction,self::$AUTHORIZEDROLE)){
            $niveau =  new Niveau();
            if(isset($_POST["niveau"]))$niveau->setNiveau($_POST["niveau"]);
            if(isset($_POST["section"]))$niveau->setNameSection($_POST["section"]);
            
            $this->bundle->vider();
            $this->bundle->put(Bundle::$NIVEAU,$niveau);
            
            $this->useCase->supprimerNiveau($this->bundle);
            
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Erreur d'autorisation");
            self::erreur("Vous n'avez pas l'autorisation d'accéder à cette page");
        }
    }
    public function modifierNiveau(){
        if(in_array($this->fonction,self::$AUTHORIZEDROLE)){
            $niveau =  new Niveau();
            if(isset($_POST["niveau"]))$niveau->setNiveau($_POST["niveau"]);
            if(isset($_POST["section"]))$niveau->setNameSection($_POST["section"]);
            $niveau->setLibelle($_POST["libelle"]);
            $niveau->setLogo($_POST["monfichier"]);
            
            $this->bundle->vider();
            $this->bundle->put(Bundle::$NIVEAU,$niveau);
            
            $this->useCase->modifierNiveau($this->bundle);
            
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Erreur d'autorisation");
            self::erreur("Vous n'avez pas l'autorisation d'accéder à cette page");
        }
    }
    public function actionDefault(){
        
    }
    
    
}
?>