<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class ControllerAccueil extends Controller{
    
    public function ControllerAccueil(){
        parent::Controller();
    }
    public function connexionAccueil(){
        if(isset($_SESSION["oMembre"])){
            $vue = new Vue("accueil");
            $vue->generer(array(),$this->fonction);
        }
        else{
            $vue = new Vue("authentifier/authentifier");
            $vue->generer(array(),$this->fonction);
        }
    }
    public function actionDefault (){
        $this->connexionAccueil();
    }
}

?>