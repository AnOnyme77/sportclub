<?php

class ControllerMembre extends Controller{

    private static $AUTHORIZEDROLE=array("A");
    private $gererMembre;
    private $gererNiveau;
    
    function ControllerMembre() {
    	parent::Controller();
    	$this->gererMembre = new GererMembreImpl();
    	$this->gererNiveau = new GererNiveauImpl();
    }
    
    public function carteMembre($idMembre=null, $complet=true){
    	if(in_array($this->fonction, array("A", "P"))){
    		if(isset($idMembre) && GererMembreImpl::idMembreOk($idMembre)){
    			$this->bundle->put(Bundle::$IDMEMBRE, $idMembre);
				$this->gererMembre->rechercherMembreID($this->bundle);
			$vue = new Vue("diver/carteMembre");
				if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
					$membre = $this->bundle->get(Bundle::$MEMBRE);
					$membre->eanIdMembre();
					if($complet)
						$vue->generer(array("membre" => $membre, "retour"=> true),$this->fonction);
					else
						echo($vue->genererFichier(Parser::getCheminRacine()."/view/diver/carteMembre.php",
							array("membre"=> $membre, "retour"=> false)));
				}
				else{
					if($complet)
						self::erreur($this->bundle->get(Bundle::$MESSAGE));
					else
						echo($this->bundle->get(Bundle::$MESSAGE));
				}
    		}
    		else{
				if($complet)
					self::erreur("Ce membre n'existe pas");
				else
					echo("Ce membre n'existe pas");
    		}
		}
		else{
			if($complet)
				self::erreur("Vous n'avez pas accès à cette partie");
			else
				echo("Vous n'avez pas accès à cette partie");
		}
    		
    }
	
	public function changerLogo(){
		if(isset($_FILES["image"]) and isset($_POST["idMembre"])){
			$lien=Uploader::getInstance()->upload(Uploader::$IMAGE,Uploader::$UTILISATEUR,$_FILES["image"]);
			$this->bundle->put(Bundle::$IDMEMBRE,$_POST["idMembre"]);
			$this->gererMembre->rechercherMembreID($this->bundle);
			if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
				$membre=$this->bundle->get(Bundle::$MEMBRE);
				$membre->setLogo($lien);
				$this->bundle->put(Bundle::$MEMBRE,$membre);
				$this->gererMembre->modifierMembre($this->bundle);
				
				if(in_array($this->fonction, array("A", "P")))
					header("Location: ".Parser::getChemin()."/Membre/afficherMembre/".$_POST["idMembre"]);
				else
					header("Location: ".Parser::getChemin()."/Membre/profil");
			}
		}
	}
	
    //obl pour le router
  	public function actionDefault (){
		if(in_array($this->fonction, array("A", "P"))){
  			$this->listerMembre();
		} else if(in_array($this->fonction, array("M"))){
			$this->profil();
		}
		else
			self::erreur("vous n'avez pas acces a cette partie");
  	}
  	public function rechercherIDMembre($nom=null,$prenom=null,$champs){
		if($nom!=null or $prenom!=null){
			$membre = new Membre();
			
			$membre->setNom($nom);
			$membre->setPrenom($prenom);
			
			$this->bundle->put(Bundle::$MEMBRE,$membre);
			
			$this->gererMembre->rechercherNomPrenom($this->bundle);
			if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
				Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
				$vue = new Vue("");
				echo($vue->genererFichier(Parser::getCheminRacine()."/outils/listeMembre.php",array("liste"=>$this->bundle->get(Bundle::$LISTE),"champs"=>$champs)));
			}
			else{
				echo("LOL".$this->bundle->get(Bundle::$MESSAGE));
			}
		}
		else{
			echo("Veuillez au moins un des champs");
		}
	}
	public function rechercheIDMembre($champs){
		if(in_array($this->fonction, array("A", "P"))){
			$this->gererMembre->listerMembreNonBan($this->bundle);
			$vue = new Vue("administrateurs/rechercherIDMembre");
			$vue->generer(array("champs"=>$champs,"membres"=>$this->bundle->get(Bundle::$LISTE),"message"=>$this->bundle->get(Bundle::$MESSAGE)),$this->fonction);
		}
		else{
			Logger::getInstance()->logify($this,"Accès non autorisé");
			self::erreur("vous n'avez pas accès à cette partie");
		}
	}
  public function listerMembre() {
		if(in_array($this->fonction, array("A", "P"))){
			if($this->fonction=="A") $this->gererMembre->listerMembre($this->bundle);
			else $this->gererMembre->listerMembreNonBan($this->bundle);
			$vue = new Vue("professeurs/rechercherMembre");
			if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
				$listeMembres = $this->bundle->get(Bundle::$LISTE);	
				$vue->generer(array("membres" => $listeMembres, "message" => $this->bundle->get(Bundle::$MESSAGE), "fonction" => $this->fonction),$this->fonction);
			}
			else{
				$vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE), "fonction" => $this->fonction),$this->fonction);
			}
		}
		else{
			self::erreur("Vous n'avez pas accès à cette partie");
		}
	}
	
	public function afficherMembre($idMembre=null) {
		if(in_array($this->fonction, self::$AUTHORIZEDROLE)){
			$this->gererNiveau->listerNiveau($this->bundle);
			$niveaux = $this->bundle->get(Bundle::$LISTE);
			if(isset($idMembre) && GererMembreImpl::idMembreOk($idMembre)){
				$this->bundle->put(Bundle::$IDMEMBRE, $idMembre);
				$this->gererMembre->rechercherMembreID($this->bundle);
				if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
					
					$membre = $this->bundle->get(Bundle::$MEMBRE);
					$vue = new Vue("administrateurs/ajouterMembre");
					$vue->generer(array("membre" => $membre, "ajout" =>false, 'niveaux'=> $niveaux, "fonction" => $this->fonction),$this->fonction);	
				}
				else{
					self::erreur($this->bundle->get(Bundle::$MESSAGE));
				}
			}
			else{
				$membre = new Membre();
				$vue = new Vue("administrateurs/ajouterMembre");
				$vue->generer(array("membre" => $membre, "ajout" =>true, 'niveaux'=> $niveaux, "fonction" => $this->fonction),$this->fonction);

			}
		}else if(in_array($this->fonction, array("P"))){
			if(isset($idMembre) && GererMembreImpl::idMembreOk($idMembre)){
				$this->bundle->put(Bundle::$IDMEMBRE, $idMembre);
				$this->gererMembre->rechercherMembreID($this->bundle);
				if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
					
					$membre = $this->bundle->get(Bundle::$MEMBRE);
					if($membre->getStatut()=="F"){
						$this->gererNiveau->getNiveau($membre->getNiveau(),$membre->getNameSection(),$this->bundle);
						$vue = new Vue("professeurs/detailMembre");
						$vue->generer(array("membre" => $membre, "ajout" =>false, 
											"niveau" => $this->bundle->get(Bundle::$NIVEAU)),$this->fonction);	
					}
					else{
					self::erreur("Ce membre n'existe pas");
					}
				}
				else{
					self::erreur($this->bundle->get(Bundle::$MESSAGE));
				}
			}else{
				self::erreur("Ce membre n'existe pas");
			}
		}
		else{
			self::erreur("Vous n'avez pas accès à cette partie");
		}
	}
	
	public function modifierMembre($idMembre){
		if(in_array($this->fonction, self::$AUTHORIZEDROLE)){
			$membre = new Membre();
			if(isset($_POST['idMembre']))$membre->setIdMembre($_POST['idMembre']);
			if(isset($_POST['nom']))$membre->setNom($_POST['nom']);
			if(isset($_POST['prenom']))$membre->setPrenom($_POST['prenom']);
			if(isset($_POST['email']))$membre->setEmail($_POST['email']);
			
			if(isset($_POST['sexe']))$membre->setSexe($_POST['sexe']);
			if(isset($_POST['naissancejour']) && isset($_POST['naissancemois']) && isset($_POST['naissanceannee']))
				$membre->setDateNaissanceDetail($_POST['naissancejour'], $_POST['naissancemois'], $_POST['naissanceannee']);
			if(isset($_POST['inscriptionjour']) && isset($_POST['inscriptionmois']) && isset($_POST['inscriptionannee']))
				$membre->setDateInscriptionDetail($_POST['inscriptionjour'], $_POST['inscriptionmois'], $_POST['inscriptionannee']);
			$adress = new Adresse();
			if(isset($_POST['rueNo']))$adress->setRue($_POST['rueNo']);
			if(isset($_POST['codePostal']))$adress->setCodePostale($_POST['codePostal']);
			if(isset($_POST['ville']))$adress->setVille($_POST['ville']);
			if(isset($_POST['pays']))$adress->setPays($_POST['pays']);
			$membre->setAdresse($adress);
			if(isset($_POST['telFix']))$membre->setTelFixe($_POST['telFix']);
			if(isset($_POST['telGsm']))$membre->setTelGsm($_POST['telGsm']);
			if(isset($_POST['niveau'])){
				$niveau=explode(";", $_POST['niveau']);
				$membre->setNameSection($niveau[0]);
				$membre->setNiveau($niveau[1]);
			}
			if(isset($_POST['fonction']))$membre->setFonction($_POST['fonction']);
			if(isset($_POST['statut']))$membre->setStatut($_POST['statut']);
			
			$this->bundle->put(Bundle::$IDMEMBRE, $membre->getIdMembre());
			$this->gererMembre->rechercherMembreID($this->bundle);
			if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
				if(isset($_POST['MDP']) && $_POST['MDP']!="")$membre->setMdp($_POST['MDP']);
				else $membre->setMdp($this->bundle->get(Bundle::$MEMBRE)->getMdp());
				if(!isset($_POST['supprimerPhoto']) || $_POST['supprimerPhoto'] == "false"){
						$membre->setLogo($this->bundle->get(Bundle::$MEMBRE)->getLogo());
				}
				else{
					$membre->setLogo(Membre::$LOGO);
				}
				$this->bundle->put(Bundle::$MEMBRE, $membre);
				$this->gererMembre->modifierMembre($this->bundle);
			}
			echo($this->bundle->get(Bundle::$MESSAGE));
		}
		else{
			echo("Erreur : opération interdite");
		}
	}
	
	public function ajouterMembre(){
		if(in_array($this->fonction, self::$AUTHORIZEDROLE)){
			
				$membre = new Membre();
				if(isset($_POST['nom']))$membre->setNom($_POST['nom']);
				if(isset($_POST['prenom']))$membre->setPrenom($_POST['prenom']);
				if(isset($_POST['email']))$membre->setEmail($_POST['email']);
				if(isset($_POST['MDP']))$membre->setMdp($_POST['MDP']);
				if(isset($_POST['sexe']))$membre->setSexe($_POST['sexe']);
				if(isset($_POST['naissancejour']) && isset($_POST['naissancemois']) && isset($_POST['naissanceannee']))
					$membre->setDateNaissanceDetail($_POST['naissancejour'], $_POST['naissancemois'], $_POST['naissanceannee']);
				if(isset($_POST['inscriptionjour']) && isset($_POST['inscriptionmois']) && isset($_POST['inscriptionannee']))
					$membre->setDateInscriptionDetail($_POST['inscriptionjour'], $_POST['inscriptionmois'], $_POST['inscriptionannee']);
				$adress = new Adresse();
				if(isset($_POST['rueNo']))$adress->setRue($_POST['rueNo']);
				if(isset($_POST['codePostal']))$adress->setCodePostale($_POST['codePostal']);
				if(isset($_POST['ville']))$adress->setVille($_POST['ville']);
				if(isset($_POST['pays']))$adress->setPays($_POST['pays']);
				$membre->setAdresse($adress);
				if(isset($_POST['telFix']))$membre->setTelFixe($_POST['telFix']);
				if(isset($_POST['telGsm']))$membre->setTelGsm($_POST['telGsm']);
				if(isset($_POST['niveau'])){
					$niveau=explode(";", $_POST['niveau']);
					$membre->setNameSection($niveau[0]);
					$membre->setNiveau($niveau[1]);
				}
				if(isset($_POST['fonction']))$membre->setFonction($_POST['fonction']);
				$this->bundle->put(Bundle::$MEMBRE, $membre);
				$this->gererMembre->ajouterMembre($this->bundle);
				$message=$this->bundle->get(Bundle::$MESSAGE);
				if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
					$this->bundle->put(Bundle::$EMAIL, $membre->getEmail());
					$this->gererMembre->rechercherMembreEmail($this->bundle);
					
					if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
						$message.=";".$this->bundle->get(Bundle::$MEMBRE)->getIdMembre();
					}
				}
				echo($message);
				
		}
		else{
			echo("Erreur : opération interdite");
		}
	}
	
	//supprime le membre passer en param et afiche un méssage qui serra afficher grace a de l'ajax.
	public function supprimerMembre($idMembre){
		if(in_array($this->fonction, self::$AUTHORIZEDROLE)){
			if(isset($idMembre) && GererMembreImpl::idMembreOk($idMembre)){
				$this->bundle->put(Bundle::$IDMEMBRE, $idMembre);
				$this->gererMembre->supprimerMembre($this->bundle);
				
				echo($this->bundle->get(Bundle::$MESSAGE));
			}
			else{
				echo("Erreur : membre incorrect");
			}
		}
		else{
			echo("Erreur : opération interdite");
		}
	}
	
	public function profil(){
		if(isset($_SESSION) && isset($_SESSION[self::$INDEXMEMBRESESSION])){
			$membre = unserialize($_SESSION[self::$INDEXMEMBRESESSION]);
			$this->bundle->put(Bundle::$IDMEMBRE, $membre->getIdMembre());
			$this->gererMembre->rechercherMembreID($this->bundle);
			if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
				$membreDB = $this->bundle->get(Bundle::$MEMBRE);
				if(!$membre->equal($membreDB)){
					$membre = $membreDB;
					$_SESSION[self::$INDEXMEMBRESESSION] = serialize($membreDB);
				}
			}
			else{
				self::erreur($this->bundle->get(Bundle::$MESSAGE));
			}
			$niveau=null;
			if($membre->getNiveau()!=null && $membre->getNameSection() !=null){
				$this->gererNiveau->getNiveau($membre->getNiveau(),$membre->getNameSection(),$this->bundle);
				$niveau = $this->bundle->get(Bundle::$NIVEAU);
			}
			$vue = new Vue("membres/gererInfos");
			$vue->generer(array("membre" => $membre, "niveau" => $niveau),$this->fonction);	
				}
		else{
			header('Location: '.Parser::getChemin()."/");
		}
	}
	
	public function modifierProfil(){
		if(isset($_SESSION) && isset($_SESSION[self::$INDEXMEMBRESESSION])){
			$membre = unserialize($_SESSION[self::$INDEXMEMBRESESSION]);
			$this->bundle->put(Bundle::$IDMEMBRE, $membre->getIdMembre());
			$this->gererMembre->rechercherMembreID($this->bundle);
			if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
				$membreDB = $this->bundle->get(Bundle::$MEMBRE);
				if($membre->equal($membreDB)){
					if(isset($_POST['idMembre']) && $_POST['idMembre'] == $membre->getIdMembre()){
						if(isset($_POST['nom']))$membre->setNom($_POST['nom']);
						if(isset($_POST['prenom']))$membre->setPrenom($_POST['prenom']);
						if(isset($_POST['email']))$membre->setEmail($_POST['email']);
						if(isset($_POST['sexe']))$membre->setSexe($_POST['sexe']);
						if(isset($_POST['naissancejour']) && isset($_POST['naissancemois']) && isset($_POST['naissanceannee']))
							$membre->setDateNaissanceDetail($_POST['naissancejour'], $_POST['naissancemois'], $_POST['naissanceannee']);
									
						if(isset($_POST['rueNo']))$membre->getAdresse()->setRue($_POST['rueNo']);
						if(isset($_POST['codePostal']))$membre->getAdresse()->setRue($_POST['codePostal']);
						if(isset($_POST['ville']))$membre->getAdresse()->setRue($_POST['ville']);
						if(isset($_POST['pays']))$membre->getAdresse()->setRue($_POST['pays']);
						
						if(isset($_POST['telFix']))$membre->setTelFixe($_POST['telFix']);
						if(isset($_POST['telGsm']))$membre->setTelGsm($_POST['telGsm']);
						
						$membre->setMdp($membreDB->getMdp());
						if($membre->verifMDP($_POST['ancMDP'], $_POST['newMDP'], $_POST['confMDP'])){
							if(!isset($_POST['supImg']) || $_POST['supImg'] == "false"){
								$membre->setLogo($this->bundle->get(Bundle::$MEMBRE)->getLogo());
							}
							else{
								$membre->setLogo(Membre::$LOGO);
							}
							//if(isset($_POST['monFichier']))$membre->setLogo($_POST['logo']);
							$this->bundle->put(Bundle::$MEMBRE, $membre);
							$this->gererMembre->modifierMembre($this->bundle);
							echo($this->bundle->get(Bundle::$MESSAGE));
							$_SESSION[self::$INDEXMEMBRESESSION] = serialize($membre);
						}
						else{
							echo("Erreur : mot de passe incorrect");
						}
					}
					else{
						echo("Une erreur est survenue");
						Logger::getInstance()->logify($this,"idMembre incorrect -> tentative de modification d'un profil d'un autre!");
					}
				}
				else{
					$_SESSION[self::$INDEXMEMBRESESSION] = serialize($membreDB);
					echo("Erreur : pressez F5 avant de continuer");
				}
			}
			else{
				echo($this->bundle->get(Bundle::$MESSAGE));
			}
		}
	}
	
	public function genererMDP(){
		echo(Membre::genererMDP());
	}
}