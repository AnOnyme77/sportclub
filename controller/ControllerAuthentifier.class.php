<?php

class ControllerAuthentifier extends Controller
{
    private $useCase;
    
    public function ControllerAuthentifier()
    {
        parent::Controller();
        $this->useCase = new AuthentifierImpl();
    }

    public function actionDefault()
    {
        $this->authentifier();
    }
    
    public function deconnecter(){
        unset($_SESSION["oMembre"]);
        header("Location:".Parser::getChemin());
    }
    
    public function authentifier()
    {
        //Fonction pour generer la vue qui se trouve dans le dossier view
        $vue = new Vue("authentifier/authentifier");
        $vue->generer(array(),$this->fonction);
    }

    public function authentification($email=null,$mdp=null,$test=false)
    {
        if($email != null && $mdp !=null)
        {
            $authentifier = new Authentifier($email, $mdp);
            $bundle = new Bundle();
            $bundle->put(Bundle::$AUTHENTIFIER,$authentifier);
            //Test est passé en paramtre pour définir le message de retour du bundle.
            $bundleRetour = $this->useCase->authentifier($bundle,$test);
        }
        else
            echo "Tous les champs ne sont pas complétés";

        if(isset($bundleRetour)) echo $bundleRetour->get(Bundle::$MESSAGE);
    }
}



?>
