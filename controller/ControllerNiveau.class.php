<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class ControllerNiveau extends Controller{
    
    private $useCase;
    private $competence;
    
    public function ControllerNiveau(){
        parent::Controller();
        $this->useCase= new GererNiveauImpl();
        $this->competence= new GererCompetenceImpl();
        $this->examen = new GererExamenImpl();
    }
    
    public function ajouterImage(){
        if(isset($_POST["niveau"]) and isset($_POST["section"]) and isset($_FILES["image"])){
            
            $section=$_POST["section"];
            $niveau=$_POST["niveau"];
            
            $lien=Uploader::getInstance()->upload(Uploader::$IMAGE,Uploader::$NIVEAU,$_FILES["image"]);
            if($lien!=false){
                $this->useCase->getNiveau($niveau,$section,$this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $niveau=$this->bundle->get(Bundle::$NIVEAU);
                    $niveau->setLogo($lien);
                    
                    $this->bundle->put(Bundle::$NIVEAU,$niveau);
                    $this->useCase->modifierNiveau($this->bundle);
                    if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                        $vue = new Vue("administrateurs/niveauAjout");
                        $this->competence->getCompetenceFromNiveau($this->bundle);
                        if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                            $vue->generer(array("ajout"=>false,
                                                    "niveau"=>$this->bundle->get(Bundle::$NIVEAU),
                                                    "competences"=>$this->bundle->get(Bundle::$LISTE)),
                                              $this->fonction);
                        }
                    }
                    else{
                        Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                    }
                    
                }
                else{
                    Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                }
            }
            else{
                 Logger::getInstance()->logify($this,$this->bundle->get("Téléversement impossible"));
            }
        }
        else{
            Logger::getInstance()->logify($this,"Les informations n'ont pas été reçues");
        }
    }
    
    public function listerNiveaux(){
        if(in_array($this->fonction,array("A","P"))){
            $this->useCase->listerNiveau($this->bundle);
            $vue = new Vue("administrateurs/niveau");
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                    "liste"=>$this->bundle->get(Bundle::$LISTE)),
                              $this->fonction);
            }
            else{
                $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE)),$this->fonction);
            }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }
    
    public function ajouterNiveau(){
        if(in_array($this->fonction,array("A","P"))){
            $vue = new Vue("administrateurs/niveauAjout");
            $vue->generer(array("ajout"=>true),$this->fonction);
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }
    
    public function ajoutNiveau($section=null,$niveau=null,$libelle=null,$logo=null){
        if($section!=null and $niveau!=null and $libelle!=null and $logo!=null){
            $objetNiveau= new Niveau();
            $objetNiveau->setNameSection($section);
            $objetNiveau->setNiveau($niveau);
            $objetNiveau->setLibelle($libelle);
            $objetNiveau->setLogo($logo);
            $objetNiveau->setSupprime("F");
            
            $this->bundle->put(Bundle::$NIVEAU,$objetNiveau);
            $this->useCase->ajouterNiveau($this->bundle);
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été remplis");
            echo("Veuillez remplir tous les champs.");
        }
    }
    
    public function modifierNiveau($section,$niveau){
        if(in_array($this->fonction,array("A","P"))){
            $vue = new Vue("administrateurs/niveauAjout");
            $this->useCase->getNiveau($niveau,$section,$this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $this->competence->getCompetenceFromNiveau($this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $vue->generer(array("ajout"=>false,
                                        "niveau"=>$this->bundle->get(Bundle::$NIVEAU),
                                        "competences"=>$this->bundle->get(Bundle::$LISTE)),
                                  $this->fonction);
                }
                else{
                    self::erreur($this->bundle->get($this->bundle->get(Bundle::$MESSAGE)));
                }
            }
            else
                self::erreur($this->bundle->get($this->bundle->get(Bundle::$MESSAGE)));
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }
    
    public function modificationNiveau($section=null,$niveau=null,$libelle=null,$logo=null){
        if($section!=null and $niveau!=null and $libelle!=null and $logo!=null){
            $objetNiveau= new Niveau();
            $objetNiveau->setNameSection($section);
            $objetNiveau->setNiveau($niveau);
            $objetNiveau->setLibelle($libelle);
            $objetNiveau->setLogo($logo);
            $objetNiveau->setSupprime("F");
            
            $this->bundle->put(Bundle::$NIVEAU,$objetNiveau);
            $this->useCase->modifierNiveau($this->bundle);
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été remplis");
            echo("Veuillez remplir tous les champs.");
        }
    }
    
    public function suppression($section=null,$niveau=null){
        if($section!=null and $niveau!=null){
            $this->useCase->getNiveau($niveau,$section,$this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $this->useCase->supprimerNiveau($this->bundle);
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
            else{
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
        }
        else{
            Logger::getInstance()->logify($this,"Parametres incorrects");
            echo("Les paramètres reçus sont incorrects");
        }
    }
    
    public function detailNiveau($niveau=null,$section=null){
        if(in_array($this->fonction,array("A","P","M"))){
            $this->useCase->listerNiveau($this->bundle);
            $vue = new Vue("membres/consulterNiveau");
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $listeNiveau=$this->bundle->get(Bundle::$LISTE);
                if(isset($_SESSION)){
                    if($niveau==null and $section==null){
                        $niveauMembre=unserialize($_SESSION["oMembre"])->getNiveau();
                        $sectionMembre=unserialize($_SESSION["oMembre"])->getNameSection();
                    }
                    else{
                        $niveauMembre=$niveau;
                        $sectionMembre=$section;
                    }
                    if($niveauMembre!=null and $niveauMembre!=""
                       and $sectionMembre!=null and $sectionMembre!=""){
                        $this->useCase->getNiveau($niveauMembre,$sectionMembre,$this->bundle);
                        if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                            $this->competence->getCompetenceFromNiveau($this->bundle);
                            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                                $listeCompetences=$this->bundle->get(Bundle::$LISTE);
                                $examen= new Examen();
                                $examen->setIdMembre(unserialize($_SESSION["oMembre"])->getIdMembre());
                                $examen->setNiveau($this->bundle->get(Bundle::$NIVEAU)->getNiveau());
                                $examen->setNameSection($this->bundle->get(Bundle::$NIVEAU)->getNameSection());
                                $this->bundle->put(Bundle::$EXAMEN,$examen);
                                $this->examen->getExamensNiveau($this->bundle);
                                if($this->bundle->get(Bundle::$OPERATION_REUSSIE) and count($this->bundle->get(Bundle::$LISTE))>0){
                                	$liste=$this->bundle->get(Bundle::$LISTE);
                                    $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                                    "liste"=>$listeNiveau,
                                                    "niveauMembre"=>$this->bundle->get(Bundle::$NIVEAU),
                                                    "competences"=>$listeCompetences,
                                                    "sectionActuelle"=>$sectionMembre,
                                                    "niveauActuel"=>$niveauMembre,
                                                    "examen"=>$liste[count($this->bundle->get(Bundle::$LISTE))-1]),
                                              $this->fonction);
                                }
                                else{
                                    $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                                    "liste"=>$listeNiveau,
                                                    "niveauMembre"=>$this->bundle->get(Bundle::$NIVEAU),
                                                    "competences"=>$listeCompetences,
                                                    "sectionActuelle"=>$sectionMembre,
                                                    "niveauActuel"=>$niveauMembre),
                                              $this->fonction);
                                }
                            }
                            else{
                                Logger::getInstance()->logify($this,"Récupération des compétences impossible");
                                $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                                "liste"=>$this->bundle->get(Bundle::$LISTE),
                                                "niveauMembre"=>$this->bundle->get(Bundle::$NIVEAU)),
                                          $this->fonction);
                            }
                        }
                        else{
                            Logger::getInstance()->logify($this,"La récupération du niveau a échoué");
                            $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                    "liste"=>$this->bundle->get(Bundle::$LISTE)),
                              $this->fonction);
                        }
                    }
                    else{
                        Logger::getInstance()->logify($this,"Le niveau du membre n'existe pas");
                        $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                    "liste"=>$this->bundle->get(Bundle::$LISTE)),
                              $this->fonction);
                    }
                }
                else{
                    Logger::getInstance()->logify($this,"Les sessions n'existent pas");
                    $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                    "liste"=>$this->bundle->get(Bundle::$LISTE)),
                              $this->fonction);
                }
            }
            else{
                $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE)),$this->fonction);
            }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }
    
    public function actionDefault (){
        $this->listerNiveaux();
    }
}

?>