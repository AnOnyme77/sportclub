<?php

class Router {

	public function __construct() {
	}

  // Traite une requête entrante
  public function routerRequete() {
    try {
      if (isset($_GET['page'])) {
      	//si le controller existe
        if ( class_exists('Controller'.ucfirst($_GET['page']) )) {
        	//utilisation de la réflecsivité vu que la classe existe
        	$selectedController = new ReflectionClass('Controller'.ucfirst($_GET['page']));
        	// instancier la class et lancer la fct demander 
        	// si $_GET['action'] est remplit alors lance la fct mise avec sinon lance la fct par défault
        	if(isset($_GET['action'])){
        		$action=$_GET['action'];
        	}
        	else{
        		$action="actionDefault";
        	}
        	call_user_func_array(array($selectedController->newInstanceArgs(), $action), (isset($_GET['param']))?$_GET['param']:array(null));
        }
		else
          throw new Exception("Action non valide");
      }
	  
      else {  // aucune action définie : affichage de l'accueil
        $accueil=new ControllerAccueil();
		$accueil->actionDefault();
      }
    }
    catch (Exception $e) {
    	//$vue = new Vue('erreurs/erreur');
    	//$vue->generer(array('message' => $e->getMessage()), $this->fonction);
    }
  }
  
}

