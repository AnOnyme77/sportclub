<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 05/05/2014 - modifée le 30/05/2014
 -----------------------------------------------------------------------------------------------------*/
class ControllerAbonnement extends Controller
{
	private $useCaseAbo;
    private $useCaseTypeAbo;
    private $useCaseMembre;

	public function ControllerAbonnement()
    {
        parent::Controller();
        $this->useCaseAbo = new GererAbonnementImpl();
        $this->useCaseTypeAbo = new GererTypeAbonnementImpl();
        $this->useCaseMembre = new GererMembreImpl();
    }

    public function actionDefault()
    {
        $this->listerAbonnements();
    }

    public function decrementer($idMembre,$dateAchat,$libelle){
        if($idMembre!=null and $libelle!=null and $dateAchat!=null){
            $abonnement = new Abonnement();
            $abonnement->setDateAchat($dateAchat);
            $abonnement->setLibelle($libelle);
            $abonnement->setIdMembre($idMembre);
            
            $this->bundle->put(Bundle::$ABONNEMENT,$abonnement);
            $this->useCaseAbo->decrementeAbonnementMembre($this->bundle);
            echo($this->bundle->get(Bundle::$MESSAGE).";".$this->bundle->get(Bundle::$ABONNEMENT)->getNbUniteRest());
        }
        else{
            echo("Veuillez remplir tous les champs");
        }
    }
    
	public function scanMembre(){
        if(in_array($this->fonction,array("A"))){
            $vue = new Vue("administrateurs/scanCarteMembre");
            $vue->generer(null,$this->fonction);
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
	}

	public function scanAbonnementMembre($idMembre){
        
        if(in_array($this->fonction,array("A"))){
       		$vue = new Vue("");
            if(isset($idMembre) && GererMembreImpl::idMembreOk($idMembre)){
                $this->bundle->put(Bundle::$IDMEMBRE,$idMembre);
                $this->useCaseAbo->scanMembre($this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $liste = $this->bundle->get(Bundle::$LISTE);
                    echo($vue->genererFichier(Parser::getCheminRacine()."/view/administrateurs/listeAbonnementMembre.php",
                                              array("listeAbonnements"=>$liste["abonnements"],
                                                    "listeTypeAbonnements"=>$liste["typeAbonnements"],
                                                    "idMembre"=>$idMembre,
                                                    "message"=>$this->bundle->get(Bundle::$MESSAGE))));
                }
            }
            else{
                 echo("<div id='message'><p> Code de membre incorrect</p></div>");
            }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
        	echo("Vous n'avez pas les droits pour accéder à cette partie");
        }
	}

    public function listerAbonnements()
    {
       if(in_array($this->fonction,array("A"))){
            //Récupération des types d'abonnements :
            $this->useCaseTypeAbo->listerTypesAbonnementValides($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE))
                $listeTypeAbo = $this->bundle->get(Bundle::$LISTE);
            //Récupération des abonnements avec leur membre :
            $this->useCaseAbo->listerAbonnements($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE))
                $listeRetour = $this->bundle->get(Bundle::$LISTE);
            
            $vue = new Vue("administrateurs/abonnement");
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                    "listeTypeAbo"=>$listeTypeAbo,
                                    "listeAbo"=>$listeRetour["abonnements"],
                                    "listeMembre"=>$listeRetour["membres"],
                                    "fonction"=>unserialize($_SESSION["oMembre"])->getFonction()),
                              $this->fonction);
            }
            else{
                $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE)),$this->fonction);
            }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }

    public function getAbonnementsMembre()
    {
       if(in_array($this->fonction,array("P","M", "A"))){
            //Récupération de l'id du membre dans le les variables de session :
            if(unserialize($_SESSION["oMembre"])){
                $this->bundle->put(Bundle::$IDMEMBRE,unserialize($_SESSION["oMembre"])->getIdMembre());

                //Récupération des types d'abonnements :
                $this->useCaseTypeAbo->listerTypesAbonnementValides($this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE))
                    $listeTypeAbo = $this->bundle->get(Bundle::$LISTE);
                //Récupération des abonnements :
                $this->useCaseAbo->getAbonnementsMembre($this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE))
                    $listeAbo = $this->bundle->get(Bundle::$LISTE);

                $vue = new Vue("administrateurs/abonnement");
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE),
                                        "listeAbo"=>$listeAbo,
                                        "listeTypeAbo"=>$listeTypeAbo),
                                  $this->fonction);
                }
                else{
                    $vue->generer(array("message" => $this->bundle->get(Bundle::$MESSAGE)),$this->fonction);
                }
            }
            else{
                Logger::getInstance()->logify($this,"Accès non autorisé");
                self::erreur("Récupération Impossible de votre ID.");
            }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }

    public function ajouterAbonnement($typeAboCourrant=null,$idMembreCourrant=null)
    {
       if(in_array($this->fonction,array("A"))){
            $vue = new Vue("administrateurs/abonnementAjout");

            //Récupération des types d'abonnements :
            $this->useCaseTypeAbo->listerTypesAbonnementValides($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE))
                $listeTypeAbo = $this->bundle->get(Bundle::$LISTE);
                $this->useCaseTypeAbo->getTypeAbonnement($typeAboCourrant,$this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE))
                    $typeAboCourrant = $this->bundle->get(Bundle::$TYPEABONNEMENT);
            if($idMembreCourrant == "")
                $vue->generer(array("ajout"=>true,"listeTypeAbo"=>$listeTypeAbo,"typeAboCourrant"=>$typeAboCourrant),$this->fonction);
            else
                $vue->generer(array("ajout"=>true,"listeTypeAbo"=>$listeTypeAbo,"typeAboCourrant"=>$typeAboCourrant,"idMembreCourrant"=>$idMembreCourrant),$this->fonction);
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }

    public function ajoutAbonnement($idMembre=null,$dateAchat=null,$dateDebut=null,$libelle=null,$prix=null,$nbUniteRest=null,$duree=null,$dateFin=null)
    {
        if($idMembre!=null && $dateAchat!=null && $dateDebut!=null && $libelle!=null && $prix!=null){
            $objetAbo= new Abonnement();
            $objetAbo->setIdMembre($idMembre);
            $objetAbo->setLibelle($libelle);
            $objetAbo->setDateDebut(new DateTime($dateDebut));
            $objetAbo->setDateAchat(new DateTime($dateAchat));
            $objetAbo->setNbUniteRest($nbUniteRest);
            $objetAbo->setPrix($prix);

            if($duree != null){
                //Calcul de la date de fin d'un abonnement via la durée du type d'abonnement duquel il hérite.
                $oneDay = 1 * 24 * 60 * 60;
                $duree = $oneDay*$duree; 
                $timeStamp = strtotime($dateDebut)+$duree;
                $timeStamp = date('Y-m-d', $timeStamp);
                $objetAbo->setDateFin(new DateTime($timeStamp));
            }
            else{
                $objetAbo->setDateFin(null);
            }

            $this->bundle->put(Bundle::$ABONNEMENT,$objetAbo);
            $this->useCaseAbo->ajouterAbonnement($this->bundle);
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été remplis");
            echo("Veuillez remplir tous les champs.");
        }
    }

    public function modifierAbonnement($idMembre,$dateAchat,$libelle)
    {
        if(in_array($this->fonction,array("A"))){
            $vue = new Vue("administrateurs/abonnementAjout");
            $this->bundle->put(Bundle::$IDMEMBRE,$idMembre);
            $abo = new Abonnement();
            $abo->setIdMembre($idMembre);
            $abo->setLibelle($libelle);
            $abo->setDateAchat(new DateTime($dateAchat));
            $this->bundle->put(Bundle::$ABONNEMENT,$abo);

            $this->useCaseAbo->getAbonnement($this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $this->useCaseTypeAbo->getTypeAbonnement($libelle,$this->bundle);
                    $vue->generer(array("ajout"=>false, "typeAbo"=> $this->bundle->get(Bundle::$TYPEABONNEMENT),
                                "abo"=>$this->bundle->get(Bundle::$ABONNEMENT)),$this->fonction);
                }
                else{
                    self::erreur($this->bundle->get($this->bundle->get(Bundle::$MESSAGE)));
                }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }

    public function modificationAbonnement($idMembre, $dateAchat, $dateDebut, $libelle, $prix, $nbUniteRest, $duree)
    {
        if($idMembre!=null && $dateAchat!=null && $dateDebut!=null && $libelle!=null && $prix!=null){
            $objetAbo= new Abonnement();
            $objetAbo->setIdMembre($idMembre);
            $objetAbo->setLibelle($libelle);
            $objetAbo->setDateDebut(new DateTime($dateDebut));
            $objetAbo->setDateAchat(new DateTime($dateAchat));
            $objetAbo->setNbUniteRest($nbUniteRest);
            $objetAbo->setPrix($prix);

            if($duree != null){
                //Calcul de la date de fin d'un abonnement via la durée du type d'abonnement duquel il hérite.
                $oneDay = 1 * 24 * 60 * 60;
                $duree = $oneDay*$duree; 
                $timeStamp = strtotime($dateDebut)+$duree;
                $timeStamp = date('Y-m-d', $timeStamp);
                $objetAbo->setDateFin(new DateTime($timeStamp));
            }
            else{
                $objetAbo->setDateFin(null);
            }
            
            $this->bundle->put(Bundle::$ABONNEMENT,$objetAbo);
            $this->useCaseAbo->modifierAbonnement($this->bundle);
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été remplis");
            echo("Veuillez remplir tous les champs.");
        }
    }

    public function suppressionAbonnement($idMembre=null, $libelle=null){
        if($libelle!=null && $idMembre!=null){
            $abo = new Abonnement();
            $abo->setIdMembre($idMembre);
            $abo->setLibelle($libelle);
            $this->bundle->put(Bundle::$ABONNEMENT,$abo);

            $this->useCaseAbo->supprimerAbonnement($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
            else{
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
        }
        else{
            Logger::getInstance()->logify($this,"Paramètres incorrects");
            echo("Les paramètres reçus sont incorrects");
        }
    }

}