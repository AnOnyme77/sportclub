<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class ControllerInscription extends Controller{
    
    private $useCaseInscription;
    private $useCaseSeance;
    private $useCaseMembre;
    private $logger;
    
    public function ControllerInscription(){
        parent::Controller();
        $this->useCaseInscription=new GererInscriptionImpl();
        $this->useCaseSeance = new GererSeanceImpl();
        $this->logger = Logger::getInstance();
        $this->useCaseMembre =  new GererMembreImpl();
    }
    
    public function marquerPresent($noSeance=null,$idMembre=null,$valeur=null){
        if($noSeance!=null and $idMembre!=null and $valeur!=null){
            $inscription = new Inscription();
            $inscription->setIdMembre($idMembre);
            $inscription->setNoSeance($noSeance);
            $inscription->setPresent($valeur);
            
            $this->bundle->put(Bundle::$INSCRIPTION,$inscription);
            $this->useCaseInscription->modifierInscription($this->bundle);
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                $this->logger->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            echo($this->bundle->get(Bundle::$MESSAGE));
            
        }
        else{
            $this->logger->logify($this,"Parametre non reçu");
            echo("Les données n'ont pas été reçues");
        }
    }
    
    public function getInscription($noSeance=null){
        if($noSeance!=null){
            $seance =  new Seance();
            $seance->setNoSeance($noSeance);
            $this->bundle->put(Bundle::$SEANCE,$seance);
            $this->useCaseInscription->getFromSeance($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $vue = new Vue("");
                $liste=$this->bundle->get(Bundle::$LISTE);
                
                $listeMembre=array();
                $listeSeance=array();
                foreach($liste as $inscription){
                    $this->bundle->put(Bundle::$IDMEMBRE,$inscription->getIdMembre());
                    $this->useCaseMembre->rechercherMembreID($this->bundle);
                    if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                        $listeMembre[]=$this->bundle->get(Bundle::$MEMBRE);
                    }
                    else{
                        $this->logger->logify($this,"La récupération du membre a échoué");
                    }
                    
                    $seance= new Seance();
                    $seance->setNoSeance($noSeance);
                    $this->bundle->put(Bundle::$SEANCE,$seance);
                    $this->useCaseSeance->getSeance($this->bundle);
                    if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                        $listeSeance[]=$this->bundle->get(Bundle::$SEANCE);
                    }
                    else{
                        $this->logger->logify($this,"La récupération de la seance a échoué");
                    }
                }
                
                echo($vue->genererFichier(Parser::getCheminRacine()."/view/listePresenceSeance.php",array("liste"=>$liste,
                                                                                                          "listeMembre"=>$listeMembre,
                                                                                                          "listeSeance"=>$listeSeance)));
            }
            else{
                $this->logger->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
            
        }
        else{
            $this->logger->logify($this,"Parametre non reçu");
            echo("Les données n'ont pas été reçues");
        }
    }
    
    public function inscription($noSeance=null,$idMembre=null){
        if($noSeance!=null and $idMembre!=null){
            
            $seance=new Seance();
            $seance->setNoSeance($noSeance);
            
            $this->bundle->put(Bundle::$IDMEMBRE,$idMembre);
            $this->bundle->put(Bundle::$SEANCE,$seance);
            
            $this->useCaseMembre->rechercherMembreID($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $this->useCaseSeance->getSeance($this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $inscription=new Inscription();
                    $inscription->setIdMembre($idMembre);
                    $inscription->setNoSeance($noSeance);
                    $inscription->setPresent("F");
                    
                    $this->bundle->put(Bundle::$INSCRIPTION,$inscription);
                    $this->useCaseInscription->ajouterInscription($this->bundle);
                    if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                        $this->logger->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                    }
                    echo($this->bundle->get(Bundle::$MESSAGE));
                }
                else{
                    $this->logger->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                    echo($this->bundle->get(Bundle::$MESSAGE));
                }
            }
            else{
                $this->logger->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
        }
        else{
            $this->logger->logify($this,"Information non reçues");
            echo("Les informations n'ont pas été reçues");
        }
    }
    
    public function desinscription($noSeance=null,$idMembre=null){
        if($noSeance!=null and $idMembre!=null){
            
            $seance=new Seance();
            $seance->setNoSeance($noSeance);
            
            $this->bundle->put(Bundle::$IDMEMBRE,$idMembre);
            $this->bundle->put(Bundle::$SEANCE,$seance);
            
            $this->useCaseMembre->rechercherMembreID($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $this->useCaseSeance->getSeance($this->bundle);
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $inscription=new Inscription();
                    $inscription->setIdMembre($idMembre);
                    $inscription->setNoSeance($noSeance);
                    $inscription->setPresent("F");
                    
                    $this->bundle->put(Bundle::$INSCRIPTION,$inscription);
                    $this->useCaseInscription->supprimerInscription($this->bundle);
                    if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                        $this->logger->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                    }
                    echo($this->bundle->get(Bundle::$MESSAGE));
                }
                else{
                    $this->logger->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                    echo($this->bundle->get(Bundle::$MESSAGE));
                }
            }
            else{
                $this->logger->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
        }
        else{
            $this->logger->logify($this,"Information non reçues");
            echo("Les informations n'ont pas été reçues");
        }
    }
    public function presence(){
        if(in_array($this->fonction,array("A","P"))){
            $this->useCaseInscription->betterListerInscriptions($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $vue = new Vue("professeurs/ajouterPresence");
                $liste=$this->bundle->get(Bundle::$LISTE);
                
                $vue->generer(array("message" =>$this->bundle->get(Bundle::$MESSAGE),
                                    "liste"=>$liste["seance"],
                                    "listeProfs"=>$liste["prof"],
                                    "listeNombre"=>$liste["compte"]),
                              $this->fonction);
            }
        }
    }
    public function inscrire(){
        if(in_array($this->fonction,array("A","P","M"))){
            $this->bundle->put(Bundle::$IDMEMBRE,unserialize($_SESSION["oMembre"])->getIdMembre());
            $this->useCaseInscription->getPresenceEleve($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $vue = new Vue("membres/inscrireSeance");
                $liste=$this->bundle->get(Bundle::$LISTE);
                $message=$this->bundle->get(Bundle::$MESSAGE);
                
                
                $vue->generer(array("message" =>$message,
                                    "liste"=>$liste["seance"],
                                    "listeProfs"=>$liste["prof"],
                                    "listeNombre"=>$liste["compte"],
                                    "listePresence"=>$liste["presence"]),
                              $this->fonction);
            }
        }
        else{
            $this->logger->logify($this,"Accès non autorisé avec un compte ".$this->fonction);
            self::erreur("Vous n'avez pas les droits d'accéder à cette partie");
        }
    }
    
    public function actionDefault(){
        $this->inscrire();
    }
}

?>