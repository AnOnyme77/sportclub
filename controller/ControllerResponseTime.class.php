<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class ControllerResponseTime extends Controller
{    
	//Objet use case
    private $logger;
    
	//Constructeur
    public function ControllerResponseTime(){
    }
	
	public function actionDefault(){
	}
    
    public function log($page=null,$duree=null){
        if(page!=null)
            Logger::getInstance()->logify($this,"Temps de réponse trop long de ".$page.": ".$duree);
        else
            Logger::getInstance()->logify($this,"Temps de réponse trop long");
    }
    
}

?>