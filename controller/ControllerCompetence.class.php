<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class ControllerCompetence extends Controller{
    private $useCase;
    
    public function ControllerCompetence(){
        parent::Controller();
        $this->useCase= new GererCompetenceImpl();
    }
    
    function actionDefault(){
    }
    
    public function ajouterCompetence($niveau=null,$nameSection=null,$titre=null,$detail=null){
        if(!is_null($niveau) and !is_null($nameSection) and !is_null($titre) and ! is_null($detail)){
            $competence= new Competence();
            $competence->setNiveau($niveau);
            $competence->setNameSection($nameSection);
            $competence->setTitre($titre);
            $competence->setDetail($detail);
            
            $this->bundle->put(Bundle::$COMPETENCE,$competence);
            $this->useCase->ajouterCompetence($this->bundle);
            echo($this->bundle->get(Bundle::$MESSAGE));
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            }
        }
        else{
            Logger::getInstance()->logify($this,"Tous les paramètres ne sont pas fournis");
            echo("Veuillez fournir tous les paramètres");
        }
    }
    
    public function modifierCompetence($niveau=null,$nameSection=null,$titre=null,$detail=null){
        if(!is_null($niveau) and !is_null($nameSection) and !is_null($titre) and ! is_null($detail)){
            $competence= new Competence();
            $competence->setNiveau($niveau);
            $competence->setNameSection($nameSection);
            $competence->setTitre($titre);
            $competence->setDetail($detail);
            
            $this->bundle->put(Bundle::$COMPETENCE,$competence);
            $this->useCase->getCompetence($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $this->bundle->put(Bundle::$COMPETENCE,$competence);
                $this->useCase->modifierCompetence($this->bundle);
                echo($this->bundle->get(Bundle::$MESSAGE));
                if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                }
            }
            else{
                echo("Cette compétence n'existe pas");
            }
        }
        else{
            Logger::getInstance()->logify($this,"Tous les paramètres ne sont pas fournis");
            echo("Veuillez fournir tous les paramètres");
        }
    }
    
    public function supprimerCompetence($niveau=null,$nameSection=null,$titre=null){
        if(!is_null($niveau) and !is_null($nameSection) and !is_null($titre)){
            $competence= new Competence();
            $competence->setNiveau($niveau);
            $competence->setNameSection($nameSection);
            $competence->setTitre($titre);
            
            $this->bundle->put(Bundle::$COMPETENCE,$competence);
            $this->useCase->getCompetence($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $this->useCase->supprimerCompetence($this->bundle);
                echo($this->bundle->get(Bundle::$MESSAGE));
                if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                }
            }
            else{
                echo("Cette compétence n'existe pas");
            }
        }
        else{
            Logger::getInstance()->logify($this,"Tous les paramètres ne sont pas fournis");
            echo("Veuillez fournir tous les paramètres");
        }
    }
}
?>