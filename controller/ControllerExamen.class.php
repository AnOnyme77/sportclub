<?php
/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
class ControllerExamen extends Controller{
    
    private $useCaseExamen;
    private $useCaseNiveau;
    private $useCaseMembre;
    
    public function ControllerExamen(){
        parent::Controller();
        $this->useCaseExamen = new GererExamenImpl();
        $this->useCaseNiveau = new GererNiveauImpl();
        $this->useCaseMembre = new GererMembreImpl();
    }
    
    public function listerExamen(){
        if(in_array($this->fonction,array('A','P'))){
            $vue = new Vue("professeurs/rechercherExamen");
            
            $this->useCaseExamen->listerExamen($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $vue->generer(array("listeExamens"=>$this->bundle->get(Bundle::$LISTE), "message"=>$this->bundle->get(Bundle::$MESSAGE)),$this->fonction);
            }
            else{
                $vue->generer(array(),$this->fonction);
            }
        }
    }
    
    public function modificationExamen($membre=null,$professeur=null,$section=null,
                                  $niveau=null,$cote=null,$commentaire=null,$date=null){
        if($membre!=null and $professeur!=null and $section!=null and
           $niveau!=null and $cote!=null and $commentaire!=null and $date!=null){
            $examen = new Examen();
            $examen->setIdMembre($membre);
            $examen->setIdProf($professeur);
            $examen->setNameSection($section);
            $examen->setNiveau($niveau);
            $examen->setCote($cote);
            $examen->setCommentaire($commentaire);
            $examen->setDatePassage($date);

            $this->bundle->put(Bundle::$EXAMEN,$examen);
            $this->useCaseExamen->modifierExamen($this->bundle);
            
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            echo("Veuillez remplir tous les champs");
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été fournis");
        }
    }
    
    
    public function ajouter(){
        if(in_array($this->fonction,array('A','P'))){
            $this->useCaseNiveau->listerNiveau($this->bundle);
            $vue = new Vue("professeurs/ajouterExamen");
            $vue->generer(array("ajout"=>true,
                                "niveaux"=>$this->bundle->get(Bundle::$LISTE),
                                "message"=>""
                                ),$this->fonction);
        } 
    }
    
    public function ajouterExamen($membre=null,$professeur=null,$section=null,
                                  $niveau=null,$cote=null,$commentaire=null,$date=null){
        if($membre!=null and $professeur!=null and $section!=null and
           $niveau!=null and $cote!=null and $commentaire!=null and $date!=null){
            $examen = new Examen();
            $examen->setIdMembre($membre);
            $examen->setIdProf($professeur);
            $examen->setNameSection($section);
            $examen->setNiveau($niveau);
            $examen->setCote($cote);
            $examen->setCommentaire($commentaire);
            $examen->setDatePassage($date);
            if($professeur!=$membre or in_array($this->fonction,array("A"))){
                $this->bundle->put(Bundle::$EXAMEN,$examen);
                
                $this->useCaseExamen->ajouterExamen($this->bundle);
                if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                    Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                    
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
            else{
                echo("Le membre et le professeur ne peuvent être identiques");
            }
        }
        else{
            echo("Veuillez remplir tous les champs");
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été fournis");
        }
    }
    
    public function modifierExamen($membre=null,$professeur=null,$niveau=null,
                                  $section=null,$date=null){
        if(in_array($this->fonction,array('A','P')) and $membre!=null and $professeur!=null and $section!=null and
           $niveau!=null and $date!=null){
            $examen = new Examen();
            $examen->setIdMembre($membre);
            $examen->setIdProf($professeur);
            $examen->setNameSection($section);
            $examen->setNiveau($niveau);
            $examen->setDatePassage($date);

            $this->bundle->put(Bundle::$EXAMEN,$examen);
            $this->useCaseExamen->getExamen($this->bundle);
            
            $examenRetour=null;
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE))
                $examenRetour=$this->bundle->get(Bundle::$EXAMEN);
            $message=$this->bundle->get(Bundle::$MESSAGE);
            
            $this->bundle->put(Bundle::$IDMEMBRE,$membre);
            $this->useCaseMembre->rechercherMembreID($this->bundle);
            
            $vue = new Vue("professeurs/ajouterExamen");
            $vue->generer(array("ajout"=>false,"examen"=>$examenRetour,"message"=>$message,"membre"=>$this->bundle->get(Bundle::$MEMBRE)),$this->fonction);
        }
        else{
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été fournis");
        }
    }
    
    public function supprimerExamen($membre=null,$professeur=null,$section=null,
                                  $niveau=null,$date=null){
        if($membre!=null and $professeur!=null and $section!=null and
           $niveau!=null and $date!=null){
            $examen = new Examen();
            $examen->setIdMembre($membre);
            $examen->setIdProf($professeur);
            $examen->setNameSection($section);
            $examen->setNiveau($niveau);
            $examen->setDatePassage($date);
            
            $this->bundle->put(Bundle::$EXAMEN,$examen);
            
            $this->useCaseExamen->supprimerExamen($this->bundle);
            
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            echo("Veuillez remplir tous les champs");
            Logger::getInstance()->logify($this,"Tous les champs n'ont pas été fournis");
        }
    }
    
    public function actionDefault (){
        $this->listerExamen();
    }
}

?>