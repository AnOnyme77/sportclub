<?php

class ControllerSeance extends Controller{
    
    private $useCaseSeance;
    
    public function ControllerSeance(){
        parent::Controller();
        $this->useCaseSeance=new GererSeanceImpl();
        $this->useCaseMembre=new GererMembreImpl();
    }
    public function ajoutSeance($cours=null,
                                $dateSeance=null,$participants=null,$idProf=null){
        if($cours!=null and $dateSeance!=null and $participants!=null and $idProf!=null){
            $seance = new Seance();
            $seance->setCours($cours);
            $seance->setDateCours($dateSeance);
            $seance->setNbMembreMax($participants);
            $seance->setIdProf($idProf);
            
            $this->bundle->put(Bundle::$SEANCE,$seance);
            
            $this->useCaseSeance->ajouterSeance($this->bundle);
            
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Veuillez saisir toutes les informations");
            echo("Veuillez saisir toutes les informations");
        }
        
    }
    public function ajoutRecursif($cours=null,
                                $dateSeanceDebut=null,
                                $dateSeanceFin=null,
                                $ecart=null,
                                $participants=null,
                                $idProf=null){
        
        if($cours!=null and $dateSeanceDebut!=null and
           $dateSeanceFin!=null and $ecart!=null and
           $participants!=null and $idProf!=null){
            
            $dateSeance = new DateTime($dateSeanceDebut);
            $dateFin = new DateTime($dateSeanceFin);
            $reussi=true;
            $erreurs="";
            while($dateSeance<=$dateFin){
                $seance = new Seance();
                $seance->setCours($cours);
                $seance->setDateCours($dateSeance->format('Y-m-d H:i:s'));
                $seance->setNbMembreMax($participants);
                $seance->setIdProf($idProf);
                
                $this->bundle->put(Bundle::$SEANCE,$seance);
                
                $this->useCaseSeance->ajouterSeance($this->bundle);
                
                if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $reussi=false;
                    $erreurs.="<p>Ajout échoué: ".$dateSeance->format('Y-m-d H:i:s')."</p>";
                    Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
                }
                
                $dateSeance->modify("+".$ecart." days");
            }
            if($reussi)
                echo("Ajouts effectués avec succès");
            else
                echo($erreurs);
        }
        else{
            Logger::getInstance()->logify($this,"Veuillez saisir toutes les informations");
            echo("Veuillez saisir toutes les informations");
        }
        
    }
    
    public function suppression($numero=null,$prof=null){
        if($numero!=null and $prof!=null){
            $seance=new Seance();
            $seance->setNoSeance($numero);
            $seance->setIdProf($prof);
            
            $this->bundle->put(Bundle::$SEANCE,$seance);
            $this->useCaseSeance->supprimerSeance($this->bundle);
            if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                Logger::getInstance()->logify($this,$this->bundle->get(Bundle::$MESSAGE));
            echo($this->bundle->get(Bundle::$MESSAGE));
        }
        else{
            Logger::getInstance()->logify($this,"Les informations n'ont pas correctement été reçues");
            echo("Les informations n'ont pas correctement été reçues");
        }
    }
    
    public function modification($numero=null,$cours=null,
                                $dateSeance=null,$participants=null,$idProf=null){
        if($numero!=null and $cours!=null and $dateSeance!=null
           and $participants!=null and $idProf!=null){
            $seance = new Seance();
            $seance->setNoSeance($numero);
            $seance->setCours($cours);
            $seance->setDateCours($dateSeance);
            $seance->setNbMembreMax($participants);
            $seance->setIdProf($idProf);
            
            $this->bundle->put(Bundle::$IDMEMBRE,$idProf);
            $this->useCaseMembre->rechercherMembreID($this->bundle);   
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $this->bundle->put(Bundle::$SEANCE,$seance);
                $this->useCaseSeance->modifierSeance($this->bundle);
                
                if(!$this->bundle->get(Bundle::$OPERATION_REUSSIE))
                    Logger::getInstance()->logify($this,"Modification échouée");
                echo($this->bundle->get(Bundle::$MESSAGE));
            }
            else{
                Logger::getInstance()->logify($this,"La récupération du professeur a échoué");
                echo("Ce professeur n'existe pas");
            }
        }
        else{
            Logger::getInstance()->logify($this,"Tous les paramètres n'ont pas été reçus");
            echo("Tous les paramètres n'ont pas été reçus");
        }
    }
    
    public function ajouterSeance(){
        if(in_array($this->fonction,array("A","P"))){
            $this->useCaseSeance->getCours($this->bundle);
            
            
            $vue = new Vue("administrateurs/ajouterSeance");
            $vue->generer(array("ajout"=>true,"listeCours"=>$this->bundle->get(Bundle::$LISTE)),$this->fonction);
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }
    
    public function modifierSeance($numSeance=null){
        if(in_array($this->fonction,array("A","P"))){
            if($numSeance!=null){
                $seance=new Seance();
                $seance->setNoSeance($numSeance);
                
                $this->bundle->put(Bundle::$SEANCE,$seance);
                
                $this->useCaseSeance->getSeance($this->bundle);
                
                if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                    $prof=false;
                    $message=$this->bundle->get(Bundle::$MESSAGE);
                    $seance=$this->bundle->get(Bundle::$SEANCE);
                    
                    $this->bundle->put(Bundle::$IDMEMBRE,$seance->getIdProf());
                    $this->useCaseMembre->rechercherMembreID($this->bundle);
                    
                    if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                        $prof=$this->bundle->get(Bundle::$MEMBRE);
                    }
                    else{
                        Logger::getInstance()->logify($this,"La récupération du professeur a échoué");
                    }
                    
                    $vue = new Vue("administrateurs/ajouterSeance");
                    $vue->generer(array("ajout"=>false,
                                        "seance"=>$this->bundle->get(Bundle::$SEANCE),
                                        "message"=>$this->bundle->get(Bundle::$MESSAGE),
                                        "prof"=>$prof),$this->fonction);
                }
                else{
                    Logger::getInstance()->logify($this,"Seance introuvable");
                    self::erreur("Séance introuvable"); 
                }
            }
            else{
                Logger::getInstance()->logify($this,"Toutes les informations n'ont pas été recues");
                self::erreur("Les informations nécessaires n'ont pas été reçues"); 
            }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }
    
    public function listerSeance(){
        if(in_array($this->fonction,array("A","P"))){
            $this->useCaseSeance->listerSeance($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $vue = new Vue("administrateurs/seance");
                $listeSeance=$this->bundle->get(Bundle::$LISTE);
                $message=$this->bundle->get(Bundle::$MESSAGE);
                $listeProf=array();
                foreach($listeSeance as $seance){
                    $this->bundle->put(Bundle::$IDMEMBRE,$seance->getIdProf());
                    
                    $this->useCaseMembre->rechercherMembreID($this->bundle);
                    
                    if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                        $listeProf[]=$this->bundle->get(Bundle::$MEMBRE);
                    }
                    else{
                        Logger::getInstance()->logify($this,"La récupération du professeur a échoué");
                    }
                }
                
                $vue->generer(array("message" =>$message,
                                    "liste"=>$listeSeance,
                                    "listeProfs"=>$listeProf),
                              $this->fonction);
            }
            else{
                Logger::getInstance()->logify($this,"La récupération à échoué");
                self::erreur($this->bundle->get(Bundle::$MESSAGE));
            }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }
    
    public function betterListerSeance(){
        if(in_array($this->fonction,array("A","P"))){
            $this->useCaseSeance->betterListerSeance($this->bundle);
            if($this->bundle->get(Bundle::$OPERATION_REUSSIE)){
                $vue = new Vue("administrateurs/seance");
                $liste=$this->bundle->get(Bundle::$LISTE);
                $message=$this->bundle->get(Bundle::$MESSAGE);
                
                $vue->generer(array("message" =>$message,
                                    "liste"=>$liste["seances"],
                                    "listeProfs"=>$liste["membres"]),
                              $this->fonction);
            }
            else{
                Logger::getInstance()->logify($this,"La récupération à échoué");
                self::erreur($this->bundle->get(Bundle::$MESSAGE));
            }
        }
        else{
            Logger::getInstance()->logify($this,"Accès non autorisé");
            self::erreur("Vous n'avez pas les droits pour accéder à cette partie");
        }
    }
    
    public function actionDefault(){
        $this->betterListerSeance();
    }
}

?>