-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Client: localhost:3306
-- Généré le: Sam 22 Février 2014 à 13:21
-- Version du serveur: 5.5.35-0+wheezy1
-- Version de PHP: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `ges4u`
--

-- --------------------------------------------------------

--
-- Structure de la table `ABONNEMENT`
--

CREATE TABLE IF NOT EXISTS `ABONNEMENT` (
  `idMembre` varchar(13) NOT NULL COMMENT 'Code EAN 13 du membre',
  `libelle` varchar(30) NOT NULL COMMENT 'Libelle de l abonnement',
  `dateDebut` date DEFAULT NULL COMMENT 'Uniquement le type d abonnement a une duree',
  `dateAchat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Initialise a la date de creation de l enregistrement',
  `nbUniteRest` int(11) DEFAULT NULL COMMENT 'Nombre d unites restant',
  `prix` float DEFAULT NULL COMMENT 'prix de l abonnement',
  `dateFin` date DEFAULT NULL COMMENT 'Uniquement le type d abonnement a une duree',
  PRIMARY KEY (`idMembre`,`libelle`,`dateAchat`),
  KEY `FKqualifie` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `COMPETENCE`
--

CREATE TABLE IF NOT EXISTS `COMPETENCE` (
  `niveau` int(11) NOT NULL COMMENT 'numero du niveau',
  `nameSection` varchar(30) NOT NULL COMMENT 'nom de la section',
  `titre` varchar(80) NOT NULL COMMENT 'Titre de la competence',
  `detail` varchar(255) DEFAULT NULL COMMENT 'Detail sur cette competence',
  PRIMARY KEY (`niveau`,`nameSection`,`titre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `EXAMEN`
--

CREATE TABLE IF NOT EXISTS `EXAMEN` (
  `niveau` int(11) NOT NULL COMMENT 'Niveau de l examen',
  `nameSection` varchar(30) NOT NULL COMMENT 'Nom de la section',
  `idProf` varchar(13) NOT NULL COMMENT 'Identifiant du professeur',
  `datePassage` date NOT NULL COMMENT 'Date de passage de l examen',
  `cote` int(11) NOT NULL COMMENT 'Cote de l examen',
  `commentaire` varchar(255) DEFAULT NULL COMMENT 'Commentaire sur l examen',
  `idMembre` varchar(13) NOT NULL COMMENT 'Identifiant du membre',
  PRIMARY KEY (`niveau`,`nameSection`,`idProf`,`datePassage`),
  KEY `FKfaitPasser` (`idProf`),
  KEY `SMembre` (`idMembre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `INSCRIPTION`
--

CREATE TABLE IF NOT EXISTS `INSCRIPTION` (
  `noSeance` int(11) NOT NULL COMMENT 'Numero de la seance',
  `idMembre` varchar(13) NOT NULL COMMENT 'Identifiant du membre',
  `present` char(1) NOT NULL COMMENT 'La membre a t il ete present',
  PRIMARY KEY (`noSeance`,`idMembre`),
  KEY `FKinscrire` (`idMembre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MEMBRE`
--

CREATE TABLE IF NOT EXISTS `MEMBRE` (
  `idMembre` varchar(13) NOT NULL COMMENT 'Identifiant du membre',
  `nom` varchar(30) NOT NULL COMMENT 'Nom du membre',
  `prenom` varchar(30) NOT NULL COMMENT 'Prenom du membre',
  `sexe` char(1) NOT NULL COMMENT 'Sexe du membre',
  `adr_pays` varchar(30) NOT NULL COMMENT 'Pays du membre',
  `adr_ville` varchar(30) NOT NULL COMMENT 'Ville du membre',
  `adr_rue` varchar(50) NOT NULL COMMENT 'Rue du membre',
  `adr_cp` varchar(10) NOT NULL COMMENT 'Code postale',
  `dateNaissance` date NOT NULL COMMENT 'Date de naissance',
  `fonction` char(1) NOT NULL DEFAULT 'M' COMMENT 'Membre/Admin/Prof',
  `statut` char(1) NOT NULL DEFAULT 'F' COMMENT 'Banni/Actif',
  `mdp` varchar(60) NOT NULL COMMENT 'Mot de passe',
  `email` varchar(50) NOT NULL COMMENT 'Email du membre',
  `telFix` varchar(25) DEFAULT NULL COMMENT 'Telephone fixe',
  `telGsm` varchar(25) DEFAULT NULL COMMENT 'Telephone GSM',
  `dateInscription` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date d inscription',
  `logo` varchar(50) NOT NULL COMMENT 'Logo du membre',
  `supprime` char(1) NOT NULL DEFAULT 'F' COMMENT 'Supprime ?',
  `niveau` int(11) DEFAULT NULL COMMENT 'Numero du niveau du membre',
  `nameSection` varchar(30) DEFAULT NULL COMMENT 'Nom de la section',
  PRIMARY KEY (`idMembre`),
  UNIQUE KEY `email` (`email`),
  KEY `FKaFK` (`niveau`,`nameSection`),
  KEY `SEmail` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `NIVEAU`
--

CREATE TABLE IF NOT EXISTS `NIVEAU` (
  `niveau` int(11) NOT NULL COMMENT 'Numero du du niveau du membre',
  `libelle` varchar(20) NOT NULL COMMENT 'Libelle du niveau',
  `nameSection` varchar(30) NOT NULL COMMENT 'Nom de la section',
  `logo` varchar(50) DEFAULT NULL COMMENT 'Logo du niveau',
  `supprime` char(1) NOT NULL DEFAULT 'F' COMMENT 'Suppression logique',
  PRIMARY KEY (`niveau`,`nameSection`),
  KEY `SLib` (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SEANCE`
--

CREATE TABLE IF NOT EXISTS `SEANCE` (
  `noSeance` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Numero de la seance',
  `cours` varchar(50) NOT NULL COMMENT 'Libelle du cours',
  `dateCours` date NOT NULL COMMENT 'Date du cours',
  `nbMembreMax` int(11) NOT NULL COMMENT 'Nombre max de membres pr�sent',
  `idProf` varchar(13) DEFAULT NULL COMMENT 'Identfiant du professeur',
  PRIMARY KEY (`noSeance`),
  KEY `FKdonne` (`idProf`),
  KEY `SSeance` (`cours`,`dateCours`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `TYPEABONNEMENT`
--

CREATE TABLE IF NOT EXISTS `TYPEABONNEMENT` (
  `libelle` varchar(30) NOT NULL,
  `prix` float NOT NULL COMMENT 'La valeur negative est autorisee en cas de reductions',
  `duree` int(11) DEFAULT NULL COMMENT 'Nombre de jours de validit� de l abonnement',
  `nbUnite` int(11) DEFAULT NULL COMMENT 'Nombre d unites',
  `cours` varchar(50) DEFAULT NULL COMMENT 'Libelle du cours',
  `supprime` char(1) NOT NULL DEFAULT 'F' COMMENT 'Supprime ?',
  PRIMARY KEY (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ABONNEMENT`
--
ALTER TABLE `ABONNEMENT`
  ADD CONSTRAINT `FKpaye` FOREIGN KEY (`idMembre`) REFERENCES `MEMBRE` (`idMembre`),
  ADD CONSTRAINT `FKqualifie` FOREIGN KEY (`libelle`) REFERENCES `TYPEABONNEMENT` (`libelle`);

--
-- Contraintes pour la table `COMPETENCE`
--
ALTER TABLE `COMPETENCE`
  ADD CONSTRAINT `FKcorrespond` FOREIGN KEY (`niveau`, `nameSection`) REFERENCES `NIVEAU` (`niveau`, `nameSection`);

--
-- Contraintes pour la table `EXAMEN`
--
ALTER TABLE `EXAMEN`
  ADD CONSTRAINT `FKassocieA` FOREIGN KEY (`niveau`, `nameSection`) REFERENCES `NIVEAU` (`niveau`, `nameSection`),
  ADD CONSTRAINT `FKfaitPasser` FOREIGN KEY (`idProf`) REFERENCES `MEMBRE` (`idMembre`),
  ADD CONSTRAINT `FKpasse` FOREIGN KEY (`idMembre`) REFERENCES `MEMBRE` (`idMembre`);

--
-- Contraintes pour la table `INSCRIPTION`
--
ALTER TABLE `INSCRIPTION`
  ADD CONSTRAINT `FKcomprend` FOREIGN KEY (`noSeance`) REFERENCES `SEANCE` (`noSeance`),
  ADD CONSTRAINT `FKinscrire` FOREIGN KEY (`idMembre`) REFERENCES `MEMBRE` (`idMembre`);

--
-- Contraintes pour la table `MEMBRE`
--
ALTER TABLE `MEMBRE`
  ADD CONSTRAINT `FKaFK` FOREIGN KEY (`niveau`, `nameSection`) REFERENCES `NIVEAU` (`niveau`, `nameSection`);

--
-- Contraintes pour la table `SEANCE`
--
ALTER TABLE `SEANCE`
  ADD CONSTRAINT `FKdonne` FOREIGN KEY (`idProf`) REFERENCES `MEMBRE` (`idMembre`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
