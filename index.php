<?php
date_default_timezone_set('Europe/Paris');
define('ROOTWAY',".");
require_once(ROOTWAY."/outils/autoloader.php");
if(isset($_SERVER) && isset($_SERVER["REDIRECT_URL"])){
	$match=explode("/",$_SERVER['REDIRECT_URL']);
	//modification du code retour
	header("Status: 200 OK", false, 200);
	//alimentation du paramètre GET
	$i=0;
	while(isset($match[$i]) && $match[$i]!=Parser::getCheminApache()) $i++;
	$i++;
	if(isset($match[$i])){
		$_GET['page'] = $match[$i];
  		$_REQUEST['page'] = $match[$i];
  		$i++;
		if(isset($match[$i])){
	    	$_GET['action'] = $match[$i];
	    	$_REQUEST['action'] = $match[$i];
	    	$i++;
		}
  	}
  	$j=0;
	while(isset($match[$i])){
		$_GET['param'][$j]=$match[$i];
		$_REQUEST['param'][$j]=$match[$i];
		$i++;
		$j++;	
	}
}

	
$routeur = new Router();
$routeur->routerRequete();
?> 