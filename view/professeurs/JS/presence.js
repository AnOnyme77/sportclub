/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/

function afficherInscriptions(chemin,noSeance) {
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    tableauInscription(xhr.responseText);       }}; 
    
    var requete=chemin+"/inscription/getInscription/"+noSeance;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");	
    xhr.send(null);
}
function tableauInscription(text){
    var resultat=document.getElementById("resultat");
    var titreResultat= document.getElementById("titreResultat");
    resultat.innerHTML=text;
    titreResultat.setAttribute("style","visibility:visible;");
}
function enregistrer(chemin){
    var groupes=document.getElementsByName("radio");
    var i=0;
    var j=0;
    for(i=0;i<groupes.length;i++){
        for(j=0;j<groupes[i].childNodes.length;j++){
            if (groupes[i].childNodes[j].checked==true) {
                tab=groupes[i].childNodes[j].name.split("#");
                idMembre=tab[0];
                noSeance=tab[1];
                enregistrerPresence(chemin,noSeance,idMembre,groupes[i].childNodes[j].value);
            }
        }
    }
}

function enregistrerPresence(chemin,noSeance,idMembre,valeur){
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    lireDonnees(xhr.responseText);       }}; 
    
    var requete=chemin+"/inscription/marquerPresent/"+noSeance+"/"+idMembre+"/"+valeur;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");	
    xhr.send(null);
}
function lireDonnees(text){
    var resultat=document.getElementById("message");
    resultat.innerHTML=text;
    window.setTimeout(function(){
        var resultat=document.getElementById("message");
        resultat.innerHTML="";},5000);
}