/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * cr�� le 03/04/2014 - modif�e le 30/05/2014
 -----------------------------------------------------------------------------------------------------*/

function ajouterExamen(chemin) {
    
    var membre=encodeURIComponent(document.getElementById("membre").value);
    var professeur=encodeURIComponent(document.getElementById("professeur").value);
    var section=encodeURIComponent(document.getElementById("section").value);
    var niveau=encodeURIComponent(document.getElementById("niveau").value);
    var cote=encodeURIComponent(document.getElementById("cote").value);
    var commentaire=encodeURIComponent(document.getElementById("commentaire").value);
    
    var jour = document.getElementById("jour").value;
    var mois = document.getElementById("mois").value;
    var annee = document.getElementById("annee").value;
    var date = encodeURIComponent(annee+"-"+mois+"-"+jour);
    
    
	var xhr=getXMLHttpRequest();

	xhr.onreadystatechange = function() {
	if (xhr.readyState == 4 && (xhr.status == 200 ||     xhr.status == 0))
	{
	lireDonnees(xhr.responseText);     }};

	var requete=chemin+"/examen/ajouterExamen/"+membre+"/"+professeur+"/"+section+"/"+niveau+"/"+cote+"/"+commentaire+"/"+date;
	xhr.open("GET",requete,true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Op�ration en cours, veuillez patienter et ne pas quitter la page");
	xhr.send(null);
}

function modifierExamen(chemin) {
    
    var membre=encodeURIComponent(document.getElementById("membre").value);
    var professeur=encodeURIComponent(document.getElementById("professeur").value);
    var section=encodeURIComponent(document.getElementById("section").value);
    var niveau=encodeURIComponent(document.getElementById("niveau").value);
    var cote=encodeURIComponent(document.getElementById("cote").value);
    var commentaire=encodeURIComponent(document.getElementById("commentaire").value);
    
    var jour = document.getElementById("jour").value;
    var mois = document.getElementById("mois").value;
    var annee = document.getElementById("annee").value;
    var date = encodeURIComponent(annee+"-"+mois+"-"+jour);
    
    
	var xhr=getXMLHttpRequest();

	xhr.onreadystatechange = function() {
	if (xhr.readyState == 4 && (xhr.status == 200 ||     xhr.status == 0))
	{
	lireDonnees(xhr.responseText);     }};

	var requete=chemin+"/examen/modificationExamen/"+membre+"/"+professeur+"/"+section+"/"+niveau+"/"+cote+"/"+commentaire+"/"+date;
	xhr.open("GET",requete,true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Op�ration en cours, veuillez patienter et ne pas quitter la page");
	xhr.send(null);
}
function changeNiveau(){
	var niveau = document.getElementById("niveau");
	var section = document.getElementById("section");
	var tableau = document.getElementById("selectNiveau").value.split("#");
	niveau.setAttribute("value",tableau[0]);
	section.setAttribute("value",tableau[1]);
}
function supprimerExamen(chemin,idMembre,idProf,niveau,section,date){
    var xhr=getXMLHttpRequest();

	xhr.onreadystatechange = function() {
	if (xhr.readyState == 4 && (xhr.status == 200 ||     xhr.status == 0))
	{
	lireDonnees(xhr.responseText);     }};

	var requete=chemin+"/examen/supprimerExamen/"+idMembre+"/"+idProf+"/"+section+"/"+niveau+"/"+date;
	xhr.open("GET",requete,true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Op�ration en cours, veuillez patienter et ne pas quitter la page");	
	xhr.send(null);
}

function lireDonnees(text){
	var resultat=document.getElementById("message");
	resultat.innerHTML=text;
	window.setTimeout(function(){
        var resultat=document.getElementById("message");
        resultat.innerHTML="";},5000);
}
