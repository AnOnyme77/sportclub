<?php 
if($membre->getIdMembre() != null)$this->titre="membre ".$membre->getIdMembre();
?>
<h1>Détails sur <?= $membre->getPrenom() ?></h1>
<div class="ssTitre">
	<div class="boutonDiv" ><a id="carte" href="<?= Parser::getChemin()."/Membre/carteMembre/".$membre->getIdMembre() ?>" class="icon" title="Carte du membre">
	<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/card.png"'); ?> alt="Carte du membre" title="Carte du membre"/></div></a></div>	
</div>
<div>
	<div id="message">
		<?php if(isset($message)){  echo($message);  } ?>
	</div>
<form action="" method="post">
	<table>
		<tr>
			<td>Code du membre</td>
			<td><input type="text"  name="idMembre" readonly="readonly" value="<?= $membre->getIdMembre()?>" /></td>
		</tr>
		<tr>
			<td>Date d'inscription</td>
			<td><input type="text"  name="dateInscription" readonly="readonly"
				<?php
				if($membre->getDateInscription() != null) {
					echo('value="'.$membre->getDateInscriptionFormat().'"');
				} ?> /> </td>
		</tr>
		<tr>
			<td>Nom</td>
			<td><input type="text" name="nom" readonly="readonly" value=<?= '"'.$membre->getNom().'"' ?> /></td>
		</tr>
		<tr>
			<td>Prénom</td>
			<td><input type="text" name="prenom" readonly="readonly" value=<?= '"'.$membre->getPrenom().'"' ?>/></td>
		</tr>
		<tr>
			<td>Adresse Mail</td>
			<td><input type="text" name="email" readonly="readonly" value= <?= '"'.$membre->getEmail().'"' ?>/></td>
		</tr>
		<tr>
			<td>Sexe</td>
			<td>
				<p> <?php if($membre->getSexe()=="F" ){ ?> Femme 
				<?php }else{ ?> Homme <?php } ?> </p>
			</td>
		</tr>
		<tr>
			<td>Date de naissance</td>
			<td><input type="text"  name="dateNaissance" readonly="readonly"
				<?php
				if($membre->getDateNaissance() != null) {
					echo('value="'.$membre->getDateNaissanceFormat().'"');
				} ?> /> </td>
		</tr>
		<tr>
            <td>
                    Adresse :
                    <br/> Rue, n°
                    <br/> Ville
                    <br/> Code Postal
                    <br/> Pays
            </td>
            <td>
                <br/><input type="text" name="rueNo" readonly="readonly" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getRue().'"':'""' ?>/>
                <br/><input type="text" name="ville" readonly="readonly" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getVille().'"':'""' ?>/>
                <br/><input type="text" name="codePostal" readonly="readonly" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getCodePostale().'"':'""' ?>/>
                <br/><input type="text" name="pays" readonly="readonly" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getPays().'"':'""' ?>/>
            </td>
        </tr>
		<tr>
			<td>Téléphone fixe</td>
			<td><input type="text" name="telFix" readonly="readonly" value=  <?= '"'.$membre->getTelFixe().'"' ?>/></td>
		</tr>
		<tr>
			<td>GSM</td>
			<td><input type="text" name="telGsm" readonly="readonly" value= <?= '"'.$membre->getTelGsm().'"' ?>/></td>
		</tr>
		<tr>
			<td>Niveau</td> 
			<td>
				<?php if(isset($niveau)){ ?>
				<p><a href="<?= Parser::getChemin()."/niveau/ModifierNiveau/".$niveau->getNameSection()."/".$niveau->getNiveau()?>"> 
				<?= $membre->getNameSection()." ".$membre->getNiveau()." ( ".$niveau->getLibelle()." )" ?></a> </p>
				<?php } ?>
			</td>
		</tr>
		<tr>
			<td>Fonction</td>
			<td>
				<p><?php if($membre->getFonction()=="M"){ ?> Membre 
				<?php }else if($membre->getFonction()=="P"){ ?> Professeur
				<?php }else if($membre->getFonction()=="A"){ ?> Administrateur 
				<?php } ?></p>
			</td>
		</tr>
		
		<tr>
			<td>Photo de profil</td>
			<td class="imgProfil">
				Apercu :
				<br/><img class="myPhoto zoom"  src=<?= '"'.Parser::getChemin().$membre->getLogo().'"' ?>/>
			</td>
		</tr>
	</table>
</form>
</div>
<div class="script">
</div>