<?php
$libExam=explode(" ",strtolower(unserialize($_SESSION["oParametres"])->getLibExamen()));
if($ajout)
	$this->titre= "Ajouter ".$libExam[0]." ".$libExam[1];
else
	$this->titre= "Détails ".$libExam[1]; 
echo "<h1>".$this->titre."</h1>";
?>
<div id="message">
	<?php if(isset($message))echo($message)?>
</div>
<div>
	<table>
		<tr>
			<td>
				Code Membre
			</td>
			<td>
				<input id="membre" type="input" name="niveau" <?php if(isset($examen))echo("value='".$examen->getIdMembre()."' readonly='readonly'");?> />
				<?php
					if(!isset($ajout) or $ajout){
				?>
				<div class="boutonDiv" ><a onclick="rechercherProf('<?=Parser::getChemin()?>','membre')" class="icon" title="">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/search.png"'); ?> alt="Rechercher membre" title="Rechercher membre"/></div></a></div>	
				<?php
					}
				?>
			</td>
		</tr>
		<?php
			if((!isset($ajout) or !$ajout) and isset($membre)){
		?>
		<tr>
			<td>
				Nom du membre
			</td>
			<td>
				<input type="input" name="niveau" value="<?=$membre->getNom()?>" readonly="readonly"/>
			</td>
		</tr>
		<tr>
			<td>
				Prénom du membre
			</td>
			<td>
				<input type="input" name="niveau" value="<?=$membre->getPrenom()?>" readonly="readonly"/>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td>
				Code Professeur
			</td>
			<td>
				<input id="professeur" type="input" name="niveau" value="<?php if(isset($examen))echo($examen->getIdProf());?>"/>
				<div class="boutonDiv" ><a onclick="rechercherProf('<?=Parser::getChemin()?>','professeur')" class="icon" title="">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/search.png"'); ?> alt="Rechercher professeur" title="Rechercher professeur"/></div></a></div>	
			</td>
		</tr>
		<?php
			if(!isset($niveaux)){
		?>
		<tr>
			<td>
				Section
			</td>
			<td>
				<input id="section" type="input" name="nameSection" <?php if(isset($examen))echo("value='".$examen->getNameSection()."' readonly='readonly'");?>/>
			</td>
		</tr>
		<tr>
			<td>
				Niveau
			</td>
			<td>
				<input id="niveau" type="input" name="niveau" <?php if(isset($examen))echo("value='".$examen->getNiveau()."' readonly='readonly'");?>/>
			</td>
		</tr>
		<?php
			}
			else{
				echo("<tr><td>".unserialize($_SESSION["oParametres"])->getLibNiveaux()."</td><td><select id='selectNiveau' onchange='changeNiveau()'>");
					echo("<option value='#'>Selectionnez une valeur</option>");
					foreach($niveaux as $niveau){
						echo("<option value='".$niveau->getNiveau()."#".$niveau->getNameSection()."'>".$niveau->getNiveau()." - ".$niveau->getNameSection()." - ".$niveau->getLibelle()."</option>");
					}
				echo("</select><input type='hidden' id='niveau' value=''/><input type='hidden' id='section' value=''/></td></tr>");
			}
		?>
		<tr>
			<td>
				Date de passage
			</td>
			<td>
				<?php Formulaire::formulaireDate("");?>
			</td>
		</tr>
		<tr>
			<td>
				Cote
			</td>
			<td>
				<input id="cote" type="text" name="cote"  value="<?php if(isset($examen))echo($examen->getCote());?>"/>/<?=unserialize($_SESSION["oParametres"])->getCoteMax()?>
			</td>
		</tr>
		<tr>
			<td>
				Commentaire
			</td>
			<td>
				<textarea id="commentaire" rows="4" cols="50" name="commentaire"><?php if(isset($examen))echo($examen->getCommentaire());?></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="centre">
				<div class="boutonDiv" ><a <?php if(!isset($ajout) or $ajout) echo("onclick='ajouterExamen(\"".Parser::getChemin()."\")'");
			  else echo("onclick='modifierExamen(\"".Parser::getChemin()."\")'");?> href="#"  class="icon" title="">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
			</td>
		</tr>
	</table>
</div>
<div class="script">
	<script src=<?= Parser::getChemin()."/view/professeurs/JS/examen.js"?>></script>
	<script src=<?= Parser::getChemin()."/outils/JS/rechercheMembre.js"?>></script>
</div>