<?php $this->titre="Les ".strtolower(unserialize($_SESSION["oParametres"])->getLibExamens()) ?>
<h1>Les <?= strtolower(unserialize($_SESSION["oParametres"])->getLibExamens()) ?></h1>
<div class="ssTitre">
	<div class="boutonDiv" ><a href="<?=Parser::getChemin()?>/examen/ajouter" class="icon" title="Ajouter <?= strtolower(unserialize($_SESSION["oParametres"])->getLibExamen()) ?>">
	<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/add.png"'); ?> alt="Ajouter <?= strtolower(unserialize($_SESSION["oParametres"])->getLibExamen()) ?>" title="Ajouter <?= strtolower(unserialize($_SESSION["oParametres"])->getLibExamen()) ?>"/></div></a></div>	
</div>
<div id="message">
	<p><?php if(isset($message)){  echo($message);  } ?></p>
</div>

<!--<div>
	<form>
		<table>
			<tr>
				<td>
					Professeur
				</td>
				<td>
					<input type="text" name="niveau"/>
				</td>
			</tr>
			<tr>
				<td>
					Section
				</td>
				<td>
					<input type="text" name="nameSection"/>
				</td>
			</tr>
			<tr>
				<td>
					Niveau
				</td>
				<td>
					<input type="text" name="niveau"/>
				</td>
			</tr>
			<tr>
				<td>
					Date de passage
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="centre">
					<input type="submit" value="Rechercher"/>
				</td>
			</tr>
		</table>
	</form>
</div>-->

<div>
	<table id="sorts">
		<thead>
			<tr>
				<th>Membre</th>
				<th>Professeur</th>
				<th>Section</th>
				<th>Niveau</th>
				<th>Date</th>
				<th class="taille2"></th>
			</tr>
		</thead>
		<tbody>
			<?php
				if(isset($listeExamens) and is_array($listeExamens)){
					foreach($listeExamens as $examen){
						echo("<tr>");
						echo("<td>".$examen->getIdMembre()."</td>");
						echo("<td>".$examen->getIdProf()."</td>");
						echo("<td>".$examen->getNameSection()."</td>");
						echo("<td>".$examen->getNiveau()."</td>");
						echo("<td>".$examen->getDatePassage()->format("d-m-Y")."</td>");
						?>
						<td class="taille2">
						
						<div class="boutonDiv" ><a href="<?= Parser::getChemin()."/examen/modifierExamen/".
							$examen->getIdMembre()."/".$examen->getIdProf()."/".$examen->getNiveau()."/".
							$examen->getNameSection()."/".$examen->getDatePassage()->format("Y-m-d") ?>
							" class="icon" title="">
						<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/edit.png"'); ?> alt="Modifier" title="Modifier"/></div></a></div>
					
						<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous sur de vouloir effectuer cette action ?',function(){supprimerExamen(<?= "'".Parser::getChemin()."'".","."'".
							$examen->getIdMembre()."'".","."'".$examen->getIdProf()."'".","."'".$examen->getNiveau()."'".",".
							"'".$examen->getNameSection()."'".","."'".$examen->getDatePassage()->format("Y-m-d")."'" ?>)})" 
							href="#"   class="icon" title="">
						<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/delete.png"'); ?> alt="Supprimer" title="Supprimer"/></div></a></div>
					
					</tr>
						<?php
					}
				}
			?>
		</tbody>
    </table>
</div>
<div class="script">
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/dynatable.jquery.js'.'"'); ?> ></script>
	<script src=<?= Parser::getChemin()."/view/professeurs/JS/examen.js"?>></script>
	<script type="text/javascript">
		$('#sorts').dynatable();
	</script>
</div>