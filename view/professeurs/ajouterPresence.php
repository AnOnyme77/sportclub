<?php $this->titre="Ajouter des présences" ?>
<h1>Ajouter des présences</h1>
<br/>
<div id="message">
	
</div>
<div>
		<h2 id="titreResultat" style="visibility:hidden;">Inscriptions pour la séance</h2>
		<div id="resultat">
			
		</div>
		<table id="sorts">
			<thead>
				<tr>
					<th>
						Cours
					</th>
					<th>
						Date et heure
					</th>
					<th>
						Nombre d'inscriptions
					</th>
					<th>
						Professeur
					</th>
					<th>
						
					</th>
				</tr>
			</thead>
			<tbody>
			<?php
				if(isset($liste) and count($liste)>0){
					$i=0;
					foreach($liste as $seance){
						echo("<tr>");
						echo("<td>".$seance->getCours()."</td>");
						echo("<td>".$seance->getDateCours()."</td>");
                        if(isset($listeNombre) and count($listeNombre)==count($liste)){
							echo("<td>".$listeNombre[$i]."</td>");
						}
						else{
							echo("<td>Non récupéré</td>");
						}
						if(isset($listeProfs) and count($listeProfs)==count($liste)){
							echo("<td>".$listeProfs[$i]->getNom()." ".$listeProfs[$i]->getPrenom()."</td>");
						}
						else{
							echo("<td>".$seance->getIdProf()."</td>");
						}
						if($listeNombre[$i]>0){
							?>
							<td>			
								<div class="boutonDiv" ><a onclick="afficherInscriptions(<?= "'".Parser::getChemin()."','".$seance->getNoSeance()."'" ?>)" class="icon" title="">
								<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/select.png"'); ?> alt="Selectionner" title="Selectionner"/></div></a></div>
							</td>
							<?php
						}
						else{
							?>
							<td>			
								<div class="boutonDiv" ><a class="icon vide" title="">
								<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/vide.png"'); ?> alt="Selectionner" title="Selectionner"/></div></a></div>
							</td>
							<?php
						}
						echo("</tr>");
						$i++;
					}
				}
			?>
			</tbody>
		</table>
</div>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/professeurs/JS/presence.js"?>></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/jquery.js'.'"'); ?> ></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/dynatable.jquery.js'.'"'); ?> ></script>
	<script type="text/javascript">
	$('#sorts').dynatable();
	</script>
</div>