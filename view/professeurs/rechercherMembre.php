<?php $this->titre="Les membres" ?>
<h1>Les membres</h1>
<?php if(isset($fonction) && strtolower($fonction) == "a") {?>
<div class="ssTitre">
	<div class="boutonDiv" ><a href="<?=Parser::getChemin()?>/Membre/afficherMembre" class="icon" title="Ajouter un membre">
	<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/add.png"'); ?> alt="Ajouter un membre" title="Ajouter un membre"/></div></a></div>	
</div>
<?php } ?>
<div>
	<?php if(isset($message)){ ?>
	<div id="message">
		<?php echo ($message); }?>
	</div>
	<table id="sorts">
	<thead>
		<tr>
			<th>Code du membre</th>
			<th>Nom</th>
			<th>Prénom</th>
			<th>Age</th>
			<th>Email</th>
			<th class="taille3" ></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(isset($membres)){
			foreach ( $membres as $membre ) {
       		?>
       		<tr id=<?= '"'.$membre->getIdMembre().'"'?>>
       			<td><a href=<?= '"Membre/afficherMembre/'.$membre->getIdMembre().'"' ?> id="<?= $membre->getIdMembre() ?>"><?= $membre->getIdMembre() ?></a></td>
       			<td><?= $membre->getNom() ?></td>
       			<td><?= $membre->getPrenom() ?></td>
       			<td><?= $membre->getAge()." ans" ?></td>
       			<td><?= $membre->getEmail() ?></td>
       			<td class="taille3">
					<div class="boutonDiv" ><a href="<?= Parser::getChemin()."/Membre/afficherMembre/".$membre->getIdMembre() ?>"   class="icon" title="">
					<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/detail.png"'); ?> alt="Détails" title="Détails"/></div></a></div>
				
					<div class="boutonDiv" ><a href="<?= Parser::getChemin()."/Membre/carteMembre/".$membre->getIdMembre() ?>"   class="icon" title="">
					<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/card.png"'); ?> alt="Carte membre" title="Carte membre"/></div></a></div>
					
					<?php if(isset($fonction) && strtolower($fonction) == "a") {?>
					<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous sur de vouloir effectuer cette action ?',
							function(){supprimer('<?= $membre->getIdMembre()?>')})" href="#"  class="icon" title="">
					<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/delete.png"'); ?> alt="Supprimer" title="Supprimer"/></div></a></div>
       			<?php } ?>
       			</td>
    	   	</tr>
    	   	<?php
			}
		}
		?>
	</tbody>
    </table>
</div>
<div class="script">
<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/jquery.js'.'"'); ?> ></script>
<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/dynatable.jquery.js'.'"'); ?> ></script>
<script type="text/javascript">
$('#sorts').dynatable();
</script>
<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/Ajax.js'.'"'); ?> ></script>
<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/view/JS/gererMembre.js'.'"'); ?> ></script>
</div>
