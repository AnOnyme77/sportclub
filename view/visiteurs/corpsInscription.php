<!DOCTYPE html>
<?php
    if(isset($_POST["envoyer"]))
    {    
	if($_POST["mdp"] != $_POST["mdpCheck"])
	{
	    $erreur=true;
	}
	else
	{
	    $dateNaissance=$_POST["annee"]."/".$_POST["mois"]."/".$_POST["jour"];
	    $db=new DB("anonyme77.no-ip.org","ges4u","projet2ig","projet2iganon");
	    $infos=array("nom"=>$_POST["nom"],
			"prenom"=>$_POST["prenom"],
			"sexe"=>$_POST["sexe"],
			"dateNaissance"=>$dateNaissance,
			"telFixe"=>$_POST["telFixe"],
			"telGsm"=>$_POST["telGsm"],
			"adr_rue"=>$_POST["adr_rue"],
			"adr_cp"=>$_POST["adr_cp"],
			"adr_ville"=>$_POST["adr_ville"],
			"adr_pays"=>$_POST["adr_pays"],
			"email"=>$_POST["email"],
			"mdp"=>sha1($_POST["mdp"]),
			"logo"=>".",
			"idMembre"=>$db->calculerIdMembre());
	    $erreurs=$db->ajouter("MEMBRE",$infos);
	}
    }
    
?>
<html>
    <head>
        <meta charset="utf-8"/>	
		<meta author="Evrard, Jamar, Hainaut"/>
		<meta author="Ges4u"/>
        <link href="../CSS/style.css" rel="stylesheet" />
		<link href="../CSS/propositionStyle.css" rel="stylesheet"/>
    </head>
    <body>
		<div class="page">
			<?php //include("../Outils/header.php"); ?>
            <!-- corps de page -->
            
            <section class="global">
		<div class="corp">
		    <form method="POST">
			Vos données:
			<hr/><br/>
			<label>Nom: </label>
			<input type="text" placeholder="Nom" name="nom"><br/>
			<label>Prénom: </label>
			<input type="text" placeholder="Prénom" name="prenom"><br/>
			<label>Sexe: </label>
			      <input type="radio" name="sexe" value="H"/> Homme
			      <input type="radio" name="sexe" value="F"/> Femme<br/>
			<label>Date de naissance:</label><br/>
						    Jour <input type="number" placeholder="31" name="jour" size="1"/>
						    Mois <input type="number" placeholder="12" name="mois" size="1"/>
						    Année <input type="number" placeholder="1910" name="annee" size="3"/><br/>
			<label>Tél Fixe:</label><input type="tel" placeholder="+3260112233" name="telFixe"><br/>
			<label>Tél Mobile:</label><input type="tel" placeholder="+32480111222" name="telGsm"><br/>
			<br/>
			<br/>
			
			Votre adresse:
			<hr/><br/>
			<label>Rue, n°: </label><input type="text" placeholder="Rue, n°" name="adr_rue"><br/>
			<label>Code Postal: </label><input type="number" placeholder="Code Postal:" name="adr_cp"><br/>
			<label>Ville: </label><input type="text" placeholder="Ville" name="adr_ville"><br/>
			<label>Pays: </label>
			<select name="adr_pays"> 
			    <option value="Belgique">Belgique</option>
			    <option value="Allemagne">Allemagne</option>
			    <option value="Autriche">Autriche</option>
			    <option value="France">France</option>
			    <option value="Italie">Italie</option>
			    <option value="Luxembourg">Luxembourg</option>
			    <option value="Pays-Bas">Pays-Bas</option>
			</select>
			<br/>
			<br/>
			
			Vos données de connexion:
			<hr/><br/>
			<label>E-mail: </label><input type="email" placeholder="votreemail@votredomaine" name="email"><br/>
			<label>Mot de passe: </label><input type="password" name="mdp"><br/>
			<?php
			    if(isset($erreur) && $erreur==true)
			    {
				echo("<h4 class='invalid' >Les mots de passe ne correspondent pas.</h4>");
			    }
			?>
			<label>Confirmez le mot de passe: </label><input type="password" name="mdpCheck">
			<br/>
			<br/>
			<input type="submit" value="Envoyer" name="envoyer">
			
		    </form>
		</div>
            </section>
		</div>
    </body>
</html> 
