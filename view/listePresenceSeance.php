<div>
    <table>
        <tr>
            <th>Membre</th>
			<th>Email</th>
            <th>Cours</th>
			<th>Date seance</th>
            <th>champs de présence</th>
        </tr>
        <?php
            if(isset($liste) and is_array($liste) and count($liste)>0){
				$i=0;
                foreach($liste as $inscription){
                    echo("<tr>");
					
					if(isset($listeMembre) and is_array($listeMembre) and count($listeMembre)==count($liste)){
						echo("<td>".$listeMembre[$i]->getNom()." ".$listeMembre[$i]->getPrenom()."</td>");
						echo("<td>".$listeMembre[$i]->getEmail()."</td>");
					}
					else
						echo("<td colspan='2'>".$inscription->getIdMembre()."</td>");
					
					if(isset($listeSeance) and is_array($listeSeance) and count($listeSeance)==count($liste)){
						echo("<td>".$listeSeance[$i]->getCours()."</td>");
						echo("<td>".$listeSeance[$i]->getDateCours()."</td>");
					}
					else{
						echo("<td colspan='2'>".$inscription->getNoSeance()."</td>");
					}
                    echo('<td name="radio">');
					if($inscription->getPresent()=="T"){
					echo('
                    <input type= "radio" name="'.$inscription->getIdMembre()."#".$inscription->getNoSeance().'" value="T" checked="checked"> Présent
                    <input type= "radio" name="'.$inscription->getIdMembre()."#".$inscription->getNoSeance().'" value="F"> Absent</td>');
					}
					else{
						echo('
                    <input type= "radio" name="'.$inscription->getIdMembre()."#".$inscription->getNoSeance().'" value="T"> Présent
                    <input type= "radio" name="'.$inscription->getIdMembre()."#".$inscription->getNoSeance().'" value="F" checked="checked"> Absent</td>');
					}
                    echo("<tr>");
					$i++;
                }
            }
            else{
                echo("<td colspan='5'>Aucune inscription à cette séance</td>");
            }
        ?>
        <tr>
            <td colspan="5" class="centre">
				<div class="boutonDiv" ><a onclick="enregistrer('<?= Parser::getChemin()?>')" href="#"  class="icon" title="">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
			</td>
        </tr>
    </table>
</div>