function supprimer( idMembre) {
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
        lireDonnees(xhr.responseText);
        
		var ligne = document.getElementById(idMembre);
		ligne.innerHTML=""; 
		setTimeout(function() {
			location.reload() ; 
		}, 5000);
    }}; 
    
    var requete="Membre/supprimerMembre/"+idMembre;
    
    xhr.open("POST",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);

    return false;	
}
function lireDonnees(text){
    var resultat=document.getElementById("message");
	resultat.innerHTML=text;
}
