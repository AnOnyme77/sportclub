<div class="menuPrincipal">
	
	<div class="divHeader">
		<div class="myClub">
			<div class="logoClub">
				<a href=<?php echo('"'.Parser::getChemin().'/'.'"'); ?>><img class="logoClubImg zoom" src=<?php if(unserialize($_SESSION["oParametres"])->getLogo()=="" or
												!file_exists(Parser::getCheminRacine().unserialize($_SESSION["oParametres"])->getLogo()))
													echo('"'.Parser::getChemin().'/images/parametres/logoClubDefault.jpg'.'"');
											else
													echo('"'.Parser::getChemin().unserialize($_SESSION["oParametres"])->getLogo().'"');
											?>/></a>
			</div>
			<h1 class="namClub"><?= unserialize($_SESSION["oParametres"])->getNomEntreprise()?></h1>
		</div>
		<div class="myProfil">
			<div class="imgProfil">
				<img class="myPhoto zoom" src=<?php if(unserialize($_SESSION["oMembre"])->getLogo()=="" or
												!file_exists(Parser::getCheminRacine().unserialize($_SESSION["oMembre"])->getLogo()))
													echo('"'.Parser::getChemin().'/images/profilsMembres/defaultMember.jpg'.'"');
											else
													echo('"'.Parser::getChemin().unserialize($_SESSION["oMembre"])->getLogo().'"');
											?>/>
			</div>
			<div class="myInfoProfil">
				<h2> Bonjour <?=unserialize($_SESSION["oMembre"])->getPrenom()?></h2>
				<a href=<?php echo('"'.Parser::getChemin().'/Membre/profil'.'"'); ?>>Mon profil</a>
				<a href="#" onclick="exWithConfirm('Êtes vous certain(e) de vouloir vous déconnecter ?',function(){document.location.href='<?php echo(Parser::getChemin().'/authentifier/deconnecter');?>'})">Déconnexion</a>
				
			</div>
		</div>
	</div>    
	<nav class="navbar navbar-inner navbar-static-top" >
	  <div class="container">
		<ul class="nav">
			<li ><a class="icon"  href=<?php echo('"'.Parser::getChemin().'/'.'"'); ?> title="Accueil">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/home.png"'); ?> alt="Accueil" title="Accueil"/></div></a></li>
			<li class="divider-vertical"></li>
			<li>
				<a class="icon" onclick="javascript:return false;" title="Abonnements" href="#"> 
				<img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/abonnement.png"'); ?> alt="Abonnements" title="Abonnements"/></a>
				<ul>
					<li><a href=<?php echo('"'.Parser::getChemin().'/Abonnement/getAbonnementsMembre'.'"'); ?>>Mes abonnements</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/TypeAbonnement'.'"'); ?>>Lister les types d'abonnements</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li>
				<a class="icon" onclick="javascript:return false;" title="Présences" href="#"> 
				<img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/presence.png"'); ?> alt="Présences" title="Présence"/></a>
				<ul>
					<li><a href=<?php echo('"'.Parser::getChemin().'/inscription'.'"'); ?>>M'inscrire</a></li></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li>
				<a class="icon" href=<?php echo('"'.Parser::getChemin().'/niveau/detailNiveau'.'"'); ?> title="Mes <?= unserialize($_SESSION["oParametres"])->getLibNiveaux()?>" >
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/niveaux.png"'); ?> alt="Mes <?= unserialize($_SESSION["oParametres"])->getLibNiveaux()?>" title="Mes <?= unserialize($_SESSION["oParametres"])->getLibNiveaux()?>"/></div></a>
			
			<li class="divider-vertical"></li>
		</ul>
		<div class="navMyLogo"> <a href=<?php echo('"'.Parser::getChemin().'/'.'"'); ?>><img class='myLogo' src=<?php echo('"'.Parser::getChemin().'/images/logo.png'.'"'); ?>></a> </div>
	  </div>
	</nav>
</div>
