<div class="menuPrincipal">
	
	<div class="divHeader">
		<div class="myClub">
			<div class="logoClub">
				<a href=<?php echo('"'.Parser::getChemin().'/'.'"'); ?>><img class="logoClubImg zoom" src=<?php if(unserialize($_SESSION["oParametres"])->getLogo()=="" or
												!file_exists(Parser::getCheminRacine().unserialize($_SESSION["oParametres"])->getLogo()))
													echo('"'.Parser::getChemin().'/images/parametres/logoClubDefault.jpg'.'"');
											else
													echo('"'.Parser::getChemin().unserialize($_SESSION["oParametres"])->getLogo().'"');
											?>/></a>
			</div>
			<h1 class="namClub"><?= unserialize($_SESSION["oParametres"])->getNomEntreprise()?></h1>
		</div>
		<div class="myProfil">
			<div class="imgProfil">
				<img class="myPhoto zoom" src=<?php if(unserialize($_SESSION["oMembre"])->getLogo()=="" or
												!file_exists(Parser::getCheminRacine().unserialize($_SESSION["oMembre"])->getLogo()))
													echo('"'.Parser::getChemin().'/images/profilsMembres/defaultMember.jpg'.'"');
											else
													echo('"'.Parser::getChemin().unserialize($_SESSION["oMembre"])->getLogo().'"');
											?>/>
			</div>
			<div class="myInfoProfil">
				<h2> Bonjour</h2>
				<h2><?=unserialize($_SESSION["oMembre"])->getPrenom()?></h2>
				<a href=<?php echo('"'.Parser::getChemin().'/Membre/profil'.'"'); ?>>Mon profil</a>
				<a href="#" onclick="exWithConfirm('Êtes vous certain(e) de vouloir vous déconnecter ?',function(){document.location.href='<?php echo(Parser::getChemin().'/authentifier/deconnecter');?>'})">Déconnexion</a>
				
			</div>
		</div>
	</div>    
	<nav class="navbar navbar-inner navbar-static-top" >
	  <div class="container">
		<ul class="nav">
			<li ><a class="icon"  href=<?php echo('"'.Parser::getChemin().'/'.'"'); ?> title="Accueil">
				<div> <img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/home.png"'); ?> alt="Accueil" title="Accueil"/></div></a>
			</li>
			<li class="divider-vertical"></li>
			<li>
				<a class="icon" onclick="javascript:return false;" title="Membres" href="#">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/membres.png"'); ?> alt="Membres" title="Membres"/></div></a>
				<ul>
					<li><a href=<?php echo('"'.Parser::getChemin().'/Abonnement/scanMembre'.'"'); ?>>Controler les membres</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/Membre/afficherMembre'.'"'); ?>>Ajouter un membre</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/Membre'.'"'); ?>>Lister les membres</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li>
				<a class="icon" onclick="javascript:return false;" title="Abonnements" href="#"> 
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/abonnement.png"'); ?> alt="Abonnements" title="Abonnements"/></div></a>
				<ul>
					<li><a href=<?php echo('"'.Parser::getChemin().'/Abonnement/ajouterAbonnement'.'"'); ?>>Ajouter un abonnement</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/Abonnement'.'"'); ?>>Lister les abonnements</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/TypeAbonnement/ajouterTypeAbonnement'.'"'); ?>>Ajouter un type d'abonnement</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/TypeAbonnement'.'"'); ?>>Lister les types d'abonnements</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/Abonnement/getAbonnementsMembre'.'"'); ?>>Mes abonnements</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li>
				<a class="icon" onclick="javascript:return false;" title="<?= unserialize($_SESSION["oParametres"])->getLibNiveaux()?>" href="#">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/niveaux.png"'); ?> alt="<?= unserialize($_SESSION["oParametres"])->getLibNiveaux()?>" title="<?= unserialize($_SESSION["oParametres"])->getLibNiveaux()?>"/></div></a>
				<ul>
					<li><a href=<?php echo('"'.Parser::getChemin().'/niveau/ajouterNiveau'.'"'); ?>>Ajouter <?= strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau())?></a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/niveau'.'"'); ?>>Lister les <?= strtolower(unserialize($_SESSION["oParametres"])->getLibNiveaux())?></a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/examen/ajouter'.'"'); ?>>Ajouter <?= strtolower(unserialize($_SESSION["oParametres"])->getLibExamen())?> </a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/examen/listerExamen'.'"'); ?>>Lister les <?= strtolower(unserialize($_SESSION["oParametres"])->getLibExamens())?></a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/niveau/detailNiveau'.'"'); ?>>Mes <?= strtolower(unserialize($_SESSION["oParametres"])->getLibNiveaux())?></a></li>					
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li>
				<a class="icon" onclick="javascript:return false;" title="Séances" href="#"> 
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/seance.png"'); ?> alt="Séances" title="Séances"/></div></a>
				<ul>
					<li><a href=<?php echo('"'.Parser::getChemin().'/seance/ajouterSeance'.'"'); ?>>Ajouter une séance</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/seance'.'"'); ?>>Lister les séances</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/inscription/presence'.'"'); ?>>Présences</a></li>
					<li class="divider-horizontal"></li>
					<li><a href=<?php echo('"'.Parser::getChemin().'/inscription'.'"'); ?>>M'inscrire</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li><a class="icon"  href=<?php echo('"'.Parser::getChemin().'/parametre'.'"'); ?> title="Paramètres">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/settings.png"'); ?> alt="Paramètres" title="Paramètres"/></div></a></li>
			<li class="divider-vertical"></li>
		</ul>
		<div class="navMyLogo"> <a href="index.php"><img class='myLogo' src=<?php echo('"'.Parser::getChemin().'/images/logo.png'.'"'); ?>></a> </div>
	  </div>
	</nav>
</div>
