<!-- /* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 16/03/2014 - modifée le 30/05/2014
 -----------------------------------------------------------------------------------------------------*/ -->
<?php $this->titre=unserialize($_SESSION["oParametres"])->getNomEntreprise()." - Connexion" ?>
<h1>Bienvenue</h1>
<div id="message">
<?php
        if(isset($message)){
                echo($message);
        }
        else
        	echo "Connectez-vous";
?>
</div>

<div class="container connexion clearfix">
    <input id="email" class="email js-email focusChamps" name="email" placeholder="E-mail" type="email">
    <input id="mdp" class="mdp js-mdp enterChamps" name="mdp" placeholder="Mot de passe" type="password">
    <button class="connexion js-button-connexion boutonChamps" onclick="authentifier('<?= Parser::getChemin()?>')">Connexion</button>
    <!-- <button class="inscription js-button-inscription">Inscription</button> -->
    <!--<button class="oubli js-button-oubli">Mot de passe oublié ?</button> -->
</div>

<div class="script">
    <script src=<?= Parser::getChemin()."/view/authentifier/JS/authentifier.js"?>></script>
</div>
