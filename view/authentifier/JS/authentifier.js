function authentifier(chemin)
{
	//On récupère les données dans le HTML:
	var email=encodeURIComponent(document.getElementById("email").value);
    var mdp=encodeURIComponent(document.getElementById("mdp").value);
    
    //Requetes AJAX
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0)){ 
		lireDonnees(xhr.responseText);
		var retour=xhr.responseText.slice(((xhr.responseText.length)-3),((xhr.responseText.length)-1));
		if (retour=="ok") {
			document.location.href=chemin; 
		}
	}}; 
    
    					//dossier/controller/parametres
    var requete=chemin+"/authentifier/authentification/"+email+"/"+mdp;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    //Oublie pas le send comme laurent :D
    xhr.send(null);
}

//Fonction de récupération des données
function lireDonnees(text)
{
    if(encodeURIComponent(text) == "ok%20"){
        var resultat=document.getElementById("message");
        resultat.innerHTML="Connexion réussie";
    }
    else{
        var resultat=document.getElementById("message");
        resultat.innerHTML=text;
    }
}
