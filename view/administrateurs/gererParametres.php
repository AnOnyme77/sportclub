<?php $this->titre="Gestion des paramètres" ?>
<h1>Gestion des paramètres</h1>
<div id="message">
    
</div>
<table>
    <tr>
        <td>Nom de l'entreprise</td>
        <td>
            <input name="nomEntreprise" value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getNomEntreprise());?>"/>
        </td>
    </tr>
    <tr>
        <td>Téléphone de l'entreprise</td>
        <td>
            <input name="numTel" value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getNumTel());?>"/>
        </td>
    </tr>
    <tr>
        <td>Rue de l'entreprise</td>
        <td>
            <input name="rue" value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getRue());?>"/>
        </td>
    </tr>
    <tr>
        <td>Code postal de l'entreprise</td>
        <td>
            <input name="cp" value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getCodePostale());?>"/>
        </td>
    </tr>
    <tr>
        <td>Ville de l'entreprise</td>
        <td>
            <input name="ville" value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getVille());?>"/>
        </td>
    </tr>
    <tr>
        <td>Pays de l'entreprise</td>
        <td>
            <input name="pays" value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getPays());?>"/>
        </td>
    </tr>
    <tr>
        <td>Nom du responsable</td>
        <td>
            <input name="nomResp" value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getNomResp());?>"/>
        </td>
    </tr>
    <tr>
        <td>Prénom du responsable</td>
        <td>
            <input name="prenomResp" value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getPrenomResp());?>"/>
        </td>
    </tr>
    <tr>
        <td>Cote maximale</td>
        <td>
            <input name="coteMax" value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getCoteMax());?>"/>
        </td>
    </tr>
    <tr>
        <td>Logo</td>
        <td>
            <form method="post" action="<?=Parser::getChemin()?>/index.php?page=parametre&action=changerLogo" enctype="multipart/form-data">
                <label for="monFichier">Fichier  :</label>
                <input type="file" name="image" id="image" /><br />
                <input type="submit" value="Enregistrer l'image"/>
			</form>
            <br/>Apercu :
            <br/><img height="60px" src='<?php if(file_exists(Parser::getCheminRacine()."/".unserialize($_SESSION["oParametres"])->getLogo()))
                                                    echo(Parser::getChemin()."/".unserialize($_SESSION["oParametres"])->getLogo());
                                                else
                                                    echo(Parser::getChemin()."/images/parametres/logoClubDefault.jpg");?>' />
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <input readonly="readonly" size=10 value="Singulier"/>
            <input readonly="readonly" size=10 value="Pluriel"/>
         </td>
    </tr>
    <tr>
        <td>Libellé des niveaux</td>
        <td>
            <input name="libNiveau" size=10 value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getLibNiveau());?>"/>
        	<input name="libNiveaux" size=10 value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getLibNiveaux());?>"/>
        </td>
    </tr>
    <tr>
        <td>Libellé des examens</td>
        <td>
            <input name="libExamen" size=10 value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getLibExamen());?>"/>
            <input name="libExamens" size=10 value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getLibExamens());?>"/>
        </td>
    </tr>
    <tr>
        <td>Libellé des compétences</td>
        <td>
            <input name="libCompetence" size=10 value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getLibCompetence());?>"/>
        	<input name="libCompetences" size=10 value="<?php if($lectureOk) echo(unserialize($_SESSION["oParametres"])->getLibCompetences());?>"/>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="centre">
        	<div class="boutonDiv" ><a onclick="modifier('<?=Parser::getChemin()?>')" href="#"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
		</td>
    </tr>
</table>
<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
<script src=<?= Parser::getChemin()."/view/administrateurs/JS/gererParametres.js"?>></script>