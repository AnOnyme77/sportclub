<?php $this->titre="Recherche des membres" ?>
<h1>Recherche des membres</h1>
<div id="message">
    <?php
		if(isset($message))echo $message
	?>
</div>
<div id="membres">
    <?php
	if(isset($membres)){?>
		<br/><table id="sorts">
	<thead>
		<tr>
			<th>Nom</th>
			<th>Prenom</th>
			<th>Email</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
		<tbody>
		<?php
		foreach($membres as $membre){
			echo("<tr>");
				echo("<td>".$membre->getNom()."</td>");
				echo("<td>".$membre->getPrenom()."</td>");
				echo("<td>".$membre->getEmail()."</td>");
				switch($membre->getFonction()){
					case "A":
						echo("<td>Administrateur</td>");
						break;
					case "P":
						echo("<td>Professeur</td>");
						break;
					case "M":
						echo("<td>Membre</td>");
						break;
				}?>
				<td>
					<div class="boutonDiv" ><a onclick="retourner('<?= $membre->getIdMembre()?>','<?=$champs?>')" class="icon" title="">
					<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/select.png"'); ?> alt="Selectionner" title="Selectionner"/></div></a></div>
				</td>
				<?php
			echo("</tr>");
		}
		echo("</tbody>");
		echo("</table>");
	}
		
	?>
</div>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/seance.js"?>></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/jquery.js'.'"'); ?> ></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/dynatable.jquery.js'.'"'); ?> ></script>
	<script src=<?= Parser::getChemin()."/outils/JS/rechercheMembre.js"?>></script>
	<script type="text/javascript">
	$('#sorts').dynatable();
	</script>
</div>