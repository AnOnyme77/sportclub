<?php if($ajout) $this->titre="Ajouter "; else $this->titre="Modifier"; 
	$this->titre.=strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau());
?>
<h1>
<?php
if($ajout)echo("Ajouter ");
else echo("Modifier ");
echo(strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau())); ?>
</h1>
<div id="message">
	<?php
		if(isset($message))
			echo($message);
	?>
</div>
<table>
	<tbody id="corpsTableau" class="veStart">
	<tr>
		<td>Section:</td>
		<td><input type="text" name="section" id="section" class="validate[required,maxSize[30]] text-input"
		<?php if(!$ajout){
				echo("readonly='true'");
				echo("value='".$niveau->getNameSection()."'");
			}
		?>/></td>
	</tr>
	<tr>
		<td>Niveau:</td>
		<td><input type="number" size="2" name="niveau" id="niveau" class="validate[required,custom[integer],min[1]] text-input"
		<?php if(!$ajout){
				echo("readonly='true'");
				echo("value='".$niveau->getNiveau()."'");
			}
		?>/></td>
	</tr>
	<tr>
		<td>Libelle</td>
		<td><input type="text" name="libelle" id="libelle" class="validate[required,maxSize[20]] text-input"
		<?php if(!$ajout)
				echo("value='".$niveau->getLibelle()."'");
			
		?>/></td>
	</tr>
	<?php if(!$ajout){ ?>
	<tr>
		<td>Logo</td>
		<td>
			<form method="post" action="<?=Parser::getChemin()?>/index.php?page=niveau&action=ajouterImage" enctype="multipart/form-data">
				<input type="file" name="image" id="image" /><br />
				<input type="hidden" name="niveau" value="<?= $niveau->getNiveau()?>"/>
				<input type="hidden" name="section" value="<?= $niveau->getNameSection()?>"/>
				<input type="submit" name="submit" value="Envoyer" />
			</form>
			<?php if(file_exists(Parser::getCheminRacine().$niveau->getLogo())){?>
			Apercu :
			<br/><img class="zoom" src="<?=Parser::getChemin().$niveau->getLogo()?>" width="70px"/>
			<?php } ?>
		</td>
	</tr>
	<tr>
		<td><b>Compétences</b></td>
		<td><b>Détails</b></td>
	</tr>
	<?php
		}
	if(isset($competences) and count($competences)>0){
		$i=0;
		foreach($competences as $competence){
			echo('
			<tr id="'.$i.'">
				<td>
					<input type="text" name="titre" value="'.$competence->getTitre().'" readonly="readonly" />
				</td>
				<td>
				<textArea id="detail'.str_replace(" ","",$competence->getTitre()).'">'.$competence->getDetail().'</textArea>');
				?>
				<div class="boutonDiv" ><a onclick="modifierCompetence(<?= "'".$niveau->getNiveau()."','".$niveau->getNameSection()."','".$competence->getTitre()."','".Parser::getChemin()."'" ?>)"   class="icon" title="">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
				
				<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous sur ce vouloir effectuer cette action ?',function(){supprimerCompetence(<?= "'".$niveau->getNiveau()."','".$niveau->getNameSection()."','".$competence->getTitre()."','".Parser::getChemin()."',".$i ?>)})"   class="icon" title="">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/delete.png"'); ?> alt="Supprimer" title="Supprimer"/></div></a></div>
				
			</td>
			</tr>
			<?php
			$i++;
		}
	}
	else{
		if(!$ajout){
			echo('<td colspan="2" class="centre">Aucun(e)(s) '.strtolower(unserialize($_SESSION["oParametres"])->getLibCompetence()).' n\'est(ne sont) encore liée(s) à ce '.strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau()).'.<br/> Cliquez ci dessous pour en ajouter</td>');
		}
	}
	?>
	<?php
		if(!$ajout){
	?>
	<tr id="boutons">
		<td colspan="2" class="centre">
			<div class="boutonDiv" ><a onclick="ajoutChamps(<?= "'".Parser::getChemin()."','".strtolower(unserialize($_SESSION["oParametres"])->getLibCompetence())."'"?>)" class="icon" title="Ajouter <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibCompetence())); ?>">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/add.png"'); ?> alt="Ajouter <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibCompetence())); ?>" title="Ajouter <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibCompetence())); ?>"/></div></a></div>
		</td>
	</tr>
	<?php
		}
		else{
			
	?>
	<tr id="boutons" class="centre">
		<td colspan="2">Enregistrer <?= strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau())?> puis recharger la page <br/>afin de pouvoir lui ajouter des <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibCompetences()))."."; ?></td>
	</tr>
	<?php
		}
	?>
	<tr>
		<td colspan="2" class="centre">
			<div class="boutonDiv" ><a <?php if($ajout) echo("onclick='ajout(\"".Parser::getChemin()."\")'");
						 else echo("onclick='modifier(\"".Parser::getChemin()."\")'");
					?> href="#"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
		</td>
	</tr>
	</tbody>
</table>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/niveau.js"?>></script>
</div>