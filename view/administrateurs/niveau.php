<?php $this->titre="Les ".strtolower(unserialize($_SESSION["oParametres"])->getLibNiveaux()) ?>
<h1>Les <?= strtolower(unserialize($_SESSION["oParametres"])->getLibNiveaux()) ?></h1>
<div class="ssTitre">
		<div class="boutonDiv" ><a href="<?= Parser::getChemin()."/niveau/ajouterNiveau" ?>" class="icon" title="Ajouter <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau())); ?>">
		<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/add.png"'); ?> alt="Ajouter <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau())); ?>" title="Ajouter <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau())); ?>"/></div></a></div>	
</div>
	
<?php if(isset($message)){ ?>
	<div id="message">
		<p> <?= $message ?> </p>
			</div>
<?php } ?>
<!-- <div>
	<form>
		<table>
		<tr>
			<td>
				Section
			</td>
			<td>
				<input type="text" name="recherche"/>
			</td>
		</tr>
		<tr>
			<td>
				Niveau
			</td>
			<td>
				<input type="text" name="recherche"/>
			</td>
		</tr>
		<tr>
			<td>
				Libellé
			</td>
			<td>
				<input type="text" name="recherche"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="centre"><input type="submit" value="Rechercher"/></td>
		</tr>
		</table>
	</form>
</div>
-->
<div>
	<table id="sorts">
		<thead>
			<tr>
				<th>
					Section<br/>
				</th>
				<th>
					Niveau<br/>
				</th>
				<th>
					Libelle<br/>
				</th>
				<th>
				</th>
			</tr>
		</thead>
		<tbody>
		<?php
			foreach($liste as $niveau){
				echo("<tr id='".$niveau->getNiveau().$niveau->getNameSection()."'>");
				if(unserialize($_SESSION["oMembre"])->getFonction()=="A")
					echo("<td><a href='".Parser::getChemin()."/niveau/modifierNiveau/".$niveau->getNameSection()."/".$niveau->getNiveau()."' title='Modifier ce niveau'>".$niveau->getNameSection()."</a></td>");
				else
					echo("<td><a href='niveau/detailNiveau/".$niveau->getNiveau()."/".$niveau->getNameSection()."' title='Detail du niveau'>".$niveau->getNameSection()."</a></td>");
				echo("<td>".$niveau->getNiveau()."</td>");
				echo("<td>".$niveau->getLibelle()."</td>"); ?>
				<td>
					<div class="boutonDiv" ><a href="<?php echo(Parser::getChemin()."/niveau/modifierNiveau/".$niveau->getNameSection()."/".$niveau->getNiveau()); ?>"   class="icon" title="">
					<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/edit.png"'); ?> alt="Modifier" title="Modifier"/></div></a></div>
				
					<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous certain(e) de vouloir effectuer cette action ?',function(){supprimer(<?php echo("'".Parser::getChemin()."'".","."'".$niveau->getNameSection()."'".","."'".$niveau->getNiveau()."'"); ?>)})" href="#"   class="icon" title="">
					<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/delete.png"'); ?> alt="Supprimer" title="Supprimer"/></div></a></div>
				</td>
				<?php echo("</tr>");
			}
		?>
		</tbody>
</table>
</div>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/niveau.js"?>></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/jquery.js'.'"'); ?> ></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/dynatable.jquery.js'.'"'); ?> ></script>
	<script type="text/javascript">
	$('#sorts').dynatable();
	</script>
</div>