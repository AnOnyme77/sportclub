<?php $this->titre="Les abonnements" ?>
<?php 
if(isset($fonction) && $fonction == "A"){ ?>
	<h1>Les abonnements</h1>
	<div class="ssTitre">
			<div class="boutonDiv" ><a href="Abonnement/ajouterAbonnement" class="icon" title="Ajouter un abonnement">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/add.png"'); ?> alt="Ajouter un abonnement" title="Ajouter un abonnement"/></div></a></div>		
	</div><?php
}
else{?>
	<h1>Vos abonnements</h1>
	<?php
	$this->titre="Vos abonnements";
}?>
<div>
	
	<div id="message">
		<p> <?php  if(isset($message)){ echo ($message); }?> </p>
	</div>
</div>
<div>
	<table id="sorts">
		<thead>
			<tr>
				<?php 
				if(isset($fonction))
					echo '<th>Propriétaire</th>';
				?>
				<th>
					Date d'achat
				</th>
				<th>
					Type d'abonnement
				</th>
				<th>
					Unités restantes
				</th>
				<th>
					État
				</th>
				<?php
				if(isset($fonction))
					echo '<th></th>';
				//Pour les boutons si l'utilisateur est un admin
				?>
			</tr>
		</thead>

		<tbody>
			<?php
			if(isset($listeAbo)){
				for($i=0; $i<sizeof($listeAbo); $i++){
					$abo = $listeAbo[$i];
					?>
					<tr>
						<?php 
						if(isset($fonction) && $fonction  == "A"){ ?>
							<td id="idMembre">
								<a href=<?= '"Membre/afficherMembre/'.$abo->getIdMembre().'"' ?> >
									<?php 
										if(isset($listeMembre)){
											echo $listeMembre[$i]->getNom()." ".$listeMembre[$i]->getPrenom();
										}
									?>
								</a>
							</td><?php
						}?>
						<td id="dateAchat"><?= $abo->getDateFormatPresFrench($abo->getDateAchat()) ?></td>
						<td id="libelle"><a href=<?= '"TypeAbonnement/modiferTypeAbonnement/'.$abo->getLibelle().'"' ?> ><?= $abo->getLibelle() ?></a</td>
						<td id="nbUniteRest">
						<?php
							if($abo->getNbUniteRest() == "-1"){
								echo "<p class='lightGrey' >∞</p>";
							}
							else{
								if($abo->getNbUniteRest() > "1")
									echo $abo->getNbUniteRest()." unités";
								else
									echo $abo->getNbUniteRest()." unité";
							}
						?>
						</td>
						<td>
							<?php
								if($abo->getDateFin() != null){
									$date = $abo->getDateFormatPres($abo->getDateFin());
									//							MOIS , JOUR , ANNEE
									$input_time = mktime(0,0,0,substr($date, 5, 2),substr($date, 8, 2),substr($date, 0, 4)); 
									if ($input_time <= time() || $abo->getNbUniteRest() == 0)
									   echo '<p class="invalid" >Expiré !</p>';
									else
										echo '<p class="valid" >Valide</p>';
								}
								else{
									if($abo->getNbUniteRest() == 0)
										echo '<p class="invalid" >Expiré !</p>';
									else
										echo '<p class="valid" >Valide</p>';
								}
							?>
						</td>
						<?php
						if(isset($fonction) && $fonction == "A"){?>
							<td>
								<div class="boutonDiv" ><a href="<?= Parser::getChemin()?>/Abonnement/modifierAbonnement/<?=$abo->getIdMembre()?>/<?=$abo->getDateFormatPres($abo->getDateAchat())?>/<?=$abo->getLibelle()?>" class="icon" title="">
								<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/edit.png"'); ?> alt="Modifier l'abonnement" title="Modifier l'abonnement"/></div></a></div>
								<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous sur de vouloir supprimer cet abonnement ?',
															function(){supprimer('<?= $abo->getIdMembre()?>','<?= $abo->getLibelle()?>')})" href="#" class="icon" title="">
								<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/delete.png"'); ?> alt="Supprimer l'abonnement" title="Supprimer l'abonnement"/></div></a></div>
							</td><?php
						}
						?>
					</tr>
				<?php }} 
			?>
		</tbody>
	</table>
</div>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/abonnement.js"?>></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/dynatable.jquery.js'.'"'); ?> ></script>
	<script type="text/javascript">
	$('#sorts').dynatable();
	</script>
</div>
