<?php $this->titre="Les séances" ?>
<h1>Les séances</h1>
<div class="ssTitre">
		<div class="boutonDiv" ><a href="<?= Parser::getChemin()."/seance/ajouterSeance"?>" class="icon" title="Ajouter une séance">
		<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/add.png"'); ?> alt="Ajouter une séance" title="Ajouter une séance"/></div></a></div>	
</div>	

<div id="message">
	<?php
		if(isset($message)){
			echo($message);
		}
	?>
</div>
<div>
	<table>
		<form>
			<tr>
				<td>Cours</td>
				<td><input type="text"/></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><?php include(Parser::getCheminRacine()."/outils/formulaireDate.php");?></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><?php include(Parser::getCheminRacine()."/outils/formulaireHeure.php");?></td>
			</tr>
		</form>
	</table>
</div>
<div>
	<table id="sorts">
		<thead>
			<tr>
				<th>
					<input type="checkbox" id="all" onclick="selectAll()"/>
				</th>
				<th>
					Séance n°
				</th>
				<th>
					Cours
				</th>
				<th>
					Date et heure
				</th>
				<th>
					Nombre maximum <br/>de participants
				</th>
				<th>
					Professeur
				</th>
				<th class="centre">
					<div class="boutonDivOther" ><a onclick="exWithConfirm('Êtes vous sur ce vouloir effectuer cette action ?',function(){supprimerSeances('<?= Parser::getChemin()?>')})" class="icon" title="Supprimer les séances">
					<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/deletes.png"'); ?> alt="Supprimer les séances" title="Supprimer les séances"/></div></a></div>	
	
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if(isset($liste) and count($liste)>0){
					$i=0;
					foreach($liste as $seance){
						echo("<tr>");
						echo("<td><input type='checkbox' name='suppression' value='".$seance->getNoSeance()."#".$seance->getIdProf()."' /></td>");
						echo("<td><a href='".Parser::getChemin()."/seance/modifierSeance/".$seance->getNoSeance()."' title='Modifier cette séance'>".$seance->getNoSeance()."</a></td>");
						echo("<td>".$seance->getCours()."</td>");
						echo("<td>".$seance->getDateCours()."</td>");
						echo("<td>".$seance->getNbMembreMax()."</td>");
						if(isset($listeProfs) and count($listeProfs)==count($liste)){
							echo("<td><a href='".Parser::getChemin()."/Membre/afficherMembre/".$seance->getIdProf()."' title='Visiter le profil du membre'>".$listeProfs[$i]->getNom()." ".$listeProfs[$i]->getPrenom()."</a></td>");
						}
						else{
							echo("<td><a href='".Parser::getChemin()."/Membre/afficherMembre/".$seance->getIdProf()."' title='Visiter le profil du membre'>".$seance->getIdProf()."</a></td>");
						}?>
						<td class='centre'>
						<div class="boutonDiv" ><a href="<?= Parser::getChemin()."/seance/modifierSeance/".$seance->getNoSeance()?>" class="icon" title="">
							<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/edit.png"'); ?> alt="Modifier" title="Modifier"/></div></a></div>	
						
							<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous sur ce vouloir effectuer cette action ?',function(){supprimer(<?= "'".Parser::getChemin()."'".","."'".$seance->getNoSeance()."'".","."'".$seance->getIdProf()."'"?>)})" class="icon" title="">
							<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/delete.png"'); ?> alt="Supprimer" title="Supprimer"/></div></a></div>	
						</td>
						<?php
						echo("</tr>");
						$i++;
					}
				}
			?>
		</tbody>
	</table>
</div>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/seance.js"?>></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/jquery.js'.'"'); ?> ></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/dynatable.jquery.js'.'"'); ?> ></script>
	<script type="text/javascript">
	$('#sorts').dynatable();
	</script>
</div>