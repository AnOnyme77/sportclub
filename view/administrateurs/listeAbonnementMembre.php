<div id="carteMembre">
	<?php $carteMembre = new ControllerMembre();
		  $carteMembre->carteMembre($idMembre, false); ?>
</div>
<div class="corpListeAbonnements">
<div id="message">
<?php if(isset($message)){ echo ($message); }?>
</div>

<table id="sorts">
	<thead>
		<tr>
			<th class="taille2">Cours</th>
			<th class="taille2">Libelle</th>
			<th class="taille2">Date de fin</th>
			<th class="taille2">Unité</th>
			<th class="taille1" >
				<div class="boutonDiv" ><a href="<?= Parser::getChemin()?>/Abonnement/ajouterAbonnement" class="icon" title="">
				<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/add.png"'); ?> alt="Ajouter un abonnement" title="Ajouter un abonnement"/></div></a></div>
			</th>
		</tr>
	</thead>
<?php
if(isset($listeAbonnements)){ 
	$i=0;
	
	echo("<tbody>");
	while ( $i<count($listeAbonnements) ) {
	?>
	<tr>
		<td><?= ($listeTypeAbonnements[$i]->getCours() != null)?$listeTypeAbonnements[$i]->getCours():"<p class='lightGrey' >Aucun cours associé</p>" ?></td>
		<td><?= $listeAbonnements[$i]->getLibelle() ?></td>
		<td><?php if($listeAbonnements[$i]->getDateFin() != null) {
					echo($listeAbonnements[$i]->getDateFin()->format(Membre::$FORMATDATEPRES));
					} else { echo("<p class='lightGrey' >∞</p>"); } ?></td>
		<td><?php 
			$unitRest=$listeAbonnements[$i]->getNbUniteRest();
			$unitRest=($unitRest>0)?$unitRest:"0 ";
			if($listeTypeAbonnements[$i]->getNbUnite()>=0){
			echo("<span id='nbUnitRest_".($i+1)."'>".$unitRest."</span> /".$listeTypeAbonnements[$i]->getNbUnite()); ?>
			<div class="boutonDiv" ><a onclick="decrement('<?= Parser::getChemin()?>','<?=$idMembre?>',
						'<?=$listeAbonnements[$i]->getDateAchat()?>','<?=$listeAbonnements[$i]->getLibelle()?>','<?= ($i+1) ?>')" class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/less.png"'); ?> alt="Compter une entrée" title="Compter une entrée"/></div></a></div>		
		<?php }
		else{
			echo("<p class='lightGrey' >∞</p>");
		}
			 ?></td>
		<td class="taille1">
			<div class="boutonDiv" ><a href="<?= Parser::getChemin()?>/Abonnement/modifierAbonnement/<?=$idMembre?>/<?=$listeAbonnements[$i]->getDateAchat()?>/<?=$listeAbonnements[$i]->getLibelle()?>" class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/abonnement.png"'); ?> alt="Abonnement" title="Abonnement"/></div></a></div>
		</td>
   	</tr>
   	<?php
   	$i++;
	}
	if(count($listeAbonnements)==0){?>
	<tr>
		<td><p class='lightGrey' >× × ×</p></td>
		<td><p class='lightGrey' >× × ×</p></td>
		<td><p class='lightGrey' >× × ×</p></td>
		<td><p class='lightGrey' >× × ×</p></td>
		<td class="taille1">
		</td>
   	</tr>
	<?php
	}
	echo("</tbody>");
}
?>
</table>
</div>
