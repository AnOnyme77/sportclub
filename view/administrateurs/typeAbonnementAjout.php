<?php $this->titre="detail type abonnement " ?>

<?php
if($ajout)
	echo "<h1>Ajout d'un type d'abonnement</h1>";
else
	echo "<h1>Détails sur le type d'abonnement</h1>"; 
?>

<div>
	<div id="message">
		<p><?php if(isset($message)){  echo($message);  } ?></p>
	</div>
	<table class="veStart">
		<tbody>
		<tr>
			<td>Libellé:</td>
			<td><input  id="libelle" type="text" name="libelle" placeholder="nom pour le type d'abonnement"  
			<?php if(!$ajout)echo("readonly='readonly'");
				else{ ?> class="validate[required]" <?php }
					if(isset($typeAbonnement))echo("value='".$typeAbonnement->getLibelle()."'");?>/></td>
		</tr>
		<tr>

			<td>Prix:</td>
			<td><input id="prix" type="number" name="prix" size="1" class="validate[required,custom[integer]]" 
				<?php if(isset($typeAbonnement))echo("value='".$typeAbonnement->getPrix()."'");?>/>&#8364;</td>
		</tr>
		<tr>
			<td>Durée:</td>
			<td><input id="duree" type="number" name="duree" size="2" class="validate[custom[integer],min[0]]" 
				<?php if(isset($typeAbonnement)){
					if($typeAbonnement->getDuree() >= 0)
						echo("value='".$typeAbonnement->getDuree()."'");
					else
						echo("value=''");
				}
				?> />jours</td>
		</tr>
		</tr>
		<tr>
			<td>Nombre d'unité:</td>
			<td><input id="nbUnite" type="number" name="nbUnite" size="1" 
				<?php 
				if(isset($typeAbonnement)){
					if($typeAbonnement->getNbUnite() == "-1")
						echo("value=''");
					else
						echo("value='".$typeAbonnement->getNbUnite()."'");
				}
				?> />
			</td>
		</tr>
		</tr>
			<td>Cours:</td>
			<td id="tdCours">
				<input type="text" name="cours"  id="cours" class="validate[maxSize[50]] text-input" 
				<?php if(isset($typeAbonnement))echo("value='".$typeAbonnement->getCours()."'");?>/>
				<?php
					if($ajout && isset($listeCours)){
						echo("<select id='selectCours' onchange='titreCours()'>");
							echo("<option value=''>Choix du cours</option>");
							foreach($listeCours as $key=>$elem){
								echo("<option value='".$key."'>".$elem."</option>");
							}
						echo("</select>");
					};
				?>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="centre">
			<div class="boutonDiv" ><a <?php if($ajout) echo("onclick='ajouterTypeAbo(\"".Parser::getChemin()."\")'");
						 else echo("onclick='modifierTypeAbo(\"".Parser::getChemin()."\")'");?> href="#"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/outils/JS/formulaire.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/typeAbonnement.js"?>></script>
</div>