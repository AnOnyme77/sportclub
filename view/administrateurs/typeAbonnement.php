<?php 
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 05/05/2014 - modifée le 29/05/2014
 -----------------------------------------------------------------------------------------------------*/

$this->titre="Les types d'abonnements" ?>
<?php 
if(isset($fonction) && $fonction == "A"){ ?>
	<h1>Les types d'abonnements</h1>
	<div class="ssTitre">
			<div class="boutonDiv" ><a href="<?= Parser::getChemin()."/TypeAbonnement/ajouterTypeAbonnement"?>" class="icon" title="Ajouter un type d'abonnement">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/add.png"'); ?> alt="Ajouter un type d'abonnement" title="Ajouter un type d'abonnement"/></div></a></div>	
	</div>	<?php
}
else{?>
	<h1>Les types d'abonnements</h1>
	<?php
}?>
<div>
	<?php if(isset($message)){ ?>
	<div id="message">
		<p> <?php echo ($message); }?> </p>
	</div>
	<table id="sorts">
		<thead>
			<tr>
				<th>Libellé</th>
				<th>Cours</th>
				<th>Durée</th>
				<th>Unités</th>
				<th>Prix</th>
				<?php
				if(isset($fonction) && $fonction == "A"){?>
					<th></th><?php
				}
				?>
			</tr>
		</thead>
		<tbody>
		<?php
		if(isset($liste)){
			foreach($liste as $typeAbo){ ?>
				<tr>
					<?php
					if(isset($fonction) && $fonction == "A"){ ?>
						<td><a href=<?= '"TypeAbonnement/modiferTypeAbonnement/'.$typeAbo->getLibelle().'"' ?> ><?= $typeAbo->getLibelle() ?></a></td><?php
					}
					else{?>
						<td><?= $typeAbo->getLibelle() ?></a></td><?php
					}?>
					<td><?php if($typeAbo->getCours() == "") echo "<p class='lightGrey' >Aucun cours associé</p>";
							  else echo $typeAbo->getCours(); ?></td>
					<td>
						<?php
						if($typeAbo->getDuree() == -1) echo "<p class='lightGrey' >∞</p>";
						else{
							if($typeAbo->getDuree() == 1)echo $typeAbo->getDuree().' jour';
							else echo $typeAbo->getDuree().' jours';
						}
						?>
					</td>
					<td>
						<?php
						if($typeAbo->getNbUnite() == -1) echo "<p class='lightGrey' >∞</p>";
						else{
							if($typeAbo->getNbUnite() == 1)echo $typeAbo->getNbUnite().' unité';
							else echo $typeAbo->getNbUnite().' unités';
						}
						?>
					</td>
					<td><?= $typeAbo->getPrix() ?>&#8364;</td>
					<?php
					if(isset($fonction) && $fonction == "A"){
						if($typeAbo->getSupprime() == "F"){?>
							<td>
								<div class="boutonDiv" ><a href="<?= Parser::getChemin()."/TypeAbonnement/modiferTypeAbonnement/".$typeAbo->getLibelle()?>" class="icon" title="">
								<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/edit.png"'); ?> alt="Modifier" title="Modifier"/></div></a></div>	
								
								<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous certain de vouloir abroger ce type d\'abonnement ?',function(){supprimerTypeAbo('<?= Parser::getChemin()?>','<?= $typeAbo->getLibelle() ?>')})" href="#" class="icon" title="">
								<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/croix.png"'); ?> alt="Abroger" title="Abroger"/></div></a></div>
								
								<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous certain de vouloir supprimer définitivement ce type d\'abonnement ?\nCette action est irréversible.',function(){supprimerPhysTypeAbo('<?= Parser::getChemin()?>','<?= $typeAbo->getLibelle() ?>')})" href="#" class="icon" title="">
								<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/delete.png"'); ?> alt="Supprimer définitivement" title="Supprimer définitivement"/></div></a></div>	
							</td>
						<?php
						}
						else{
							?>
							<td>
								<div class="boutonDiv" ><a onclick="renouvelerTypeAbonnement('<?= Parser::getChemin()?>','<?= $typeAbo->getLibelle() ?>')" href="#" class="icon" title="">
								<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/reload.png"'); ?> alt="Renouveler" title="Renouveler"/></div></a></div>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous certain de vouloir supprimer définitivement ce type d\'abonnement ?\nCette action est irréversible.',function(){supprimerPhysTypeAbo('<?= Parser::getChemin()?>','<?= $typeAbo->getLibelle() ?>')})" href="#" class="icon" title="">
								<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/delete.png"'); ?> alt="Supprimer définitivement" title="Supprimer définitivement"/></div></a></div>
							</td>
							<?php
						}
					}
					?>
					</tr>
			<?php }} ?>
		</tbody>
</table>
</div>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/typeAbonnement.js"?>></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/dynatable.jquery.js'.'"'); ?> ></script>
	<script type="text/javascript">
	$('#sorts').dynatable();
	</script>
</div>
