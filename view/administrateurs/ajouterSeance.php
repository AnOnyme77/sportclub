<?php if($ajout) $this->titre="Ajouter une séance"; else $this->titre="Modifier une séance"; ?>
<h1><?php if($ajout) echo("Ajouter"); else echo("Modifier");?> une séance</h1>
<div id="message">
	
</div>
<table>
	<tbody id="tableSeance" class="veStart">
	<?php
	if(!$ajout){
	?>
	<tr>
		<td>Numéro de la séance</td>
		<td><input type="number" class="validate[required,custom[integer],min[1]] text-input" id="number" <?php if(!$ajout)echo("readonly='readonly'");?> <?php if(isset($seance))echo("value='".$seance->getNoSeance()."'");?>name="numSeance" /></td>
	</tr>
	<?php
	}
	?>
	<tr>
		<td>Cours:</td>
		<td id="tdCours">
			<input type="text" name="cours"  class="validate[required,maxSize[50]] text-input" id="cours" <?php if(isset($seance))echo("value='".$seance->getCours()."'");?>/>
			<?php
				if(isset($listeCours)){
					echo("<select id='selectCours' onchange='titreCours()'>");
						echo("<option value=''>Choix du cours</option>");
						foreach($listeCours as $key=>$elem){
							echo("<option value='".$key."'>".$elem."</option>");
						}
					echo("</select>");
				};
			?>
		</td>
	</tr>
	<?php
	if($ajout){
	?>
	<tr>
		<td>Recursivité</td>
		<td>
			<input type= "radio" name="recursivite" value="T" onchange="recursion(1,'<?=Parser::getChemin()?>')"> Une fois
            <input type= "radio" name="recursivite" value="F" onchange="recursion(2,'<?=Parser::getChemin()?>')"> Plusieurs fois
		</td>
	</tr>
	<?php
	}
	else{
		?>
		<tr>
			<td>Date du cours:</td>
			<td><?php include(Parser::getCheminRacine()."/outils/formulaireDate.php");?></td>
		</tr>
		<tr>
			<td>Heure du cours:</td>
			<td><?php include(Parser::getCheminRacine()."/outils/formulaireHeure.php");?></td>
		</tr>
		<?php
	}
	?>
	<tr id="afterDate">
		<td>Nombre max de participant:</td>
		<td><input type="num" name="participants" id="participants"  class="validate[required,custom[integer]] text-input" <?php if(isset($seance))echo("value='".$seance->getNbMembreMax()."'");?>/></td> <!-- valeur par default -->
	</tr>
	<tr>
		<td>Code du professeur:</td>
		<td>
			<input type="text" name="idProf"  class="validate[required] text-input" id="idProf" <?php if(isset($seance))echo("value='".$seance->getIdProf()."'");?>/> 
			<div class="boutonDiv" ><a onclick="rechercherProf('<?=Parser::getChemin()?>','idProf')" class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/search.png"'); ?> alt="Rechercher prof" title="Rechercher prof"/></div></a></div>	
		</td>
	</tr>
	<?php
		if(!$ajout){
	?>
	<tr>
		<td>Nom du professeur:</td>
		<td><input type="text" name="nomProf"  readonly = "readonly"  <?php if(isset($prof) and $prof!=false)echo("value='".$prof->getNom()."'");?> /> </td>
	</tr>
	<tr>
		<td>Prénom du professeur:</td>
		<td><input type="text" name="prenomProf" readonly = "readonly"  <?php if(isset($prof) and $prof!=false)echo("value='".$prof->getPrenom()."'");?> /> </td>
	</tr>
	<?php
		}
	?>
	<tr>
		<td colspan="2" class="centre">
		<div class="boutonDiv" ><a id="boutonAjout" <?php if($ajout) echo("onclick='ajouterSeance(\"".Parser::getChemin()."\",0)'");
			  else echo("onclick='modifier(\"".Parser::getChemin()."\")'");?> href="#"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
		</td>
	</tr>
	</tbody>
</table>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/outils/JS/formulaire.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/seance.js"?>></script>
	<script src=<?= Parser::getChemin()."/outils/JS/rechercheMembre.js"?>></script>
</div>