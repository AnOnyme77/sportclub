/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
function supprimer(chemin,numero,prof) {
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
        lireDonnees(xhr.responseText);
    }}; 
    
    var requete=chemin+"/seance/suppression/"+numero+"/"+prof;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}
function titreCours() {
    var valeur=document.getElementById("selectCours").value;
    var cours=document.getElementById("cours");
    var tdCours=document.getElementById("tdCours");
    
    cours.value=valeur;
    
    var liste=document.getElementsByName("prevention");
    while(liste.length>0){
        liste[0].parentNode.removeChild(liste[0]);
    }
    
    var prevenu = document.createElement("p");
    prevenu.setAttribute("name","prevention");
    var textePrevenu = document.createTextNode("Ne changez la valeur du champs texte que si ");
    prevenu.appendChild(textePrevenu);
    tdCours.appendChild(prevenu);
    
    var prevenu = document.createElement("p");
    prevenu.setAttribute("name","prevention");
    var textePrevenu = document.createTextNode("vous voulez créer un nouveau type de cours");
    prevenu.appendChild(textePrevenu);
    tdCours.appendChild(prevenu);
}
function recursion(type,chemin){
    var table=document.getElementById("tableSeance");
    var afterElem=document.getElementById("afterDate");
    
    var aSupprimer=document.getElementsByName("dyn");
    for(;aSupprimer.length>0;){
        table.removeChild(aSupprimer[0]);
    }
    
    switch(type){
        case 1:
            //Date
            var enteteDate = document.createElement("tr");
            enteteDate.setAttribute("name","dyn");
            var tdDate = document.createElement("td");
            var titreDate = document.createTextNode("Date");
            
            tdDate.appendChild(titreDate);
            enteteDate.appendChild(tdDate);
            table.insertBefore(enteteDate,afterElem);
            
            var date = document.createElement("td");
            date.appendChild(formDate(""));
            enteteDate.appendChild(date);
            //Heure
            
            var enteteHeure = document.createElement("tr");
            enteteHeure.setAttribute("name","dyn");
            var tdHeure = document.createElement("td");
            var titreHeure = document.createTextNode("Heure");
            
            tdHeure.appendChild(titreHeure);
            enteteHeure.appendChild(tdHeure);
            table.insertBefore(enteteHeure,afterElem);
            
            var heure = document.createElement("td");
            heure.appendChild(formHeure(""));
            enteteHeure.appendChild(heure);
            break;
        
        case 2:
            //Date Début
            var enteteDate = document.createElement("tr");
            enteteDate.setAttribute("name","dyn");
            var tdDate = document.createElement("td");
            var titreDate = document.createTextNode("Date de début");
            
            tdDate.appendChild(titreDate);
            enteteDate.appendChild(tdDate);
            table.insertBefore(enteteDate,afterElem);
            
            var date = document.createElement("td");
            date.appendChild(formDate("debut"));
            enteteDate.appendChild(date);
            
            //Date Fin
            var enteteDate = document.createElement("tr");
            enteteDate.setAttribute("name","dyn");
            var tdDate = document.createElement("td");
            var titreDate = document.createTextNode("Date de fin");
            
            tdDate.appendChild(titreDate);
            enteteDate.appendChild(tdDate);
            table.insertBefore(enteteDate,afterElem);
            
            var date = document.createElement("td");
            date.appendChild(formDate("fin"));
            enteteDate.appendChild(date);
            
            //Heure
            
            var enteteHeure = document.createElement("tr");
            enteteHeure.setAttribute("name","dyn");
            var tdHeure = document.createElement("td");
            var titreHeure = document.createTextNode("Heure");
            
            tdHeure.appendChild(titreHeure);
            enteteHeure.appendChild(tdHeure);
            table.insertBefore(enteteHeure,afterElem);
            
            var heure = document.createElement("td");
            heure.appendChild(formHeure(""));
            enteteHeure.appendChild(heure);
            
            //Ecart
            var enteteDate = document.createElement("tr");
            enteteDate.setAttribute("name","dyn");
            var tdDate = document.createElement("td");
            var titreDate = document.createTextNode("Ecart");
            
            tdDate.appendChild(titreDate);
            enteteDate.appendChild(tdDate);
            table.insertBefore(enteteDate,afterElem);
            
            var date = document.createElement("td");
            var valeur = document.createElement("div");
            var texteJour=document.createTextNode(" jour(s)");
            var ecart= document.createElement("input");
            ecart.setAttribute("type","number");
            ecart.setAttribute("id","ecart");
            valeur.appendChild(ecart);
            valeur.appendChild(texteJour);
            date.appendChild(valeur);
            enteteDate.appendChild(date);
            break;
    }
    var bouton = document.getElementById("boutonAjout");
    bouton.setAttribute("onclick","ajouterSeance('"+chemin+"',"+type+")")
}
function ajouterSeance(chemin,type){
    var xhr=getXMLHttpRequest();
    xhr.onreadystatechange = function() {     
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))    
    {
        lireDonnees(xhr.responseText);
    }};
    xhr.onprogress=function(){
        lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    }
    
    switch(type){
        case 0:
            alert("Veuillez choisir entre un ajout simple ou multiple");
            break;
        case 1:
            var cours = encodeURIComponent(document.getElementById("cours").value);
            
            var jour = document.getElementById("jour").value;
            var mois = document.getElementById("mois").value;
            var annee = document.getElementById("annee").value;
            var heure = document.getElementById("heure").value;
            var minute = document.getElementById("minute").value;
            
            var dateSeance=encodeURIComponent(annee+"-"+mois+"-"+jour+" "+heure+":"+minute+":00");
            
            var participants = encodeURIComponent(document.getElementById("participants").value);
            var idProf =encodeURIComponent(document.getElementById("idProf").value);      
            
            var requete=chemin+"/seance/ajoutSeance/"+cours+"/"+dateSeance+"/"+participants+"/"+idProf;

            xhr.open("GET",requete,true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");         
            xhr.send(null);
            break;
        case 2:
            var cours = encodeURIComponent(document.getElementById("cours").value);
            
            var jourDebut = document.getElementById("jourdebut").value;
            var moisDebut = document.getElementById("moisdebut").value;
            var anneeDebut = document.getElementById("anneedebut").value;
            
            var jourFin = document.getElementById("jourfin").value;
            var moisFin = document.getElementById("moisfin").value;
            var anneeFin = document.getElementById("anneefin").value;
            
            var heure = document.getElementById("heure").value;
            var minute = document.getElementById("minute").value;
            
            var dateSeanceDebut=encodeURIComponent(anneeDebut+"-"+moisDebut+"-"+jourDebut+" "+heure+":"+minute+":00");
            var dateSeanceFin=encodeURIComponent(anneeFin+"-"+moisFin+"-"+jourFin+" "+heure+":"+minute+":00");
            
            var ecart = document.getElementById("ecart").value;
            
            var participants = encodeURIComponent(document.getElementById("participants").value);
            var idProf =encodeURIComponent(document.getElementById("idProf").value);
            
            var requete=chemin+"/seance/ajoutRecursif/"+cours+"/"+dateSeanceDebut+"/"+dateSeanceFin+"/"+ecart+"/"+participants+"/"+idProf;
            
            lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
            xhr.open("GET",requete,true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");             
            xhr.send(null);
            
            break;
    }
}
function modifier(chemin) {
    var xhr=getXMLHttpRequest();
    
    var numero = encodeURIComponent(document.getElementById("number").value);
    var cours = encodeURIComponent(document.getElementById("cours").value);
    
    var jour = document.getElementById("jour").value;
    var mois = document.getElementById("mois").value;
    var annee = document.getElementById("annee").value;
    var heure = document.getElementById("heure").value;
    var minute = document.getElementById("minute").value;
    
    var dateSeance=encodeURIComponent(annee+"-"+mois+"-"+jour+" "+heure+":"+minute+":00");
    
    var participants = encodeURIComponent(document.getElementById("participants").value);
    var idProf =encodeURIComponent(document.getElementById("idProf").value);
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
        lireDonnees(xhr.responseText);
    }}; 
    
    var requete=chemin+"/seance/modification/"+numero+"/"+cours+"/"+dateSeance+"/"+participants+"/"+idProf;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}
function supprimerSeances(chemin){
    var elements = document.getElementsByName("suppression");
    var i=0;
    for(i=0;i<elements.length;i++){
        if(elements[i].checked){
            var valeurs = elements[i].value.split("#");
            supprimer(chemin,valeurs[0],valeurs[1]);
        }
    }
}
function lireDonnees(text){
    var resultat=document.getElementById("message");
    resultat.innerHTML=text;
    window.setTimeout(function(){
        var resultat=document.getElementById("message");
        resultat.innerHTML="";},5000);
}
function selectAll(){
    var all = document.getElementById("all");
    var elements = document.getElementsByName("suppression");
    var i=0;
    for(i=0;i<elements.length;i++){
        if (all.checked) {
            elements[i].setAttribute("checked","checked");
        }
        else{
            elements[i].removeAttribute("checked");   
        }
    }
}
