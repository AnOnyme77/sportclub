/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
i=1;
function supprimer(chemin,section,niveau){
    
    //var asupprimer=window.document.getElementById(""+niveau+section);
    //alert(asupprimer);
    //asupprimer.parentNode.removeChild(asupprimer);
    
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
        lireDonnees(xhr.responseText);
    }}; 
    
    var requete=chemin+"/niveau/suppression/"+section+"/"+niveau;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}
function update(chemin) {
    var tab=document.getElementById("liste").value.split("|#|");
    
    var niveau=tab[0];
    var section=tab[1];
    
    document.location.href=(chemin+"/niveau/detailNiveau/"+niveau+"/"+section); 
}
function ajout(chemin){
    var section=encodeURIComponent(document.getElementById("section").value);
    var niveau=encodeURIComponent(document.getElementById("niveau").value);
    var libelle=encodeURIComponent(document.getElementById("libelle").value);
    
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    lireDonnees(xhr.responseText);       }}; 
    
    var requete=chemin+"/niveau/ajoutNiveau/"+section+"/"+niveau+"/"+libelle+"/logo";
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}

function modifier(chemin){
    var section=encodeURIComponent(document.getElementById("section").value);
    var niveau=encodeURIComponent(document.getElementById("niveau").value);
    var libelle=encodeURIComponent(document.getElementById("libelle").value);
    
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    lireDonnees(xhr.responseText);       }}; 
    
    var requete=chemin+"/niveau/modificationNiveau/"+section+"/"+niveau+"/"+libelle+"/logo";
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}
function ajouterCompetence(num,niveau,section,chemin){
    var detail=encodeURIComponent(document.getElementById("detail"+num).value);
    var titre=encodeURIComponent(document.getElementById("titre"+num).value);
    var titreNorm=document.getElementById("titre"+num).value;
    var detailNorm=document.getElementById("detail"+num).value;
    
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function(){
    if (xhr.readyState == 4 && (xhr.status==200||xhr.status==0)){
    lireDonnees(xhr.responseText);
    }};
    
    var requete=chemin+"/competence/ajouterCompetence/"+niveau+"/"+section+"/"+titre+"/"+detail;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
    var titreVerrouillage=document.getElementById("titre"+num);
    titreVerrouillage.setAttribute("readonly","readonly");
    
    var competence = document.getElementById("competence"+num);
    while(competence.hasChildNodes()){
        competence.removeChild(competence.lastChild);
    }
    
    var detail= document.createElement("textarea");
    detail.setAttribute("id",("detail"+titre.replace(" ","")));
    texteDetail=document.createTextNode(detailNorm);
    detail.appendChild(texteDetail);
    competence.appendChild(detail);
    
    
    var bsupprimer =  document.createElement("input");
    bsupprimer.setAttribute("type","submit");
    bsupprimer.setAttribute("value","Supprimer");
    bsupprimer.setAttribute("onclick","exWithConfirm('Êtes vous certain de vouloir effectuer cette action ?',function(){supprimerCompetence('"+niveau+"','"+section+"','"+titreNorm+"','"+chemin+"',"+num+")})");
    competence.appendChild(bsupprimer);
    
    var bmodifier =  document.createElement("input");
    bmodifier.setAttribute("type","submit");
    bmodifier.setAttribute("value","Modifier");
    bmodifier.setAttribute("onclick","modifierCompetence('"+niveau+"','"+section+"','"+titreNorm+"','"+chemin+"')");
    competence.appendChild(bmodifier);
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}
function ajoutChamps(chemin,nom){
    var niveau=document.getElementById("niveau").value;
    var section=document.getElementById("section").value;
    if(niveau!="" && section!=""){
        var corpsTableau=document.getElementById("corpsTableau");
        var boutons= document.getElementById("boutons");
        var tr=document.createElement("tr");
        tr.setAttribute("id",i);
        
        var cell1 = document.createElement("td");
        var titre = document.createElement("input");
        titre.setAttribute("type","text");
        titre.setAttribute("name","titre");
        titre.setAttribute("id","titre"+i);
        cell1.appendChild(titre);
        
        var cell2 = document.createElement("td");
        cell2.setAttribute("id","competence"+i);
        var detail= document.createElement("textarea");
        detail.setAttribute("id","detail"+i);
        cell2.appendChild(detail);
        var benreg =  document.createElement("input");
        benreg.setAttribute("type","submit");
        benreg.setAttribute("value","Enregistrer "+nom);
        benreg.setAttribute("onclick","ajouterCompetence("+i+",'"+niveau+"','"+section+"','"+chemin+"')");
        cell2.appendChild(benreg);
        
        tr.appendChild(cell1);
        tr.appendChild(cell2);
        
        i++;
        
        corpsTableau.insertBefore(tr,boutons);
    }
    else{
        var message = document.getElementById("message");
        message.innerHTML="Le niveau et la section doivent être complété pour pouvoir ajouter des compétences";
    }
}
function modifierCompetence(niveau,section,titre,chemin) {
    var detail=encodeURIComponent(document.getElementById("detail"+titre.replace(" ","")).value);
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    lireDonnees(xhr.responseText);       }}; 
    
    var requete=chemin+"/competence/modifierCompetence/"+niveau+"/"+section+"/"+titre+"/"+detail;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}
function supprimerCompetence(niveau,section,titre,chemin,numero){
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    lireDonnees(xhr.responseText);       }}; 
    
    var requete=chemin+"/competence/supprimerCompetence/"+niveau+"/"+section+"/"+titre;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    
    
    xhr.send(null);
    
    var element=document.getElementById(numero);
    element.parentNode.removeChild(element);
}
function lireDonnees(text){
    var resultat=document.getElementById("message");
    resultat.innerHTML=text;
    window.setTimeout(function(){
        var resultat=document.getElementById("message");
        resultat.innerHTML="";},5000);
}