/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric & Hainaut Jérôme
 * Team : Dev4u
 * créé le 14/05/2014 - modifée le 14/05/2014
 -----------------------------------------------------------------------------------------------------*/

 function lireDonnees(text){
    var resultat=document.getElementById("message");
    resultat.innerHTML=text;
}
function decrement(chemin,idMembre,dateAchat,libelle, nb){
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))  
    {
        decrementMessage(xhr.responseText, nb);
    }};
    
    var requete=chemin+"/abonnement/decrementer/"+idMembre+"/"+dateAchat+"/"+libelle;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}
function decrementMessage(text, nb){
	var resultat=document.getElementById("message");

	var message = text.split(";");
    resultat.innerHTML=message[0];
    if(typeof( message[1] ) != "undefined" && message[1]!=""){
	var nbUnitRest=document.getElementById("nbUnitRest_"+nb);
	nbUnitRest.innerHTML=message[1];
    }
}
function ajoutListeAbonnement(text){
	var resultat= document.querySelector('#tableauAbonnement');
	resultat.innerHTML=text;
	$('#sorts').dynatable();
   	document.getElementById("idMembre").value = "";
}
function scanMembre(chemin){
    var xhr=getXMLHttpRequest();
    var idMembre = encodeURIComponent(document.getElementById("idMembre").value); 
    
    xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))  
    {
        ajoutListeAbonnement(xhr.responseText);
    }};
    
    var requete=chemin+"/abonnement/scanAbonnementMembre/"+idMembre;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(null);
}


function changeTypeAbo(chemin) {
    var typeAboCourrant=document.getElementById("selectTypeAbo").value;
    var idMembreCourrant=document.getElementById("idMembre").value;
    if(idMembreCourrant == "")
        document.location.href=(chemin+"/abonnement/ajouterAbonnement/"+typeAboCourrant);
    else
        document.location.href=(chemin+"/abonnement/ajouterAbonnement/"+typeAboCourrant+"/"+idMembreCourrant);
}

function modifierAbo(chemin,dateAchatCourrante)
{
    //Récupération des valeurs dans le formulaire pour le passage au controleur.
    var libelle=encodeURIComponent(document.getElementById("selectTypeAbo").value);
    var idMembre=encodeURIComponent(document.getElementById("idMembre").value);
    var dateAchat=encodeURIComponent(dateAchatCourrante);

    var dateDebutjour = document.getElementById("dateDebutjour").value;
    var dateDebutmois = document.getElementById("dateDebutmois").value;
    var dateDebutannee = document.getElementById("dateDebutannee").value;
    var dateDebut=encodeURIComponent(dateDebutjour+"-"+dateDebutmois+"-"+dateDebutannee);

    var prix=encodeURIComponent(document.getElementById("prix").value);
    var nbUniteRest=encodeURIComponent(document.getElementById("nbUniteRest").value);
    var duree=encodeURIComponent(document.getElementById("duree").value);

    if(nbUniteRest == "")
        nbUniteRest = encodeURIComponent("-1");
    if(duree == "")
        duree = encodeURIComponent("");

    //Requetes AJAX
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0)){ 
        lireDonnees(xhr.responseText);
    }}; 
    
                      // /controller/parametres
    var requete=chemin+"/Abonnement/modificationAbonnement/"+idMembre+"/"+dateAchat+"/"+dateDebut+"/"+libelle+"/"+prix+"/"+nbUniteRest+"/"+duree;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}

function ajouterAbo(chemin)
{
    //Récupération des valeurs dans le formulaire pour le passage au controleur.
    var libelle=encodeURIComponent(document.getElementById("selectTypeAbo").value);
    var idMembre=encodeURIComponent(document.getElementById("idMembre").value);

    var dateAchatjour = document.getElementById("dateAchatjour").value;
    var dateAchatmois = document.getElementById("dateAchatmois").value;
    var dateAchatannee = document.getElementById("dateAchatannee").value;
    var heure = document.getElementById("heure").value;
    var minute = document.getElementById("minute").value;
    var dateAchat=encodeURIComponent(dateAchatannee+"-"+dateAchatmois+"-"+dateAchatjour+" "+heure+":"+minute+":00");

    var dateDebutjour = document.getElementById("dateDebutjour").value;
    var dateDebutmois = document.getElementById("dateDebutmois").value;
    var dateDebutannee = document.getElementById("dateDebutannee").value;
    var dateDebut=encodeURIComponent(dateDebutjour+"-"+dateDebutmois+"-"+dateDebutannee);

    var prix=encodeURIComponent(document.getElementById("prix").value);
    var nbUniteRest=encodeURIComponent(document.getElementById("nbUniteRest").value);
    var duree=encodeURIComponent(document.getElementById("duree").value);

    if(nbUniteRest == "")
        nbUniteRest = encodeURIComponent("-1");
    if(duree == "")
        duree = encodeURIComponent("");


    //Requetes AJAX
    var xhr=getXMLHttpRequest();
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0)){ 
        lireDonnees(xhr.responseText);
    }}; 
    
                      // /controller/parametres
    var requete=chemin+"/Abonnement/ajoutAbonnement/"+idMembre+"/"+dateAchat+"/"+dateDebut+"/"+libelle+"/"+prix+"/"+nbUniteRest+"/"+duree;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}

function supprimer(idMembre,libelle) {
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
        lireDonnees(xhr.responseText);
        
        var ligne = document.getElementById(idMembre);
        ligne.innerHTML=""; 
        setTimeout(function() {
            location.reload() ; 
        }, 5000);
    }}; 
    
    var requete="Abonnement/suppressionAbonnement/"+idMembre+"/"+libelle;
    
    xhr.open("POST",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    xhr.send(null);

    return false;   
}
