/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 07/05/2014 - modifée le 29/05/2014
 -----------------------------------------------------------------------------------------------------*/



function lireDonnees(text){
    var resultat=document.getElementById("message");
    resultat.innerHTML=text;
}

function titreCours() {
    var valeur=document.getElementById("selectCours").value;
    var cours=document.getElementById("cours");
    var tdCours=document.getElementById("tdCours");
    
    cours.value=valeur;
    
    var liste=document.getElementsByName("prevention");
    while(liste.length>0){
        liste[0].parentNode.removeChild(liste[0]);
    }
    
    var prevenu = document.createElement("p");
    prevenu.setAttribute("name","prevention");
    var textePrevenu = document.createTextNode("Ne changez la valeur du champs texte que si ");
    prevenu.appendChild(textePrevenu);
    tdCours.appendChild(prevenu);
    
    var prevenu = document.createElement("p");
    prevenu.setAttribute("name","prevention");
    var textePrevenu = document.createTextNode("vous voulez créer un nouveau type de cours");
    prevenu.appendChild(textePrevenu);
    tdCours.appendChild(prevenu);
}

function supprimerTypeAbo(chemin, libelle)
{
    //Requetes AJAX
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0)){ 
		lireDonnees(xhr.responseText);
	}}; 
    
   					  // /controller/parametres
    var requete=chemin+"/typeAbonnement/suppressionTypeAbonnement/"+libelle;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}

function supprimerPhysTypeAbo(chemin, libelle)
{
    //Requetes AJAX
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0)){ 
        lireDonnees(xhr.responseText);
    }}; 
    
                      // /controller/parametres
    var requete=chemin+"/typeAbonnement/suppressionPhysTypeAbonnement/"+libelle;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}

function modifierTypeAbo(chemin)
{
    //Récupération des valeurs dans le formulaire pour le passage au controleur.
    var libelle=encodeURIComponent(document.getElementById("libelle").value);
    var prix=encodeURIComponent(document.getElementById("prix").value);
    var duree=encodeURIComponent(document.getElementById("duree").value);
    var nbUnite=encodeURIComponent(document.getElementById("nbUnite").value);
    var cours=encodeURIComponent(document.getElementById("cours").value);

    if(nbUnite == "")
        nbUnite = encodeURIComponent("-1");
    if(duree == "")
        duree = encodeURIComponent("-1");
    if(cours == "")
        cours = encodeURIComponent("");

    //Requetes AJAX
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0)){ 
        lireDonnees(xhr.responseText);
    }}; 
    
                      // /controller/parametres
    var requete=chemin+"/typeAbonnement/modificationTypeAbonnement/"+libelle+"/"+prix+"/"+duree+"/"+nbUnite+"/"+cours+"/F";
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}

function renouvelerTypeAbonnement(chemin,libelle)
{
    //Récupération des valeurs dans le formulaire pour le passage au controleur.
    libelle=encodeURIComponent(libelle);

    //Requetes AJAX
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0)){ 
        lireDonnees(xhr.responseText);
    }}; 
    
                      // /controller/parametres
    var requete=chemin+"/typeAbonnement/renouvellementTypeAbonnement/"+libelle;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}

function ajouterTypeAbo(chemin)
{
    //Récupération des valeurs dans le formulaire pour le passage au controleur.
    var libelle=encodeURIComponent(document.getElementById("libelle").value);
    var prix=encodeURIComponent(document.getElementById("prix").value);
    var duree=encodeURIComponent(document.getElementById("duree").value);
    var nbUnite=encodeURIComponent(document.getElementById("nbUnite").value);
    var cours=encodeURIComponent(document.getElementById("cours").value);

    if(nbUnite == "")
        nbUnite = encodeURIComponent("-1");
    if(duree == "")
        duree = encodeURIComponent("-1");
    if(cours == "")
        cours = encodeURIComponent("");
    //Requetes AJAX
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0)){ 
        lireDonnees(xhr.responseText);
    }}; 
    
                      // /controller/parametres
    var requete=chemin+"/typeAbonnement/ajoutTypeAbonnement/"+libelle+"/"+prix+"/"+duree+"/"+nbUnite+"/"+cours+"/F";
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}
