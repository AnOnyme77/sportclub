function ajouterMembre(chemin) {
    var xhr=getXMLHttpRequest();

    if(xhr && xhr.readyState!=0){
	alert("Requete en cour d'éxécution");
	return;
    }    

    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)){ 
        lireDonnees(xhr.responseText, chemin);       }}; 
    
    var inscriptionjour = encodeURIComponent(document.getElementsByName("inscriptionjour")[0].value);
    var inscriptionmois = encodeURIComponent(document.getElementsByName("inscriptionmois")[0].value);
    var inscriptionannee = encodeURIComponent(document.getElementsByName("inscriptionannee")[0].value);

    var nom = encodeURIComponent(document.getElementsByName("nom")[0].value);
    var prenom = encodeURIComponent(document.getElementsByName("prenom")[0].value);
    var email = encodeURIComponent(document.getElementsByName("email")[0].value);
    var MDP = encodeURIComponent(document.getElementsByName("MDP")[0].value);

    var naissancejour = encodeURIComponent(document.getElementsByName("naissancejour")[0].value);
    var naissancemois = encodeURIComponent(document.getElementsByName("naissancemois")[0].value);
    var naissanceannee = encodeURIComponent(document.getElementsByName("naissanceannee")[0].value);


    var rueNo = encodeURIComponent(document.getElementsByName("rueNo")[0].value);
    var ville = encodeURIComponent(document.getElementsByName("ville")[0].value);
    var codePostal = encodeURIComponent(document.getElementsByName("codePostal")[0].value);
    var pays = encodeURIComponent(document.getElementsByName("pays")[0].value);
    var telFix = encodeURIComponent(document.getElementsByName("telFix")[0].value);
    var telGsm = encodeURIComponent(document.getElementsByName("telGsm")[0].value);
    var fonction = encodeURIComponent(document.getElementsByName("fonction")[0].value);
    
    if(document.getElementsByName("sexe")[0].checked==true)
	var sexe = encodeURIComponent(document.getElementsByName("sexe")[0].value);
    else
	var sexe = encodeURIComponent(document.getElementsByName("sexe")[1].value);
    
    var requete=chemin+"/index.php?page=membre&action=ajouterMembre";
    
    xhr.open("POST",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send("inscriptionjour="+inscriptionjour+
             "&inscriptionmois="+inscriptionmois+
             "&inscriptionannee="+inscriptionannee+
             "&nom="+nom+
             "&prenom="+prenom+
             "&email="+email+
             "&MDP="+MDP+
             "&sexe="+sexe+
             "&naissancejour="+naissancejour+
             "&naissancemois="+naissancemois+
             "&naissanceannee="+naissanceannee+
             "&rueNo="+rueNo+
             "&ville="+ville+
             "&codePostal="+codePostal+
             "&pays="+pays+
             "&telFix="+telFix+
             "&telGsm="+telGsm+
             "&fonction="+fonction);
    return false;
}

function modifierMembre(chemin) {
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)){ 
        lireDonnees(xhr.responseText);       }}; 
    
    var idMembre = encodeURIComponent(document.getElementsByName("idMembre")[0].value);
    var inscriptionjour = encodeURIComponent(document.getElementsByName("inscriptionjour")[0].value);
    var inscriptionmois = encodeURIComponent(document.getElementsByName("inscriptionmois")[0].value);
    var inscriptionannee = encodeURIComponent(document.getElementsByName("inscriptionannee")[0].value);

    var nom = encodeURIComponent(document.getElementsByName("nom")[0].value);
    var prenom = encodeURIComponent(document.getElementsByName("prenom")[0].value);
    var email = encodeURIComponent(document.getElementsByName("email")[0].value);
    var MDP = encodeURIComponent(document.getElementsByName("MDP")[0].value);

    var naissancejour = encodeURIComponent(document.getElementsByName("naissancejour")[0].value);
    var naissancemois = encodeURIComponent(document.getElementsByName("naissancemois")[0].value);
    var naissanceannee = encodeURIComponent(document.getElementsByName("naissanceannee")[0].value);

    var rueNo = encodeURIComponent(document.getElementsByName("rueNo")[0].value);
    var ville = encodeURIComponent(document.getElementsByName("ville")[0].value);
    var codePostal = encodeURIComponent(document.getElementsByName("codePostal")[0].value);
    var pays = encodeURIComponent(document.getElementsByName("pays")[0].value);
    var telFix = encodeURIComponent(document.getElementsByName("telFix")[0].value);
    var telGsm = encodeURIComponent(document.getElementsByName("telGsm")[0].value);
    var fonction = encodeURIComponent(document.getElementsByName("fonction")[0].value);
    var supprimerPhoto = encodeURIComponent(document.getElementsByName("supprimerPhoto")[0].checked);
    
    if(document.getElementsByName("sexe")[0].checked==true)
	var sexe = encodeURIComponent(document.getElementsByName("sexe")[0].value);
    else
	var sexe = encodeURIComponent(document.getElementsByName("sexe")[1].value);
    
    if(document.getElementsByName("statut")[0].checked==true)
	var statut = encodeURIComponent(document.getElementsByName("statut")[0].value);
    else
	var statut = encodeURIComponent(document.getElementsByName("statut")[1].value);

    var requete=chemin+"/index.php?page=membre&action=modifierMembre";
    
    xhr.open("POST",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send("idMembre="+idMembre+
             "&inscriptionjour="+inscriptionjour+
             "&inscriptionmois="+inscriptionmois+
             "&inscriptionannee="+inscriptionannee+
             "&nom="+nom+
             "&prenom="+prenom+
             "&email="+email+
             "&MDP="+MDP+
             "&sexe="+sexe+
             "&naissancejour="+naissancejour+
             "&naissancemois="+naissancemois+
             "&naissanceannee="+naissanceannee+
             "&rueNo="+rueNo+
             "&ville="+ville+
             "&codePostal="+codePostal+
             "&pays="+pays+
             "&telFix="+telFix+
             "&telGsm="+telGsm+
             "&fonction="+fonction+
             "&statut="+statut+
             "&supprimerPhoto="+supprimerPhoto);
    return false;
}
function supprimerMembre( chemin) {
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))  
    { 
        lireDonnees(xhr.responseText);
    }}; 
    var idMembre = encodeURIComponent(document.getElementsByName("idMembre")[0].value);
    
    var requete=chemin+"/index.php?page=membre&action=supprimerMembre&param[0]="+idMembre;
    
    xhr.open("POST",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);

    return false;	
}
function lireDonnees(text, chemin){
    var resultat=document.getElementById("message");
    var message = text.split(";");
    resultat.innerHTML=message[0];
    if(typeof( message[1] ) != "undefined" && message[1]!=""){
	var balSsTitreA=document.getElementById("carte");
	balSsTitreA.setAttribute("href",chemin+"/Membre/carteMembre/"+message[1]);
	balSsTitreA.style.display = "inline";
    }
}

function generarMDP(chemin){
	var xhr=getXMLHttpRequest();

	xhr.onreadystatechange = function() {  
	if (xhr.readyState == 4 && (xhr.status == 200 ||  	xhr.status == 0))      
	{ 
		lireMDP(xhr.responseText);  	}}; 

	var requete=chemin+"/index.php?page=membre&action=genererMDP";

	xhr.open("POST",requete,true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
	xhr.send(null);
	return false;
}

function lireMDP(text){
    var resultat=document.getElementsByName("MDP")[0];
    resultat.value = text;
}
