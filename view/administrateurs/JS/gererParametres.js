function modifier(chemin) {
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    lireDonnees(xhr.responseText);       }}; 
    
    var nomEntreprise = encodeURIComponent(document.getElementsByName("nomEntreprise")[0].value);
    var numTel = encodeURIComponent(document.getElementsByName("numTel")[0].value);
    var rue = encodeURIComponent(document.getElementsByName("rue")[0].value);
    var cp = encodeURIComponent(document.getElementsByName("cp")[0].value);
    var ville = encodeURIComponent(document.getElementsByName("ville")[0].value);
    var pays = encodeURIComponent(document.getElementsByName("pays")[0].value);
    var nomResp = encodeURIComponent(document.getElementsByName("nomResp")[0].value);
    var prenomResp = encodeURIComponent(document.getElementsByName("prenomResp")[0].value);
    var coteMax = encodeURIComponent(document.getElementsByName("coteMax")[0].value);
    var libNiveau = encodeURIComponent(document.getElementsByName("libNiveau")[0].value);
    var libExamen = encodeURIComponent(document.getElementsByName("libExamen")[0].value);
    var libCompetence = encodeURIComponent(document.getElementsByName("libCompetence")[0].value);
    var libNiveaux = encodeURIComponent(document.getElementsByName("libNiveaux")[0].value);
    var libExamens = encodeURIComponent(document.getElementsByName("libExamens")[0].value);
    var libCompetences = encodeURIComponent(document.getElementsByName("libCompetences")[0].value);
    
    
    var requete=chemin+"/index.php?page=parametre&action=modifierParametres";
    
    xhr.open("POST",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send("nomEntreprise="+nomEntreprise+
             "&numTel="+numTel+
             "&rue="+rue+
             "&cp="+cp+
             "&ville="+ville+
             "&pays="+pays+
             "&nomResp="+nomResp+
             "&prenomResp="+prenomResp+
             "&coteMax="+coteMax+
             "&libNiveau="+libNiveau+
             "&libExamen="+libExamen+
             "&libCompetence="+libCompetence+
             "&libNiveaux="+libNiveaux+
             "&libExamens="+libExamens+
             "&libCompetences="+libCompetences);
}
function lireDonnees(text){
    var resultat=document.getElementById("message");
    resultat.innerHTML=text;
    window.setTimeout(function(){
        var resultat=document.getElementById("message");
        resultat.innerHTML="";},5000);
}
