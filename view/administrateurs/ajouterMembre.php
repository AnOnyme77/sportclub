<?php 
if($membre->getIdMembre() != null)$this->titre="membre ".$membre->getIdMembre();
else $this->titre="Ajouter un membre";	
if($ajout)
	echo ("<h1>Ajouter un membre</h1>");
else
	echo("<h1>Détails sur le membre</h1>");
?>
<div class="ssTitre">
		<div class="boutonDiv" ><a id="carte" href="<?= Parser::getChemin()."/Membre/carteMembre/".$membre->getIdMembre() ?>" class="icon <?php if($ajout) echo("cacher"); ?>" title="Carte du membre">
		<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/card.png"'); ?> alt="Carte du membre" title="Carte du membre"/></div></a></div>	
</div>
<div>
	<div id="message">
		<?php if(isset($message)){  echo($message);  } ?>
	</div>
<table class="veStart">
	<tr>
		<td>Code du membre</td>
		<td><input type="text"  name="idMembre" readonly="readonly" <?php 
			if(!$ajout){
				echo('value="'.$membre->getIdMembre().'"');
			}
			else{
				echo('value="Généré à l\'ajout"');
			}
			 ?> /></td>
	</tr>
	<tr>
		<td>Date d'inscription</td>
		<td><?php Formulaire::formulaireDate("inscription", $membre->getDateInscription());?> </td>
	</tr>
	<tr>
		<td>Nom</td>
		<td><input class="validate[required,min[1]] text-input" maxlength="30" size=10 type="text" name="nom" value=<?= '"'.$membre->getNom().'"' ?> /></td>
	</tr>
	<tr>
		<td>Prénom</td>
		<td><input class="validate[required,min[1]] text-input" maxlength="30" size=10 type="text" name="prenom" value=<?= '"'.$membre->getPrenom().'"' ?>/></td>
	</tr>
	<tr>
		<td>Adresse Mail</td>
		<td><input class="validate[required,custom[email]] text-input" maxlength="50" type="text" name="email" value= <?= '"'.$membre->getEmail().'"' ?>/></td>
	</tr>
	<tr>
		<td>
			Mot de passe :
		</td>
		<td> 
			<input <?php if($ajout) echo('class="validate[required,min[1]] text-input"') ?> maxlength="30" size=10 type="text" name="MDP" /> 
			<div class="boutonDiv" ><a onclick="generarMDP(<?php echo("'".Parser::getChemin()."'"); ?>)"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/generer.png"'); ?> alt="Générer" title="Générer"/></div></a></div>
		</td>
	</tr>
	<tr>
		<td>Sexe</td>
		<td>
			<label> <input type= "radio" name="sexe" value="F" <?php if(  $membre->getSexe()==null || $membre->getSexe()=="F"){ echo('checked="checked"'); }?>> Femme </label>
			<label> <input type= "radio" name="sexe" value="M" <?php if(  $membre->getSexe()!=null && $membre->getSexe()!="F"){ echo('checked="checked"'); }?>> Homme </label>
		</td>
	</tr>
	<tr>
		<td>Date de naissance</td>
		<td><?php Formulaire::formulaireDate("naissance", $membre->getDateNaissance());?> </td>
	</tr>
	<tr>
		<td>
				Adresse :
				<br/> Rue, n°
				<br/> Ville
				<br/> Code Postal
				<br/> Pays
		</td>
		<td>
			<br/><input class="validate[required,min[1]] text-input" maxlength="50" type="text" name="rueNo" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getRue().'"':'""' ?>/>
			<br/><input class="validate[required,min[1]] text-input" maxlength="30" size=10 type="text" name="ville" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getVille().'"':'""' ?>/>
			<br/><input class="validate[required,min[1]] text-input" maxlength="10" size=10 type="text" name="codePostal" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getCodePostale().'"':'""' ?>/>
			<br/><input maxlength="30" size=10 type="text" name="pays" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getPays().'"':'""' ?>/>
		</td>
	</tr>
	<tr>
		<td>Téléphone fixe</td>
		<td><input class="validate[custom[phone]] text-input" maxlength="25" size=8 type="text" name="telFix" value=  <?= '"'.$membre->getTelFixe().'"' ?>/></td>
	</tr>
	<tr>
		<td>GSM</td>
		<td><input class="validate[custom[phone]] text-input" maxlength="25" size=8 type="text" name="telGsm" value= <?= '"'.$membre->getTelGsm().'"' ?>/></td>
	</tr>
	<!--<tr>
		<td>Niveau</td> 
		<td>
			<select name="niveau" >
				<option value="" >choix du niveau</option>
				<?php
				if(isset($niveaux))
					foreach($niveaux as $niveau){
						echo("<option value='".$niveau->getNameSection().";".$niveau->getNiveau()."'");
						if($niveau->getNameSection()==$membre->getNameSection() &&
							$niveau->getNiveau()==$membre->getNiveau()) echo(" selected='selected' ");
						echo(" >".$niveau->getNameSection()." ".$niveau->getLibelle()."</option>");
					}
				?>
			</select>
		</td>
	</tr>-->
	<tr>
		<td>Fonction</td>
		<td>
			<select name = "fonction">
				<option value="M" <?php if($membre->getFonction()=="M")echo('selected="selected"'); ?>> Membre </option>
				<option value="P" <?php if($membre->getFonction()=="P")echo('selected="selected"'); ?>> Professeur </option>
				<option value="A" <?php if($membre->getFonction()=="A")echo('selected="selected"'); ?>> Administrateur </option>
			</select>
		</td>
	</tr>
	<?php if(!$ajout){ ?>
	<tr>
		<td>Status</td>
		<td>
			<label> <input type= "radio" name="statut" value="F" <?php if($membre->getStatut()==null || $membre->getStatut()=="F")echo('checked="checked"'); ?>> Actif </label>
			<label> <input type= "radio" name="statut" value="T" <?php if($membre->getStatut()=="T")echo('checked="checked"'); ?>> Banni </label>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<?php if(!$ajout){ ?>
		<td>Photo de profil</td>
		<td class="imgProfil">
			<label ><input type="checkbox" value="supprimerPhoto" name="supprimerPhoto"/>Supprimer la photo</label>

			<br/>Apercu :
			<br/><img class="myPhoto zoom" src=<?php  echo('"'.Parser::getChemin().$membre->getLogo().'"');  ?>/><br/>
	
		</td>
		<?php } ?>
	</tr>
	<tr class="centre">
		<?php if($ajout){ ?>
		<td colspan="2" class="centre">
			<div class="boutonDiv" ><a  onclick="ajouterMembre(<?php echo("'".Parser::getChemin()."'"); ?>)" href="#"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
		<?php }
		else{?>
       	<td>
			<div class="boutonDiv" ><a onclick="modifierMembre(<?php echo("'".Parser::getChemin()."'"); ?>)" href="#"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
		</td>
       	<td>
			<div class="boutonDiv" ><a onclick="exWithConfirm('Êtes vous sur de vouloir effectuer cette action ?',
							function(){supprimerMembre(<?= "'".Parser::getChemin()."'" ?>)})" href="#"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/delete.png"'); ?> alt="Supprimer" title="Supprimer"/></div></a></div>
			<?php } ?>
		</td>
	</tr>
</table>
</div>
<div class="script">
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/Ajax.js'.'"'); ?> ></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/view/administrateurs/JS/membre.js'.'"'); ?> ></script>
</div>
