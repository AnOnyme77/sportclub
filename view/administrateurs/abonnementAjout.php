<?php 
/* ---------------------------------------------------------------------------------------------------
 * Author : Jamar Frédéric
 * Team : Dev4u
 * créé le 16/03/2014 - modifée le 28/04/2014
 -----------------------------------------------------------------------------------------------------*/
if($ajout){
	$this->titre="Ajouter un abonnement";
	echo "<h1>Ajouter un abonnement</h1>";
}
else{
	$this->titre="Modifier un abonnement";
	echo "<h1>Modifier un abonnement</h1>";
}?>
<div id="message">
	
</div>
<table>
	<tbody>
	<tr>
		<td>Type d'abonnement:</td>
		<td id="libelle">
			<?php
			if($ajout){ ?>
				<select id="selectTypeAbo" onchange='changeTypeAbo("<?=Parser::getChemin()?>")'><?php
				if(isset($listeTypeAbo))
				{
					if(!isset($typeAboCourrant))
						echo("<option>Choisissez un type d'abonnement</option>");
					foreach($listeTypeAbo as $typeAbo){ ?>
						<option value="<?= $typeAbo->getLibelle(); ?>" 
								<?php if(isset($typeAboCourrant)){
										if($typeAboCourrant->getLibelle() == $typeAbo->getLibelle()){
											?>selected="selected"<?php
										}
								} ?> 
						>
							<?= $typeAbo->getLibelle(); ?>
						</option><?php 
					}
				}?>
				</select><?php
			
			}else{ ?>
				<input id="selectTypeAbo" type="text" name="selectTypeAbo" readonly="readonly" value="<?= $abo->getLibelle() ?>"/>
			<?php
			}
			?>
		</td>
	</tr>
	<tr>
		<td>Code du membre :</td>
		<td>
			<?php if($ajout){ ?>
				<input id="idMembre" type="text" name="idMembre" <?php if(isset($idMembreCourrant))echo 'value="'.$idMembreCourrant.'"' ?>/><div class="boutonDiv" >
					<a onclick="rechercherProf('<?=Parser::getChemin()?>','idMembre')" class="icon" title=""><div>
						<img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/search.png"'); ?> alt="Rechercher membre" title="Rechercher membre"/></div></a>
				</div><?php
			}
			else{ ?>
				<input id="idMembre" type="text" name="idMembre" readonly="readonly" value=<?php echo('"'.$abo->getIdMembre().'"');
			}?>
		</td>
	</tr>
	<tr>
		<td>Date d'achat :</td>
		<td><?php 
			if($ajout){
				Formulaire::formulaireDate("dateAchat");
				include(Parser::getCheminRacine()."/outils/formulaireHeure.php");
			}
			elseif(isset($abo)){
				echo $abo->getDateFormatPresDateFrench($abo->getDateAchat());
			}
		?></td>
	</tr>
	<tr>
		<td>Date de début:</td>
		<td><?php
			if($ajout){
				Formulaire::formulaireDate("dateDebut");
			}	
			else{
				if(isset($abo)){
					Formulaire::formulaireDate("dateDebut",$abo->getDateDebut());
				}
			}?>
		</td>
	</tr>
	<tr>
		<td>Prix:</td>
		<td>
			<input id="prix" type="number" name="prix" readonly = "readonly" <?php 
				if(!$ajout && isset($abo)) 
					echo("value='".$abo->getPrix()."'");
				elseif($ajout && isset($typeAboCourrant))
				{
					echo ("value='".$typeAboCourrant->getPrix()."'");
				}
			?> />&euro; 
		</td>
	</tr>

	<tr>
		<td>Nombre d'unités restantes :</td>
		<td>
			<input id="nbUniteRest" type="text" name="nbunit" <?php 
			if(!$ajout && isset($abo)){
				if($abo->getNbUniteRest() == "-1")
					echo ("readonly='readonly' value=''");
				else
					echo ("value='".$abo->getNbUniteRest()."'");
			}
			elseif($ajout && isset($typeAboCourrant))
			{
				if($typeAboCourrant->getNbUnite() == "-1")
					echo ("readonly = 'readonly' value=''");
				else
					echo ("readonly = 'readonly' value='".$typeAboCourrant->getNbUnite()."'");
			}
			?> size="4"/>

			/

			<input type="text" name="nbunitTot" <?php 
			if(!$ajout && isset($typeAbo)){
				if($typeAbo->getNbUnite() == "-1")
					echo ("readonly='readonly' value=''");
				else
					echo ("readonly='readonly' value='".$typeAbo->getNbUnite()."'");
			}
			elseif($ajout && isset($typeAboCourrant))
			{
				if($typeAboCourrant->getNbUnite() == "-1")
					echo ("readonly='readonly' value=''");
				else
					echo ("readonly='readonly' value='".$typeAboCourrant->getNbUnite()."'");
			}

			?> size="4" />
		</td>
	</tr>
	<tr>
		<td>Durée:</td>
		<td><input id="duree" type="text" name="duree" readonly = "readonly" <?php
			if(!$ajout && isset($typeAbo)){
				if($typeAbo->getDuree() != "-1")echo("readonly='readonly' value='".$typeAbo->getDuree()."'");
				else echo("readonly='readonly' value=''");
			}
			elseif($ajout && isset($typeAboCourrant))
			{
				if($typeAboCourrant->getDuree() != "-1")echo("readonly='readonly' value='".$typeAboCourrant->getDuree()."'");
				else echo("readonly='readonly' value=''");
			} 
			?> size="4" alt="nombre de jour de l'abonnement"/>Jours</td>
	</tr>
	<tr>
		<td>Valable jusqu'au:</td>
		<td> 
			<?php
			if(!$ajout && isset($abo) && isset($typeAbo))
			{
				if($typeAbo->getDuree() != "-1")
					echo($abo->getDateFormatPresDateFrench($abo->getDateFin()));
				else
					echo('∞');
			}
			elseif($ajout && isset($typeAboCourrant))
			{
				echo "Date calculée en fonction du type d'abonnement";
			}?>

		</td>
	</tr>
	<tr>
		<td colspan="2" class="centre">
		<div class="boutonDiv" ><a <?php if($ajout) echo("onclick='ajouterAbo(\"".Parser::getChemin()."\")'");
					 else echo("onclick='modifierAbo(\"".Parser::getChemin()."\",\"".$abo->getDateFormatPresFrenchHeures($abo->getDateAchat())."\")'");?>  class="icon" title="">
		<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
		</td>
	</tr>
	</tbody>
</table>

<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/abonnement.js"?>></script>
	<script src=<?= Parser::getChemin()."/outils/JS/rechercheMembre.js"?>></script>
</div>
