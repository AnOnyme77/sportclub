<div class="CM">
	<div class="CMlogoClub"><img  src=<?php if(unserialize($_SESSION["oParametres"])->getLogo()=="" or
											!file_exists(Parser::getCheminRacine().unserialize($_SESSION["oParametres"])->getLogo()))
												echo('"'.Parser::getChemin().'/images/parametres/logoClubDefault.jpg'.'"');
										else
												echo('"'.Parser::getChemin().unserialize($_SESSION["oParametres"])->getLogo().'"');
										?>></div>
	<div class="CMclub"><?= unserialize($_SESSION["oParametres"])->getNomEntreprise()?></div>
	<div>
		<div class="CMlogoMembre"><img src=<?php echo('"'.Parser::getChemin().$membre->getLogo().'"'); ?>></div>
		<div class="CMinfoMb">
			<p><?php
				switch ( $membre->getFonction() ) {
					case "": echo("Membre :");
						break;
					case "P": echo("Professeur :");
						break;
					case "A": echo("Administrateur :");
						break;
						
					default: echo("Membre :");
						break;
				}?></p>
			<p><?= $membre->getNom() ?></p>
			<p><?= $membre->getPrenom() ?></p>
			<p><?php
			if($membre->getDateNaissance() != null) {
				echo($membre->getDateNaissanceFormat());} ?></p>
		</div>
	</div>
	<div>
		<div class="CMcodeBarre">
			<p>Code ID : </p>
			<p><?= $membre->getIdMembre()?></p>
		</div>
		<div class="CMcodeBarreImg"><img src=<?php echo('"'.Parser::getChemin().'/images/barcode/'.$membre->getIdMembre().'.png'.'"'); ?> alt="code barre du memebre"/></div>
	</div>
</div>
<?php if($retour){ ?>
<div class="noPrint">
	<div class="boutonDiv" ><a  href="javascript:history.back()"  class="icon" title="Retour">
	<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/retour.png"'); ?> alt="Retour" title="Retour"/></div></a></div>
	<div class="boutonDiv" ><a  OnClick="javascript:window.print()"  class="icon" title="Imprimer">
	<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/print.png"'); ?> alt="Imprimer" title="Imprimer"/></div></a></div>
</div>
<?php } ?>