<?php $this->titre="Votre profil" ?>
<h1>Votre profil</h1>
<div class="ssTitre">
	<?php if(isset($niveau)){ ?>
	<div class="boutonDiv" ><a id="carte" href="<?= Parser::getChemin()."/niveau/detailNiveau/".$niveau->getNiveau()."/".$niveau->getNameSection() ?>" class="icon" title="Mon niveau">
	<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/niveaux.png"'); ?> alt="Mon niveau" title="Mon niveau"/></div></a></div>	
	<?php }?>
</div>

<div>
	<div id="message">
	<?php if(isset($message)){ echo($message);  } ?>
	</div>                
<table>
	<tr>
		<td>identifiant : </td>
		<td><input type="text"  name="idMembre" readonly="readonly" <?php echo('value="'.$membre->getIdMembre().'"'); ?> /></td>
	</tr>
	<tr>
		<td>Date d'inscription : </td>
		<td><input type="text"  name="dateInscription" readonly="readonly"
		<?php
		if($membre->getDateInscription() != null) {
			echo('value="'.$membre->getDateInscriptionFormat().'"');
		} ?> /></td>
	</tr>
	<tr>
		<td>Nom : </td>
		<td><input type="text" name="nom" value=<?= '"'.$membre->getNom().'"' ?> /></td>
	</tr>
	<tr>
		<td>Prénom : </td>
		<td><input type="text" name="prenom" value=<?= '"'.$membre->getPrenom().'"' ?>/></td>
	</tr>
    <tr>
        <td> E-mail : </td>
        <td><input type="text" name="email" value=<?= '"'.$membre->getEmail().'"' ?>/> </td>
    </tr>
    <tr>
        <td>
        	Ancien mot de passe :<br/>
        	Nouveau mot de passe :<br/>
        	Confirmer mot de passe :
        </td>
        <td> 
        	<input type="password" name="ancMDP" /> <br/>
        	<input type="text" name="newMDP" /> 
        		<div class="boutonDiv" ><a onclick="generarMDP(<?php echo("'".Parser::getChemin()."'"); ?>)"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/generer.png"'); ?> alt="Générer" title="Générer"/></div></a></div><br/>
        	<input type="text" name="confMDP" /> 
        </td>
    </tr>
    <tr>
		<td>Sexe</td>
		<td>
			<label> <input type= "radio" name="sexe" value="F" <?php if(  $membre->getSexe()==null || $membre->getSexe()=="F"){ echo('checked="checked"'); }?>> Femme </label>
			<label> <input type= "radio" name="sexe" value="M" <?php if(  $membre->getSexe()!=null && $membre->getSexe()!="F"){ echo('checked="checked"'); }?>> Homme </label>
		</td>
    </tr>
    <tr>
        <td>Date de naissance : </td>
        <td><?php Formulaire::formulaireDate("naissance", $membre->getDateNaissance());?></td>
    </tr>
    <tr>
        <td>
                Adresse :
                <br/> Rue, n°
                <br/> Ville
                <br/> Code Postal
                <br/> Pays
        </td>
        <td>
            <br/><input type="text" name="rueNo" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getRue().'"':'""' ?>/>
            <br/><input type="text" name="ville" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getVille().'"':'""' ?>/>
            <br/><input type="text" name="codePostal" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getCodePostale().'"':'""' ?>/>
            <br/><input type="text" name="pays" value=<?= ($membre->getAdresse()!= null)?'"'.$membre->getAdresse()->getPays().'"':'""' ?>/>
        </td>
    </tr>
    <tr>
        <td>Numéro de tél Fix :</td>
        <td><input type="text" name="telFix" value=<?= '"'.$membre->getTelFixe().'"' ?>/></td>
    </tr>
    <tr>
        <td>Numéro de gsm :</td>
        <td><input type="text" name="telGsm" value=<?= '"'.$membre->getTelGsm().'"' ?>/></td>
    </tr>
    <tr>
        <td>
        	Image de profil :
            <br/>(max. 1 Mo)
        </td>
        <td class="imgProfil">
					<input type="checkbox" id="supprime" name="supImg"/><label for="supprime"> Supprimer l'image du profil</label>
					<form method="post" action="<?=Parser::getChemin()?>/index.php?page=membre&action=changerLogo" enctype="multipart/form-data">
						<label for="monFichier">Fichier  :</label>
						<input type="file" name="image" id="image" /><br />
						<input type="hidden" name="idMembre" id="idMembre" value="<?=$membre->getIdMembre()?>"/><br />
						<input type="submit" value="Enregistrer l'image"/>
					</form>
                    <br/>Apercu :
                    <br/><img class="myPhoto zoom" src=<?php echo('"'.Parser::getChemin().$membre->getLogo().'"'); ?> />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="centre">
        	<div ><div class="boutonDiv" ><a onclick="modifierProfil(<?php echo("'".Parser::getChemin()."'"); ?>)" href="#"  class="icon" title="">
			<div><img class="iconImg" src=<?php echo('"'.Parser::getChemin().'/images/icons/save.png"'); ?> alt="Enregistrer" title="Enregistrer"/></div></a></div>
        	</div>
        </td>
    </tr>
</table>
</div>
<div class="script">
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/Ajax.js'.'"'); ?> ></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/view/membres/JS/gererProfil.js'.'"'); ?> ></script>
</div>