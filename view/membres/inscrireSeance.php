<?php $this->titre= "Inscription aux séances";?>
<h1>Incription aux séances</h1>
<div id="message">
	<?php
		if(isset($message)){
			echo($message);
		}
	?>
</div>
<div>
	<!--<table>
		<form>
			<tr>
				<td>Cours</td>
				<td><input type="text"/></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><?php include(Parser::getCheminRacine()."/outils/formulaireDate.php");?></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><?php include(Parser::getCheminRacine()."/outils/formulaireHeure.php");?></td>
			</tr>
		</form>
	</table>-->
</div>
<div>
	<table id="sorts">
		<thead>
			<tr>
				<th>
					Cours
				</th>
				<th>
					Date et heure
				</th>
                <th>
                    Places disponibles
                </th>
				<th>
					Professeur
				</th>
				<th>
					
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if(isset($liste) and count($liste)>0){
					$i=0;
					foreach($liste as $seance){
						echo("<tr>");
						echo("<td>".$seance->getCours()."</td>");
						echo("<td>".$seance->getDateCours()."</td>");
                        if(isset($listeNombre) and count($listeNombre)==count($liste)){
							echo("<td>".($seance->getNbMembreMax()-$listeNombre[$i])."</td>");
						}
						else{
							echo("<td>Non récupéré</td>");
						}
						if(isset($listeProfs) and count($listeProfs)==count($liste)){
							echo("<td>".$listeProfs[$i]->getNom()." ".$listeProfs[$i]->getPrenom()."</td>");
						}
						else{
							echo("<td>".$seance->getIdProf()."</td>");
						}
						if(isset($listePresence) and count($listePresence)==count($liste)){
							if(!$listePresence[$i]){
								echo("<td><input type='submit' id='".$seance->getNoSeance().unserialize($_SESSION["oMembre"])->getIdMembre()."' value='Inscrivez-vous' onclick='inscrire(\"".Parser::getChemin()."\",\"".$seance->getNoSeance()."\",\"".unserialize($_SESSION["oMembre"])->getIdMembre()."\")'/></td>");
							}
							else{
								echo("<td><input type='submit' id='".$seance->getNoSeance().unserialize($_SESSION["oMembre"])->getIdMembre()."' value='Desinscrirez vous' onclick='exWithConfirm(\"Êtes vous sur ce vouloir effectuer cette action ?\",function(){desinscrire(\"".Parser::getChemin()."\",\"".$seance->getNoSeance()."\",\"".unserialize($_SESSION["oMembre"])->getIdMembre()."\")})'/></td>");
							}
						}
						else{
							echo("<td><input type='submit' id='".$seance->getNoSeance().unserialize($_SESSION["oMembre"])->getIdMembre()."' value='Inscrirez vous' onclick='inscrire(\"".Parser::getChemin()."\",\"".$seance->getNoSeance()."\",\"".unserialize($_SESSION["oMembre"])->getIdMembre()."\")'/></td>");
						}
						echo("</tr>");
						$i++;
					}
				}
			?>
		</tbody>
	</table>
</div>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/membres/JS/inscription.js"?>></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/jquery.js'.'"'); ?> ></script>
	<script type="text/javascript" src=<?php echo('"'.Parser::getChemin().'/outils/JS/dynatable.jquery.js'.'"'); ?> ></script>
	<script type="text/javascript">
	$('#sorts').dynatable();
	</script>
</div>