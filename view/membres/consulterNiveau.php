<?php $this->titre="Vos ".strtolower(unserialize($_SESSION["oParametres"])->getLibNiveaux());
?>
<h1>Vos <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibNiveaux())); ?></h1>
<div id="message">
<?php
        if(isset($message)){
                echo($message);
        }
?>
</div>
<div>
		<p>Sélectionnez <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau())); ?> pour en voir les détails</p>
        Choix pour les <?php echo(strtolower(unserialize($_SESSION["oParametres"])->getLibNiveaux())); ?> :
        <?php
        if(isset($liste) and count($liste)>0){
                echo("<select name=\"niveau\" onchange='update(\"".Parser::getChemin()."\")' id='liste'>");
				echo("<option >Non selectionné</option>");
                foreach($liste as $niveau){
                        if(isset($sectionActuelle) and isset($niveauActuel)
                           and $sectionActuelle==$niveau->getNameSection()
                           and $niveauActuel==$niveau->getNiveau())
                                echo("<option value='".$niveau->getNiveau()."|#|".$niveau->getNameSection()."' selected='selected'>".$niveau->getNameSection()." - ".$niveau->getLibelle()."</option>");
                        else
                                echo("<option value='".$niveau->getNiveau()."|#|".$niveau->getNameSection()."'>".$niveau->getNameSection()." - ".$niveau->getLibelle()."</option>");
                }
                echo("</select>");
        }
        else{
                echo("<p>Liste des ".strtolower(unserialize($_SESSION["oParametres"])->getLibNiveaux())." vide. Contactez l'administrateur</p>");
        }
?>
</div>
<br/>
<?php
if(isset($niveauMembre)){
        echo("<div id='infosNiveau'>");
		echo("<table>");
		if(file_exists(Parser::getCheminRacine().$niveauMembre->getLogo())){
				echo("<tr>");
				echo("<td colspan='2' class='centre'>");
				echo("<img class='zoom' src='".Parser::getChemin().$niveauMembre->getLogo()."' width='60px'>");
				echo("</td>");
				echo("</tr>");
		}
				echo("<tr>");
						$libNiveau = explode(" ",strtolower(unserialize($_SESSION["oParametres"])->getLibNiveau()));
						echo("<td>".$libNiveau[1]."</td>");
				echo("<td>".$niveauMembre->getLibelle()."</td>");
				echo("</tr>");
				echo("<tr>");
						echo("<td>Section</td>");
						echo("<td>".$niveauMembre->getNameSection()."</td>");
				echo("</tr>");
				if(isset($examen)){
						if($examen->getCote()>unserialize($_SESSION["oParametres"])->getCoteMax()/2){
								echo("<tr>");
										echo("<td>Aquisition</td>");
										echo("<td > <span class='valid'> Acquis </span> le <a href=\"dateExamen\">25/07/2005</a></td>");
								echo("</tr>");
						}
						else{
								echo("<tr>");
										echo("<td>Aquisition</td>");
										echo("<td  class='invalid'> Non-acquis </td>");
								echo("</tr>");	
						}
						echo("<tr>");
								echo("<td>Cote</td>");
								echo("<td>".$examen->getCote()."/".unserialize($_SESSION["oParametres"])->getCoteMax()."</td>");
						echo("</tr>");
						
				}
				else{
						echo("<tr>");
								echo("<td>Aquisition</td>");
								echo("<td class='invalid'> Non-acquis </td>");
						echo("</tr>");
				}
		echo("</table>");
        echo("<u>".strtolower(unserialize($_SESSION["oParametres"])->getLibCompetences())." :</u>");
        if(isset($competences) and count($competences)>0){
                echo("<ul>");
                foreach($competences as $competence){
                        echo("<li>".$competence->getTitre().": ".$competence->getDetail()."</li>");
                }
                
                echo("</ul>");
        }
        echo("</div>");
}
?>
<div class="script">
	<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
	<script src=<?= Parser::getChemin()."/view/administrateurs/JS/niveau.js"?>></script>
</div>