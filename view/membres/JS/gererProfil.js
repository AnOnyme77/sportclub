function modifierProfil(chemin) {
    var xhr=getXMLHttpRequest();
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    lireDonnees(xhr.responseText);       }}; 
    
    var idMembre = encodeURIComponent(document.getElementsByName("idMembre")[0].value);
    var nom = encodeURIComponent(document.getElementsByName("nom")[0].value);
    var prenom = encodeURIComponent(document.getElementsByName("prenom")[0].value);
    var email = encodeURIComponent(document.getElementsByName("email")[0].value);
    var ancMDP = encodeURIComponent(document.getElementsByName("ancMDP")[0].value);
    var newMDP = encodeURIComponent(document.getElementsByName("newMDP")[0].value);
    var confMDP = encodeURIComponent(document.getElementsByName("confMDP")[0].value);

    var naissancejour = encodeURIComponent(document.getElementsByName("naissancejour")[0].value);
    var naissancemois = encodeURIComponent(document.getElementsByName("naissancemois")[0].value);
    var naissanceannee = encodeURIComponent(document.getElementsByName("naissanceannee")[0].value);

    var rueNo = encodeURIComponent(document.getElementsByName("rueNo")[0].value);
    var ville = encodeURIComponent(document.getElementsByName("ville")[0].value);
    var codePostal = encodeURIComponent(document.getElementsByName("codePostal")[0].value);
    var pays = encodeURIComponent(document.getElementsByName("pays")[0].value);
    var telFix = encodeURIComponent(document.getElementsByName("telFix")[0].value);
    var telGsm = encodeURIComponent(document.getElementsByName("telGsm")[0].value);

    var supImg = encodeURIComponent(document.getElementsByName("supImg")[0].checked);
    
    if(document.getElementsByName("sexe")[0].checked==true)
		var sexe = encodeURIComponent(document.getElementsByName("sexe")[0].value);
    else
		var sexe = encodeURIComponent(document.getElementsByName("sexe")[1].value);
    
    var requete=chemin+"/index.php?page=membre&action=modifierProfil";
    
    xhr.open("POST",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send("idMembre="+idMembre+
             "&nom="+nom+
             "&prenom="+prenom+
             "&email="+email+
             "&ancMDP="+ancMDP+
             "&newMDP="+newMDP+
             "&confMDP="+confMDP+
             "&sexe="+sexe+
             "&naissancejour="+naissancejour+
             "&naissancemois="+naissancemois+
             "&naissanceannee="+naissanceannee+
             "&rueNo="+rueNo+
             "&ville="+ville+
             "&codePostal="+codePostal+
             "&pays="+pays+
             "&telFix="+telFix+
             "&telGsm="+telGsm+
             "&supImg="+supImg);
    
    return false;
}

function lireDonnees(text){
    var resultat=document.getElementById("message");
	resultat.innerHTML=text;
}

function generarMDP(chemin){
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    	lireMDP(xhr.responseText);       }}; 
    
    var requete=chemin+"/index.php?page=membre&action=genererMDP";
    
    xhr.open("POST",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    xhr.send(null);
    return false;
}

function lireMDP(text){
    var resultat=document.getElementsByName("newMDP")[0];
    resultat.value = text;
}
