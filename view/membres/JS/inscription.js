/* ---------------------------------------------------------------------------------------------------
 * Author : Evrard Laurent
 * Team : Dev4u
 * créé le 03/04/2014 - modifée le 17/04/2014
 -----------------------------------------------------------------------------------------------------*/
function inscrire(chemin,noSeance,idMembre){
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    lireDonnees(xhr.responseText);       }}; 
    
    var requete=chemin+"/inscription/inscription/"+noSeance+"/"+idMembre;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    
    var bouton = document.getElementById("".concat(noSeance,idMembre));
    
    bouton.setAttribute("value","Désinscrivez-vous");
    bouton.setAttribute("onclick","desinscrire('"+chemin+"','"+noSeance+"','"+idMembre+"')");
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
    xhr.send(null);
}
function desinscrire(chemin,noSeance,idMembre){
    var xhr=getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4 && (xhr.status == 200 ||       xhr.status == 0))      
    { 
    lireDonnees(xhr.responseText);       }}; 
    
    var requete=chemin+"/inscription/desinscription/"+noSeance+"/"+idMembre;
    xhr.open("GET",requete,true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    
    var bouton = document.getElementById("".concat(noSeance,idMembre));
    
    bouton.setAttribute("value","Inscrivez vous");
    bouton.setAttribute("onclick","inscrire('"+chemin+"','"+noSeance+"','"+idMembre+"')");
    lireDonnees("<img src='"+chemin+"/images/ajax-loader.gif' />Opération en cours, veuillez patienter et ne pas quitter la page");
     xhr.send(null);
}
function lireDonnees(text){
    var resultat=document.getElementById("message");
    resultat.innerHTML=text;
}
