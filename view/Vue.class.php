<?php
class Vue {

	//récupère la racine du projet 
	// Nom du fichier associé à la vue
	private $fichier;
	// Titre de la vue (défini dans le fichier vue)
	private $titre;
	private $controllerParametre;

	public function __construct($action) {
    	// Détermination du nom du fichier vue à partir de l'action
    	$this->fichier = Parser::getCheminRacine()."/view/" . $action . ".php";
		$this->controllerParametre= new ControllerParametre();
    }

	// Génère et affiche la vue
	public function generer($donnees=null, $droitAccess="I") {
		$this->controllerParametre->getParametres();
    	// Génération de la partie spécifique de la vue
    	$contenu = $this->genererFichier($this->fichier, $donnees);
    	$footer = $this->genererFichier(Parser::getCheminRacine()."/view/footers/footer.php");
    	switch ( $droitAccess ) {
			case "A":
				$header=$this->genererFichier(Parser::getCheminRacine()."/view/headers/headerAdmin.php");
				break;
			case "P":
				$header=$this->genererFichier(Parser::getCheminRacine()."/view/headers/headerProf.php");
				break;
			case "M":
				$header=$this->genererFichier(Parser::getCheminRacine()."/view/headers/headerMembre.php");
				break;
				
			default:
				$header=$this->genererFichier(Parser::getCheminRacine()."/view/headers/headerBase.php");
				break;
		}
    	// Génération du gabarit commun utilisant la partie spécifique
    	$vue = $this->genererFichier(Parser::getCheminRacine()."/view/gabarit.php", 
    		array('header' => $header, 'titre' => $this->titre, 
			'contenu' => $contenu, 'footer' => $footer));
    	// Renvoi de la vue au navigateur
    	echo $vue;
	}

	// Génère un fichier vue et renvoie le résultat produit
	public function genererFichier($fichier, $donnees=null) {
    	if (file_exists($fichier)) {
      	// Rend les éléments du tableau $donnees accessibles dans la vue
      	if(!empty($donnees)) extract($donnees);
      	// Démarrage de la temporisation de sortie
      	ob_start();
      	// Inclut le fichier vue
      	// Son résultat est placé dans le tampon de sortie
      	require $fichier;
      	// Arrêt de la temporisation et renvoi du tampon de sortie
      	return ob_get_clean();
    }
    else {
      throw new Exception("Fichier '$fichier' introuvable");
    }
  }
}
