<?php $this->titre="Accueil" ?>
<h1>Bienvenue sur le portail de votre club favoris</h1>
<br/>
<div>
	<p>Vous désirez des informations supplémentaires?<br/>Contactez nous, nous serons heureux de vous répondre !</p>
</div>
<br/>
<div>
	<table>
		<tr>
			<td> <b>Gestionnaire du club</b></td>
			<td> <?= unserialize($_SESSION["oParametres"])->getNomResp()." ".unserialize($_SESSION["oParametres"])->getPrenomResp()?></td>
		</tr>
		<tr>
			<td> <b>Nom du club</b></td>
			<td> <?= unserialize($_SESSION["oParametres"])->getNomEntreprise()?></td>
		</tr>
		<tr>
			<td> <b>Adresse</b></td>
			<td> <?= unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getRue()."<br/>".unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getCodePostale()." ".unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getVille()."<br/>".unserialize($_SESSION["oParametres"])->getAdresseEntreprise()->getPays()?></td>
		</tr>
		<tr>
			<td> <b>Téléphone</b></td>
			<td> <?= unserialize($_SESSION["oParametres"])->getNumTel()?></td>
		</tr>
	</table>
</div>