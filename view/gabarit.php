<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8" />
<meta author="Evrart Jamar Hainaut"/>
<meta author="Ges4u"/>
<meta name="viewport" content="width=device-width" />
<link href=<?php echo('"'.Parser::getChemin().'/outils/jQuery-Validation-Engine-master/css/validationEngine.jquery.css'.'"'); ?> rel="stylesheet" />
<link href=<?php echo('"'.Parser::getChemin().'/CSS/dynatable.css'.'"'); ?> type="text/css" rel="stylesheet" />
<link href=<?php echo('"'.Parser::getChemin().'/CSS/style.css'.'"'); ?> type="text/css" rel="stylesheet" />
<link href=<?php echo('"'.Parser::getChemin().'/CSS/styleEan.css'.'"'); ?> type="text/css" rel="stylesheet">
<link media="print" href=<?php echo('"'.Parser::getChemin().'/CSS/print.css'.'"'); ?> type="text/css" rel="stylesheet">
<link media="only screen and (max-width: 850px), media all and (max-width: 850px)" href=<?php echo('"'.Parser::getChemin().'/CSS/styleSmall.css'.'"'); ?> type= "text/css" rel="stylesheet">
<title><?= $titre ?></title>   <!-- Élément spécifique -->
<script language="javascript" type="application/javascript" src=<?= Parser::getChemin()."/outils/JS/jquery.js"?>></script>
<script language="javascript" type="application/javascript" src=<?= Parser::getChemin()."/outils/JS/JQTools.js"?>></script>
<script src=<?=Parser::getChemin()."/outils/jQuery-Validation-Engine-master/js/languages/jquery.validationEngine-fr.js"?> type="text/javascript" charset="utf-8"></script>
<script src=<?=Parser::getChemin()."/outils/jQuery-Validation-Engine-master/js/jquery.validationEngine.js"?> type="text/javascript" charset="utf-8"></script>
<script src=<?= Parser::getChemin()."/outils/JS/Ajax.js"?>></script>
<script src=<?= Parser::getChemin()."/outils/JS/tpsChargement.js"?>></script>
<script src=<?= Parser::getChemin()."/outils/JS/tools.js"?>></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".nav li").hover(function(){
			$("ul", this).stop(true, true).slideDown(300);
	},function(){
			$("ul", this).stop(true, false).slideUp(300);
	});
	
	$(".focusChamps").focus();	
	
   $('.enterChamps').bind('keypress',function(event) {
		if (event.keyCode == 13) {
			$('.boutonChamps').trigger('click');
		}
    });
    $('a').click(function(){
		$(".focusChamps").focus();	
	});
});
   
jQuery(document).ready(function(){
		// binds form submission and fields to the validation engine
		jQuery(".veStart").validationEngine();
});
   
   TpsDebut = (new Date()).getTime(); 
    
   function CalculTempsChargement() 
   {
      TpsFin = (new Date()).getTime(); 
      TpsDuree = ((TpsFin - TpsDebut) / 1000);
	  aleatoire=Math.floor(Math.random()*1000)+1;
      if (TpsDuree>2||aleatoire%10==0) {
		 var nom=window.location.pathname;
		 var duree=TpsDuree;
		 logify(nom,duree);
	  }
	  

   } 

	
</script>

</head>

<body onload="CalculTempsChargement();">
<div class="page">
	<header>
		<?= $header ?>
	</header>
<!-- corps de page -->
<section class="global">
	<div class="corps">
	<div class="contenu">
		<?= $contenu ?>   <!-- Élément spécifique -->
	</div>
	</div>
</section>
	<footer>
		<?= $footer ?>
	</footer>
</div>
</body>
</html>
